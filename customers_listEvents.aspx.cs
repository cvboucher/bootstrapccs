//Using statements @1-AFBC0BCB
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
using InMotion.Web.Features;
//End Using statements

//Namespace @1-B50C8C34
namespace BootstrapDesign
{
//End Namespace

//Page class @1-E0983E90
public partial class customers_list_Page : MTPage
{
//End Page class

//Attributes constants @1-2F9F6C23
    public const string Attribute_rowNumber = "rowNumber";
//End Attributes constants

//Page customers_list Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page customers_list Event Init

//Enable Scripting Support @1-C2BE9EBD
        this.ScriptingSupport = true;
//End Enable Scripting Support

//Page customers_list Init event tail @1-FCB6E20C
    }
//End Page customers_list Init event tail

//Page customers_list Event Load @1-D9372652
    protected void Page_Load(object sender, EventArgs e) {
//End Page customers_list Event Load

//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();
//End DataBind

//Page customers_list Load event tail @1-FCB6E20C
    }
//End Page customers_list Load event tail

//Page customers_list Event On PreInit @1-6204FEF7
    protected void Page_PreInit(object sender, EventArgs e) {
//End Page customers_list Event On PreInit

//MasterPageInit @1-25245C17
        this.DesignMasterPagePath = "{CCS_PathToMasterPage}/MasterPage.master";
//End MasterPageInit

//ScriptIncludesInit @1-83B6322D
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|js/jquery/ui/jquery.ui.core.js|js/jquery/ui/jquery.ui.widget.js|js/jquery/ui/jquery.ui.position.js|js/jquery/ui/jquery.ui.menu.js|js/jquery/ui/jquery.ui.autocomplete.js|js/jquery/autocomplete/ccs-autocomplete.js|";
//End ScriptIncludesInit

//Page customers_list On PreInit event tail @1-FCB6E20C
    }
//End Page customers_list On PreInit event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

