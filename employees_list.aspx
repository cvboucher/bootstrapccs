<!--ASPX page header @1-8BDF9F59-->
<%@ Page language="C#" CodeFile="employees_listEvents.aspx.cs" Inherits="BootstrapDesign.employees_list_Page" MasterPageFile="~/Designs/Bootstrap/MasterPage.master"  %>
<%@ Register TagPrefix="BootstrapDesign" TagName="MenuIncludablePage" Src="MenuIncludablePage.ascx" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASPX page header-->

<!--ASPX page @1-A56CEC8E-->

<asp:Content ContentPlaceHolderID="Head" ID="Head_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Head" runat="server">
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-65835CB2
</script>
<script type="text/javascript" charset="windows-1252" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>


<title>employees</title>

<meta content="text/html; charset=windows-1252" http-equiv="Content-Type">
</mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Content" ID="Content_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Content" runat="server">
<mt:Record ReturnPage="~/employees_list.aspx" ID="employeesSearch" runat="server"><ItemTemplate><div data-emulate-form="ContentemployeesSearch" id="ContentemployeesSearch">
<div role="form" class="form-horizontal">
  



    <legend>Search </legend>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <div class="row">
      <div class="col-sm-offset-2 col-sm-10">
        <div role="alert" class="alert alert-danger">
          <mt:MTLabel id="ErrorLabel" runat="server"/> 
        </div>
      </div>
    </div>
    </mt:MTPanel><fieldset>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Emp Login</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="emp_login" Caption="Emp Login" maxlength="20" Columns="20" ID="s_emp_login" data-id="ContentemployeesSearchs_emp_login" runat="server"/>
      </div>
      <div class="col-sm-2 control-label">
        <label>Emp Password</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="emp_password" Caption="Emp Password" maxlength="20" Columns="20" ID="s_emp_password" data-id="ContentemployeesSearchs_emp_password" runat="server"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Emp Name</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="emp_name" Caption="Emp Name" maxlength="50" Columns="50" ID="s_emp_name" data-id="ContentemployeesSearchs_emp_name" runat="server"/>
      </div>
      <div class="col-sm-2 control-label">
        <label>Title</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="title" Caption="Title" maxlength="50" Columns="50" ID="s_title" data-id="ContentemployeesSearchs_title" runat="server"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Email</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="email" Caption="Email" maxlength="50" Columns="50" ID="s_email" data-id="ContentemployeesSearchs_email" runat="server"/>
      </div>
      <div class="col-sm-2 control-label">
        <label>Picture</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="picture" Caption="Picture" maxlength="50" Columns="50" ID="s_picture" data-id="ContentemployeesSearchs_picture" runat="server"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Phone Home</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="phone_home" Caption="Phone Home" maxlength="50" Columns="50" ID="s_phone_home" data-id="ContentemployeesSearchs_phone_home" runat="server"/>
      </div>
      <div class="col-sm-2 control-label">
        <label>Phone Work</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="phone_work" Caption="Phone Work" maxlength="50" Columns="50" ID="s_phone_work" data-id="ContentemployeesSearchs_phone_work" runat="server"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Phone Cell</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="phone_cell" Caption="Phone Cell" maxlength="50" Columns="50" ID="s_phone_cell" data-id="ContentemployeesSearchs_phone_cell" runat="server"/>
      </div>
      <div class="col-sm-2 control-label">
        <label>Fax</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="fax" Caption="Fax" maxlength="50" Columns="50" ID="s_fax" data-id="ContentemployeesSearchs_fax" runat="server"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>City</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="city" Caption="City" maxlength="50" Columns="50" ID="s_city" data-id="ContentemployeesSearchs_city" runat="server"/>
      </div>
      <div class="col-sm-2 control-label">
        <label>Zip</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="zip" Caption="Zip" maxlength="20" Columns="20" ID="s_zip" data-id="ContentemployeesSearchs_zip" runat="server"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Address</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="address" Caption="Address" maxlength="50" Columns="50" ID="s_address" data-id="ContentemployeesSearchs_address" runat="server"/>
      </div>
      <div class="col-sm-2 control-label">
        <label for="<%# employeesSearch.GetControl<InMotion.Web.Controls.MTListBox>("employeesPageSize").ClientID %>">Records per page</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTListBox Rows="1" ID="employeesPageSize" data-id="ContentemployeesSearchemployeesPageSize" runat="server">
          <asp:ListItem Value="" Text="Select Value"/>
          <asp:ListItem Value="5" Text="5"/>
          <asp:ListItem Value="10" Text="10"/>
          <asp:ListItem Value="25" Text="25"/>
          <asp:ListItem Value="100" Text="100"/>
        </mt:MTListBox>
      </div>
    </div>
    </fieldset> 
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <mt:MTButton CommandName="Search" Text="Search" CssClass="Button" ID="Button_DoSearch" data-id="ContentemployeesSearchButton_DoSearch" runat="server"/>
        <mt:MTLink Text="Clear" CssClass="btn btn-default" ID="ClearParameters" data-id="ContentemployeesSearchClearParameters" runat="server" HrefSource="~/employees_list.aspx" PreserveParameters="Get" RemoveParameters="s_emp_login;s_emp_password;s_emp_name;s_title;s_email;s_picture;s_phone_home;s_phone_work;s_phone_cell;s_fax;s_city;s_zip;s_address;employeesPageSize"/>
      </div>
    </div>
  

</div>

</div></ItemTemplate></mt:Record><br>
<mt:Grid PageSizeLimit="100" RecordsPerPage="10" SortExpression="emp_login" DataSourceID="employeesDataSource" ID="employees" runat="server">
<HeaderTemplate>
<div class="panel panel-primary">
  <div class="panel-heading">
    <strong>List of Employees </strong>
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-hover">
        <tr>
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="emp_login" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Emp Login" ID="Sorter_emp_login" data-id="ContentemployeesSorter_emp_login" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="emp_password" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Emp Password" ID="Sorter_emp_password" data-id="ContentemployeesSorter_emp_password" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="emp_name" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Emp Name" ID="Sorter_emp_name" data-id="ContentemployeesSorter_emp_name" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="title" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Title" ID="Sorter_title" data-id="ContentemployeesSorter_title" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="email" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Email" ID="Sorter_email" data-id="ContentemployeesSorter_email" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="picture" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Picture" ID="Sorter_picture" data-id="ContentemployeesSorter_picture" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="phone_home" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Phone Home" ID="Sorter_phone_home" data-id="ContentemployeesSorter_phone_home" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="phone_work" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Phone Work" ID="Sorter_phone_work" data-id="ContentemployeesSorter_phone_work" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="phone_cell" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Phone Cell" ID="Sorter_phone_cell" data-id="ContentemployeesSorter_phone_cell" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="fax" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Fax" ID="Sorter_fax" data-id="ContentemployeesSorter_fax" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="city" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="City" ID="Sorter_city" data-id="ContentemployeesSorter_city" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="zip" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Zip" ID="Sorter_zip" data-id="ContentemployeesSorter_zip" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="address" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Address" ID="Sorter_address" data-id="ContentemployeesSorter_address" runat="server"/></th>
        </tr>
 
        
</HeaderTemplate>
<ItemTemplate>
        <tr>
          <td><mt:MTLink Source="emp_login" ID="emp_login" data-id="Contentemployeesemp_login_{employees:rowNumber}" runat="server" HrefSource="~/employees_maint.aspx" PreserveParameters="Get"><Parameters>
            <mt:UrlParameter Name="emp_id" SourceType="DataSourceColumn" Source="emp_id"/>
          </Parameters></mt:MTLink></td> 
          <td><mt:MTLabel Source="emp_password" ID="emp_password" runat="server"/></td> 
          <td><mt:MTLabel Source="emp_name" ID="emp_name" runat="server"/></td> 
          <td><mt:MTLabel Source="title" ID="title" runat="server"/></td> 
          <td><mt:MTLabel Source="email" ID="email" runat="server"/></td> 
          <td><mt:MTLabel Source="picture" ID="picture" runat="server"/></td> 
          <td><mt:MTLabel Source="phone_home" ID="phone_home" runat="server"/></td> 
          <td><mt:MTLabel Source="phone_work" ID="phone_work" runat="server"/></td> 
          <td><mt:MTLabel Source="phone_cell" ID="phone_cell" runat="server"/></td> 
          <td><mt:MTLabel Source="fax" ID="fax" runat="server"/></td> 
          <td><mt:MTLabel Source="city" ID="city" runat="server"/></td> 
          <td><mt:MTLabel Source="zip" ID="zip" runat="server"/></td> 
          <td><mt:MTLabel Source="address" ID="address" runat="server"/></td>
        </tr>
 
</ItemTemplate>
<FooterTemplate>
        <mt:MTPanel id="NoRecords" visible="False" runat="server">
        <tr class="NoRecords">
          <td colspan="13">No records</td>
        </tr>
        </mt:MTPanel>
        <tr class="Footer">
          <td colspan="13">
            <mt:MTNavigator FirstOnValue="First " FirstOffValue="First " PreviousOnValue="Prev " PreviousOffValue="Prev " NextOnValue="Next " NextOffValue="Next " LastOnValue="Last " LastOffValue="Last " PageOffPreValue="" PageOffPostValue="&amp;nbsp;" DisplayStyle="Links" PageLinksNumber="10" PageNumbersStyle="CurrentPageCentered" TotalPagesText="of" ShowTotalPages="true" ID="Navigator" runat="server"/></td>
        </tr>
      </table>
    </div>
  </div>
</div>

</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="employeesDataSource" ConnectionString="InMotion:IntranetDB" SelectCommandType="Table" SelectOrderBy="emp_login" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM employees {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM employees
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="Urls_emp_login" SourceType="URL" Source="s_emp_login" DataType="Text" Operation="And" Condition="Contains" SourceColumn="emp_login"/>
     <mt:WhereParameter Name="Urls_emp_password" SourceType="URL" Source="s_emp_password" DataType="Text" Operation="And" Condition="Contains" SourceColumn="emp_password"/>
     <mt:WhereParameter Name="Urls_emp_name" SourceType="URL" Source="s_emp_name" DataType="Text" Operation="And" Condition="Contains" SourceColumn="emp_name"/>
     <mt:WhereParameter Name="Urls_title" SourceType="URL" Source="s_title" DataType="Text" Operation="And" Condition="Contains" SourceColumn="title"/>
     <mt:WhereParameter Name="Urls_email" SourceType="URL" Source="s_email" DataType="Text" Operation="And" Condition="Contains" SourceColumn="email"/>
     <mt:WhereParameter Name="Urls_picture" SourceType="URL" Source="s_picture" DataType="Text" Operation="And" Condition="Contains" SourceColumn="picture"/>
     <mt:WhereParameter Name="Urls_phone_home" SourceType="URL" Source="s_phone_home" DataType="Text" Operation="And" Condition="Contains" SourceColumn="phone_home"/>
     <mt:WhereParameter Name="Urls_phone_work" SourceType="URL" Source="s_phone_work" DataType="Text" Operation="And" Condition="Contains" SourceColumn="phone_work"/>
     <mt:WhereParameter Name="Urls_phone_cell" SourceType="URL" Source="s_phone_cell" DataType="Text" Operation="And" Condition="Contains" SourceColumn="phone_cell"/>
     <mt:WhereParameter Name="Urls_fax" SourceType="URL" Source="s_fax" DataType="Text" Operation="And" Condition="Contains" SourceColumn="fax"/>
     <mt:WhereParameter Name="Urls_city" SourceType="URL" Source="s_city" DataType="Text" Operation="And" Condition="Contains" SourceColumn="city"/>
     <mt:WhereParameter Name="Urls_zip" SourceType="URL" Source="s_zip" DataType="Text" Operation="And" Condition="Contains" SourceColumn="zip"/>
     <mt:WhereParameter Name="Urls_address" SourceType="URL" Source="s_address" DataType="Text" Operation="And" Condition="Contains" SourceColumn="address"/>
   </SelectParameters>
</mt:MTDataSource><br>
</mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Menu" ID="Menu_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Menu" runat="server"><BootstrapDesign:MenuIncludablePage ID="MenuIncludablePage" runat="server"/></mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Footer" ID="Footer_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Footer" runat="server">&nbsp;</mt:MTPanel></asp:Content>
<!--End ASPX page-->

