//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-B50C8C34
namespace BootstrapDesign
{
//End Namespace

//Page class @1-0194102A
public partial class MenuIncludablePage_Page : MTUserControl
{
//End Page class

//Page MenuIncludablePage Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page MenuIncludablePage Event Init

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page MenuIncludablePage Init event tail @1-FCB6E20C
    }
//End Page MenuIncludablePage Init event tail

//Panel Panel1 Event Before Show @7-B56E1421
    protected void Panel1_BeforeShow(object sender, EventArgs e) {
//End Panel Panel1 Event Before Show

//Panel Panel1 Event Before Show. Action Hide-Show Component @8-FE2E76AA
        MTText UrlViewMode_8_1 = MTText.Parse((new InMotion.Web.Controls.DataParameter(DataParameterSourceType.Url,"ViewMode")).Evaluate(((IMTControl)sender).OwnerForm as Control));
        MTText ExprParam2_8_2 = MTText.Parse((new InMotion.Web.Controls.DataParameter(DataParameterSourceType.Expression,"Print")).Evaluate(((IMTControl)sender).OwnerForm as Control));
        if (UrlViewMode_8_1 == ExprParam2_8_2) {
            Panel1.Visible = false;
        }
//End Panel Panel1 Event Before Show. Action Hide-Show Component

//Panel Panel1 Before Show event tail @7-FCB6E20C
    }
//End Panel Panel1 Before Show event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

