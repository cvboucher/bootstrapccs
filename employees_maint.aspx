<!--ASPX page header @1-F1C27B28-->
<%@ Page language="C#" CodeFile="employees_maintEvents.aspx.cs" Inherits="BootstrapDesign.employees_maint_Page" MasterPageFile="~/Designs/Bootstrap/MasterPage.master"  %>
<%@ Register TagPrefix="BootstrapDesign" TagName="MenuIncludablePage" Src="MenuIncludablePage.ascx" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASPX page header-->

<!--ASPX page @1-F77D3AE2-->

<asp:Content ContentPlaceHolderID="Head" ID="Head_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Head" runat="server">
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-65835CB2
</script>
<script src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>' type="text/javascript" charset="windows-1252"></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//Common Script Start @1-8BFA436B
jQuery(function ($) {
    var features = { };
    var actions = { };
    var params = { };
//End Common Script Start

//ContentemployeesButton_DeleteOnClick Event Start @-914249C4
    actions["ContentemployeesButton_DeleteOnClick"] = function (eventType, parameters) {
        var result = true;
//End ContentemployeesButton_DeleteOnClick Event Start

//Confirmation Message @34-8243B274
        return confirm('Delete record?');
//End Confirmation Message

//ContentemployeesButton_DeleteOnClick Event End @-A5B9ECB8
        return result;
    };
//End ContentemployeesButton_DeleteOnClick Event End

//Event Binding @1-C66C83DC
    $('*:ccsControl(Content, employees, Button_Delete)').ccsBind(function() {
        this.bind("click", actions["ContentemployeesButton_DeleteOnClick"]);
    });
//End Event Binding

//Plugin Calls @1-DD96640F
    $('*:ccsControl(Content, employees, Button_Delete)').ccsBind(function() {
        this.bind("click", function(){ $("body").data("disableValidation", true); });
    });
    $('*:ccsControl(Content, employees, Button_Cancel)').ccsBind(function() {
        this.bind("click", function(){ $("body").data("disableValidation", true); });
    });
//End Plugin Calls

//Common Script End @1-562554DE
});
//End Common Script End

//End CCS script
</script>


<title>Employees</title>

<meta content="text/html; charset=windows-1252" http-equiv="Content-Type">
</mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Content" ID="Content_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Content" runat="server"><br>
&nbsp;
<mt:Record PreserveParameters="Get" ReturnPage="~/employees_list.aspx" DataSourceID="employeesDataSource" ID="employees" runat="server"><ItemTemplate><div data-emulate-form="Contentemployees" id="Contentemployees">
<div class="form-horizontal">
  



    <legend>Add/Edit Employees </legend>
 
    <mt:MTPanel id="Error" visible="False" runat="server">
    <div class="row">
      <div class="col-sm-offset-2 col-sm-10">
        <div role="alert" class="alert alert-danger">
          <span id="ErrorBlock"><mt:MTLabel id="ErrorLabel" runat="server"/></span> 
        </div>
 
      </div>
 
    </div>
 </mt:MTPanel><fieldset>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Emp Login</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTTextBox Source="emp_login" Caption="Emp Login" maxlength="20" Columns="20" ID="emp_login" data-id="Contentemployeesemp_login" runat="server"/>
      </div>
 
    </div>
 
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Emp Password</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTTextBox Source="emp_password" Caption="Emp Password" maxlength="20" Columns="20" ID="emp_password" data-id="Contentemployeesemp_password" runat="server"/>
      </div>
 
    </div>
 
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Emp Name</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTTextBox Source="emp_name" Caption="Emp Name" maxlength="50" Columns="50" ID="emp_name" data-id="Contentemployeesemp_name" runat="server"/>
      </div>
 
    </div>
 
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Title</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTTextBox Source="title" Caption="Title" maxlength="50" Columns="50" ID="title" data-id="Contentemployeestitle" runat="server"/>
      </div>
 
    </div>
 
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Group Id</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTListBox Rows="1" Source="group_id" DataType="Integer" Caption="Group Id" DataSourceID="group_idDataSource" DataValueField="group_id" DataTextField="group_name" ID="group_id" data-id="Contentemployeesgroup_id" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="group_idDataSource" ConnectionString="InMotion:IntranetDB" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM groups {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 
      </div>
 
    </div>
 
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Department Id</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTListBox Rows="1" Source="department_id" DataType="Integer" Caption="Department Id" DataSourceID="department_idDataSource" DataValueField="department_id" DataTextField="department_name" ID="department_id" data-id="Contentemployeesdepartment_id" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="department_idDataSource" ConnectionString="InMotion:IntranetDB" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM departments {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 
      </div>
 
    </div>
 
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Email</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTTextBox Source="email" Caption="Email" maxlength="50" Columns="50" ID="email" data-id="Contentemployeesemail" runat="server"/>
      </div>
 
    </div>
 
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Picture</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTTextBox Source="picture" Caption="Picture" maxlength="50" Columns="50" ID="picture" data-id="Contentemployeespicture" runat="server"/>
      </div>
 
    </div>
 
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Phone Home</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTTextBox Source="phone_home" Caption="Phone Home" maxlength="50" Columns="50" ID="phone_home" data-id="Contentemployeesphone_home" runat="server"/>
      </div>
 
    </div>
 
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Phone Work</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTTextBox Source="phone_work" Caption="Phone Work" maxlength="50" Columns="50" ID="phone_work" data-id="Contentemployeesphone_work" runat="server"/>
      </div>
 
    </div>
 
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Phone Cell</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTTextBox Source="phone_cell" Caption="Phone Cell" maxlength="50" Columns="50" ID="phone_cell" data-id="Contentemployeesphone_cell" runat="server"/>
      </div>
 
    </div>
 
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Fax</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTTextBox Source="fax" Caption="Fax" maxlength="50" Columns="50" ID="fax" data-id="Contentemployeesfax" runat="server"/>
      </div>
 
    </div>
 
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>City</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTTextBox Source="city" Caption="City" maxlength="50" Columns="50" ID="city" data-id="Contentemployeescity" runat="server"/>
      </div>
 
    </div>
 
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Zip</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTTextBox Source="zip" Caption="Zip" maxlength="20" Columns="20" ID="zip" data-id="Contentemployeeszip" runat="server"/>
      </div>
 
    </div>
 
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Address</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTTextBox Source="address" Caption="Address" maxlength="50" Columns="50" ID="address" data-id="Contentemployeesaddress" runat="server"/>
      </div>
 
    </div>
 
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Employee Is Active</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTCheckBox Source="employee_is_active" DataType="Boolean" OnLoad="employeesemployee_is_active_Load" ID="employee_is_active" data-id="Contentemployeesemployee_is_active" runat="server"/>
      </div>
 
    </div>
 </fieldset> 
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <mt:MTButton CommandName="Insert" Text="Add" CssClass="Button" ID="Button_Insert" data-id="ContentemployeesButton_Insert" runat="server"/>
        <mt:MTButton CommandName="Update" Text="Submit" CssClass="Button" ID="Button_Update" data-id="ContentemployeesButton_Update" runat="server"/>
        <mt:MTButton CommandName="Delete" EnableValidation="False" Text="Delete" CssClass="Button" ID="Button_Delete" data-id="ContentemployeesButton_Delete" runat="server"/>
        <mt:MTButton CommandName="Cancel" EnableValidation="False" Text="Cancel" CssClass="Button" ID="Button_Cancel" data-id="ContentemployeesButton_Cancel" runat="server"/>
      </div>
 
    </div>
 
  

</div>

</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="employeesDataSource" ConnectionString="InMotion:IntranetDB" SelectCommandType="Table" InsertCommandType="Table" UpdateCommandType="Table" DeleteCommandType="Table" runat="server">
   <SelectCommand>
SELECT * 
FROM employees {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <InsertCommand>
INSERT INTO employees(emp_login, emp_password, emp_name, title, group_id, department_id, email, picture, phone_home, phone_work, phone_cell, fax, city, zip, address, employee_is_active) VALUES ({emp_login}, {emp_password}, {emp_name}, {title}, {group_id}, {department_id}, {email}, {picture}, {phone_home}, {phone_work}, {phone_cell}, {fax}, {city}, {zip}, {address}, {employee_is_active})
  </InsertCommand>
   <UpdateCommand>
UPDATE employees SET emp_login={emp_login}, emp_password={emp_password}, emp_name={emp_name}, title={title}, group_id={group_id}, department_id={department_id}, email={email}, picture={picture}, phone_home={phone_home}, phone_work={phone_work}, phone_cell={phone_cell}, fax={fax}, city={city}, zip={zip}, address={address}, employee_is_active={employee_is_active}
  </UpdateCommand>
   <DeleteCommand>
DELETE FROM employees
  </DeleteCommand>
   <SelectParameters>
     <mt:WhereParameter Name="Urlemp_id" SourceType="URL" Source="emp_id" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="emp_id" Required="true"/>
   </SelectParameters>
</mt:MTDataSource><br>
</mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Menu" ID="Menu_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Menu" runat="server"><BootstrapDesign:MenuIncludablePage ID="MenuIncludablePage" runat="server"/></mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Footer" ID="Footer_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Footer" runat="server">&nbsp;</mt:MTPanel></asp:Content>
<!--End ASPX page-->

