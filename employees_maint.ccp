<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" validateRequest="True" scriptingSupport="Automatic" cachingDuration="1 minutes" masterPage="{CCS_PathToMasterPage}/MasterPage.ccp" useDesign="True" wizardTheme="None" needGeneration="0" wizardThemeVersion="3.0">
	<Components>
		<Panel id="23" visible="True" generateDiv="False" name="Head" contentPlaceholder="Head">
			<Components/>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="24" visible="True" generateDiv="False" name="Content" contentPlaceholder="Content">
			<Components>
				<Record id="28" sourceType="Table" urlType="Relative" secured="False" allowInsert="True" allowUpdate="True" allowDelete="True" validateData="True" preserveParameters="GET" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="employees" connection="IntranetDB" dataSource="employees" errorSummator="Error" allowCancel="True" recordDeleteConfirmation="True" buttonsType="button" wizardRecordKey="emp_id" encryptPasswordField="False" wizardUseInterVariables="False" pkIsAutoincrement="True" wizardCaption="Add/Edit Employees " wizardThemeApplyTo="Page" wizardFormMethod="post" wizardType="Record" changedCaptionRecord="False" recordDirection="Custom" templatePageRecord="D:/Users/CBoucher.CSUS2100/Documents/CodeChargeStudio5/Projects/BootstrapDesign/Templates/Record/BootstrapHorizontalForm.ccp|userTemplate" recordAddTemplatePanel="False" PathID="Contentemployees" returnPage="employees_list.ccp">
					<Components>
						<Button id="31" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Insert" operation="Insert" wizardCaption="Add" PathID="ContentemployeesButton_Insert">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Button>
						<Button id="32" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Update" operation="Update" wizardCaption="Submit" PathID="ContentemployeesButton_Update">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Button>
						<Button id="33" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Delete" operation="Delete" wizardCaption="Delete" PathID="ContentemployeesButton_Delete">
							<Components/>
							<Events>
								<Event name="OnClick" type="Client">
									<Actions>
										<Action actionName="Confirmation Message" actionCategory="General" id="34" message="Delete record?"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</Button>
						<Button id="35" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Cancel" operation="Cancel" wizardCaption="Cancel" PathID="ContentemployeesButton_Cancel">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Button>
						<TextBox id="36" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="emp_login" fieldSource="emp_login" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Emp Login" caption="Emp Login" required="False" unique="False" wizardSize="20" wizardMaxLength="20" PathID="Contentemployeesemp_login">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="37" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="emp_password" fieldSource="emp_password" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Emp Password" caption="Emp Password" required="False" unique="False" wizardSize="20" wizardMaxLength="20" PathID="Contentemployeesemp_password">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="38" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="emp_name" fieldSource="emp_name" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Emp Name" caption="Emp Name" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentemployeesemp_name">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="39" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="title" fieldSource="title" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Title" caption="Title" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentemployeestitle">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<ListBox id="40" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Integer" returnValueType="Number" name="group_id" fieldSource="group_id" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Group Id" caption="Group Id" required="False" unique="False" connection="IntranetDB" wizardEmptyCaption="Select Value" dataSource="groups" boundColumn="group_id" textColumn="group_name" PathID="Contentemployeesgroup_id">
							<Components/>
							<Events/>
							<TableParameters/>
							<SPParameters/>
							<SQLParameters/>
							<JoinTables>
								<JoinTable id="41" tableName="groups"/>
							</JoinTables>
							<JoinLinks/>
							<Fields/>
							<PKFields/>
							<Attributes/>
							<Features/>
						</ListBox>
						<ListBox id="42" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Integer" returnValueType="Number" name="department_id" fieldSource="department_id" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Department Id" caption="Department Id" required="False" unique="False" connection="IntranetDB" wizardEmptyCaption="Select Value" dataSource="departments" boundColumn="department_id" textColumn="department_name" PathID="Contentemployeesdepartment_id">
							<Components/>
							<Events/>
							<TableParameters/>
							<SPParameters/>
							<SQLParameters/>
							<JoinTables>
								<JoinTable id="43" tableName="departments"/>
							</JoinTables>
							<JoinLinks/>
							<Fields/>
							<PKFields/>
							<Attributes/>
							<Features/>
						</ListBox>
						<TextBox id="44" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="email" fieldSource="email" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Email" caption="Email" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentemployeesemail">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="45" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="picture" fieldSource="picture" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Picture" caption="Picture" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentemployeespicture">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="46" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="phone_home" fieldSource="phone_home" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Phone Home" caption="Phone Home" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentemployeesphone_home">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="47" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="phone_work" fieldSource="phone_work" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Phone Work" caption="Phone Work" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentemployeesphone_work">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="48" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="phone_cell" fieldSource="phone_cell" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Phone Cell" caption="Phone Cell" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentemployeesphone_cell">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="49" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="fax" fieldSource="fax" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Fax" caption="Fax" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentemployeesfax">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="50" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="city" fieldSource="city" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="City" caption="City" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentemployeescity">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="51" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="zip" fieldSource="zip" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Zip" caption="Zip" required="False" unique="False" wizardSize="20" wizardMaxLength="20" PathID="Contentemployeeszip">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="52" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="address" fieldSource="address" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Address" caption="Address" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentemployeesaddress">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<CheckBox id="53" visible="Yes" fieldSourceType="DBColumn" dataType="Boolean" name="employee_is_active" fieldSource="employee_is_active" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Employee Is Active" PathID="Contentemployeesemployee_is_active" defaultValue="Unchecked">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</CheckBox>
					</Components>
					<Events/>
					<TableParameters>
						<TableParameter id="30" conditionType="Parameter" useIsNull="False" field="emp_id" parameterSource="emp_id" dataType="Integer" logicOperator="And" searchConditionType="Equal" parameterType="URL" orderNumber="1"/>
					</TableParameters>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="29" tableName="employees"/>
					</JoinTables>
					<JoinLinks/>
					<Fields/>
					<PKFields/>
					<ISPParameters/>
					<ISQLParameters/>
					<IFormElements/>
					<USPParameters/>
					<USQLParameters/>
					<UConditions/>
					<UFormElements/>
					<DSPParameters/>
					<DSQLParameters/>
					<DConditions/>
					<SecurityGroups/>
					<Attributes/>
					<Features/>
				</Record>
			</Components>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="25" visible="True" generateDiv="False" name="Menu" contentPlaceholder="Menu">
			<Components>
				<IncludePage id="27" name="MenuIncludablePage" PathID="MenuMenuIncludablePage" page="MenuIncludablePage.ccp">
					<Components/>
					<Events/>
					<Features/>
				</IncludePage>
			</Components>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="26" visible="True" generateDiv="False" name="Footer" contentPlaceholder="Footer">
			<Components/>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
	</Components>
	<CodeFiles>
		<CodeFile id="1" language="C#InMotion" name="employees_maint.aspx" forShow="True" url="employees_maint.aspx" comment="&lt;!--" commentEnd="--&gt;" codePage="windows-1252"/>
		<CodeFile id="1.cs" language="C#InMotion" name="employees_maintEvents.aspx.cs" forShow="False" comment="//" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
