<!--ASPX page header @1-41F946F6-->
<%@ Page language="C#" CodeFile="customers_listEvents.aspx.cs" Inherits="BootstrapDesign.customers_list_Page" MasterPageFile="~/Designs/Bootstrap/MasterPage.master"  %>
<%@ Register TagPrefix="BootstrapDesign" TagName="MenuIncludablePage" Src="MenuIncludablePage.ascx" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASPX page header-->

<!--ASPX page @1-714AD19A-->

<asp:Content ContentPlaceHolderID="Head" ID="Head_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Head" runat="server">
<script language="JavaScript" type="text/javascript">

</script>


<title>customers</title>

<meta content="text/html; charset=windows-1252" http-equiv="Content-Type">
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-5EA5EF98
</script>
<script src="ClientI18N.aspx?file=globalize.js&locale=<%#ResManager.GetString("CCS_LocaleID")%>" type="text/javascript" charset="windows-1252"></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-65CC74A2
</script>
<mt:MTPanel ID="___link_panel_09358083588923988" runat="server"><link rel="stylesheet" type="text/css" href="<%#PathToMasterPage%>jquery-ui.css">
</mt:MTPanel>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//Features CSS @1-27E8264D
</script>
<style type="text/css">
    .ContentcustomersSearchs_customer_name-autocomplete {
        max-height: 250px;
        width: 250px;
        overflow-y: auto;
        overflow-x: hidden;
    }
    .ContentcustomersSearchs_customer_address-autocomplete {
        max-height: 250px;
        width: 250px;
        overflow-y: auto;
        overflow-x: hidden;
    }
    .ContentcustomersSearchs_customer_city-autocomplete {
        max-height: 250px;
        width: 250px;
        overflow-y: auto;
        overflow-x: hidden;
    }
    .ContentcustomersSearchs_customer_state-autocomplete {
        max-height: 250px;
        width: 250px;
        overflow-y: auto;
        overflow-x: hidden;
    }
    .ContentcustomersSearchs_customer_zip-autocomplete {
        max-height: 250px;
        width: 250px;
        overflow-y: auto;
        overflow-x: hidden;
    }
    .ContentcustomersSearchs_customer_phone-autocomplete {
        max-height: 250px;
        width: 250px;
        overflow-y: auto;
        overflow-x: hidden;
    }
</style>
<script type="text/javascript">
//End Features CSS

//Common Script Start @1-8BFA436B
jQuery(function ($) {
    var features = { };
    var actions = { };
    var params = { };
//End Common Script Start

//Controls Selecting @1-8565DCFC
    $('body').ccsBind(function() {
        features["ContentcustomersSearchs_customer_nameAutocomplete1"] = $('*:ccsControl(Content, customersSearch, s_customer_name)');
        features["ContentcustomersSearchs_customer_addressAutocomplete3"] = $('*:ccsControl(Content, customersSearch, s_customer_address)');
        features["ContentcustomersSearchs_customer_cityAutocomplete4"] = $('*:ccsControl(Content, customersSearch, s_customer_city)');
        features["ContentcustomersSearchs_customer_stateAutocomplete5"] = $('*:ccsControl(Content, customersSearch, s_customer_state)');
        features["ContentcustomersSearchs_customer_zipAutocomplete6"] = $('*:ccsControl(Content, customersSearch, s_customer_zip)');
        features["ContentcustomersSearchs_customer_phoneJAutocomplete1"] = $('*:ccsControl(Content, customersSearch, s_customer_phone)');
    });
//End Controls Selecting

//Feature Parameters @1-3E0CB930
    params["ContentcustomersSearchs_customer_nameAutocomplete1"] = { 
        template: '{@text}',
        customCssClassName: "ContentcustomersSearchs_customer_name-autocomplete",
        source: "services/customers_list_s_customer_name_Autocomplete1.aspx"
    };
    params["ContentcustomersSearchs_customer_addressAutocomplete3"] = { 
        template: '{@text}',
        customCssClassName: "ContentcustomersSearchs_customer_address-autocomplete",
        source: "services/customers_list_s_customer_address_Autocomplete1.aspx"
    };
    params["ContentcustomersSearchs_customer_cityAutocomplete4"] = { 
        template: '{@text}',
        customCssClassName: "ContentcustomersSearchs_customer_city-autocomplete",
        source: "services/customers_list_s_customer_city_Autocomplete1.aspx"
    };
    params["ContentcustomersSearchs_customer_stateAutocomplete5"] = { 
        template: '{@text}',
        customCssClassName: "ContentcustomersSearchs_customer_state-autocomplete",
        source: "services/customers_list_s_customer_state_Autocomplete1.aspx"
    };
    params["ContentcustomersSearchs_customer_zipAutocomplete6"] = { 
        template: '{@text}',
        customCssClassName: "ContentcustomersSearchs_customer_zip-autocomplete",
        source: "services/customers_list_s_customer_zip_Autocomplete1.aspx"
    };
    params["ContentcustomersSearchs_customer_phoneJAutocomplete1"] = { 
        template: '{@text}',
        customCssClassName: "ContentcustomersSearchs_customer_phone-autocomplete",
        source: "services/customers_list_s_customer_phone_Autocomplete1.aspx"
    };
//End Feature Parameters

//Feature Init @1-3D26E0BB
    features["ContentcustomersSearchs_customer_nameAutocomplete1"].ccsBind(function() {
        this.ccsAutoComplete(params["ContentcustomersSearchs_customer_nameAutocomplete1"]);
    });
    features["ContentcustomersSearchs_customer_addressAutocomplete3"].ccsBind(function() {
        this.ccsAutoComplete(params["ContentcustomersSearchs_customer_addressAutocomplete3"]);
    });
    features["ContentcustomersSearchs_customer_cityAutocomplete4"].ccsBind(function() {
        this.ccsAutoComplete(params["ContentcustomersSearchs_customer_cityAutocomplete4"]);
    });
    features["ContentcustomersSearchs_customer_stateAutocomplete5"].ccsBind(function() {
        this.ccsAutoComplete(params["ContentcustomersSearchs_customer_stateAutocomplete5"]);
    });
    features["ContentcustomersSearchs_customer_zipAutocomplete6"].ccsBind(function() {
        this.ccsAutoComplete(params["ContentcustomersSearchs_customer_zipAutocomplete6"]);
    });
    features["ContentcustomersSearchs_customer_phoneJAutocomplete1"].ccsBind(function() {
        this.ccsAutoComplete(params["ContentcustomersSearchs_customer_phoneJAutocomplete1"]);
    });
//End Feature Init

//Common Script End @1-562554DE
});
//End Common Script End

//End CCS script
</script>
</mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Content" ID="Content_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Content" runat="server">
<mt:Record ReturnPage="~/customers_list.aspx" ID="customersSearch" runat="server"><ItemTemplate><div data-emulate-form="ContentcustomersSearch" id="ContentcustomersSearch">
<div role="form" class="form-horizontal">
    



        <fieldset>
            <legend>Search </legend>
            <mt:MTPanel id="Error" visible="False" runat="server">
            <div class="row">
                <div class="col-sm-offset-2 col-sm-10">
                    <div role="alert" class="alert alert-danger">
                        <mt:MTLabel id="ErrorLabel" runat="server"/>
                    </div>
                </div>
            </div>
            </mt:MTPanel>
            <div class="form-group">
                <div class="col-sm-2 control-label">
                    <label>Customer Name</label>
                </div>
                <div class="col-sm-4">
                    <mt:MTTextBox Source="customer_name" IsOmitEmptyValue="true" Caption="Customer Name" maxlength="50" Columns="50" ID="s_customer_name" data-id="ContentcustomersSearchs_customer_name" runat="server"/>
                </div>
                <div class="col-sm-2 control-label">
                    <label>Customer Phone</label>
                </div>
                <div class="col-sm-4">
                    <mt:MTTextBox Source="customer_phone" IsOmitEmptyValue="true" Caption="Customer Phone" maxlength="50" Columns="50" ID="s_customer_phone" data-id="ContentcustomersSearchs_customer_phone" runat="server"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2 control-label">
                    <label>Customer Address</label>
                </div>
                <div class="col-sm-4">
                    <mt:MTTextBox Source="customer_address" IsOmitEmptyValue="true" Caption="Customer Address" maxlength="50" Columns="50" ID="s_customer_address" data-id="ContentcustomersSearchs_customer_address" runat="server"/>
                </div>
                <div class="col-sm-2 control-label">
                    <label>Customer City</label>
                </div>
                <div class="col-sm-4">
                    <mt:MTTextBox Source="customer_city" IsOmitEmptyValue="true" Caption="Customer City" maxlength="50" Columns="50" ID="s_customer_city" data-id="ContentcustomersSearchs_customer_city" runat="server"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2 control-label">
                    <label>Customer State</label>
                </div>
                <div class="col-sm-4">
                    <mt:MTTextBox Source="customer_state" IsOmitEmptyValue="true" Caption="Customer State" maxlength="50" Columns="50" ID="s_customer_state" data-id="ContentcustomersSearchs_customer_state" runat="server"/>
                </div>
                <div class="col-sm-2 control-label">
                    <label>Customer Zip</label>
                </div>
                <div class="col-sm-4">
                    <mt:MTTextBox Source="customer_zip" IsOmitEmptyValue="true" Caption="Customer Zip" maxlength="50" Columns="50" ID="s_customer_zip" data-id="ContentcustomersSearchs_customer_zip" runat="server"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2 control-label">
                    <label for="<%# customersSearch.GetControl<InMotion.Web.Controls.MTListBox>("customersPageSize").ClientID %>">Records per page</label>
                </div>
                <div class="col-sm-4">
                    <mt:MTListBox Rows="1" ID="customersPageSize" data-id="ContentcustomersSearchcustomersPageSize" runat="server">
                      <asp:ListItem Value="" Text="Select Value"/>
                      <asp:ListItem Value="5" Text="5"/>
                      <asp:ListItem Value="10" Text="10"/>
                      <asp:ListItem Value="25" Text="25"/>
                      <asp:ListItem Value="100" Text="100"/>
                    </mt:MTListBox>
                </div>
            </div>
        </fieldset>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <mt:MTButton CommandName="Search" Text="Search" CssClass="Button" ID="Button_DoSearch" data-id="ContentcustomersSearchButton_DoSearch" runat="server"/>
                <mt:MTLink Text="Clear" CssClass="btn btn-default" ID="ClearParameters" data-id="ContentcustomersSearchClearParameters" runat="server" HrefSource="~/customers_list.aspx" PreserveParameters="Get" RemoveParameters="s_customer_name;s_customer_phone;s_customer_address;s_customer_city;s_customer_state;s_customer_zip;customersPageSize"/>
            </div>
        </div>
    

</div>

</div></ItemTemplate></mt:Record>
<mt:Grid PageSizeLimit="100" RecordsPerPage="10" SortExpression="customer_id" DataSourceID="customersDataSource" ID="customers" runat="server">
<HeaderTemplate>
<div class="panel panel-primary">
    <div class="panel-heading">
        <mt:MTLink Text="&lt;span class=&quot;glyphicon glyphicon-plus&quot;&gt;&lt;/span&gt;" CssClass="btn btn-primary" ID="Link1" data-id="ContentcustomersLink1_{customers:rowNumber}" runat="server" HrefSource="~/customers_maint.aspx" PreserveParameters="Get" RemoveParameters="customer_id"/>&nbsp;<strong>List of Customers</strong>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th scope="col">
                        <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="customer_id" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Customer Id" ID="Sorter_customer_id" data-id="ContentcustomersSorter_customer_id" runat="server"/>
                    </th>

                    <th scope="col">
                        <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="customer_name" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Customer Name" ID="Sorter_customer_name" data-id="ContentcustomersSorter_customer_name" runat="server"/>
                    </th>

                    <th scope="col">
                        <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="customer_phone" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Customer Phone" ID="Sorter_customer_phone" data-id="ContentcustomersSorter_customer_phone" runat="server"/>
                    </th>

                    <th scope="col">
                        <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="customer_address" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Customer Address" ID="Sorter_customer_address" data-id="ContentcustomersSorter_customer_address" runat="server"/>
                    </th>

                    <th scope="col">
                        <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="customer_city" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Customer City" ID="Sorter_customer_city" data-id="ContentcustomersSorter_customer_city" runat="server"/>
                    </th>

                    <th scope="col">
                        <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="customer_state" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Customer State" ID="Sorter_customer_state" data-id="ContentcustomersSorter_customer_state" runat="server"/>
                    </th>

                    <th scope="col">
                        <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="customer_zip" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Customer Zip" ID="Sorter_customer_zip" data-id="ContentcustomersSorter_customer_zip" runat="server"/>
                    </th>
                </tr>

                
</HeaderTemplate>
<ItemTemplate>
                <tr>
                    <td><mt:MTLink Source="customer_id" DataType="Integer" ID="customer_id" data-id="Contentcustomerscustomer_id_{customers:rowNumber}" runat="server" HrefSource="~/customers_maint.aspx" PreserveParameters="Get"><Parameters>
                      <mt:UrlParameter Name="customer_id" SourceType="DataSourceColumn" Source="customer_id"/>
                    </Parameters></mt:MTLink></td>
                    <td><mt:MTLabel Source="customer_name" ID="customer_name" runat="server"/></td>
                    <td><mt:MTLabel Source="customer_phone" ID="customer_phone" runat="server"/></td>
                    <td><mt:MTLabel Source="customer_address" ID="customer_address" runat="server"/></td>
                    <td><mt:MTLabel Source="customer_city" ID="customer_city" runat="server"/></td>
                    <td><mt:MTLabel Source="customer_state" ID="customer_state" runat="server"/></td>
                    <td><mt:MTLabel Source="customer_zip" ID="customer_zip" runat="server"/></td>
                </tr>
                
</ItemTemplate>
<FooterTemplate>
                <mt:MTPanel id="NoRecords" visible="False" runat="server">
                <tr class="NoRecords">
                    <td colspan="7">No records</td>
                </tr>
                </mt:MTPanel>
                <tr class="Footer">
                    <td colspan="7">
                        <mt:MTNavigator FirstOnValue="First " FirstOffValue="First " PreviousOnValue="Prev " PreviousOffValue="Prev " NextOnValue="Next " NextOffValue="Next " LastOnValue="Last " LastOffValue="Last " PageOffPreValue="" PageOffPostValue="&amp;nbsp;" DisplayStyle="Links" PageLinksNumber="10" PageNumbersStyle="CurrentPageCentered" TotalPagesText="of" ShowTotalPages="true" ID="Navigator" runat="server"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>

</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="customersDataSource" ConnectionString="InMotion:IntranetDB" SelectCommandType="Table" SelectOrderBy="customer_id" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM customers {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM customers
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="Urls_customer_name" SourceType="URL" Source="s_customer_name" DataType="Text" Operation="And" Condition="Contains" SourceColumn="customer_name"/>
     <mt:WhereParameter Name="Urls_customer_phone" SourceType="URL" Source="s_customer_phone" DataType="Text" Operation="And" Condition="Contains" SourceColumn="customer_phone"/>
     <mt:WhereParameter Name="Urls_customer_address" SourceType="URL" Source="s_customer_address" DataType="Text" Operation="And" Condition="Contains" SourceColumn="customer_address"/>
     <mt:WhereParameter Name="Urls_customer_city" SourceType="URL" Source="s_customer_city" DataType="Text" Operation="And" Condition="Contains" SourceColumn="customer_city"/>
     <mt:WhereParameter Name="Urls_customer_state" SourceType="URL" Source="s_customer_state" DataType="Text" Operation="And" Condition="Contains" SourceColumn="customer_state"/>
     <mt:WhereParameter Name="Urls_customer_zip" SourceType="URL" Source="s_customer_zip" DataType="Text" Operation="And" Condition="Contains" SourceColumn="customer_zip"/>
   </SelectParameters>
</mt:MTDataSource><br>
</mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Menu" ID="Menu_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Menu" runat="server"><BootstrapDesign:MenuIncludablePage ID="MenuIncludablePage" runat="server"/></mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Footer" ID="Footer_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Footer" runat="server">&nbsp;</mt:MTPanel></asp:Content>
<!--End ASPX page-->

