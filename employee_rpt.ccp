<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" validateRequest="True" scriptingSupport="Automatic" cachingDuration="1 minutes" useDesign="True" masterPage="{CCS_PathToMasterPage}/MasterPage.ccp" wizardTheme="None" wizardThemeVersion="3.0">
	<Components>
		<Panel id="2" visible="True" generateDiv="False" name="Head" contentPlaceholder="Head">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Panel>
<Panel id="3" visible="True" generateDiv="False" name="Content" contentPlaceholder="Content">
<Components>
<Report id="10" secured="False" enablePrint="True" showMode="Web" sourceType="Table" returnValueType="Number" linesPerWebPage="10" name="departments_employees" dataSource="departments, employees" composition="6" isParent="True" orderBy="emp_name" pageSizeLimit="100" wizardCaption="Employee By Department Report" changedCaptionReport="True" wizardLayoutType="GroupLeftAbove" wizardGridPaging="Centered" wizardGridSortingType="SimpleDir" wizardHideDetail="False" wizardPercentForSums="False" wizardEnablePrintMode="True" wizardReportSeparator="False" wizardReportAddTotalRecords="False" wizardReportAddPageNumbers="False" wizardReportAddNbsp="False" wizardReportAddDateTime="True" wizardReportDateTimeAs="CurrentDateTime" wizardReportAddRowNumber="False" wizardReportRowNumberResetAt="Report" wizardUseSearch="True" wizardNoRecords="No records" wizardUseInterVariables="False" wizardThemeApplyTo="Page" reportAddTemplatePanel="False" wizardCreateLinkToPrintablePage="True" wizardUseClientPaging="True" connection="IntranetDB">
<Components>
<Section id="16" visible="True" lines="0" name="Report_Header" wizardSectionType="ReportHeader">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Section>
<Section id="17" visible="True" lines="1" name="Page_Header" wizardSectionType="PageHeader">
<Components>
<Sorter id="32" visible="True" name="Sorter_emp_name" column="emp_name" wizardCaption="Emp Name" wizardSortingType="SimpleDir" wizardControl="emp_name">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Sorter>
<Sorter id="34" visible="True" name="Sorter_emp_login" column="emp_login" wizardCaption="Emp Login" wizardSortingType="SimpleDir" wizardControl="emp_login">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Sorter>
<Sorter id="36" visible="True" name="Sorter_title" column="title" wizardCaption="Title" wizardSortingType="SimpleDir" wizardControl="title">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Sorter>
<Sorter id="38" visible="True" name="Sorter_email" column="email" wizardCaption="Email" wizardSortingType="SimpleDir" wizardControl="email">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Sorter>
<Sorter id="40" visible="True" name="Sorter_phone_work" column="phone_work" wizardCaption="Phone Work" wizardSortingType="SimpleDir" wizardControl="phone_work">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Sorter>
<Sorter id="42" visible="True" name="Sorter_employee_is_active" column="employee_is_active" wizardCaption="Employee Is Active" wizardSortingType="SimpleDir" wizardControl="employee_is_active">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Sorter>
</Components>
<Events/>
<Attributes/>
<Features/>
</Section>
<Section id="19" visible="True" lines="1" name="department_name_Header">
<Components>
<ReportLabel id="25" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="department_name" fieldSource="department_name" fieldTableSource="departments" wizardCaption="Department Name" wizardIsPassword="False" visible="Yes" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentdepartments_employeesdepartment_name_Headerdepartment_name">
<Components/>
<Events/>
<Attributes/>
<Features/>
</ReportLabel>
</Components>
<Events/>
<Attributes/>
<Features/>
</Section>
<Section id="20" visible="True" lines="1" name="Detail">
<Components>
<ReportLabel id="33" fieldSourceType="DBColumn" dataType="Text" html="Text" hideDuplicates="False" resetAt="Report" name="emp_name" fieldSource="emp_name" fieldTableSource="employees" wizardCaption="Emp Name" wizardIsPassword="False" visible="Yes" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentdepartments_employeesDetailemp_name">
<Components/>
<Events/>
<Attributes/>
<Features/>
</ReportLabel>
<ReportLabel id="35" fieldSourceType="DBColumn" dataType="Text" html="Text" hideDuplicates="False" resetAt="Report" name="emp_login" fieldSource="emp_login" fieldTableSource="employees" wizardCaption="Emp Login" wizardIsPassword="False" visible="Yes" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentdepartments_employeesDetailemp_login">
<Components/>
<Events/>
<Attributes/>
<Features/>
</ReportLabel>
<ReportLabel id="37" fieldSourceType="DBColumn" dataType="Text" html="Text" hideDuplicates="False" resetAt="Report" name="title" fieldSource="title" fieldTableSource="employees" wizardCaption="Title" wizardIsPassword="False" visible="Yes" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentdepartments_employeesDetailtitle">
<Components/>
<Events/>
<Attributes/>
<Features/>
</ReportLabel>
<ReportLabel id="39" fieldSourceType="DBColumn" dataType="Text" html="Text" hideDuplicates="False" resetAt="Report" name="email" fieldSource="email" fieldTableSource="employees" wizardCaption="Email" wizardIsPassword="False" visible="Yes" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentdepartments_employeesDetailemail">
<Components/>
<Events/>
<Attributes/>
<Features/>
</ReportLabel>
<ReportLabel id="41" fieldSourceType="DBColumn" dataType="Text" html="Text" hideDuplicates="False" resetAt="Report" name="phone_work" fieldSource="phone_work" fieldTableSource="employees" wizardCaption="Phone Work" wizardIsPassword="False" visible="Yes" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentdepartments_employeesDetailphone_work">
<Components/>
<Events/>
<Attributes/>
<Features/>
</ReportLabel>
<ReportLabel id="43" fieldSourceType="DBColumn" dataType="Boolean" html="Text" hideDuplicates="False" resetAt="Report" name="employee_is_active" fieldSource="employee_is_active" fieldTableSource="employees" wizardCaption="Employee Is Active" wizardIsPassword="False" visible="Yes" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentdepartments_employeesDetailemployee_is_active">
<Components/>
<Events/>
<Attributes/>
<Features/>
</ReportLabel>
</Components>
<Events/>
<Attributes/>
<Features/>
</Section>
<Section id="21" visible="True" lines="1" name="department_name_Footer">
<Components>
<ReportLabel id="26" fieldSourceType="DBColumn" dataType="Integer" html="False" hideDuplicates="False" resetAt="department_name" name="Count_emp_name" summarised="True" function="Count" wizardCaption="Emp Name" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="Count: " wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" PathID="Contentdepartments_employeesdepartment_name_FooterCount_emp_name">
<Components/>
<Events/>
<Attributes/>
<Features/>
</ReportLabel>
</Components>
<Events/>
<Attributes/>
<Features/>
</Section>
<Section id="22" visible="True" lines="1" name="Report_Footer" wizardSectionType="ReportFooter">
<Components>
<Panel id="23" visible="True" generateDiv="False" name="NoRecords" wizardNoRecords="No records">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Panel>
<ReportLabel id="29" fieldSourceType="DBColumn" dataType="Integer" html="False" hideDuplicates="False" resetAt="Report" name="TotalCount_emp_name" summarised="True" function="Count" wizardCaption="Emp Name" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="Count: " wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" PathID="Contentdepartments_employeesReport_FooterTotalCount_emp_name">
<Components/>
<Events/>
<Attributes/>
<Features/>
</ReportLabel>
</Components>
<Events/>
<Attributes/>
<Features/>
</Section>
<Section id="24" visible="True" lines="1" name="Page_Footer" wizardSectionType="PageFooter" pageBreakAfter="True">
<Components>
<ReportLabel id="27" fieldSourceType="SpecialValue" dataType="Date" html="False" hideDuplicates="False" resetAt="Report" name="Report_CurrentDateTime" fieldSource="CurrentDateTime" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardInsertToDateTD="True" PathID="Contentdepartments_employeesPage_FooterReport_CurrentDateTime">
<Components/>
<Events/>
<Attributes/>
<Features/>
</ReportLabel>
<Navigator id="28" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="of" wizardImagesScheme="None">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Navigator>
</Components>
<Events/>
<Attributes/>
<Features/>
</Section>
</Components>
<Events/>
<TableParameters>
<TableParameter id="30" conditionType="Parameter" useIsNull="False" field="employees.department_id" parameterSource="s_employees_department_id" dataType="Integer" logicOperator="And" searchConditionType="Equal" parameterType="URL" orderNumber="1" searchFormParameter="True"/>
<TableParameter id="31" conditionType="Parameter" useIsNull="False" field="emp_name" parameterSource="s_emp_name" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="2" searchFormParameter="True"/>
</TableParameters>
<JoinTables>
<JoinTable id="11" posHeight="120" posLeft="10" posTop="10" posWidth="158" schemaName="undefined" tableName="departments"/>
<JoinTable id="12" posHeight="180" posLeft="189" posTop="10" posWidth="151" schemaName="undefined" tableName="employees"/>
</JoinTables>
<JoinLinks>
<JoinTable2 id="13" conditionType="Equal" fieldLeft="departments.department_id" fieldRight="employees.department_id" joinType="inner" tableLeft="departments" tableRight="employees"/>
</JoinLinks>
<Fields/>
<PKFields/>
<SPParameters/>
<SQLParameters/>
<ReportGroups>
<ReportGroup id="18" name="department_name" field="department_name" sqlField="departments.department_name" sortOrder="asc"/>
</ReportGroups>
<SecurityGroups/>
<Attributes/>
<Features/>
</Report>
<Record id="44" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="employees_departments" searchIds="44" fictitiousConnection="IntranetDB" wizardCaption="Search  " wizardOrientation="Custom" wizardFormMethod="post" gridSearchClearLink="True" wizardTypeComponent="Search" gridSearchType="And" wizardInteractiveSearch="False" gridSearchRecPerPage="True" wizardTypeButtons="button" wizardDefaultButton="True" gridSearchSortField="False" wizardUseInterVariables="False" templatePageSearch="D:/Users/CBoucher.CSUS2100/Documents/CodeChargeStudio5/Projects/BootstrapDesign/Templates/Search/BootstrapSearch.ccp|userTemplate" wizardThemeApplyTo="Page" addTemplatePanel="False" wizardType="Report" returnPage="employee_rpt.ccp" PathID="Contentemployees_departments" composition="6">
<Components>
<Link id="45" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="ClearParameters" hrefSource="employee_rpt.ccp" removeParameters="s_employees_department_id;s_emp_name;departments_employeesPageSize" wizardThemeItem="SorterLink" wizardDefaultValue="Clear" PathID="Contentemployees_departmentsClearParameters">
<Components/>
<Events/>
<LinkParameters/>
<Attributes/>
<Features/>
</Link>
<Button id="46" urlType="Relative" enableValidation="True" isDefault="True" name="Button_DoSearch" operation="Search" wizardCaption="Search" PathID="Contentemployees_departmentsButton_DoSearch">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Button>
<ListBox id="47" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Integer" returnValueType="Number" name="s_employees_department_id" fieldSource="employees_department_id" wizardIsPassword="False" wizardEmptyCaption="Select Value" wizardCaption="Department Id" caption="Department Id" required="False" unique="False" dataSource="departments" boundColumn="department_id" textColumn="department_name" PathID="Contentemployees_departmentss_employees_department_id" connection="IntranetDB" orderBy="department_name">
<Components/>
<Events/>
<TableParameters/>
<SPParameters/>
<SQLParameters/>
<JoinTables>
<JoinTable id="50" posHeight="120" posLeft="10" posTop="10" posWidth="158" tableName="departments"/>
</JoinTables>
<JoinLinks/>
<Fields>
<Field id="51" fieldName="*"/>
</Fields>
<PKFields>
<PKField id="52" dataType="Integer" fieldName="department_id" tableName="departments"/>
</PKFields>
<Attributes/>
<Features/>
</ListBox>
<TextBox id="48" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_emp_name" fieldSource="emp_name" wizardIsPassword="False" wizardCaption="Emp Name" caption="Emp Name" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentemployees_departmentss_emp_name">
<Components/>
<Events/>
<Attributes/>
<Features/>
</TextBox>
<ListBox id="49" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" returnValueType="Number" name="departments_employeesPageSize" dataSource=";Select Value;5;5;10;10;25;25;100;100" wizardCaption="Records per page" wizardNoEmptyValue="True" PathID="Contentemployees_departmentsdepartments_employeesPageSize">
<Components/>
<Events/>
<TableParameters/>
<SPParameters/>
<SQLParameters/>
<JoinTables/>
<JoinLinks/>
<Fields/>
<PKFields/>
<Attributes/>
<Features/>
</ListBox>
</Components>
<Events>
<Event name="BeforeShow" type="Server">
<Actions>
<Action actionName="Hide-Show Component" actionCategory="General" id="53" action="Hide" conditionType="Parameter" dataType="Text" condition="Equal" name1="ViewMode" sourceType1="URL" name2="&quot;Print&quot;" sourceType2="Expression"/>
</Actions>
</Event>
</Events>
<TableParameters/>
<SPParameters/>
<SQLParameters/>
<JoinTables/>
<JoinLinks/>
<Fields/>
<PKFields/>
<ISPParameters/>
<ISQLParameters/>
<IFormElements/>
<USPParameters/>
<USQLParameters/>
<UConditions/>
<UFormElements/>
<DSPParameters/>
<DSQLParameters/>
<DConditions/>
<SecurityGroups/>
<Attributes/>
<Features/>
</Record>
</Components>
<Events/>
<Attributes/>
<Features/>
</Panel>
<Panel id="4" visible="True" generateDiv="False" name="Menu" contentPlaceholder="Menu">
<Components>
<IncludePage id="6" name="MenuIncludablePage" wizardTheme="None" wizardThemeType="File" wizardThemeVersion="3.0" PathID="MenuMenuIncludablePage" page="MenuIncludablePage.ccp">
<Components/>
<Events/>
<Features/>
</IncludePage>
</Components>
<Events/>
<Attributes/>
<Features/>
</Panel>
<Panel id="5" visible="True" generateDiv="False" name="Footer" contentPlaceholder="Footer">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Panel>
</Components>
	<CodeFiles>
		<CodeFile id="1" language="C#InMotion" name="employee_rpt.aspx" forShow="True" url="employee_rpt.aspx" comment="&lt;!--" commentEnd="--&gt;" codePage="windows-1252"/>
<CodeFile id="1.cs" language="C#InMotion" name="employee_rptEvents.aspx.cs" forShow="False" comment="//" codePage="windows-1252"/>
</CodeFiles>
	<SecurityGroups/>
<CachingParameters/>
<Attributes/>
<Features/>
<Events/>
</Page>
