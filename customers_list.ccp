<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" validateRequest="True" scriptingSupport="Automatic" cachingDuration="1 minutes" masterPage="{CCS_PathToMasterPage}/MasterPage.ccp" useDesign="True" wizardTheme="None" needGeneration="0" wizardThemeVersion="3.0">
	<Components>
		<Panel id="45" visible="True" generateDiv="False" name="Head" contentPlaceholder="Head">
			<Components/>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="46" visible="True" generateDiv="False" name="Content" contentPlaceholder="Content">
			<Components>
				<Record id="73" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="customersSearch" searchIds="73" fictitiousConnection="IntranetDB" wizardCaption="Search  " wizardOrientation="Custom" wizardFormMethod="post" gridSearchClearLink="True" wizardTypeComponent="Search" gridSearchType="And" wizardInteractiveSearch="True" gridSearchRecPerPage="True" wizardTypeButtons="button" wizardDefaultButton="True" gridSearchSortField="False" wizardUseInterVariables="False" templatePageSearch="D:/Users/CBoucher.CSUS2100/Documents/CodeChargeStudio5/Projects/BootstrapDesign/Templates/Search/BootstrapSearch.ccp|userTemplate" wizardThemeApplyTo="Page" addTemplatePanel="False" wizardType="Grid" returnPage="customers_list.ccp" PathID="ContentcustomersSearch" composition="1">
					<Components>
						<Button id="75" urlType="Relative" enableValidation="True" isDefault="True" name="Button_DoSearch" operation="Search" wizardCaption="Search" PathID="ContentcustomersSearchButton_DoSearch">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Button>
						<TextBox id="76" visible="Dynamic" fieldSourceType="DBColumn" dataType="Text" name="s_customer_name" fieldSource="customer_name" wizardIsPassword="False" wizardCaption="Customer Name" caption="Customer Name" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentcustomersSearchs_customer_name" features="(assigned)">
							<Components/>
							<Events/>
							<Attributes/>
							<Features>
								<JAutocomplete id="83" enabled="True" istemplate="Text" template="{@text}" advanced="This is parent control, all controls below is children" height="default" width="default" hscrollbar="default" vscrollbar="default" name="Autocomplete1" servicePage="services/customers_list_s_customer_name_Autocomplete1.ccp" category="jQuery" searchField="customer_name" connection="IntranetDB" featureNameChanged="No" dataSource="customers">
									<Components/>
									<Events/>
									<TableParameters/>
									<SPParameters/>
									<SQLParameters/>
									<JoinTables/>
									<JoinLinks/>
									<Fields/>
									<Features/>
								</JAutocomplete>
							</Features>
						</TextBox>
						<TextBox id="78" visible="Dynamic" fieldSourceType="DBColumn" dataType="Text" name="s_customer_address" fieldSource="customer_address" wizardIsPassword="False" wizardCaption="Customer Address" caption="Customer Address" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentcustomersSearchs_customer_address" features="(assigned)">
							<Components/>
							<Events/>
							<Attributes/>
							<Features>
								<JAutocomplete id="85" enabled="True" istemplate="Text" template="{@text}" advanced="This is parent control, all controls below is children" height="default" width="default" hscrollbar="default" vscrollbar="default" name="Autocomplete3" servicePage="services/customers_list_s_customer_address_Autocomplete1.ccp" category="jQuery" searchField="customer_address" connection="IntranetDB" featureNameChanged="No" dataSource="customers">
									<Components/>
									<Events/>
									<TableParameters/>
									<SPParameters/>
									<SQLParameters/>
									<JoinTables/>
									<JoinLinks/>
									<Fields/>
									<Features/>
								</JAutocomplete>
							</Features>
						</TextBox>
						<TextBox id="79" visible="Dynamic" fieldSourceType="DBColumn" dataType="Text" name="s_customer_city" fieldSource="customer_city" wizardIsPassword="False" wizardCaption="Customer City" caption="Customer City" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentcustomersSearchs_customer_city" features="(assigned)">
							<Components/>
							<Events/>
							<Attributes/>
							<Features>
								<JAutocomplete id="86" enabled="True" istemplate="Text" template="{@text}" advanced="This is parent control, all controls below is children" height="default" width="default" hscrollbar="default" vscrollbar="default" name="Autocomplete4" servicePage="services/customers_list_s_customer_city_Autocomplete1.ccp" category="jQuery" searchField="customer_city" connection="IntranetDB" featureNameChanged="No" dataSource="customers">
									<Components/>
									<Events/>
									<TableParameters/>
									<SPParameters/>
									<SQLParameters/>
									<JoinTables/>
									<JoinLinks/>
									<Fields/>
									<Features/>
								</JAutocomplete>
							</Features>
						</TextBox>
						<TextBox id="80" visible="Dynamic" fieldSourceType="DBColumn" dataType="Text" name="s_customer_state" fieldSource="customer_state" wizardIsPassword="False" wizardCaption="Customer State" caption="Customer State" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentcustomersSearchs_customer_state" features="(assigned)">
							<Components/>
							<Events/>
							<Attributes/>
							<Features>
								<JAutocomplete id="87" enabled="True" istemplate="Text" template="{@text}" advanced="This is parent control, all controls below is children" height="default" width="default" hscrollbar="default" vscrollbar="default" name="Autocomplete5" servicePage="services/customers_list_s_customer_state_Autocomplete1.ccp" category="jQuery" searchField="customer_state" connection="IntranetDB" featureNameChanged="No" dataSource="customers">
									<Components/>
									<Events/>
									<TableParameters/>
									<SPParameters/>
									<SQLParameters/>
									<JoinTables/>
									<JoinLinks/>
									<Fields/>
									<Features/>
								</JAutocomplete>
							</Features>
						</TextBox>
						<TextBox id="81" visible="Dynamic" fieldSourceType="DBColumn" dataType="Text" name="s_customer_zip" fieldSource="customer_zip" wizardIsPassword="False" wizardCaption="Customer Zip" caption="Customer Zip" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentcustomersSearchs_customer_zip" features="(assigned)">
							<Components/>
							<Events/>
							<Attributes/>
							<Features>
								<JAutocomplete id="88" enabled="True" istemplate="Text" template="{@text}" advanced="This is parent control, all controls below is children" height="default" width="default" hscrollbar="default" vscrollbar="default" name="Autocomplete6" servicePage="services/customers_list_s_customer_zip_Autocomplete1.ccp" category="jQuery" searchField="customer_zip" connection="IntranetDB" featureNameChanged="No" dataSource="customers">
									<Components/>
									<Events/>
									<TableParameters/>
									<SPParameters/>
									<SQLParameters/>
									<JoinTables/>
									<JoinLinks/>
									<Fields/>
									<Features/>
								</JAutocomplete>
							</Features>
						</TextBox>
						<ListBox id="82" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" returnValueType="Number" name="customersPageSize" dataSource=";Select Value;5;5;10;10;25;25;100;100" wizardCaption="Records per page" wizardNoEmptyValue="True" PathID="ContentcustomersSearchcustomersPageSize">
							<Components/>
							<Events/>
							<TableParameters/>
							<SPParameters/>
							<SQLParameters/>
							<JoinTables/>
							<JoinLinks/>
							<Fields/>
							<PKFields/>
							<Attributes/>
							<Features/>
						</ListBox>
						<TextBox id="77" visible="Dynamic" fieldSourceType="DBColumn" dataType="Text" name="s_customer_phone" fieldSource="customer_phone" wizardIsPassword="False" wizardCaption="Customer Phone" caption="Customer Phone" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentcustomersSearchs_customer_phone" features="(assigned)">
							<Components/>
							<Events/>
							<Attributes/>
							<Features>
								<JAutocomplete id="84" enabled="True" istemplate="Text" template="{@text}" advanced="This is parent control, all controls below is children" height="default" width="default" hscrollbar="default" vscrollbar="default" name="JAutocomplete1" servicePage="services/customers_list_s_customer_phone_Autocomplete1.ccp" category="jQuery" searchField="customer_phone" connection="IntranetDB" featureNameChanged="No" dataSource="customers">
									<Components/>
									<Events/>
									<TableParameters/>
									<SPParameters/>
									<SQLParameters/>
									<JoinTables/>
									<JoinLinks/>
									<Fields/>
									<Features/>
								</JAutocomplete>
							</Features>
						</TextBox>
						<Link id="74" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="ClearParameters" hrefSource="customers_list.ccp" removeParameters="s_customer_name;s_customer_phone;s_customer_address;s_customer_city;s_customer_state;s_customer_zip;customersPageSize" wizardThemeItem="SorterLink" wizardDefaultValue="Clear" PathID="ContentcustomersSearchClearParameters">
							<Components/>
							<Events/>
							<LinkParameters/>
							<Attributes/>
							<Features/>
						</Link>
					</Components>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<PKFields/>
					<ISPParameters/>
					<ISQLParameters/>
					<IFormElements/>
					<USPParameters/>
					<USQLParameters/>
					<UConditions/>
					<UFormElements/>
					<DSPParameters/>
					<DSQLParameters/>
					<DConditions/>
					<SecurityGroups/>
					<Attributes/>
					<Features/>
				</Record>
				<Grid id="89" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" name="customers" connection="IntranetDB" dataSource="customers" pageSizeLimit="100" pageSize="False" wizardCaption="List of Customers " wizardThemeApplyTo="Page" wizardGridType="Custom" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="No records" wizardGridPagingType="Centered" wizardUseSearch="False" wizardAddNbsp="False" gridTotalRecords="False" wizardAddPanels="False" wizardType="Grid" wizardUseInterVariables="False" templatePage="D:/Users/CBoucher.CSUS2100/Documents/CodeChargeStudio5/Projects/BootstrapDesign/Templates/Grid/BootstrapTabular.ccp|userTemplate" addTemplatePanel="False" changedCaptionGrid="False" gridExtendedHTML="False" PathID="Contentcustomers" orderBy="customer_id">
					<Components>
						<Sorter id="91" visible="True" name="Sorter_customer_id" column="customer_id" wizardCaption="Customer Id" wizardSortingType="SimpleDir" wizardControl="customer_id" wizardAddNbsp="False" PathID="ContentcustomersSorter_customer_id">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="92" visible="True" name="Sorter_customer_name" column="customer_name" wizardCaption="Customer Name" wizardSortingType="SimpleDir" wizardControl="customer_name" wizardAddNbsp="False" PathID="ContentcustomersSorter_customer_name">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="93" visible="True" name="Sorter_customer_phone" column="customer_phone" wizardCaption="Customer Phone" wizardSortingType="SimpleDir" wizardControl="customer_phone" wizardAddNbsp="False" PathID="ContentcustomersSorter_customer_phone">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="94" visible="True" name="Sorter_customer_address" column="customer_address" wizardCaption="Customer Address" wizardSortingType="SimpleDir" wizardControl="customer_address" wizardAddNbsp="False" PathID="ContentcustomersSorter_customer_address">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="95" visible="True" name="Sorter_customer_city" column="customer_city" wizardCaption="Customer City" wizardSortingType="SimpleDir" wizardControl="customer_city" wizardAddNbsp="False" PathID="ContentcustomersSorter_customer_city">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="96" visible="True" name="Sorter_customer_state" column="customer_state" wizardCaption="Customer State" wizardSortingType="SimpleDir" wizardControl="customer_state" wizardAddNbsp="False" PathID="ContentcustomersSorter_customer_state">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="97" visible="True" name="Sorter_customer_zip" column="customer_zip" wizardCaption="Customer Zip" wizardSortingType="SimpleDir" wizardControl="customer_zip" wizardAddNbsp="False" PathID="ContentcustomersSorter_customer_zip">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Link id="98" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" html="False" hrefType="Page" preserveParameters="GET" name="customer_id" fieldSource="customer_id" wizardCaption="Customer Id" wizardIsPassword="False" wizardUseTemplateBlock="False" hrefSource="customers_maint.ccp" linkProperties="{'textSource':'','textSourceDB':'customer_id','hrefSource':'customers_maint.ccp','hrefSourceDB':'customer_id','title':'','target':'','name':'','linkParameters':{'0':{'sourceType':'DataField','parameterSource':'customer_id','parameterName':'customer_id'},'length':1,'objectType':'linkParameters'}}" wizardAddNbsp="False" PathID="Contentcustomerscustomer_id" urlType="Relative">
							<Components/>
							<Events/>
							<LinkParameters>
								<LinkParameter id="124" sourceType="DataField" name="customer_id" source="customer_id"/>
							</LinkParameters>
							<Attributes/>
							<Features/>
						</Link>
						<Label id="99" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_name" fieldSource="customer_name" wizardCaption="Customer Name" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentcustomerscustomer_name">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="100" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_phone" fieldSource="customer_phone" wizardCaption="Customer Phone" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentcustomerscustomer_phone">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="101" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_address" fieldSource="customer_address" wizardCaption="Customer Address" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentcustomerscustomer_address">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="102" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_city" fieldSource="customer_city" wizardCaption="Customer City" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentcustomerscustomer_city">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="103" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_state" fieldSource="customer_state" wizardCaption="Customer State" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentcustomerscustomer_state">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="104" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_zip" fieldSource="customer_zip" wizardCaption="Customer Zip" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentcustomerscustomer_zip">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Navigator id="105" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="of" wizardPageSize="False" wizardImagesScheme="None">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Navigator>
						<Link id="125" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link1" PathID="ContentcustomersLink1" hrefSource="customers_maint.ccp" wizardUseTemplateBlock="False" linkProperties="{'textSource':'Add New Customer','textSourceDB':'','hrefSource':'customers_maint.ccp','hrefSourceDB':'','title':'','target':'','name':'','linkParameters':{'length':0,'objectType':'linkParameters'}}" removeParameters="customer_id">
<Components/>
							<Events/>
							<LinkParameters/>
							<Attributes/>
							<Features/>
						</Link>
					</Components>
					<Events/>
					<TableParameters>
						<TableParameter id="116" conditionType="Parameter" useIsNull="False" dataType="Text" field="customer_name" logicOperator="And" parameterSource="s_customer_name" parameterType="URL" searchConditionType="Contains"/>
						<TableParameter id="117" conditionType="Parameter" useIsNull="False" dataType="Text" field="customer_phone" logicOperator="And" parameterSource="s_customer_phone" parameterType="URL" searchConditionType="Contains"/>
						<TableParameter id="118" conditionType="Parameter" useIsNull="False" dataType="Text" field="customer_address" logicOperator="And" parameterSource="s_customer_address" parameterType="URL" searchConditionType="Contains"/>
						<TableParameter id="119" conditionType="Parameter" useIsNull="False" dataType="Text" field="customer_city" logicOperator="And" parameterSource="s_customer_city" parameterType="URL" searchConditionType="Contains"/>
						<TableParameter id="120" conditionType="Parameter" useIsNull="False" dataType="Text" field="customer_state" logicOperator="And" parameterSource="s_customer_state" parameterType="URL" searchConditionType="Contains"/>
						<TableParameter id="121" conditionType="Parameter" useIsNull="False" dataType="Text" field="customer_zip" logicOperator="And" parameterSource="s_customer_zip" parameterType="URL" searchConditionType="Contains"/>
					</TableParameters>
					<JoinTables>
						<JoinTable id="115" posHeight="168" posLeft="10" posTop="10" posWidth="141" tableName="customers"/>
					</JoinTables>
					<JoinLinks/>
					<Fields>
						<Field id="122" fieldName="*"/>
					</Fields>
					<PKFields>
						<PKField id="123" dataType="Integer" fieldName="customer_id" tableName="customers"/>
					</PKFields>
					<SPParameters/>
					<SQLParameters/>
					<SecurityGroups/>
					<Attributes/>
					<Features/>
				</Grid>
			</Components>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="47" visible="True" generateDiv="False" name="Menu" contentPlaceholder="Menu">
			<Components>
				<IncludePage id="49" name="MenuIncludablePage" PathID="MenuMenuIncludablePage" page="MenuIncludablePage.ccp">
					<Components/>
					<Events/>
					<Features/>
				</IncludePage>
			</Components>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="48" visible="True" generateDiv="False" name="Footer" contentPlaceholder="Footer">
			<Components/>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
	</Components>
	<CodeFiles>
		<CodeFile id="1" language="C#InMotion" name="customers_list.aspx" forShow="True" url="customers_list.aspx" comment="&lt;!--" commentEnd="--&gt;" codePage="windows-1252"/>
		<CodeFile id="1.cs" language="C#InMotion" name="customers_listEvents.aspx.cs" forShow="False" comment="//" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
