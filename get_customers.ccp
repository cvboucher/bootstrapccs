<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" validateRequest="True" scriptingSupport="Automatic" cachingDuration="1 minutes" wizardTheme="None" wizardThemeVersion="3.0" useDesign="False">
	<Components>
		<Grid id="2" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" name="customers" connection="IntranetDB" dataSource="customers" pageSizeLimit="100" pageSize="False" wizardCaption="List of Customers " wizardThemeApplyTo="Page" wizardGridType="Custom" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="No records" wizardUseSearch="False" wizardAddNbsp="False" gridTotalRecords="False" wizardAddPanels="False" wizardType="Grid" wizardUseInterVariables="False" templatePage="C:/Program Files (x86)/CodeChargeStudio5/Templates/Grid/jQueryData.ccp|ccsTemplate" addTemplatePanel="False" changedCaptionGrid="False" gridExtendedHTML="False" PathID="customers">
<Components>
<Label id="4" fieldSourceType="DBColumn" dataType="Integer" html="False" generateSpan="False" name="customer_id" fieldSource="customer_id" wizardCaption="Customer Id" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="customerscustomer_id">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Label>
<Label id="5" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_name" fieldSource="customer_name" wizardCaption="Customer Name" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="customerscustomer_name">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Label>
<Label id="6" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_phone" fieldSource="customer_phone" wizardCaption="Customer Phone" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="customerscustomer_phone">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Label>
<Label id="7" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_address" fieldSource="customer_address" wizardCaption="Customer Address" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="customerscustomer_address">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Label>
<Label id="8" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_city" fieldSource="customer_city" wizardCaption="Customer City" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="customerscustomer_city">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Label>
<Label id="9" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_state" fieldSource="customer_state" wizardCaption="Customer State" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="customerscustomer_state">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Label>
<Label id="10" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_zip" fieldSource="customer_zip" wizardCaption="Customer Zip" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="customerscustomer_zip">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Label>
</Components>
<Events/>
<TableParameters/>
<JoinTables>
<JoinTable id="3" tableName="customers"/>
</JoinTables>
<JoinLinks/>
<Fields/>
<PKFields/>
<SPParameters/>
<SQLParameters/>
<SecurityGroups/>
<Attributes/>
<Features/>
</Grid>
</Components>
	<CodeFiles>
		<CodeFile id="1" language="C#InMotion" name="get_customers.aspx" forShow="True" url="get_customers.aspx" comment="&lt;!--" commentEnd="--&gt;" codePage="windows-1252"/>
<CodeFile id="1.cs" language="C#InMotion" name="get_customersEvents.aspx.cs" forShow="False" comment="//" codePage="windows-1252"/>
</CodeFiles>
	<SecurityGroups/>
<CachingParameters/>
<Attributes/>
<Features/>
<Events/>
</Page>
