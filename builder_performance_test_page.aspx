<!--ASPX page header @1-F29E32F5-->
<%@ Page language="C#" CodeFile="builder_performance_test_pageEvents.aspx.cs" Inherits="BootstrapDesign.builder_performance_test_page_Page" MasterPageFile="~/Designs/Bootstrap/MasterPage.master"  %>
<%@ Register TagPrefix="BootstrapDesign" TagName="MenuIncludablePage" Src="MenuIncludablePage.ascx" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASPX page header-->

<!--ASPX page @1-79DFB384-->

<asp:Content ContentPlaceHolderID="Head" ID="Head_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Head" runat="server">
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-65835CB2
</script>
<script src="ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>" type="text/javascript" charset="windows-1252"></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>


<title>builder_performance_test_page</title>

<meta content="text/html; charset=windows-1252" http-equiv="Content-Type">
</mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Content" ID="Content_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Content" runat="server">
<mt:Record ReturnPage="~/builder_performance_test_page.aspx" ID="customersSearch" runat="server"><ItemTemplate><div data-emulate-form="ContentcustomersSearch" id="ContentcustomersSearch">
<div role="form" class="form-horizontal">
  



    <legend>Search </legend>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <div class="row">
      <div class="col-sm-offset-2 col-sm-10">
        <div role="alert" class="alert alert-danger">
          <mt:MTLabel id="ErrorLabel" runat="server"/> 
        </div>
      </div>
    </div>
    </mt:MTPanel><fieldset>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Customer Name</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="customer_name" Caption="Customer Name" maxlength="50" Columns="50" ID="s_customer_name" data-id="ContentcustomersSearchs_customer_name" runat="server"/>
      </div>
      <div class="col-sm-2 control-label">
        <label>Customer Phone</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="customer_phone" Caption="Customer Phone" maxlength="50" Columns="50" ID="s_customer_phone" data-id="ContentcustomersSearchs_customer_phone" runat="server"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Customer Address</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="customer_address" Caption="Customer Address" maxlength="50" Columns="50" ID="s_customer_address" data-id="ContentcustomersSearchs_customer_address" runat="server"/>
      </div>
      <div class="col-sm-2 control-label">
        <label>Customer City</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="customer_city" Caption="Customer City" maxlength="50" Columns="50" ID="s_customer_city" data-id="ContentcustomersSearchs_customer_city" runat="server"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Customer State</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="customer_state" Caption="Customer State" maxlength="50" Columns="50" ID="s_customer_state" data-id="ContentcustomersSearchs_customer_state" runat="server"/>
      </div>
      <div class="col-sm-2 control-label">
        <label>Customer Zip</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="customer_zip" Caption="Customer Zip" maxlength="50" Columns="50" ID="s_customer_zip" data-id="ContentcustomersSearchs_customer_zip" runat="server"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label for="<%# customersSearch.GetControl<InMotion.Web.Controls.MTListBox>("customersPageSize").ClientID %>">Records per page</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTListBox Rows="1" ID="customersPageSize" data-id="ContentcustomersSearchcustomersPageSize" runat="server">
          <asp:ListItem Value="" Text="Select Value"/>
          <asp:ListItem Value="5" Text="5"/>
          <asp:ListItem Value="10" Text="10"/>
          <asp:ListItem Value="25" Text="25"/>
          <asp:ListItem Value="100" Text="100"/>
        </mt:MTListBox>
 
      </div>
    </div>
    </fieldset> 
    <div class="form-group">
      <div class="col-sm-2 text-right">
<mt:MTLink Text="Clear" ID="ClearParameters" data-id="ContentcustomersSearchClearParameters" runat="server" HrefSource="~/builder_performance_test_page.aspx" PreserveParameters="Get" RemoveParameters="s_customer_name;s_customer_phone;s_customer_address;s_customer_city;s_customer_state;s_customer_zip;customersPageSize"/> 
      </div>
      <div class="col-sm-4">
        <mt:MTButton CommandName="Search" Text="Search" CssClass="Button" ID="Button_DoSearch" data-id="ContentcustomersSearchButton_DoSearch" runat="server"/>
      </div>
    </div>
  

</div>

</div></ItemTemplate></mt:Record><br>
<mt:Grid PageSizeLimit="100" RecordsPerPage="10" SortExpression="customer_name" DataSourceID="customersDataSource" ID="customers" runat="server">
<HeaderTemplate>
<div class="panel panel-primary">
  <div class="panel-heading">
    <strong>List of Customers </strong>
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-hover">
        <tr>
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="customer_name" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Customer Name" ID="Sorter_customer_name" data-id="ContentcustomersSorter_customer_name" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="customer_phone" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Customer Phone" ID="Sorter_customer_phone" data-id="ContentcustomersSorter_customer_phone" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="customer_address" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Customer Address" ID="Sorter_customer_address" data-id="ContentcustomersSorter_customer_address" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="customer_city" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Customer City" ID="Sorter_customer_city" data-id="ContentcustomersSorter_customer_city" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="customer_state" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Customer State" ID="Sorter_customer_state" data-id="ContentcustomersSorter_customer_state" runat="server"/></th>
 
          <th scope="col">
          <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="customer_zip" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Customer Zip" ID="Sorter_customer_zip" data-id="ContentcustomersSorter_customer_zip" runat="server"/></th>
        </tr>
 
        
</HeaderTemplate>
<ItemTemplate>
        <tr>
          <td><mt:MTLink Source="customer_name" ID="customer_name" data-id="Contentcustomerscustomer_name_{customers:rowNumber}" runat="server" HrefSource="~/customers_maint.aspx" PreserveParameters="Get"><Parameters>
            <mt:UrlParameter Name="customer_id" SourceType="DataSourceColumn" Source="customer_id"/>
          </Parameters></mt:MTLink></td> 
          <td><mt:MTLabel Source="customer_phone" ID="customer_phone" runat="server"/></td> 
          <td><mt:MTLabel Source="customer_address" ID="customer_address" runat="server"/></td> 
          <td><mt:MTLabel Source="customer_city" ID="customer_city" runat="server"/></td> 
          <td><mt:MTLabel Source="customer_state" ID="customer_state" runat="server"/></td> 
          <td><mt:MTLabel Source="customer_zip" ID="customer_zip" runat="server"/></td>
        </tr>
 
</ItemTemplate>
<FooterTemplate>
        <mt:MTPanel id="NoRecords" visible="False" runat="server">
        <tr class="bg-warning">
          <td colspan="6">No records</td>
        </tr>
        </mt:MTPanel>
        <tr class="Footer">
          <td colspan="6">
            <mt:MTNavigator FirstOnValue="First " FirstOffValue="First " PreviousOnValue="Prev " PreviousOffValue="Prev " NextOnValue="Next " NextOffValue="Next " LastOnValue="Last " LastOffValue="Last " PageOffPreValue="" PageOffPostValue="&amp;nbsp;" DisplayStyle="Links" PageLinksNumber="10" PageNumbersStyle="CurrentPageCentered" TotalPagesText="of" ShowTotalPages="true" ID="Navigator" runat="server"/></td>
        </tr>
      </table>
    </div>
  </div>
</div>

</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="customersDataSource" ConnectionString="InMotion:IntranetDB" SelectCommandType="Table" SelectOrderBy="customer_name" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM customers {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM customers
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="Urls_customer_name" SourceType="URL" Source="s_customer_name" DataType="Text" Operation="And" Condition="Contains" SourceColumn="customer_name"/>
     <mt:WhereParameter Name="Urls_customer_phone" SourceType="URL" Source="s_customer_phone" DataType="Text" Operation="And" Condition="Contains" SourceColumn="customer_phone"/>
     <mt:WhereParameter Name="Urls_customer_address" SourceType="URL" Source="s_customer_address" DataType="Text" Operation="And" Condition="Contains" SourceColumn="customer_address"/>
     <mt:WhereParameter Name="Urls_customer_city" SourceType="URL" Source="s_customer_city" DataType="Text" Operation="And" Condition="Contains" SourceColumn="customer_city"/>
     <mt:WhereParameter Name="Urls_customer_state" SourceType="URL" Source="s_customer_state" DataType="Text" Operation="And" Condition="Contains" SourceColumn="customer_state"/>
     <mt:WhereParameter Name="Urls_customer_zip" SourceType="URL" Source="s_customer_zip" DataType="Text" Operation="And" Condition="Contains" SourceColumn="customer_zip"/>
   </SelectParameters>
</mt:MTDataSource></mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Menu" ID="Menu_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Menu" runat="server"><BootstrapDesign:MenuIncludablePage ID="MenuIncludablePage" runat="server"/></mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Footer" ID="Footer_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Footer" runat="server">&nbsp;</mt:MTPanel></asp:Content>
<!--End ASPX page-->

