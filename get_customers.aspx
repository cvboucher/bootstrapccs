

<!--ASPX page header @1-78E99CFB-->
<%@ Page language="C#" CodeFile="get_customersEvents.aspx.cs" Inherits="BootstrapDesign.get_customers_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASPX page header-->

<!--ASPX page @1-2864CD32-->
<form id="Form1" runat="server" data-need-form-emulation="data-need-form-emulation"><!--
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-5EA5EF98
</script>
<script src="ClientI18N.aspx?file=globalize.js&locale=<%#ResManager.GetString("CCS_LocaleID")%>" type="text/javascript" charset="windows-1252"></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>
-->
<mt:Grid PageSizeLimit="100" RecordsPerPage="10" DataSourceID="customersDataSource" ID="customers" runat="server">
<HeaderTemplate>[ 

</HeaderTemplate>
<ItemTemplate>{ "Customer Id" : "<mt:MTLabel Source="customer_id" DataType="Integer" ID="customer_id" runat="server"/>" , "Customer Name" : "<mt:MTLabel Source="customer_name" ID="customer_name" runat="server"/>" , "Customer Phone" : "<mt:MTLabel Source="customer_phone" ID="customer_phone" runat="server"/>" , "Customer Address" : "<mt:MTLabel Source="customer_address" ID="customer_address" runat="server"/>" , "Customer City" : "<mt:MTLabel Source="customer_city" ID="customer_city" runat="server"/>" , "Customer State" : "<mt:MTLabel Source="customer_state" ID="customer_state" runat="server"/>" , "Customer Zip" : "<mt:MTLabel Source="customer_zip" ID="customer_zip" runat="server"/>" } 
</ItemTemplate>
<FooterTemplate>]
</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="customersDataSource" ConnectionString="InMotion:IntranetDB" SelectCommandType="Table" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM customers {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM customers
   </CountCommand>
</mt:MTDataSource>







</form>
<!--End ASPX page-->

