<!--ASPX page header @1-A3A3871F-->
<%@ Page language="C#" CodeFile="groups_maintEvents.aspx.cs" Inherits="BootstrapDesign.groups_maint_Page" MasterPageFile="~/Designs/Bootstrap/MasterPage.master"  %>
<%@ Register TagPrefix="BootstrapDesign" TagName="MenuIncludablePage" Src="MenuIncludablePage.ascx" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASPX page header-->

<!--ASPX page @1-1DC5F54C-->

<asp:Content ContentPlaceHolderID="Head" ID="Head_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Head" runat="server">
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-65835CB2
</script>
<script src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>' type="text/javascript" charset="windows-1252"></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//Common Script Start @1-8BFA436B
jQuery(function ($) {
    var features = { };
    var actions = { };
    var params = { };
//End Common Script Start

//ContentgroupsButton_DeleteOnClick Event Start @-B29D19D4
    actions["ContentgroupsButton_DeleteOnClick"] = function (eventType, parameters) {
        var result = true;
//End ContentgroupsButton_DeleteOnClick Event Start

//Confirmation Message @18-8243B274
        return confirm('Delete record?');
//End Confirmation Message

//ContentgroupsButton_DeleteOnClick Event End @-A5B9ECB8
        return result;
    };
//End ContentgroupsButton_DeleteOnClick Event End

//Event Binding @1-9F6DD51E
    $('*:ccsControl(Content, groups, Button_Delete)').ccsBind(function() {
        this.bind("click", actions["ContentgroupsButton_DeleteOnClick"]);
    });
//End Event Binding

//Plugin Calls @1-D936FA43
    $('*:ccsControl(Content, groups, Button_Delete)').ccsBind(function() {
        this.bind("click", function(){ $("body").data("disableValidation", true); });
    });
    $('*:ccsControl(Content, groups, Button_Cancel)').ccsBind(function() {
        this.bind("click", function(){ $("body").data("disableValidation", true); });
    });
//End Plugin Calls

//Common Script End @1-562554DE
});
//End Common Script End

//End CCS script
</script>


<title>Groups</title>

<meta content="text/html; charset=windows-1252" http-equiv="Content-Type">
</mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Content" ID="Content_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Content" runat="server">
<mt:Record PreserveParameters="Get" ReturnPage="~/groups_list.aspx" DataSourceID="groupsDataSource" ID="groups" runat="server"><ItemTemplate><div data-emulate-form="Contentgroups" id="Contentgroups">
<div class="form-horizontal">
  



    <legend>Add/Edit Groups </legend>
 
    <mt:MTPanel id="Error" visible="False" runat="server">
    <div class="row">
      <div class="col-sm-offset-2 col-sm-10">
        <div role="alert" class="alert alert-danger">
          <span id="ErrorBlock"><mt:MTLabel id="ErrorLabel" runat="server"/></span> 
        </div>
 
      </div>
 
    </div>
 </mt:MTPanel><fieldset>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Group Name</label> 
      </div>
 
      <div class="col-sm-10">
        <mt:MTTextBox Source="group_name" Caption="Group Name" maxlength="50" Columns="50" ID="group_name" data-id="Contentgroupsgroup_name" runat="server"/>
      </div>
 
    </div>
 </fieldset> 
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <mt:MTButton CommandName="Insert" Text="Add" CssClass="Button" ID="Button_Insert" data-id="ContentgroupsButton_Insert" runat="server"/>
        <mt:MTButton CommandName="Update" Text="Submit" CssClass="Button" ID="Button_Update" data-id="ContentgroupsButton_Update" runat="server"/>
        <mt:MTButton CommandName="Delete" EnableValidation="False" Text="Delete" CssClass="Button" ID="Button_Delete" data-id="ContentgroupsButton_Delete" runat="server"/>
        <mt:MTButton CommandName="Cancel" EnableValidation="False" Text="Cancel" CssClass="Button" ID="Button_Cancel" data-id="ContentgroupsButton_Cancel" runat="server"/>
      </div>
 
    </div>
 
  

</div>

</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="groupsDataSource" ConnectionString="InMotion:IntranetDB" SelectCommandType="Table" InsertCommandType="Table" UpdateCommandType="Table" DeleteCommandType="Table" runat="server">
   <SelectCommand>
SELECT * 
FROM groups {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <InsertCommand>
INSERT INTO groups(group_name) VALUES ({group_name})
  </InsertCommand>
   <UpdateCommand>
UPDATE groups SET group_name={group_name}
  </UpdateCommand>
   <DeleteCommand>
DELETE FROM groups
  </DeleteCommand>
   <SelectParameters>
     <mt:WhereParameter Name="Urlgroup_id" SourceType="URL" Source="group_id" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="group_id" Required="true"/>
   </SelectParameters>
</mt:MTDataSource><br>
</mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Menu" ID="Menu_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Menu" runat="server"><BootstrapDesign:MenuIncludablePage ID="MenuIncludablePage" runat="server"/></mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Footer" ID="Footer_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Footer" runat="server">&nbsp;</mt:MTPanel></asp:Content>
<!--End ASPX page-->

