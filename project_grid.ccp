<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" validateRequest="True" scriptingSupport="Automatic" cachingDuration="1 minutes" useDesign="True" masterPage="{CCS_PathToMasterPage}/MasterPage.ccp" wizardTheme="None" wizardThemeVersion="3.0">
	<Components>
		<Panel id="2" visible="True" generateDiv="False" name="Head" contentPlaceholder="Head">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Panel>
<Panel id="3" visible="True" generateDiv="False" name="Content" contentPlaceholder="Content">
<Components>
<EditableGrid id="6" urlType="Relative" secured="False" emptyRows="3" allowInsert="True" allowUpdate="True" allowDelete="True" validateData="True" preserveParameters="GET" sourceType="Table" defaultPageSize="10" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="projects" connection="IntranetDB" dataSource="projects" orderBy="project_name" pageSizeLimit="100" wizardGridPageSize="False" wizardUseSearch="False" allowCancel="True" wizardSubmitConfirmation="False" wizardCaption="Add/Edit Projects " wizardGridType="Custom" wizardSortingType="SimpleDir" wizardAltRecord="False" wizardRecordSeparator="False" wizardNoRecords="No records" wizardGridKey="project_id" wizardGridPaging="Centered" wizardAddNbsp="False" wizardTotalRecords="False" wizardButtonsType="button" changedCaptionEditableGrid="False" wizardUseInterVariables="False" pkIsAutoincrement="True" templatePage="D:/Users/CBoucher.CSUS2100/Documents/CodeChargeStudio5/Projects/BootstrapDesign/Templates/EditableGrid/BootstrapTabular.ccp|userTemplate" wizardType="EditableGrid" wizardThemeApplyTo="Page" addTemplatePanel="False" PathID="Contentprojects" deleteControl="CheckBox_Delete">
<Components>
<Sorter id="10" visible="True" name="Sorter_project_name" column="project_name" wizardCaption="Project Name" wizardSortingType="SimpleDir" wizardControl="project_name" wizardAddNbsp="False" PathID="ContentprojectsSorter_project_name">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Sorter>
<Sorter id="11" visible="True" name="Sorter_project_is_active" column="project_is_active" wizardCaption="Project Is Active" wizardSortingType="SimpleDir" wizardControl="project_is_active" wizardAddNbsp="False" PathID="ContentprojectsSorter_project_is_active">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Sorter>
<TextBox id="12" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="project_name" fieldSource="project_name" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardCaption="Project Name" caption="Project Name" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentprojectsproject_name">
<Components/>
<Events/>
<Attributes/>
<Features/>
</TextBox>
<CheckBox id="13" visible="Yes" fieldSourceType="DBColumn" dataType="Boolean" name="project_is_active" fieldSource="project_is_active" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardCaption="Project Is Active" PathID="Contentprojectsproject_is_active">
<Components/>
<Events/>
<Attributes/>
<Features/>
</CheckBox>
<Panel id="14" visible="True" generateDiv="False" name="CheckBox_Delete_Panel">
<Components>
<CheckBox id="15" visible="Dynamic" fieldSourceType="CodeExpression" dataType="Boolean" defaultValue="Unchecked" name="CheckBox_Delete" checkedValue="true" uncheckedValue="false" wizardCaption="Delete" wizardAddNbsp="True" PathID="ContentprojectsCheckBox_Delete_PanelCheckBox_Delete">
<Components/>
<Events/>
<Attributes/>
<Features/>
</CheckBox>
</Components>
<Events/>
<Attributes/>
<Features/>
</Panel>
<Navigator id="16" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardPageSize="False">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Navigator>
<Button id="17" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Submit" operation="Submit" wizardCaption="Submit" PathID="ContentprojectsButton_Submit">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Button>
<Button id="18" urlType="Relative" enableValidation="False" isDefault="False" name="Cancel" operation="Cancel" wizardCaption="Cancel" PathID="ContentprojectsCancel">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Button>
</Components>
<Events/>
<TableParameters>
<TableParameter id="20" conditionType="Parameter" useIsNull="False" dataType="Text" field="project_name" logicOperator="And" orderNumber="1" parameterSource="s_project_name" parameterType="URL" searchConditionType="Contains" searchFormParameter="True"/>
</TableParameters>
<SPParameters/>
<SQLParameters/>
<JoinTables>
<JoinTable id="19" posHeight="104" posLeft="10" posTop="10" posWidth="111" tableName="projects"/>
</JoinTables>
<JoinLinks/>
<Fields>
<Field id="21" fieldName="*"/>
</Fields>
<PKFields>
<PKField id="22" dataType="Integer" fieldName="project_id" tableName="projects"/>
</PKFields>
<ISPParameters/>
<ISQLParameters/>
<IFormElements/>
<USPParameters/>
<USQLParameters/>
<UConditions/>
<UFormElements/>
<DSPParameters/>
<DSQLParameters/>
<DConditions/>
<SecurityGroups/>
<Attributes/>
<Features/>
</EditableGrid>
<Record id="23" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="projects1" searchIds="23" fictitiousConnection="IntranetDB" fictitiousDataSource="projects" wizardCaption="Search  " changedCaptionSearch="False" wizardOrientation="Custom" wizardFormMethod="post" gridSearchClearLink="True" wizardTypeComponent="Search" gridSearchType="And" wizardInteractiveSearch="False" gridSearchRecPerPage="True" wizardTypeButtons="button" wizardDefaultButton="True" gridSearchSortField="True" wizardUseInterVariables="False" templatePageSearch="D:/Users/CBoucher.CSUS2100/Documents/CodeChargeStudio5/Projects/BootstrapDesign/Templates/Search/BootstrapSearch.ccp|userTemplate" wizardThemeApplyTo="Page" addTemplatePanel="False" wizardSpecifyResultsForm="False" PathID="Contentprojects1">
<Components>
<Link id="24" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="ClearParameters" hrefSource="project_grid.ccp" removeParameters="s_project_name;OrderOrder;OrderDir;PageSize" wizardThemeItem="SorterLink" wizardDefaultValue="Clear" PathID="Contentprojects1ClearParameters">
<Components/>
<Events/>
<LinkParameters/>
<Attributes/>
<Features/>
</Link>
<Button id="25" urlType="Relative" enableValidation="True" isDefault="True" name="Button_DoSearch" operation="Search" wizardCaption="Search" PathID="Contentprojects1Button_DoSearch">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Button>
<TextBox id="26" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_project_name" fieldSource="project_name" wizardIsPassword="False" wizardCaption="Project Name" caption="Project Name" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentprojects1s_project_name">
<Components/>
<Events/>
<Attributes/>
<Features/>
</TextBox>
<ListBox id="29" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" returnValueType="Number" name="PageSize" dataSource=";Select Value;5;5;10;10;25;25;100;100" wizardCaption="Records per page" wizardNoEmptyValue="True" PathID="Contentprojects1PageSize">
<Components/>
<Events/>
<TableParameters/>
<SPParameters/>
<SQLParameters/>
<JoinTables/>
<JoinLinks/>
<Fields/>
<PKFields/>
<Attributes/>
<Features/>
</ListBox>
</Components>
<Events/>
<TableParameters/>
<SPParameters/>
<SQLParameters/>
<JoinTables/>
<JoinLinks/>
<Fields/>
<PKFields/>
<ISPParameters/>
<ISQLParameters/>
<IFormElements/>
<USPParameters/>
<USQLParameters/>
<UConditions/>
<UFormElements/>
<DSPParameters/>
<DSQLParameters/>
<DConditions/>
<SecurityGroups/>
<Attributes/>
<Features/>
</Record>
</Components>
<Events/>
<Attributes/>
<Features/>
</Panel>
<Panel id="4" visible="True" generateDiv="False" name="Menu" contentPlaceholder="Menu">
<Components>
<IncludePage id="30" name="MenuIncludablePage" PathID="MenuMenuIncludablePage" page="MenuIncludablePage.ccp">
					<Components/>
					<Events/>
					<Features/>
				</IncludePage>
</Components>
<Events/>
<Attributes/>
<Features/>
</Panel>
<Panel id="5" visible="True" generateDiv="False" name="Footer" contentPlaceholder="Footer">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Panel>
</Components>
	<CodeFiles>
		<CodeFile id="1" language="C#InMotion" name="project_grid.aspx" forShow="True" url="project_grid.aspx" comment="&lt;!--" commentEnd="--&gt;" codePage="windows-1252"/>
<CodeFile id="1.cs" language="C#InMotion" name="project_gridEvents.aspx.cs" forShow="False" comment="//" codePage="windows-1252"/>
</CodeFiles>
	<SecurityGroups/>
<CachingParameters/>
<Attributes/>
<Features/>
<Events/>
</Page>
