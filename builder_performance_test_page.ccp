<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" validateRequest="True" scriptingSupport="Automatic" cachingDuration="1 minutes" useDesign="True" masterPage="{CCS_PathToMasterPage}/MasterPage.ccp" wizardTheme="None" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<Panel id="2" visible="True" generateDiv="False" name="Head" contentPlaceholder="Head">
			<Components/>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="3" visible="True" generateDiv="False" name="Content" contentPlaceholder="Content">
			<Components>
				<Grid id="6" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" name="customers" connection="IntranetDB" dataSource="customers" orderBy="customer_name" pageSizeLimit="100" pageSize="False" wizardCaption="List of Customers " wizardThemeApplyTo="Page" wizardGridType="Custom" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="No records" wizardGridPagingType="Centered" wizardUseSearch="True" wizardAddNbsp="False" gridTotalRecords="False" wizardAddPanels="False" wizardType="Grid" wizardUseInterVariables="False" templatePage="D:/Users/CBoucher.CSUS2100/Documents/CodeChargeStudio5/Projects/BootstrapDesign/Templates/Grid/BootstrapTabular.ccp|userTemplate" addTemplatePanel="False" changedCaptionGrid="False" gridExtendedHTML="False" PathID="Contentcustomers" composition="4" isParent="True">
					<Components>
						<Sorter id="14" visible="True" name="Sorter_customer_name" column="customer_name" wizardCaption="Customer Name" wizardSortingType="SimpleDir" wizardControl="customer_name" wizardAddNbsp="False" PathID="ContentcustomersSorter_customer_name">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="15" visible="True" name="Sorter_customer_phone" column="customer_phone" wizardCaption="Customer Phone" wizardSortingType="SimpleDir" wizardControl="customer_phone" wizardAddNbsp="False" PathID="ContentcustomersSorter_customer_phone">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="16" visible="True" name="Sorter_customer_address" column="customer_address" wizardCaption="Customer Address" wizardSortingType="SimpleDir" wizardControl="customer_address" wizardAddNbsp="False" PathID="ContentcustomersSorter_customer_address">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="17" visible="True" name="Sorter_customer_city" column="customer_city" wizardCaption="Customer City" wizardSortingType="SimpleDir" wizardControl="customer_city" wizardAddNbsp="False" PathID="ContentcustomersSorter_customer_city">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="18" visible="True" name="Sorter_customer_state" column="customer_state" wizardCaption="Customer State" wizardSortingType="SimpleDir" wizardControl="customer_state" wizardAddNbsp="False" PathID="ContentcustomersSorter_customer_state">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="19" visible="True" name="Sorter_customer_zip" column="customer_zip" wizardCaption="Customer Zip" wizardSortingType="SimpleDir" wizardControl="customer_zip" wizardAddNbsp="False" PathID="ContentcustomersSorter_customer_zip">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Link id="20" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" preserveParameters="GET" name="customer_name" fieldSource="customer_name" wizardCaption="Customer Name" wizardIsPassword="False" wizardUseTemplateBlock="False" hrefSource="customers_maint.ccp" linkProperties="{'textSource':'','textSourceDB':'customer_name','hrefSource':'customers_maint.ccp','hrefSourceDB':'customer_name','title':'','target':'','name':'','linkParameters':{'0':{'sourceType':'DataField','parameterSource':'customer_id','parameterName':'customer_id'},'length':1,'objectType':'linkParameters'}}" wizardAddNbsp="False" PathID="Contentcustomerscustomer_name" urlType="Relative">
							<Components/>
							<Events/>
							<LinkParameters>
<LinkParameter id="38" sourceType="DataField" name="customer_id" source="customer_id"/>
</LinkParameters>
							<Attributes/>
							<Features/>
						</Link>
						<Label id="21" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_phone" fieldSource="customer_phone" wizardCaption="Customer Phone" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentcustomerscustomer_phone">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="22" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_address" fieldSource="customer_address" wizardCaption="Customer Address" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentcustomerscustomer_address">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="23" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_city" fieldSource="customer_city" wizardCaption="Customer City" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentcustomerscustomer_city">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="24" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_state" fieldSource="customer_state" wizardCaption="Customer State" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentcustomerscustomer_state">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="25" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_zip" fieldSource="customer_zip" wizardCaption="Customer Zip" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentcustomerscustomer_zip">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Navigator id="26" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="of" wizardPageSize="False" wizardImagesScheme="None">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Navigator>
					</Components>
					<Events/>
					<TableParameters>
						<TableParameter id="8" conditionType="Parameter" useIsNull="False" field="customer_name" parameterSource="s_customer_name" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="1" searchFormParameter="True"/>
						<TableParameter id="9" conditionType="Parameter" useIsNull="False" field="customer_phone" parameterSource="s_customer_phone" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="2" searchFormParameter="True"/>
						<TableParameter id="10" conditionType="Parameter" useIsNull="False" field="customer_address" parameterSource="s_customer_address" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="3" searchFormParameter="True"/>
						<TableParameter id="11" conditionType="Parameter" useIsNull="False" field="customer_city" parameterSource="s_customer_city" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="4" searchFormParameter="True"/>
						<TableParameter id="12" conditionType="Parameter" useIsNull="False" field="customer_state" parameterSource="s_customer_state" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="5" searchFormParameter="True"/>
						<TableParameter id="13" conditionType="Parameter" useIsNull="False" field="customer_zip" parameterSource="s_customer_zip" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="6" searchFormParameter="True"/>
					</TableParameters>
					<JoinTables>
						<JoinTable id="7" tableName="customers"/>
					</JoinTables>
					<JoinLinks/>
					<Fields/>
					<PKFields/>
					<SPParameters/>
					<SQLParameters/>
					<SecurityGroups/>
					<Attributes/>
					<Features/>
				</Grid>
				<Record id="27" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="customersSearch" searchIds="27" fictitiousConnection="IntranetDB" wizardCaption="Search  " wizardOrientation="Custom" wizardFormMethod="post" gridSearchClearLink="True" wizardTypeComponent="Search" gridSearchType="And" wizardInteractiveSearch="False" gridSearchRecPerPage="True" wizardTypeButtons="button" wizardDefaultButton="True" gridSearchSortField="False" wizardUseInterVariables="False" templatePageSearch="D:/Users/CBoucher.CSUS2100/Documents/CodeChargeStudio5/Projects/BootstrapDesign/Templates/Search/BootstrapSearch.ccp|userTemplate" wizardThemeApplyTo="Page" addTemplatePanel="False" wizardType="Grid" returnPage="builder_performance_test_page.ccp" PathID="ContentcustomersSearch" composition="4">
					<Components>
						<Link id="28" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="ClearParameters" hrefSource="builder_performance_test_page.ccp" removeParameters="s_customer_name;s_customer_phone;s_customer_address;s_customer_city;s_customer_state;s_customer_zip;customersPageSize" wizardThemeItem="SorterLink" wizardDefaultValue="Clear" PathID="ContentcustomersSearchClearParameters">
							<Components/>
							<Events/>
							<LinkParameters/>
							<Attributes/>
							<Features/>
						</Link>
						<Button id="29" urlType="Relative" enableValidation="True" isDefault="True" name="Button_DoSearch" operation="Search" wizardCaption="Search" PathID="ContentcustomersSearchButton_DoSearch">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Button>
						<TextBox id="30" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_customer_name" fieldSource="customer_name" wizardIsPassword="False" wizardCaption="Customer Name" caption="Customer Name" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentcustomersSearchs_customer_name">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="31" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_customer_phone" fieldSource="customer_phone" wizardIsPassword="False" wizardCaption="Customer Phone" caption="Customer Phone" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentcustomersSearchs_customer_phone">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="32" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_customer_address" fieldSource="customer_address" wizardIsPassword="False" wizardCaption="Customer Address" caption="Customer Address" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentcustomersSearchs_customer_address">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="33" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_customer_city" fieldSource="customer_city" wizardIsPassword="False" wizardCaption="Customer City" caption="Customer City" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentcustomersSearchs_customer_city">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="34" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_customer_state" fieldSource="customer_state" wizardIsPassword="False" wizardCaption="Customer State" caption="Customer State" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentcustomersSearchs_customer_state">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="35" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_customer_zip" fieldSource="customer_zip" wizardIsPassword="False" wizardCaption="Customer Zip" caption="Customer Zip" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentcustomersSearchs_customer_zip">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<ListBox id="36" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" returnValueType="Number" name="customersPageSize" dataSource=";Select Value;5;5;10;10;25;25;100;100" wizardCaption="Records per page" wizardNoEmptyValue="True" PathID="ContentcustomersSearchcustomersPageSize">
							<Components/>
							<Events/>
							<TableParameters/>
							<SPParameters/>
							<SQLParameters/>
							<JoinTables/>
							<JoinLinks/>
							<Fields/>
							<PKFields/>
							<Attributes/>
							<Features/>
						</ListBox>
					</Components>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<PKFields/>
					<ISPParameters/>
					<ISQLParameters/>
					<IFormElements/>
					<USPParameters/>
					<USQLParameters/>
					<UConditions/>
					<UFormElements/>
					<DSPParameters/>
					<DSQLParameters/>
					<DConditions/>
					<SecurityGroups/>
					<Attributes/>
					<Features/>
				</Record>
			</Components>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="4" visible="True" generateDiv="False" name="Menu" contentPlaceholder="Menu">
			<Components>
<IncludePage id="37" name="MenuIncludablePage" PathID="MenuMenuIncludablePage" page="MenuIncludablePage.ccp">
<Components/>
<Events/>
<Features/>
</IncludePage>
</Components>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="5" visible="True" generateDiv="False" name="Footer" contentPlaceholder="Footer">
			<Components/>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
	</Components>
	<CodeFiles>
		<CodeFile id="1" language="C#InMotion" name="builder_performance_test_page.aspx" forShow="True" url="builder_performance_test_page.aspx" comment="&lt;!--" commentEnd="--&gt;" codePage="windows-1252"/>
		<CodeFile id="1.cs" language="C#InMotion" name="builder_performance_test_pageEvents.aspx.cs" forShow="False" comment="//" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
