<!--ASPX page header @1-5EA05C09-->
<%@ Page language="C#" CodeFile="employee_rptEvents.aspx.cs" Inherits="BootstrapDesign.employee_rpt_Page" MasterPageFile="~/Designs/Bootstrap/MasterPage.master"  %>
<%@ Register TagPrefix="BootstrapDesign" TagName="MenuIncludablePage" Src="MenuIncludablePage.ascx" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASPX page header-->

<!--ASPX page @1-4584CCD2-->

<asp:Content ContentPlaceHolderID="Head" ID="Head_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Head" runat="server">
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-65835CB2
</script>
<script src="ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>" type="text/javascript" charset="windows-1252"></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>


<title>employee_rpt</title>

<meta content="text/html; charset=windows-1252" http-equiv="Content-Type">
</mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Content" ID="Content_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Content" runat="server">
<mt:Record ReturnPage="~/employee_rpt.aspx" OnBeforeShow="employees_departments_BeforeShow" ID="employees_departments" runat="server"><ItemTemplate><div data-emulate-form="Contentemployees_departments" id="Contentemployees_departments">
<div role="form" class="form-horizontal hidden-print">
  



    <legend>Search </legend>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <div class="row">
      <div class="col-sm-offset-2 col-sm-10">
        <div role="alert" class="alert alert-danger">
          <mt:MTLabel id="ErrorLabel" runat="server"/> 
        </div>
      </div>
    </div>
    </mt:MTPanel><fieldset>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Department</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTListBox Rows="1" Source="employees_department_id" DataType="Integer" Caption="Department Id" DataSourceID="s_employees_department_idDataSource" SortExpression="department_name" DataValueField="department_id" DataTextField="department_name" ID="s_employees_department_id" data-id="Contentemployees_departmentss_employees_department_id" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="s_employees_department_idDataSource" ConnectionString="InMotion:IntranetDB" SelectCommandType="Table" SelectOrderBy="department_name" runat="server">
           <SelectCommand>
SELECT * 
FROM departments {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 
      </div>
      <div class="col-sm-2 control-label">
        <label>Emp Name</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="emp_name" Caption="Emp Name" maxlength="50" Columns="50" ID="s_emp_name" data-id="Contentemployees_departmentss_emp_name" runat="server"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label for="<%# employees_departments.GetControl<InMotion.Web.Controls.MTListBox>("departments_employeesPageSize").ClientID %>">Records per page</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTListBox Rows="1" ID="departments_employeesPageSize" data-id="Contentemployees_departmentsdepartments_employeesPageSize" runat="server">
          <asp:ListItem Value="" Text="Select Value"/>
          <asp:ListItem Value="5" Text="5"/>
          <asp:ListItem Value="10" Text="10"/>
          <asp:ListItem Value="25" Text="25"/>
          <asp:ListItem Value="100" Text="100"/>
        </mt:MTListBox>
 
      </div>
    </div>
    </fieldset> 
    <div class="form-group">
      <div class="col-sm-2 text-right">
<mt:MTLink Text="Clear" ID="ClearParameters" data-id="Contentemployees_departmentsClearParameters" runat="server" HrefSource="~/employee_rpt.aspx" PreserveParameters="Get" RemoveParameters="s_employees_department_id;s_emp_name;departments_employeesPageSize"/> 
      </div>
      <div class="col-sm-10">
        <mt:MTButton CommandName="Search" Text="Search" CssClass="Button" ID="Button_DoSearch" data-id="Contentemployees_departmentsButton_DoSearch" runat="server"/>
      </div>
    </div>
  

</div>

</div></ItemTemplate></mt:Record>
<mt:Report WebPageSize="10" PageSizeLimit="100" SortExpression="emp_name" DataSourceID="departments_employeesDataSource" ID="departments_employees" runat="server">
<Groups>
<mt:ReportGroup Name="department_name" Field="department_name" SqlField="departments.department_name" SortOrder="asc"/>
		</Groups>
<LayoutHeaderTemplate>
<div class="table-responsive small">
  <table class="table table-bordered table-hover table-condensed">
    </LayoutHeaderTemplate>

<Section Name="Report_Header" Height="0"><Template></Template></Section>
<Section Name="Page_Header" Height="1"><Template>
    <thead style="DISPLAY: table-header-group">
      <tr class="text-primary">
        <td colspan="7">
          <h3><strong>Employee By Department Report</strong></h3>
        </td>
      </tr>
 
      <tr class="Active">
        <th scope="col">Department</th>
 
        <th scope="col">
        <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="" DescOnAlt="" SortOrder="emp_name" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Name" ID="Sorter_emp_name" runat="server"/></th>
 
        <th scope="col">
        <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="" DescOnAlt="" SortOrder="emp_login" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Login" ID="Sorter_emp_login" runat="server"/>&nbsp;</th>
 
        <th scope="col">
        <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="" DescOnAlt="" SortOrder="title" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Title" ID="Sorter_title" runat="server"/>&nbsp;</th>
 
        <th scope="col">
        <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="" DescOnAlt="" SortOrder="email" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Email" ID="Sorter_email" runat="server"/>&nbsp;</th>
 
        <th scope="col">
        <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="" DescOnAlt="" SortOrder="phone_work" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Work Phone" ID="Sorter_phone_work" runat="server"/>&nbsp;</th>
 
        <th scope="col">
        <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="" DescOnAlt="" SortOrder="employee_is_active" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Is Active" ID="Sorter_employee_is_active" runat="server"/>&nbsp;</th>
      </tr>
 
    </thead>
 </Template></Section>
<Section Name="Page_Footer" Height="1"><Template>
    <tfoot style="DISPLAY: table-footer-group">
      <tr class="Footer">
        <td colspan="2"><mt:MTReportLabel SourceType="SpecialValue" Source="CurrentDateTime" DataType="Date" ID="Report_CurrentDateTime" runat="server"/></td> 
        <td colspan="5">
          <mt:MTNavigator FirstOnValue="First " FirstOffValue="First " PreviousOnValue="Prev " PreviousOffValue="Prev " NextOnValue="Next " NextOffValue="Next " LastOnValue="Last " LastOffValue="Last " PageOffPreValue="" PageOffPostValue="&amp;nbsp;" DisplayStyle="Links" PageLinksNumber="10" PageNumbersStyle="CurrentPageCentered" TotalPagesText="of" ShowTotalPages="true" ID="Navigator" runat="server"/></td>
      </tr>
    </tfoot>
 </Template></Section>
<Section Name="department_name_Header" Height="1"><Template>
    <tr class="GroupCaption">
      <th colspan="7" scope="col"><mt:MTReportLabel Source="department_name" ID="department_name" runat="server"/> </th>
    </tr>
 </Template></Section>
<Section Name="Detail" Height="1"><Template>
    <tr class="Row">
      <td>&nbsp; </td> 
      <td><mt:MTReportLabel Source="emp_name" ContentType="Html" ID="emp_name" runat="server"/> </td> 
      <td><mt:MTReportLabel Source="emp_login" ContentType="Html" ID="emp_login" runat="server"/> </td> 
      <td><mt:MTReportLabel Source="title" ContentType="Html" ID="title" runat="server"/> </td> 
      <td><mt:MTReportLabel Source="email" ContentType="Html" ID="email" runat="server"/> </td> 
      <td><mt:MTReportLabel Source="phone_work" ContentType="Html" ID="phone_work" runat="server"/> </td> 
      <td><mt:MTReportLabel Source="employee_is_active" DataType="Boolean" ContentType="Html" ID="employee_is_active" runat="server"/> </td>
    </tr>
 </Template></Section>
<Section Name="department_name_Footer" Height="1"><Template>
    <tr class="SubTotal info">
      <td>Sub Total &nbsp; </td> 
      <td style="TEXT-ALIGN: right" valign="baseline">Count: <mt:MTReportLabel Function="Count" ResetAt="department_name" DataType="Integer" ID="Count_emp_name" runat="server"/> </td> 
      <td><%= Variables["ReportCaption_emp_login"]%> </td> 
      <td><%= Variables["ReportCaption_title"]%> </td> 
      <td><%= Variables["ReportCaption_email"]%> </td> 
      <td><%= Variables["ReportCaption_phone_work"]%> </td> 
      <td><%= Variables["ReportCaption_employee_is_active"]%> </td>
    </tr>
 </Template></Section>
<Section Name="Report_Footer" Height="1"><Template>
    <mt:MTPanel GenerateDiv="false" ID="NoRecords" runat="server">
    <tr class="NoRecords danger">
      <td colspan="7">No records</td>
    </tr>
 </mt:MTPanel>
    <tr class="Total info">
      <td valign="baseline">Grand Total </td> 
      <td style="TEXT-ALIGN: right" valign="baseline">Count: <mt:MTReportLabel Function="Count" ResetAt="Report" DataType="Integer" ID="TotalCount_emp_name" runat="server"/> </td> 
      <td><%= Variables["ReportCaption_emp_login"]%> </td> 
      <td><%= Variables["ReportCaption_title"]%> </td> 
      <td><%= Variables["ReportCaption_email"]%> </td> 
      <td><%= Variables["ReportCaption_phone_work"]%> </td> 
      <td><%= Variables["ReportCaption_employee_is_active"]%> </td>
    </tr>
 </Template></Section>
<LayoutFooterTemplate>
  </table>
</div>
</LayoutFooterTemplate>
</mt:Report>
<mt:MTDataSource ID="departments_employeesDataSource" ConnectionString="InMotion:IntranetDB" SelectCommandType="Table" SelectOrderBy="emp_name" runat="server">
   <SelectCommand>
SELECT * 
FROM departments INNER JOIN employees ON
departments.department_id = employees.department_id {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <SelectParameters>
     <mt:WhereParameter Name="Urls_employees_department_id" SourceType="URL" Source="s_employees_department_id" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="employees.department_id"/>
     <mt:WhereParameter Name="Urls_emp_name" SourceType="URL" Source="s_emp_name" DataType="Text" Operation="And" Condition="Contains" SourceColumn="emp_name"/>
   </SelectParameters>
</mt:MTDataSource></mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Menu" ID="Menu_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Menu" runat="server"><BootstrapDesign:MenuIncludablePage ID="MenuIncludablePage" runat="server"/></mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Footer" ID="Footer_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Footer" runat="server">&nbsp;</mt:MTPanel></asp:Content>
<!--End ASPX page-->

