<Page id="1" templateExtension="html" relativePath="..\.." fullRelativePath=".\Designs\Bootstrap" secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" cachingDuration="1 minutes" needGeneration="0" validateRequest="True" scriptingSupport="Automatic">
	<Components>
		<ContentPlaceholder id="2" name="Head" wizardTheme="None" wizardThemeType="File" wizardThemeVersion="3.0" PathID="Head">
			<Components/>
			<Events/>
			<Features/>
		</ContentPlaceholder>
		<ContentPlaceholder id="3" name="Content" wizardTheme="None" wizardThemeType="File" wizardThemeVersion="3.0" PathID="Content">
			<Components/>
			<Events/>
			<Features/>
		</ContentPlaceholder>
		<ContentPlaceholder id="4" name="Menu" wizardTheme="None" wizardThemeType="File" wizardThemeVersion="3.0" PathID="Menu">
			<Components/>
			<Events/>
			<Features/>
		</ContentPlaceholder>
		<ContentPlaceholder id="5" name="Footer" wizardTheme="None" wizardThemeType="File" wizardThemeVersion="3.0" PathID="Footer">
			<Components/>
			<Events/>
			<Features/>
		</ContentPlaceholder>
	</Components>
	<CodeFiles>
		<CodeFile id="1" language="C#InMotion" name="MasterPage.master" forShow="True" url="MasterPage.master" comment="&lt;!--" commentEnd="--&gt;" codePage="windows-1252"/>
		<CodeFile id="1.cs" language="C#InMotion" name="MasterPageEvents.master.cs" forShow="False" comment="//" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
