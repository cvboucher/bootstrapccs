

<!--ASPX page header @1-7473A07E-->
<%@ Control language="C#" CodeFile="MasterPageEvents.ascx.cs" Inherits="BootstrapDesign.Designs.Bootstrap.MasterPage_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASPX page header-->

<!--ASPX page @1-DD2390DA-->
<html>
<head>
<title></title>
<link rel="stylesheet" type="text/css" href="<mt:MTPanel runat=server><%= PathToCurrentPage %></mt:MTPanel>jquery-ui.css" media="screen">
<mt:MTPanel ID="___head_link_panel" runat="server"></mt:MTPanel>
</head>
<body>
<form id="Form1" runat="server" data-need-form-emulation="data-need-form-emulation">

﻿<meta http-equiv="X-UA-Compatible" content="IE=edge"><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1">




<!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
<!-- <link rel="stylesheet" href="//apps.csustan.edu/OITCommon/css/bootstrap-theme-csustan-apps.css"> -->

<!-- <link rel="shortcut icon" href="//www.csustan.edu/sites/default/files/favicon.ico"> -->
<link rel="shortcut icon" href="//getbootstrap.com/favicon.ico">
<mt:MTPanel ID="Head_Main" runat="server" />
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-37CF177B
</script>
<script src="../../ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>" type="text/javascript" charset="windows-1252"></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>
 
<mt:MTPanel ID="Menu_Main" runat="server" /> 
<div class="container">
  <div class="row" style="MARGIN-TOP: 20px">
    <div class="col-sm-12">
      <mt:MTPanel ID="Content_Main" runat="server" /> 
    </div>
  </div>
</div>
<footer class="footer"><mt:MTPanel ID="Footer_Main" runat="server" /></footer>
<script src="<mt:MTPanel runat=server><%= PathToCurrentPage %></mt:MTPanel>jquery.js"></script>
<script src="<mt:MTPanel runat=server><%= PathToCurrentPage %></mt:MTPanel>custom.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

</form>
</body>
</html>

<!--End ASPX page-->

