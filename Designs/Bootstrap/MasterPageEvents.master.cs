//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-C1668ADF
namespace BootstrapDesign.Designs.Bootstrap
{
//End Namespace

//Page class @1-3CCAC69A
public partial class MasterPage_Page : MTMasterPage
{
//End Page class

//Attributes constants @1-B6B554B5
    public const string Attribute_pathToCurrentPage = "pathToCurrentPage";
//End Attributes constants

//Page MasterPage Event Load @1-D9372652
    protected void Page_Load(object sender, EventArgs e) {
//End Page MasterPage Event Load

//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();
//End DataBind

//Page MasterPage Load event tail @1-FCB6E20C
    }
//End Page MasterPage Load event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

