$().ready(function() {
        $('input[type=text]').addClass('form-control');
        $('select').addClass('form-control');
        $('input[type=submit]').removeClass('Button');
        $('input[type=submit]').addClass('btn');
        $("input[type=submit][value='Add']").addClass('btn-success');
        $("input[type=submit][value='Submit']").addClass('btn-success');
        $("input[type=submit][value='Delete']").addClass('btn-danger');
        $("input[type=submit][value='Cancel']").addClass('btn-warning');
        $("input[type=submit][value='Search']").addClass('btn-primary');
});