//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-B50C8C34
namespace BootstrapDesign
{
//End Namespace

//Page class @1-2B0B1467
public partial class employees_maint_Page : MTPage
{
//End Page class

//Page employees_maint Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page employees_maint Event Init

//Enable Scripting Support @1-C2BE9EBD
        this.ScriptingSupport = true;
//End Enable Scripting Support

//Page employees_maint Init event tail @1-FCB6E20C
    }
//End Page employees_maint Init event tail

//Page employees_maint Event Load @1-D9372652
    protected void Page_Load(object sender, EventArgs e) {
//End Page employees_maint Event Load

//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();
//End DataBind

//Page employees_maint Load event tail @1-FCB6E20C
    }
//End Page employees_maint Load event tail

//Page employees_maint Event On PreInit @1-6204FEF7
    protected void Page_PreInit(object sender, EventArgs e) {
//End Page employees_maint Event On PreInit

//MasterPageInit @1-25245C17
        this.DesignMasterPagePath = "{CCS_PathToMasterPage}/MasterPage.master";
//End MasterPageInit

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page employees_maint On PreInit event tail @1-FCB6E20C
    }
//End Page employees_maint On PreInit event tail

//CheckBox employee_is_active Event Load @53-7C1C9CE0
    protected void employeesemployee_is_active_Load(object sender, EventArgs e) {
//End CheckBox employee_is_active Event Load

//Set Default Value @53-083A3319
        ((InMotion.Web.Controls.MTCheckBox)sender).DefaultValue = ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue;
//End Set Default Value

//CheckBox employee_is_active Load event tail @53-FCB6E20C
    }
//End CheckBox employee_is_active Load event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

