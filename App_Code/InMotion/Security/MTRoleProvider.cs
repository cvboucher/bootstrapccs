//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using System.Web.Security;
using System.Configuration.Provider;
using System.Diagnostics;
using InMotion.Data;
using InMotion.Common;
using InMotion.Configuration;

namespace InMotion.Security
{
    /// <summary>
    /// Manages storage of role membership information for an InMotion application in a database. 
    /// </summary>
    public class MTRoleProvider : RoleProvider
    {
        private string connName;
        private string tableName;
        private string userLoginField;
        private string userGroupField;
        private bool higherLevel;

        /// <summary>
        /// Initializes the InMotion role provider with the property values specified in the ASP.NET application's configuration file. This method is not intended to be used directly from your code.
        /// </summary>
        /// <param name="name">The name of the <see cref="MTRoleProvider"/> instance to initialize.</param>
        /// <param name="config">A <see cref="System.Collections.Specialized.NameValueCollection"/> that contains the names and values of configuration options for the role provider.</param>
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            if (config == null)
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("The parameter 'config' can not be null.\n{0}", Environment.StackTrace);
                throw new ArgumentNullException("config");
            }

            if (name == null || name.Length == 0)
                name = "MTRoleProvider";

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "InMotion Role provider");
            }

            if (config["applicationName"] == null || config["applicationName"].Trim() == "")
            {
                pApplicationName = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
            }
            else
            {
                pApplicationName = config["applicationName"];
            }
            connName = config["connectionString"];
            tableName = config["tableName"];
            userLoginField = config["userLoginField"];
            userGroupField = config["userGroupField"];
            higherLevel = bool.Parse(config["higherLevel"]);

            base.Initialize(name, config);
        }

        private string pApplicationName;
        /// <summary>
        /// Gets or sets the name of the application for which to store and retrieve role information.
        /// </summary>
        public override string ApplicationName
        {
            get { return pApplicationName; }
            set { pApplicationName = value; }
        }

        private void CreateDataCommand(out Connection conn, out DataCommand dc)
        {
            conn = AppConfig.GetConnection(connName);
            dc = (DataCommand)conn.CreateCommand();
            dc.MTCommandType = MTCommandType.Sql;
        }

        /// <summary>
        /// Adds the specified user names to each of the specified roles.
        /// </summary>
        /// <param name="usernames">A string array of user names to be added to the specified roles.</param>
        /// <param name="rolenames">A string array of role names to add the specified user names to.</param>
        public override void AddUsersToRoles(string[] usernames, string[] rolenames)
        {
            if (rolenames.Length > 1)
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("InMotion Role provide doesn't support multiple roles.\n{0}", Environment.StackTrace);
                throw new ArgumentException("InMotion Role provide doesn't support multiple roles");
            }

            if (!RoleExists(rolenames[0]))
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("Role name not found.\n{0}", Environment.StackTrace);
                throw new ProviderException("Role name not found.");
            }


            foreach (string username in usernames)
            {
                if (username.IndexOf(',') > 0)
                {
                    if (AppConfig.IsMTErrorHandlerUse("critical"))
                        Trace.TraceError("User names cannot contain commas.\n{0}", Environment.StackTrace);
                    throw new ArgumentException("User names cannot contain commas.");
                }

                if (IsUserInRole(username, rolenames[0]))
                {
                    if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                        Trace.TraceError("User is already in role.\n{0}", Environment.StackTrace);
                    throw new ProviderException("User is already in role.");
                }
            }

            Connection conn;
            DataCommand dc;
            CreateDataCommand(out conn, out dc);
            int groupID = GetGroupID(rolenames[0]);

            DbTransaction tran = null;

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                dc.Transaction = tran;

                foreach (string username in usernames)
                {
                    dc.CommandText = String.Format("UPDATE {0} SET {1}={2} WHERE {3}={4}",
                    tableName,
                    userGroupField,
                    conn.EncodeSqlValue(groupID.ToString(), DataType.Integer, true),
                    userLoginField,
                    conn.EncodeSqlValue(username, DataType.Text, true));
                }

                tran.Commit();
            }
            catch (DbException e)
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError(e.ToString());
                tran.Rollback();
                throw;
            }
            catch (Exception e)
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError(e.ToString());
            }
            finally
            {
                conn.Close();
            }
        }

        private static int GetGroupID(string rolename)
        {
            int groupID = 0;
            foreach (KeyValuePair<int, SecurityGroup> sg in AppConfig.SecurityGroups)
                if (sg.Value.Name == rolename)
                    groupID = sg.Value.Id;
            return groupID;
        }

        /// <summary>
        /// Gets a value indicating whether the specified user is in the specified role.
        /// </summary>
        /// <param name="username">The user name to search for.</param>
        /// <param name="roleName">The role to search in.</param>
        /// <returns>true if the specified user name is in the specified role; otherwise, false.</returns>
        public override bool IsUserInRole(string username, string roleName)
        {
            if (!RoleExists(roleName))
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("Role name not found.\n{0}", Environment.StackTrace);
                throw new ProviderException("Role name not found.");
            }
            Connection conn;
            DataCommand dc;
            CreateDataCommand(out conn, out dc);
            dc.CommandText = String.Format("SELECT {1} FROM {0} WHERE {2}={1}",
                    tableName,
                    userGroupField,
                    userLoginField,
                    conn.EncodeSqlValue(username, DataType.Text, true));
            int result;
            conn.Open();
            result = Convert.ToInt16(dc.ExecuteScalar());
            conn.Close();
            int id = GetGroupID(roleName);
            if (id == result || higherLevel && result > id) return true;
            return false;
        }

        /// <summary>
        /// Gets a list of the roles that a user is in.
        /// </summary>
        /// <param name="username">The user to return a list of roles for.</param>
        /// <returns>A string array containing the names of all the roles that the specified user is in.</returns>
        public override string[] GetRolesForUser(string username)
        {
            Connection conn;
            DataCommand dc;
            CreateDataCommand(out conn, out dc);
            dc.CommandText = String.Format("SELECT {1} FROM {0} WHERE {2}={3}",
                    tableName,
                    userGroupField,
                    userLoginField,
                    conn.EncodeSqlValue(username, DataType.Text, true));

            conn.Open();
						long groupId = 0;
						string dbValue = dc.ExecuteScalar().ToString();
						if (!long.TryParse(dbValue, out groupId))
							groupId = 0;
            conn.Close();

            List<string> roles = new List<string>();
            foreach (KeyValuePair<int, SecurityGroup> item in AppConfig.SecurityGroups)
            {
                SecurityGroup sg = item.Value;
                MTBoolean isUserRole = higherLevel ? (MTBoolean)(sg.Id <= groupId) : (MTBoolean)(sg.Id == groupId);
                if (isUserRole) roles.Add(sg.Name);
            }
            string[] result = new string[roles.Count];
            roles.CopyTo(result);
            return result;
        }

        /// <summary>
        /// Adds a new role to the data source for the configured applicationName.
        /// </summary>
        /// <param name="roleName">The name of the role to create</param>
        public override void CreateRole(string roleName)
        {
            if (AppConfig.IsMTErrorHandlerUse("all"))
                Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Removes a role from the data source for the configured applicationName.
        /// </summary>
        /// <param name="roleName">The name of the role to delete.</param>
        /// <param name="throwOnPopulatedRole">If true, throw an exception if roleName has one or more members and do not delete roleName.</param>
        /// <returns>true if the role was successfully deleted; otherwise, false.</returns>
        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            if (AppConfig.IsMTErrorHandlerUse("all"))
                Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Gets a value indicating whether the specified role name already exists.
        /// </summary>
        /// <param name="roleName">The name of the role to search for in the database.</param>
        /// <returns>true if the role name already exists in the database; otherwise, false.</returns>
        public override bool RoleExists(string roleName)
        {
            return GetGroupID(roleName) != 0;
        }

        /// <summary>
        /// Removes the specified user names from the specified roles for the configured applicationName.
        /// </summary>
        /// <param name="usernames">A string array of user names to be removed from the specified roles.</param>
        /// <param name="roleNames">A string array of role names to remove the specified user names from.</param>
        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            if (AppConfig.IsMTErrorHandlerUse("all"))
                Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Gets a list of users in the specified role.
        /// </summary>
        /// <param name="roleName">The name of the role to get the list of users for.</param>
        /// <returns>A string array containing the names of all the users who are members of the specified role.</returns>
        public override string[] GetUsersInRole(string roleName)
        {
            if (!RoleExists(roleName))
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("Role name not found.\n{0}", Environment.StackTrace);
                throw new ProviderException("Role name not found.");
            }

            Connection conn;
            DataCommand dc;
            CreateDataCommand(out conn, out dc);
            dc.CommandText = String.Format("SELECT {1} FROM {0} WHERE {2}={3}",
                    tableName,
                    userLoginField,
                    userGroupField,
                    conn.EncodeSqlValue(GetGroupID(roleName).ToString(), DataType.Integer, true));

            DbDataReader reader = dc.ExecuteReader();
            StringBuilder userNames = new StringBuilder();
            while (reader.Read())
            {
                userNames.Append(reader[0]);
                userNames.Append(',');
            }
            if (userNames.Length > 0)
            {
                return userNames.ToString().TrimEnd(',').Split(',');
            }

            return null;

        }

        /// <summary>
        /// Gets an array of user names in a role where the user name contains the specified user name to match.
        /// </summary>
        /// <param name="roleName">The role to search in.</param>
        /// <param name="usernameToMatch">The user name to search for.</param>
        /// <returns>A string array containing the names of all the users where the user name matches usernameToMatch and the user is a member of the specified role.</returns>
        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            if (!RoleExists(roleName))
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("Role name not found.\n{0}", Environment.StackTrace);
                throw new ProviderException("Role name not found.");
            }

            Connection conn;
            DataCommand dc;
            CreateDataCommand(out conn, out dc);
            dc.CommandText = String.Format("SELECT {1} FROM {0} WHERE {2}={3} AND {1} LIKE {4}",
                    tableName,
                    userLoginField,
                    userGroupField,
                    conn.EncodeSqlValue(GetGroupID(roleName).ToString(), DataType.Integer, true),
                    conn.EncodeSqlValue(usernameToMatch, DataType.Text, true));

            DbDataReader reader = dc.ExecuteReader();
            StringBuilder userNames = new StringBuilder();
            while (reader.Read())
            {
                userNames.Append(reader[0]);
                userNames.Append(',');
            }
            if (userNames.Length > 0)
            {
                return userNames.ToString().TrimEnd(',').Split(',');
            }

            return null;
        }

        /// <summary>
        /// Gets a list of all the roles for the application.
        /// </summary>
        /// <returns>A string array containing the names of all the roles stored in the database for a particular application.</returns>
        public override string[] GetAllRoles()
        {
            string[] result = new string[AppConfig.SecurityGroups.Count];
            int i = 0;
            foreach (KeyValuePair<int, SecurityGroup> sg in AppConfig.SecurityGroups)
            {
                result[i] = sg.Value.Name;
                i++;
            }
            return result;
        }
    }
}
