//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Security
{
    /// <summary>
    /// Encapsulate a set of access rights of InMotion form.
    /// </summary>
    [Serializable]
    public struct AccessRights
    {

        private bool _read;
        /// <summary>
        /// Gets or sets the value that indicating whether user have Read permission for a form.
        /// </summary>
        public bool Read
        {
            get { return _read; }
            set { _read = value; }
        }
        private bool _insert;

        /// <summary>
        /// Gets or sets the value that indicating whether user have Insert permission for a form.
        /// </summary>
        public bool Insert
        {
            get { return _insert; }
            set { _insert = value; }
        }
        private bool _update;

        /// <summary>
        /// Gets or sets the value that indicating whether user have Update permission for a form.
        /// </summary>
        public bool Update
        {
            get { return _update; }
            set { _update = value; }
        }
        private bool _delete;

        /// <summary>
        /// Gets or sets the value that indicating whether user have Delete permission for a form.
        /// </summary>
        public bool Delete
        {
            get { return _delete; }
            set { _delete = value; }
        }

        /// <summary>
        /// Initialize the new instance of the <see cref="AccessRights"/> class with given Read permission.
        /// </summary>
        /// <param name="read">The value of Read permission.</param>
        public AccessRights(bool read)
            : this(read, false, false, false)
        {
        }
        /// <summary>
        /// Initialize the new instance of the <see cref="AccessRights"/> class with given Read, Insert, Update and Delete permissions.
        /// </summary>
        /// <param name="read">The value of Read permission.</param>
        /// <param name="insert">The value of Insert permission.</param>
        /// <param name="update">The value of Update permission.</param>
        /// <param name="delete">The value of Delete permission.</param>
        public AccessRights(bool read, bool insert, bool update, bool delete)
        {
            _read = read;
            _insert = insert;
            _update = update;
            _delete = delete;
        }
    }
}
