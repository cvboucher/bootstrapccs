//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using InMotion.Configuration;

namespace InMotion.Security
{
    /// <summary>
    /// Represent a security group.
    /// </summary>
    public class SecurityGroup
    {
        /// <summary>
        /// Initialize a new instance of the <see cref="SecurityGroup"/> class with given id and name.
        /// </summary>
        /// <param name="id">The id of the new group.</param>
        /// <param name="name">The name of the new group.</param>
        public SecurityGroup(int id, string name)
        {
            Id = id;
            Name = name;
        }
        private int _groupId;

        /// <summary>
        /// Gets or sets the Id of the security group.
        /// </summary>
        public int Id
        {
            get { return _groupId; }
            set { _groupId = value; }
        }
        private string _groupName;

        /// <summary>
        /// Gets or sets the name of the security group.
        /// </summary>
        public string Name
        {
            get { return _groupName; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    if (AppConfig.IsMTErrorHandlerUse("critical"))
                        Trace.TraceError("The group name can not be null or empty.\n{0}", Environment.StackTrace);
                    throw new ArgumentException("The group name can not be null or empty", "name");
                }
                _groupName = value;
            }
        }
    }
}
