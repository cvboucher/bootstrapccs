//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace InMotion.Security
{
    /// <summary>
    /// Provides the helper methods and base information about security configuration.
    /// </summary>
    public class Utility
    {
        /// <summary>
        /// Gets or set the ID of current user from Session.
        /// </summary>
        public static string UserId
        {
            get
            {
                if (HttpContext.Current.Session[InMotion.Configuration.AppConfig.UserIdSessionVariableName] == null)
                    return "";
                return HttpContext.Current.Session[InMotion.Configuration.AppConfig.UserIdSessionVariableName].ToString();
            }
            set
            {
                HttpContext.Current.Session[InMotion.Configuration.AppConfig.UserIdSessionVariableName] = value;
            }
        }

        /// <summary>
        /// Gets or set the login of current user from Session.
        /// </summary>
        public static string UserLogin
        {
            get
            {
                if (HttpContext.Current.Session[InMotion.Configuration.AppConfig.UserLoginSessionVariableName] == null)
                    return "";
                return HttpContext.Current.Session[InMotion.Configuration.AppConfig.UserLoginSessionVariableName].ToString();
            }
            set
            {
                HttpContext.Current.Session[InMotion.Configuration.AppConfig.UserLoginSessionVariableName] = value;
            }
        }

        /// <summary>
        /// Gets or set the group's id of current user from Session;
        /// </summary>
        public static string UserGroupId
        {
            get
            {
                if (HttpContext.Current.Session[InMotion.Configuration.AppConfig.UserGroupSessionVariableName] == null)
                    return "";
                return HttpContext.Current.Session[InMotion.Configuration.AppConfig.UserGroupSessionVariableName].ToString();
            }
            set
            {
                HttpContext.Current.Session[InMotion.Configuration.AppConfig.UserGroupSessionVariableName] = value;
            }
        }

        private static bool _needAutologin = true;

        public static bool NeedAutologin
        {
            get
            {
                return _needAutologin;
            }
            set
            {
                _needAutologin = value;
            }
        }
    }
}