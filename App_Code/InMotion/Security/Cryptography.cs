//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Globalization;

namespace InMotion.Security
{
    /// <summary>
    /// Encapsulate methods which implements RC4 cryptographic algorithm.
    /// </summary>
    public class Cryptography
    {
        private static byte[] GetRC4(byte[] pStrMessage, string key)
        {
            byte[] lBytAsciiAry = new byte[256];
            byte[] lBytKeyAry = new byte[256];
            int lLngIndex;

            // Validate data
            if (key.Length == 0) return null;
            if (pStrMessage.Length == 0) return null;

            // transfer repeated key to array
            int lLngKeyLength = key.Length;
            for (lLngIndex = 0; lLngIndex <= 255; lLngIndex++)
                lBytKeyAry[lLngIndex] = System.Text.Encoding.ASCII.GetBytes(key.Substring(lLngIndex % lLngKeyLength, 1))[0];

            // Initialize S
            for (lLngIndex = 0; lLngIndex <= 255; lLngIndex++)
                lBytAsciiAry[lLngIndex] = (byte)lLngIndex;

            // Switch values of S arround based off of index and Key value
            byte lBytTemp, lBytJump = 0;
            for (lLngIndex = 0; lLngIndex <= 255; lLngIndex++)
            {
                // Figure index to switch
                lBytJump = Convert.ToByte((lBytJump + lBytAsciiAry[lLngIndex] + lBytKeyAry[lLngIndex]) % 256);

                // Do the switch
                lBytTemp = lBytAsciiAry[lLngIndex];
                lBytAsciiAry[lLngIndex] = lBytAsciiAry[lBytJump];
                lBytAsciiAry[lBytJump] = lBytTemp;
            }

            lLngIndex = 0;
            lBytJump = 0;
            byte[] result = new byte[pStrMessage.Length];
            for (byte lLngX = 0; lLngX < pStrMessage.Length; lLngX++)
            {
                lLngIndex = Convert.ToByte((lLngIndex + 1) % 256); // wrap index;
                lBytJump = Convert.ToByte((lBytJump + lBytAsciiAry[lLngIndex]) % 256); // wrap J+S();
                // Add/Wrap those two
                byte lLngT = Convert.ToByte((lBytAsciiAry[lLngIndex] + lBytAsciiAry[lBytJump]) % 256);
                // Switcheroo
                lBytTemp = lBytAsciiAry[lLngIndex];
                lBytAsciiAry[lLngIndex] = lBytAsciiAry[lBytJump];
                lBytAsciiAry[lBytJump] = lBytTemp;

                byte lBytY = lBytAsciiAry[lLngT];
                // Character Encryption ...
                result[lLngX] = Convert.ToByte(pStrMessage[lLngX] ^ lBytY);
            }
            return result;
        }

        private static string to_hex(int n)
        {
            if (n < 16)
                return "0" + String.Format("{0:X}", n);
            else
                return String.Format("{0:X}", n);
        }

        private static string to_hex(string n)
        {
            string result = "";
            for (int i = 0; i < n.Length; i++)
                result += to_hex(System.Text.Encoding.ASCII.GetBytes(n.Substring(i, 1))[0]);
            return result;
        }

        private static string to_hex(byte[] n)
        {
            string result = "";
            for (int i = 0; i < n.Length; i++)
            {
                string val = String.Format("{0:X}", n[i]);
                if (val.Length == 1) val = "0" + val;
                result += val;
            }
            return result;
        }

        private static byte[] from_hex(string n)
        {
            int size = n.Length / 2 + n.Length % 2;
            byte[] result = new byte[size];
            for (int i = 0; i < n.Length; i += 2)
            {
                result[i / 2] = Byte.Parse(n[i] + "0", NumberStyles.HexNumber);
                if (i + 1 < n.Length)
                    result[i / 2] += Byte.Parse(n[i + 1] + "", NumberStyles.HexNumber);
            }

            return result;
        }

        /// <summary>
        /// This method encrypt the string by specified key
        /// </summary>
        /// <param name="pStrMessage">The text string which will be encrypted</param>
        /// <param name="key">The key which will be used to encrypt text</param>
        /// <returns>Encrypted string</returns>
        public static string EncodeString(string pStrMessage, string key)
        {
            byte[] result = GetRC4(System.Text.Encoding.ASCII.GetBytes(pStrMessage), key);
            if (result == null)
                return "";
            return to_hex(result);
        }

        /// <summary>
        /// This method decrypt the string by specified key
        /// </summary>
        /// <param name="pStrMessage">The text string which will be decrypted</param>
        /// <param name="key">The key which will be used to decrypt text</param>
        /// <returns>Decrypted string</returns>
        public static string DecodeString(string pStrMessage, string key)
        {
            return System.Text.Encoding.ASCII.GetString(GetRC4(from_hex(pStrMessage), key));
        }

        /// <summary>
        /// Calculate md5 hash
        /// </summary>
        /// <param name="password">String which needs to be crypted</param>
        /// <returns>String representation of md5 hash</returns>
        public static string MD5(string password)
        {
            byte[] source = System.Text.ASCIIEncoding.ASCII.GetBytes(password);
            byte[] hash = new System.Security.Cryptography.MD5CryptoServiceProvider().ComputeHash(source);
            System.Text.StringBuilder output = new System.Text.StringBuilder();
            for (int i = 0; i < hash.Length; i++)
                output.Append(hash[i].ToString("x2"));
            return output.ToString();
        }
    }
}