//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using InMotion.Configuration;
using System.Globalization;

namespace InMotion.Globalization
{
    /// <summary>
    /// Provide helper methods for work with locales.
    /// </summary>
    public static class Utility
    {
        private static Dictionary<string, MTCultureInfo> _locales;
        /// <summary>
        /// Gets the <see cref="Dictionary&lt;TKey,TValue&gt;"/> that contain a set of supported by application locales.
        /// </summary>
        public static Dictionary<string, MTCultureInfo> SupportedLocales
        {
            get
            {
                if (_locales == null)
                {
                    _locales = AppConfig.Locales;
                }
                return _locales;
            }
        }

        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code.
        /// </summary>
        /// <returns>Current culture information.</returns>
        public static MTCultureInfo SetThreadCulture()
        {
            bool isCultureSelected = false;
            HttpContext current = HttpContext.Current;
            string culture = "";
            if (AppConfig.UseI18N)
            {

                if (AppConfig.UseUrlLocaleParameter && current.Request.QueryString[AppConfig.UrlLocaleParameterName] != null)
                    culture = current.Request.QueryString[AppConfig.UrlLocaleParameterName];


                if (AppConfig.UseCookieLocaleParameter && string.IsNullOrEmpty(culture) && current.Request.Cookies[AppConfig.CookieLocaleParameterName] != null)
                    culture = current.Request.Cookies[AppConfig.CookieLocaleParameterName].Value;


                if (string.IsNullOrEmpty(culture) && AppConfig.UseSessionLocaleParameter && current.Session[AppConfig.SessionLocaleParameterName] != null)
                    culture = current.Session[AppConfig.SessionLocaleParameterName].ToString();
                else if (string.IsNullOrEmpty(culture) && AppConfig.UseSessionLocaleParameter && current.Session[AppConfig.SessionLanguageParameterName] != null)
                    culture = current.Session[AppConfig.SessionLanguageParameterName].ToString();


                if (string.IsNullOrEmpty(culture) && AppConfig.UseHttpHeaderLocaleParameter && current.Request.UserLanguages != null)
                {
                    foreach (string cult in current.Request.UserLanguages)
                    {
                        string name = cult.Split(';')[0];
                        if (SupportedLocales.ContainsKey(name) && name.IndexOf("-") > 0)
                        {
                            culture = name;
                            isCultureSelected = true;
                            break;
                        }
                        if (!SupportedLocales.ContainsKey(name) && name.IndexOf("-") > 0)
                            name = name.Split('-')[0];

                        if (SupportedLocales.ContainsKey(name) && name.IndexOf("-") < 0)
                        {
                            culture = name;
                            isCultureSelected = true;
                            break;
                        }
                    }
                }
            }

            if (!isCultureSelected)
            {
                if (SupportedLocales.ContainsKey(culture) && culture.IndexOf("-") > 0)
                    isCultureSelected = true;

                if (!SupportedLocales.ContainsKey(culture) && culture.IndexOf("-") > 0)
                    culture = culture.Split(new char[] { '-' })[0];

                if (SupportedLocales.ContainsKey(culture) && culture.IndexOf("-") < 0)
                    isCultureSelected = true;
            }

            if (!isCultureSelected)
                culture = AppConfig.DefaultLanguage;

            System.Threading.Thread.CurrentThread.CurrentCulture = (CultureInfo)SupportedLocales[culture];
            System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.CurrentCulture;
            if (AppConfig.UseI18N)
            {
                if (AppConfig.UseCookieLocaleParameter)
                {
                    HttpCookie cookie = new HttpCookie(AppConfig.CookieLocaleParameterName, culture);
                    if (AppConfig.LocaleCookieExpiration > 0)
                        cookie.Expires = DateTime.Now.AddDays(AppConfig.LocaleCookieExpiration);
                    current.Response.Cookies.Add(cookie);
                }
                if (AppConfig.UseSessionLocaleParameter)
                {
                    current.Session[AppConfig.SessionLocaleParameterName] = culture;
                    current.Session[AppConfig.SessionLanguageParameterName] = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;

                }
            }

            if (current.Application[current.Request.PhysicalPath] == null)
                current.Application.Add(current.Request.PhysicalPath, ((MTCultureInfo)CultureInfo.CurrentCulture).Encoding);
            current.Request.ContentEncoding = System.Text.Encoding.GetEncoding(current.Application[current.Request.PhysicalPath].ToString());

            return (MTCultureInfo)CultureInfo.CurrentCulture;
        }
    }
}
