//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;

namespace InMotion.Data
{
    /// <summary>
    /// Represents a collection of <see cref="WhereParameter"/> associated with a <see cref="DataCommand"/> and their respective mappings to columns in a DataSet.
    /// </summary>
    public class WhereParameterCollection : DataParameterCollection
    {
        /// <summary>
        /// Initialize the new instance of <see cref="WhereParameterCollection"/> using the specified owner.
        /// </summary>
        /// <param name="owner">The owner <see cref="DataCommand"/>.</param>
        public WhereParameterCollection(DataCommand owner)
            : base(owner)
        {
        }
        private List<WhereParameter> _innerList = new List<WhereParameter>();

        /// <summary>
        /// Adds the specified <see cref="DataParameter"/> object to the DataParameterCollection.
        /// </summary>
        /// <param name="value">An Object.</param>
        /// <returns>The index of the new DataParameter object.</returns>
        public override int Add(object value)
        {
            _innerList.Add((WhereParameter)value);
            ((WhereParameter)value).Owner = Owner;
            return _innerList.Count - 1;
        }
        /// <summary>
        /// Adds the specified <see cref="DataParameter"/> object to the DataParameterCollection.
        /// </summary>
        /// <param name="value">A <see cref="DataParameter"/> object.</param>
        /// <returns>The index of the new DataParameter object.</returns>
        public int Add(WhereParameter value)
        {
            return Add((object)value);
        }
        /// <summary>
        /// Adds an array of values to the end of the DataParameterCollection.
        /// </summary>
        /// <param name="values">The Array values to add.</param>
        public override void AddRange(Array values)
        {
            foreach (object var in values)
            {
                ((WhereParameter)var).Owner = Owner;
                _innerList.Add((WhereParameter)var);

            }
        }
        /// <summary>
        /// Removes all the DataParameter objects from the DataParameterCollection. 
        /// </summary>
        public override void Clear()
        {
            _innerList.Clear();
        }
        /// <summary>
        /// Determines whether the parameter with the specified name 
        /// is in this WhereParameterCollection. 
        /// </summary>
        /// <param name="parameterName">The name of parameter.</param>
        /// <returns>true if the WhereParameterCollection contains the parameter; otherwise false.</returns>
        public override bool Contains(string parameterName)
        {
            foreach (WhereParameter p in _innerList)
            {
                if (p.ParameterName == parameterName) return true;
            }
            return false;
        }
        /// <summary>
        /// Determines whether the specified Object is in this WhereParameterCollection. 
        /// </summary>
        /// <param name="value">The Object value.</param>
        /// <returns>true if the WhereParameterCollection contains the value; otherwise false.</returns>
        public override bool Contains(object value)
        {
            return _innerList.Contains((WhereParameter)value);
        }
        /// <summary>
        /// Copies all the elements of the current WhereParameterCollection to the specified one-dimensional 
        /// Array starting at the specified destination Array index.
        /// </summary>
        /// <param name="array">The one-dimensional Array that is the destination of the 
        /// elements copied from the current WhereParameterCollection.</param>
        /// <param name="index">A 32-bit integer that represents the index in the Array at which copying starts.</param>
        public override void CopyTo(Array array, int index)
        {
            int k = 0;
            for (int i = index; i < array.Length; i++)
            {
                array.SetValue(this[k], i);
                k++;
            }
        }

        /// <summary>
        /// Copies the elements of the ParameterCollections to an array of Parameter, starting at a particular Array index.
        /// </summary>
        /// <param name="array">The one-dimensional Array that is the destination of the elements copied from ICollection. The Array must have zero-based indexing. </param>
        /// <param name="index">The zero-based index in array at which copying begins.</param>
        public void CopyTo(WhereParameter[] array, int index)
        {
            this.CopyTo((Array)array, index);
        }

        /// <summary>
        /// Returns an Integer that contains the number of elements in the WhereParameterCollection. Read-only.
        /// </summary>
        public override int Count
        {
            get
            {
                return _innerList.Count;
            }
        }

        /// <summary>
        /// Returns an enumerator that iterates through the WhereParameterCollection.
        /// </summary>
        /// <returns>An enumerator that iterates through the WhereParameterCollection.</returns>
        public override System.Collections.IEnumerator GetEnumerator()
        {
            return _innerList.GetEnumerator();
        }

        /// <summary>
        /// Returns DbParameter the object with the specified name.
        /// </summary>
        /// <param name="parameterName">The name of the DbParameter in the collection.</param>
        /// <returns>The DbParameter the object with the specified name. </returns>
        protected override DbParameter GetParameter(string parameterName)
        {
            foreach (DataParameter p in _innerList)
            {
                if (p.ParameterName == parameterName) return p;
            }
            return null;
        }

        /// <summary>
        /// Returns the DbParameter object at the specified index in the collection. 
        /// </summary>
        /// <param name="index">The index of the DbParameter in the collection.</param>
        /// <returns>The DbParameter object at the specified index in the collection. </returns>
        protected override DbParameter GetParameter(int index)
        {
            return (DbParameter)_innerList[index];
        }

        /// <summary>
        /// Returns the index of the specified DbParameter object.
        /// </summary>
        /// <param name="parameterName">The name of the DbParameter object in the collection.</param>
        /// <returns>The index of the DbParameter object with the specified name.</returns>
        public override int IndexOf(string parameterName)
        {
            foreach (WhereParameter p in _innerList)
            {
                if (p.ParameterName == parameterName) return ((IList)this).IndexOf(p);
            }
            return -1;
        }

        /// <summary>
        /// Returns the index of the DbParameter object with the specified name.
        /// </summary>
        /// <param name="value">The DbParameter object in the collection.</param>
        /// <returns>The index of the specified DbParameter object.</returns>
        public override int IndexOf(object value)
        {
            return _innerList.IndexOf((WhereParameter)value);
        }

        /// <summary>
        /// Inserts the specified the index of the DbParameter object with the specified name into the collection at the specified index.
        /// </summary>
        /// <param name="index">The index at which to insert the DbParameter object.</param>
        /// <param name="value">The DbParameter object to insert into the collection.</param>
        public override void Insert(int index, object value)
        {
            ((WhereParameter)value).Owner = Owner;
            _innerList.Insert(index, (WhereParameter)value);
        }

        /// <summary>
        /// Inserts the specified the index of the WhereParameter object with the specified name into the collection at the specified index.
        /// </summary>
        /// <param name="index">The index at which to insert the WhereParameter object.</param>
        /// <param name="value">The WhereParameter object to insert into the collection.</param>
        public void Insert(int index, WhereParameter value)
        {
            Insert(index, (object)value);
        }
        /// <summary>
        /// Specifies whether the collection is a fixed size.
        /// </summary>
        public override bool IsFixedSize
        {
            get { return ((IList)_innerList).IsFixedSize; }
        }
        /// <summary>
        /// Specifies whether the collection is read-only. 
        /// </summary>
        public override bool IsReadOnly
        {
            get { return ((IList)_innerList).IsReadOnly; }
        }
        /// <summary>
        /// Specifies whether the collection is synchronized.
        /// </summary>
        public override bool IsSynchronized
        {
            get { return ((ICollection)_innerList).IsSynchronized; }
        }
        /// <summary>
        /// Removes the specified DbParameter object from the collection.
        /// </summary>
        /// <param name="value">The DbParameter object to remove.</param>
        public override void Remove(object value)
        {
            ((IList)this).Remove(value);
        }

        /// <summary>
        /// Removes the specified WhereParameter object from the collection.
        /// </summary>
        /// <param name="value">The WhereParameter object to remove.</param>
        public void Remove(WhereParameter value)
        {
            ((IList)this).Remove(value);
        }
        /// <summary>
        /// Removes the DbParameter object with the specified name from the collection. 
        /// </summary>
        /// <param name="parameterName">The name of parameter to remove.</param>
        public override void RemoveAt(string parameterName)
        {
            foreach (DataParameter p in _innerList)
            {
                if (p.ParameterName == parameterName) _innerList.Remove((WhereParameter)p);
            }
        }

        /// <summary>
        /// Gets and sets the DbParameter at the specified index. 
        /// </summary>
        /// <param name="index">The zero-based index of the parameter.</param>
        /// <returns>The DbParameter at the specified index.</returns>
        public new WhereParameter this[int index]
        {
            get
            {
                return (WhereParameter)GetParameter(index);
            }
            set
            {
                SetParameter(index, value);
            }
        }

        /// <summary>
        /// Gets and sets the DbParameter with the specified name. 
        /// </summary>
        /// <param name="parameterName">The name of the parameter.</param>
        /// <returns>The DbParameter with the specified name.</returns>
        public new WhereParameter this[string parameterName]
        {
            get
            {
                int i = IndexOf(parameterName);
                return (WhereParameter)GetParameter(i);
            }
            set
            {
                int i = IndexOf(parameterName);
                SetParameter(i, value);
            }
        }
        /// <summary>
        /// Removes the DbParameter object at the specified from the collection. 
        /// </summary>
        /// <param name="index">The index where the DbParameter object is located.</param>
        public override void RemoveAt(int index)
        {
            _innerList.RemoveAt(index);
        }

        /// <summary>
        /// Sets the DbParameter object with the specified name to a new value.
        /// </summary>
        /// <param name="parameterName">The name of the DbParameter object in the collection.</param>
        /// <param name="value">The new DbParameter value.</param>
        protected override void SetParameter(string parameterName, DbParameter value)
        {
            ((WhereParameter)value).Owner = Owner;
            foreach (WhereParameter p in _innerList)
            {
                if (p.ParameterName == parameterName) _innerList[_innerList.IndexOf(p)] = (WhereParameter)value;
                return;
            }
            Add(value);

        }
        /// <summary>
        /// Sets the DbParameter object at the specified index to a new value.
        /// </summary>
        /// <param name="index">The index where the DbParameter object is located.</param>
        /// <param name="value">The new DbParameter value.</param>
        protected override void SetParameter(int index, DbParameter value)
        {
            ((WhereParameter)value).Owner = Owner;
            _innerList[index] = (WhereParameter)value;
        }
        /// <summary>
        /// Specifies the Object to be used to synchronize access to the collection.
        /// </summary>
        public override object SyncRoot
        {
            get { return ((ICollection)_innerList).SyncRoot; }
        }
    }
}
