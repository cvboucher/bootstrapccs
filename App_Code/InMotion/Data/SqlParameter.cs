//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using InMotion.Common;

namespace InMotion.Data
{
    /// <summary>
    /// Represents a parameter to a <see cref="DataCommand"/> and optionally its mapping to DataSet columns.
    /// </summary>
    public class SqlParameter : DataParameter
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SqlParameter"/> class. 
        /// </summary>
        public SqlParameter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlParameter"/> class that uses 
        /// the parameter name and the value of the new <see cref="SqlParameter"/>
        /// </summary>
        /// <param name="name">The name of the parameter to map.</param>
        /// <param name="value">The value of the new <see cref="SqlParameter"/> object.</param>
        public SqlParameter(String name, Object value)
            : base(name, value)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlParameter"/> class 
        /// that uses the parameter name and data type. 
        /// </summary>
        /// <param name="name">The name of the parameter to map.</param>
        /// <param name="type">One of the <see cref="DataType"/> values.</param>
        public SqlParameter(String name, DataType type)
            : base(name, type)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlParameter"/> class that uses 
        /// the parameter name, data type, and length. 
        /// </summary>
        /// <param name="name">The name of the parameter to map.</param>
        /// <param name="type">One of the <see cref="DataType"/> values.</param>
        /// <param name="size">The length of the parameter.</param>
        public SqlParameter(String name, DataType type, int size)
            : base(name, type, size)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlParameter"/> class that uses the parameter name, 
        /// data type, length, and source column name. 
        /// </summary>
        /// <param name="name">The name of the parameter to map.</param>
        /// <param name="type">One of the <see cref="DataType"/> values.</param>
        /// <param name="size">The length of the parameter.</param>
        /// <param name="sourceColumn">The name of the source column.</param>
        public SqlParameter(String name, DataType type, int size, string sourceColumn)
            : base(name, type, size, sourceColumn)
        {
        }

        #endregion

        /// <summary>
        /// Gets a string that contains the formatted value.
        /// </summary>
        /// <returns>A string that contains the formatted value.</returns>
        public override string ToString()
        {
            return ToString(true);
        }

        private bool _quoted;

        /// <summary>
        /// Gets or sets whether value of parameter will be surrounded by the single quotation mark.
        /// In case of true, the quotes will be added, depending on <see cref="Type"/>.
        /// In case of false, the owner data command will define this.
        /// </summary>
        public bool AddQuotes
        {
            get { return _quoted; }
            set { _quoted = value; }
        }

        /// <summary>
        /// Returns the string representation of parameter value for inserting in SQL statement.
        /// </summary>
        /// <param name="addQuotes">Indicates whether result string will be quoted. In case if <see cref="AddQuotes"/> property is set to true this parameter will be ignored</param>
        /// <returns>String that representthe current value of parameter.</returns>
        public string ToString(bool addQuotes)
        {
            Connection conn = (Connection)Owner.Connection;
            return conn.EncodeSqlValue(Text, Type, addQuotes || AddQuotes);
        }
    }
}
