//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using InMotion.Common;
using System.Globalization;
using System.Collections.ObjectModel;

namespace InMotion.Data
{
    /// <summary>
    /// Represents a connection to a database. 
    /// </summary>
    public class Connection : DbConnection
    {
        private DbConnection _connection;
        private DbProviderFactory _factory;

        /// <summary>
        /// Initialize the new istance of Connection class using given provider name.
        /// </summary>
        /// <param name="providerName">The DBProvider name.</param>
        /// <remarks>The <paramref name="providerName"/> should contain the fully qualified provider name. For example: System.Data.SqlClient</remarks>
        public Connection(string providerName)
        {
            ProviderName = providerName;
            _factory = DbProviderFactories.GetFactory(providerName);
            _connection = _factory.CreateConnection();
        }

        public DbConnection NativeConnection
        {
            get
            {
                return _connection;
            }
        }

        private void ExtractLoginInfo()
        {
            string[] pars = this.ConnectionString.Split(new Char[] { ';' });
            for (int i = 0; i < pars.Length; i++)
            {
                string[] par = pars[i].Split(new Char[] { '=' });
                if (par.Length == 2)
                {
                    string name = par[0].ToUpper(CultureInfo.CurrentCulture).Trim();
                    if (name == "USER ID" || name == "UID")
                        _login = par[1];
                    if (name == "PASSWORD" || name == "PWD")
                        _password = par[1];
                }
            }
        }

        private string _password;
        /// <summary>
        /// Gets the password that used for connect to database.
        /// </summary>
        public string Password
        {
            get
            {
                if (_password == null) ExtractLoginInfo();
                return _password;
            }
        }

        private string _login;
        /// <summary>
        /// Gets the login name that used for connect to database.
        /// </summary>
        public string Login
        {
            get
            {
                if (_login == null) ExtractLoginInfo();
                return _login;
            }
        }

        private string _providerName;
        /// <summary>
        /// Gets or sets the DBProvider name.
        /// </summary>
        public string ProviderName
        {
            get { return _providerName; }
            set { _providerName = value; }
        }
        /// <summary>
        /// Starts a database transaction.
        /// </summary>
        /// <param name="isolationLevel">Specifies the isolation level for the transaction.</param>
        /// <returns>An object representing the new transaction.</returns>
        protected override DbTransaction BeginDbTransaction(IsolationLevel isolationLevel)
        {
            return _connection.BeginTransaction(isolationLevel);
        }
        /// <summary>
        /// Changes the current database for an open connection. 
        /// </summary>
        /// <param name="databaseName">The name of the database.</param>
        public override void ChangeDatabase(string databaseName)
        {
            _connection.ChangeDatabase(databaseName);
        }
        /// <summary>
        /// Closes the connection to the database. 
        /// This is the preferred method of closing any open connection.
        /// </summary>
        public override void Close()
        {
            _connection.Close();
        }
        /// <summary>
        /// Gets or sets the connection string that used to open the connection.
        /// </summary>
        public override string ConnectionString
        {
            get
            {
                return _connection.ConnectionString;
            }
            set
            {
                _connection.ConnectionString = value;
            }
        }
        /// <summary>
        /// Creates and returns a DbCommand object associated 
        /// with the current connection. 
        /// </summary>
        /// <returns>A DbCommand object.</returns>
        protected override DbCommand CreateDbCommand()
        {
            return new DataCommand(_providerName, this);
        }
        /// <summary>
        /// Gets the name of the database server to which to connect.
        /// </summary>
        public override string DataSource
        {
            get { return _connection.DataSource; }
        }
        /// <summary>
        /// Gets the name of the current database after a connection is opened, 
        /// or the database name specified in the connection string before 
        /// the connection is opened.
        /// </summary>
        public override string Database
        {
            get { return _connection.Database; }
        }
        /// <summary>
        /// Opens a database connection 
        /// with the settings specified by the <see cref="ConnectionString"/>.
        /// </summary>
        public override void Open()
        {
            _connection.Open();
        }
        /// <summary>
        /// Gets a string that represents the version of the server to 
        /// which the object is connected.
        /// </summary>
        public override string ServerVersion
        {
            get { return _connection.ServerVersion; }
        }

        /// <summary>
        /// Gets a string that describes the state of the connection.
        /// </summary>
        public override ConnectionState State
        {
            get { return _connection.State; }
        }


        private Collection<string> _connectionCommands = new Collection<string>();
        private string _server = "";
        private bool _optimized;
        private string _dateFormat = "";
        private string _boolFormat = "";
        private string _dateLeftDelim = "";
        private string _dateRightDelim = "";

        /// <summary>
        /// Gets or sets the name of database server.
        /// </summary>
        public string Server
        {
            get
            {
                return _server;
            }
            set
            {
                _server = value;
            }
        }

        /// <summary>
        /// Gets or sets the value that indicates is command performance optimization enabled.
        /// </summary>
        public bool UseOptimization
        {
            get
            {
                return _optimized;
            }
            set
            {
                _optimized = value;
            }
        }

        /// <summary>
        /// Gets the Collection&lt;string&gt; containing SQL queries that executed each time when connection opened.
        /// </summary>
        public Collection<string> ConnectionCommands
        {
            get
            {
                return _connectionCommands;
            }
        }

        /// <summary>
        /// Gets or sets the default date format string that associated with connection.
        /// </summary>
        public string DateFormat
        {
            get
            {
                return _dateFormat;
            }
            set
            {
                _dateFormat = value;
            }
        }
        /// <summary>
        /// Gets or sets the default boolean format string that associated with connection.
        /// </summary>
        public string BooleanFormat
        {
            get
            {
                return _boolFormat;
            }
            set
            {
                _boolFormat = value;
            }
        }

        /// <summary>
        /// Gets or sets the left delimiter that used for format date value inside the SQL statements.
        /// </summary>
        public string DateLeftDelimiter
        {
            get
            {
                return _dateLeftDelim;
            }
            set
            {
                _dateLeftDelim = value;
            }
        }

        /// <summary>
        /// Gets or sets the right delimiter that used for format date value inside the SQL statements.
        /// </summary>
        public string DateRightDelimiter
        {
            get
            {
                return _dateRightDelim;
            }
            set
            {
                _dateRightDelim = value;
            }
        }

        /// <summary>
        /// Encode string value for inserting it into a SQL statement.
        /// </summary>
        /// <param name="value">The <see cref="string"/> value.</param>
        /// <param name="type">The <see cref="DataType"/> of value.</param>
        /// <param name="addQuotes"><see cref="bool"/> that indicate if result value will be quoted. <b>true</b> - the quotes will be added; otherwise no</param>
        /// <returns><b>string</b> that contains encoded value.</returns>
        public string EncodeSqlValue(string value, DataType type, bool addQuotes)
        {
            string startQuoteSign = "";
            string endQuoteSign = "";
            if (addQuotes && String.IsNullOrEmpty(value)) return "NULL";
            if (addQuotes)
                if (Server == "MSSQLServer")
                {
                    startQuoteSign = "N'";
                    endQuoteSign = "'";
                }
                else
                {
                    startQuoteSign = "'";
                    endQuoteSign = "'";
                }
            switch (type)
            {
                case DataType.Text:
                case DataType.Memo:
                    if (Server == "MySQL") value = value.Replace(@"\", @"\\");
                    return startQuoteSign + value.Replace("'", "''") + endQuoteSign;
                case DataType.Integer:
                case DataType.Float:
                case DataType.Single:
                    return value.Replace(",", ".");
                case DataType.Date:
                    return (addQuotes ? DateLeftDelimiter : "") + value + (addQuotes ? DateRightDelimiter : "");
                case DataType.Boolean:
                    return value;
                default:
                    return "";
            }
        }

    }
}
