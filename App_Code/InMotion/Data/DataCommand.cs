//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Diagnostics;
using InMotion.Configuration;

namespace InMotion.Data
{
    /// <summary>
    /// Represents an SQL statement or stored procedure to execute 
    /// against a data source. 
    /// </summary>
    public class DataCommand : DbCommand
    {
        private DbCommand _command;

        private string _providerName;
        private DbProviderFactory _factory;

        /// <summary>
        /// Gets the reference on <see cref="DbProviderFactory"/> object that used by instance of DataCommand.
        /// </summary>
        public DbProviderFactory Factory
        {
            get
            {
                return _factory;
            }
        }

        /// <summary>
        /// Constructs an instance of the <see cref="DataCommand"/> object with the provider name and <see cref="Connection"/>. 
        /// </summary>
        /// <param name="providerName">The name of DbProvider</param>
        /// <param name="connection">A <see cref="Connection"/> that represent the connection to a data source.</param>
        public DataCommand(string providerName, Connection connection)
        {
            _providerName = providerName;
            _factory = DbProviderFactories.GetFactory(providerName);
            _command = _factory.CreateCommand();
            Connection = connection;
        }
        /// <summary>
        /// Tries to cancel the execution of an <see cref="DataCommand"/>.
        /// </summary>
        public override void Cancel()
        {
            _command.Cancel();
        }

        private string _commandText;
        /// <summary>
        /// Gets or sets the SQL statement or stored procedure to execute at the data source. 
        /// </summary>
        public override string CommandText
        {
            get
            {
                return _commandText;
            }
            set
            {
                _commandText = value;
            }
        }

        private int _timeout;
        /// <summary>
        /// Gets or sets the wait time before terminating an attempt to execute a command and generating an error.
        /// </summary>
        public override int CommandTimeout
        {
            get
            {
                return _timeout;
            }
            set
            {
                _timeout = value;
                _command.CommandTimeout = _timeout;
            }
        }

        private CommandType _commandType;
        /// <summary>
        /// Gets or sets a value that indicates how the <see cref="CommandText"/> property is interpreted. 
        /// </summary>
        /// <value>One of the <b>CommandType</b> values. The default is Text.</value>
        public override CommandType CommandType
        {
            get
            {
                return _commandType;
            }
            set
            {
                _commandType = value;
            }
        }

        private MTCommandType _MTCommandType;
        /// <summary>
        /// Gets or sets a value that indicates how the <see cref="CommandText"/> property is interpreted. 
        /// </summary>
        /// /// <value>One of the <b>MTCommandType</b> values.</value>
        public MTCommandType MTCommandType
        {
            get
            {
                return _MTCommandType;
            }
            set
            {
                _MTCommandType = value;
            }
        }

        private string _sortExpression;
        /// <summary>
        /// Gets or sets the default sort expression that append to the SQL or Table command.
        /// </summary>
        public string SortExpression
        {
            get
            {
                if (_sortExpression == null) _sortExpression = "";
                return _sortExpression;
            }
            set
            {
                _sortExpression = value;
            }
        }

        /// <summary>
        /// Creates a new instance of a <see cref="DbParameter"/> object.
        /// </summary>
        /// <returns>A DbParameter object.</returns>
        protected override DbParameter CreateDbParameter()
        {
            if (AppConfig.IsMTErrorHandlerUse("critical"))
                Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        private Connection _conn;
        /// <summary>
        /// Gets or sets the <see cref="DbConnection"/> used by this <see cref="DataCommand"/>.
        /// </summary>
        protected override DbConnection DbConnection
        {
            get
            {
                return _conn;
            }
            set
            {
                if (value != null)
                    _command.Connection = ((Connection)value).NativeConnection;
                _conn = (Connection)value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="DbTransaction"/> within which this <see cref="DataCommand"/> object executes.
        /// </summary>
        protected override DbTransaction DbTransaction
        {
            get
            {
                return _command.Transaction;
            }
            set
            {
                _command.Transaction = value;
            }
        }

        private bool _designTimeVisible;
        /// <summary>
        /// Gets or sets a value that indicates whether the command object should be visible in a customized Windows Forms Designer control.
        /// </summary>
        public override bool DesignTimeVisible
        {
            get
            {
                return _designTimeVisible;
            }
            set
            {
                _designTimeVisible = value;
            }
        }

        /// <summary>
        /// Executes the command text against the connection.
        /// </summary>
        /// <param name="behavior">An instance of <see cref="CommandBehavior"/>.</param>
        /// <returns>A <see cref="DbDataReader"/>.</returns>
        protected override DbDataReader ExecuteDbDataReader(CommandBehavior behavior)
        {
            InitializeInnerCommand();
            DbDataReader reader = _command.ExecuteReader(behavior);
            ProceedSPReturnParameters();
            return reader;
        }

        private string ReplaceSqlParameters(string command, bool quoted, int maxRecords)
        {
            StringBuilder text = new StringBuilder(command);
            foreach (SqlParameter param in Parameters)
            {
                text.Replace("{" + param.ParameterName + "}", param.ToString(quoted));
            }
            if (command.IndexOf("{SQL_OrderBy}") > -1)
            {
                if (SortExpression.Length > 0)
                    text.Replace("{SQL_OrderBy}", " ORDER BY " + SortExpression);
                else
                    text.Replace("{SQL_OrderBy}", String.Empty);
            }
            else if (SortExpression.Length > 0)
                text.Append(" ORDER BY " + SortExpression);
            if (Connection != null && ((Connection)Connection).UseOptimization && command.IndexOf("{SqlParam_endRecord}") > -1 && maxRecords > 0)
            {
                text.Replace("{SqlParam_endRecord}", maxRecords.ToString());
            }
            else if (command.IndexOf("{SqlParam_endRecord}") > -1)
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("The {SqlParam_endRecord} variable is supplied, but the value of maxRecords is invalid or optimization is disabled for connection.\n{0}", Environment.StackTrace);
                throw new ArgumentOutOfRangeException("maxRecords", maxRecords, "The {SqlParam_endRecord} variable is supplied, but the value of maxRecords is invalid or optimization is disabled for connection");
            }
            return text.ToString();
        }

        private string ReplaceWhereParameters(string command)
        {
            StringBuilder text = new StringBuilder(command);
            if (WhereParameters.Count > 0)
            {
                List<string> WhereBuilder = new List<string>();
                string _operation = "";
                foreach (WhereParameter param in WhereParameters)
                {
                    string p = param.ToString();
                    bool isFirstOperand = WhereBuilder.Count == 0;
                    bool PrevItemIsOpenBracket = WhereBuilder.Count > 0 && WhereBuilder[WhereBuilder.Count - 1].EndsWith("(");
                    if ((param.Prefix + p).Length > 0 && !PrevItemIsOpenBracket && !isFirstOperand)
                    {
                        WhereBuilder.Add(_operation);
                    }
                    if (param.Prefix != null && param.Prefix.Length > 0) WhereBuilder.Add(param.Prefix);
                    if (p.Length > 0) WhereBuilder.Add(p);
                    if (param.Postfix != null && param.Postfix.Length > 0) WhereBuilder.Add(param.Postfix);
                    if (WhereBuilder.Count >= 2 &&
                        WhereBuilder[WhereBuilder.Count - 1].StartsWith(")") &&
                        WhereBuilder[WhereBuilder.Count - 2].EndsWith("("))
                    {
                        WhereBuilder.RemoveAt(WhereBuilder.Count - 1);
                        WhereBuilder.RemoveAt(WhereBuilder.Count - 1);
                        if (WhereBuilder.Count > 0) WhereBuilder.RemoveAt(WhereBuilder.Count - 1);
                    }
                    _operation = " " + param.Operation.ToString() + " ";
                }
                StringBuilder strWhereBuilder = new StringBuilder();
                for (int i = 0; i < WhereBuilder.Count; i++)
                    strWhereBuilder.Append(WhereBuilder[i]);
                string WhereClause = strWhereBuilder.ToString();
                WhereClause = Where + (String.IsNullOrEmpty(WhereClause) ? "" : (" " + Operation + " (" + WhereClause + ")"));
                if (WhereClause.Length > 0)
                {
                    if (command.IndexOf("{SQL_Where}") > -1)
                    {
                        text.Replace("{SQL_Where}", " WHERE " + WhereClause);
                    }
                    else
                    {
                        text.Append(" WHERE " + WhereClause);
                    }
                }
                else
                {
                    text.Replace("{SQL_Where}", "");
                }
            }
            else
            {
                if (command.IndexOf("{SQL_Where}") > -1 && Where.Length > 0)
                {
                    text.Replace("{SQL_Where}", " WHERE " + Where);
                }
                else if (Where.Length > 0)
                {
                    text.Append(" WHERE " + Where);
                }
                else
                {
                    text.Replace("{SQL_Where}", String.Empty);
                }
            }
            return text.ToString();
        }

        private bool isBuilded = false;
        /// <summary>
        /// Execute replacing Parameters into command.
        /// </summary>
        public void BuildCommand()
        {
            BuildCommand(0);
        }

        /// <summary>
        /// Execute replacing Parameters into command.
        /// </summary>
        /// <param name="maxRecords"> The maximum number of data rows that a data source returns for a data retrieval operation.</param>
        public void BuildCommand(int maxRecords)
        {
            if (String.IsNullOrEmpty(CommandText))
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("The CommandText can not be null or empty.\n{0}", Environment.StackTrace);
                throw new InvalidOperationException("The CommandText can not be null or empty");
            }
            if (MTCommandType == MTCommandType.Sql)
            {
                CommandText = ReplaceSqlParameters(CommandText, false, maxRecords);
            }
            else if (MTCommandType == MTCommandType.Table)
            {
                CommandText = ReplaceWhereParameters(CommandText);
                CommandText = ReplaceSqlParameters(CommandText, true, maxRecords);
            }
            isBuilded = true;
        }

        private void InitializeInnerCommand()
        {
            InitializeInnerCommand(0);
        }
        private void InitializeInnerCommand(int maxRecords)
        {
            if (ConfigurationManager.AppSettings["CommandTimeout"] != null)
            {
                CommandTimeout = Int32.Parse(ConfigurationManager.AppSettings["CommandTimeout"]);
            }
            if (String.IsNullOrEmpty(CommandText))
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("The CommandText can not be null or empty.\n{0}", Environment.StackTrace);
                throw new InvalidOperationException("The CommandText can not be null or empty");
            }
            if (MTCommandType == MTCommandType.StoredProcedure)
            {
                _command.CommandText = CommandText;
                _command.CommandType = CommandType.StoredProcedure;
                _command.Parameters.Clear();
                for (int i = 0; i < this.SPParameters.Count; i++)
                {
                    DbParameter p = _command.CreateParameter();
                    p.DbType = SPParameters[i].DbType;
                    p.Direction = SPParameters[i].Direction;
                    p.ParameterName = SPParameters[i].ParameterName;
                    if (SPParameters[i].Size != 0) p.Size = SPParameters[i].Size;
                    if (SPParameters[i].Value == null)
                        p.Value = DBNull.Value;
                    else
                        p.Value = SPParameters[i].Value;
                    _command.Parameters.Add(p);
                }
            }
            else if (isBuilded)
            {
                _command.CommandText = CommandText;
                _command.CommandType = CommandType.Text;
            }
            else if (MTCommandType == MTCommandType.Sql)
            {
                _command.CommandText = ReplaceSqlParameters(CommandText, false, maxRecords);
                _command.CommandType = CommandType.Text;
            }
            else if (MTCommandType == MTCommandType.Table)
            {
                string cmd = ReplaceWhereParameters(CommandText);
                _command.CommandText = ReplaceSqlParameters(cmd, true, maxRecords);
                _command.CommandType = CommandType.Text;
            }
        }

        private void ProceedSPReturnParameters()
        {
            if (MTCommandType.StoredProcedure != MTCommandType) return;
            this.SPParameters.Clear();
            for (int i = 0; i < _command.Parameters.Count; i++)
            {
                SPParameter p = new SPParameter();
                p.DbType = _command.Parameters[i].DbType;
                p.Direction = _command.Parameters[i].Direction;
                p.ParameterName = _command.Parameters[i].ParameterName;
                p.Size = _command.Parameters[i].Size;
                p.Value = _command.Parameters[i].Value;
                this.SPParameters.Add(p);
            }
        }

        /// <summary>
        /// Executes an SQL statement against the <see cref="Connection"/> and returns the number of rows affected. 
        /// </summary>
        /// <returns>The number of rows affected.</returns>
        public override int ExecuteNonQuery()
        {
            InitializeInnerCommand();
            bool isClose = false;
            if (_command.Connection.State == ConnectionState.Closed)
            {
                _command.Connection.Open();
                isClose = true;
            }
            int result;
            try
            {
                result = _command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("System.Data.DataException: {0}\n{1}", _command.CommandText, e);

                if (AppConfig.ShowSqlOnError)
                    throw new DataException(_command.CommandText, e);
                else
                    throw;
            }
            ProceedSPReturnParameters();
            if (isClose)
                _command.Connection.Close();
            return result;
        }

        /// <summary>
        /// Executes the query, and returns the first column of the first row in the result set returned by the query. Additional columns or rows are ignored.
        /// </summary>
        /// <returns>The first column of the first row in the result set, or a null reference if the result set is empty.</returns>
        public override object ExecuteScalar()
        {
            InitializeInnerCommand();
            bool isClose = false;
            if (_command.Connection.State == ConnectionState.Closed)
            {
                _command.Connection.Open();
                isClose = true;
            }
            object result;
            try
            {
                result = _command.ExecuteScalar();
            }
            catch (Exception e)
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("System.Data.DataException: {0}\n{1}", _command.CommandText, e);

                if (AppConfig.ShowSqlOnError)
                    throw new DataException(_command.CommandText, e);
                else
                    throw;
            }
            ProceedSPReturnParameters();
            if (isClose)
                _command.Connection.Close();
            return result;
        }

        /// <summary>
        /// Creates a prepared (or compiled) version of the command on the data source.
        /// </summary>
        public override void Prepare()
        {
        }

        private UpdateRowSource _updateRowSource;
        /// <summary>
        /// Gets or sets how command results are applied to the DataRow when used by the Update method of the DbDataAdapter.
        /// </summary>
        public override UpdateRowSource UpdatedRowSource
        {
            get
            {
                return _updateRowSource;
            }
            set
            {
                _updateRowSource = value;
            }
        }

        /// <summary>
        /// Executes a SQL statement against a connection object and returns a <see cref="DataSet"/> object.
        /// </summary>
        /// <returns>A <see cref="DataSet"/> object.</returns>
        public DataSet Execute()
        {
            InitializeInnerCommand();
            DataSet ds = new DataSet();
            ds.Locale = CultureInfo.InvariantCulture;
            DbDataAdapter da = _factory.CreateDataAdapter();
            da.SelectCommand = _command;
            try
            {
                da.Fill(ds);
            }
            catch (Exception e)
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("System.Data.DataException: {0}\n{1}", _command.CommandText, e);

                if (AppConfig.ShowSqlOnError)
                    throw new DataException(_command.CommandText, e);
                else
                    throw;
            }
            ProceedSPReturnParameters();
            return ds;
        }
        /// <summary>
        /// Executes a SQL statement against a connection object and returns a <see cref="DataSet"/> object.
        /// </summary>
        /// <param name="startRecord">An integer indicating the location of the starting record.</param>
        /// <param name="maxRecords">An integer indicating the maximum number of records</param>
        /// <returns>A <see cref="DataSet"/> object.</returns>
        public DataSet Execute(int startRecord, int maxRecords)
        {
            InitializeInnerCommand(maxRecords + startRecord);
            DataSet ds = new DataSet();
            ds.Locale = CultureInfo.InvariantCulture;
            DbDataAdapter da = _factory.CreateDataAdapter();
            da.SelectCommand = _command;
            try
            {
                da.Fill(ds, startRecord, maxRecords, "Table1");
            }
            catch (Exception e)
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("System.Data.DataException: {0}\n{1}", _command.CommandText, e);

                if (AppConfig.ShowSqlOnError)
                    throw new DataException(_command.CommandText, e);
                else
                    throw;
            }
            ProceedSPReturnParameters();
            return ds;
        }

        private SPParameterCollection _SPParameters;

        /// <summary>
        /// Gets the <see cref="SPParameterCollection"/>
        /// </summary>
        /// <value>The parameters of the stored procedure. The default is an empty collection.</value>
        public SPParameterCollection SPParameters
        {
            get
            {
                if (_SPParameters == null) _SPParameters = new SPParameterCollection(this);
                return _SPParameters;
            }
        }

        private WhereParameterCollection _whereParameters;
        /// <summary>
        /// Gets the <see cref="WhereParameterCollection"/>
        /// </summary>
        /// <value>The where parameters of the Table command. The default is an empty collection.</value>
        public WhereParameterCollection WhereParameters
        {
            get
            {
                if (_whereParameters == null) _whereParameters = new WhereParameterCollection(this);
                return _whereParameters;
            }
        }

        private string _Where;
        /// <summary>
        /// Gets or sets the additional where clause part that append to the main where clause of Table command.
        /// </summary>
        public string Where
        {
            get
            {
                return _Where == null ? "" : _Where;
            }
            set
            {
                _Where = value;
            }
        }

        private string _operation;
        /// <summary>
        /// Get or sets the logical operation that used for append <see cref="Where"/> to the main clause.
        /// </summary>
        public string Operation
        {
            get
            {
                return _operation == null ? "" : _operation;
            }
            set
            {
                _operation = value;
            }
        }

        private SqlParameterCollection _parameters;
        /// <summary>
        /// Gets the collection of <see cref="DbParameter"/> objects.
        /// </summary>
        protected override DbParameterCollection DbParameterCollection
        {
            get
            {
                if (_parameters == null) _parameters = new SqlParameterCollection(this);
                return _parameters;
            }
        }

        /// <summary>
        /// Gets the SQL statement associated with a command.
        /// </summary>
        /// <returns>The SQL statement associated with a command.</returns>
        public override string ToString()
        {
            return _command.CommandText;
        }
    }
}