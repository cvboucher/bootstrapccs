//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using InMotion.Common;
using System.Data;

namespace InMotion.Data
{
    /// <summary>
    /// Represents a where parameter of table command for using with <see cref="DataCommand"/>.
    /// </summary>
    public class WhereParameter : DataParameter
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WhereParameter"/> class. 
        /// </summary>
        public WhereParameter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WhereParameter"/> class that uses 
        /// the parameter name and the value of the new <see cref="WhereParameter"/>
        /// </summary>
        /// <param name="name">The name of the parameter to map.</param>
        /// <param name="value">The value of the new <see cref="WhereParameter"/> object.</param>
        /// <param name="op">One of the <see cref="WhereParameterOperation"/> values.</param>
        /// <param name="condition">One of the <see cref="WhereParameterCondition"/> values.</param>
        public WhereParameter(String name, Object value, WhereParameterOperation op, WhereParameterCondition condition)
            : this(name, value, op, condition, "", "")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WhereParameter"/> class that uses 
        /// the parameter name and the value of the new <see cref="WhereParameter"/>
        /// </summary>
        /// <param name="name">The name of the parameter to map.</param>
        /// <param name="value">The value of the new <see cref="WhereParameter"/> object.</param>
        /// <param name="op">One of the <see cref="WhereParameterOperation"/> values.</param>
        /// <param name="condition">One of the <see cref="WhereParameterCondition"/> values.</param>
        /// <param name="prefix">Prefix part of SQL expression like a opening bracket.</param>
        /// <param name="postfix">Postfix part of SQL expression like a closing bracket.</param>
        public WhereParameter(String name, Object value, WhereParameterOperation op, WhereParameterCondition condition, string prefix, string postfix)
            : base(name, value)
        {
            Condition = condition;
            Operation = op;
            Prefix = prefix;
            Postfix = postfix;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WhereParameter"/> class 
        /// that uses the parameter name and data type. 
        /// </summary>
        /// <param name="name">The name of the parameter to map.</param>
        /// <param name="type">One of the <see cref="DataType"/> values.</param>
        /// <param name="sourceColumn">The name of the data source or source column.</param>
        /// <param name="op">One of the <see cref="WhereParameterOperation"/> values.</param>
        /// <param name="condition">One of the <see cref="WhereParameterCondition"/> values.</param>
        public WhereParameter(String name, DataType type, string sourceColumn, WhereParameterOperation op, WhereParameterCondition condition)
            : this(name, type, sourceColumn, op, condition, "", "")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WhereParameter"/> class 
        /// that uses the parameter name and data type. 
        /// </summary>
        /// <param name="name">The name of the parameter to map.</param>
        /// <param name="type">One of the <see cref="DataType"/> values.</param>
        /// <param name="sourceColumn">The name of the data source or source column.</param>
        /// <param name="op">One of the <see cref="WhereParameterOperation"/> values.</param>
        /// <param name="condition">One of the <see cref="WhereParameterCondition"/> values.</param>
        /// <param name="prefix">Prefix part of SQL expression like a opening bracket.</param>
        /// <param name="postfix">Postfix part of SQL expression like a closing bracket.</param>
        public WhereParameter(String name, DataType type, string sourceColumn, WhereParameterOperation op, WhereParameterCondition condition, string prefix, string postfix)
            : base(name, type, 0, sourceColumn)
        {
            Condition = condition;
            Operation = op;
            Prefix = prefix;
            Postfix = postfix;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WhereParameter"/> class that uses the parameter name, data type,
        /// length, source column name, parameter direction, and other properties.
        /// </summary>
        /// <param name="op">One of the <see cref="WhereParameterOperation"/> values.</param>
        /// <param name="condition">One of the <see cref="WhereParameterCondition"/> values.</param>
        /// <param name="prefix">Prefix part of SQL expression like a opening bracket.</param>
        /// <param name="postfix">Postfix part of SQL expression like a closing bracket.</param>
        /// <param name="name">The name of the parameter to map.</param>
        /// <param name="type">One of the <see cref="DataType"/> values.</param>
        /// <param name="size">The length of the parameter.</param>
        /// <param name="sourceColumn">The name of the data source or source column.</param>
        public WhereParameter(String name, DataType type, int size, string sourceColumn, WhereParameterOperation op, WhereParameterCondition condition, string prefix, string postfix)
            : base(name, type, size, ParameterDirection.Input, sourceColumn, DataRowVersion.Default, true, null)
        {
            Condition = condition;
            Operation = op;
            Prefix = prefix;
            Postfix = postfix;
        }

        #endregion

        #region Properties
        private WhereParameterCondition _condition;
        /// <summary>
        /// Get or sets the <see cref="WhereParameterCondition"/> operation that will be used for building parameter.
        /// </summary>
        public WhereParameterCondition Condition
        {
            get { return _condition; }
            set { _condition = value; }
        }

        private WhereParameterOperation _operation;
        /// <summary>
        /// Gets or sets the <see cref="WhereParameterOperation"/> that will be used for logical join parameter with following.
        /// </summary>
        public WhereParameterOperation Operation
        {
            get { return _operation; }
            set { _operation = value; }
        }

        private bool _UseIsNullIfEmpty;
        /// <summary>
        /// Gets or sets a value indicating whether to use IS NULL comparison if the parameter value is empty.
        /// </summary>
        public bool UseIsNullIfEmpty
        {
            get { return _UseIsNullIfEmpty; }
            set { _UseIsNullIfEmpty = value; }
        }

        private bool __IsExpression;
        /// <summary>
        /// Gets or sets a value indicating whether parameter value is SQL expression.
        /// </summary>
        public bool IsExpression
        {
            get { return __IsExpression; }
            set { __IsExpression = value; }
        }

        private string _prefix;

        /// <summary>
        /// Prefix part of SQL expression like a opening bracket.
        /// </summary>
        public string Prefix
        {
            get { return _prefix; }
            set { _prefix = value; }
        }

        private string _postfix;

        /// <summary>
        /// Postfix part of SQL expression like a closing bracket.
        /// </summary>
        public string Postfix
        {
            get { return _postfix; }
            set { _postfix = value; }
        }
        #endregion

        /// <summary>
        /// Returns a string representation of parameter for inserting into a SQL statement.
        /// </summary>
        /// <returns>String that represent the parameter.</returns>
        public override string ToString()
        {
            string result = "";
            if (IsExpression) return Text;
            Connection conn = (Connection)Owner.Connection;
            if (Value == null || Value == DBNull.Value || Value is IMTField && ((IMTField)Value).IsNull)
            {
                if (UseIsNullIfEmpty) result = SourceColumn + " IS NULL";
                return result;
            }
            string wildcard = (Type == DataType.Text | Type == DataType.Memo) ? "%" : "";
            string val = "";
            string val1 = "";
            string qprefix = "";
            string qpostfix = "";
            if (Condition == WhereParameterCondition.In || Condition == WhereParameterCondition.NotIn)
            {
                for (int i = 0; i < Values.Length; i++)
                {
                    string temp_res = ToString(i);
                    EscapeValue(conn, ref temp_res, ref qprefix, ref qpostfix);
                    val += qprefix + temp_res + qpostfix + ",";

                }
                val = val.TrimEnd(',');
            }
            else if (Condition == WhereParameterCondition.Between || Condition == WhereParameterCondition.NotBetween)
            {
                val = ToString(0);
                EscapeValue(conn, ref val, ref qprefix, ref qpostfix);
                val = qprefix + val + qpostfix;
                if (IsMultiple)
                {
                    val1 = ToString(1);
                    EscapeValue(conn, ref val1, ref qprefix, ref qpostfix);
                    val1 = qprefix + val1 + qpostfix;
                }
                else
                    val1 = val;

            }
            else
            {
                val = ToString(0);
                EscapeValue(conn, ref val, ref qprefix, ref qpostfix);
            }


            switch (Condition)
            {
                case WhereParameterCondition.Equal:
                    result = SourceColumn + " = " + qprefix + val + qpostfix;
                    break;
                case WhereParameterCondition.NotEqual:
                    result = SourceColumn + " <> " + qprefix + val + qpostfix;
                    break;
                case WhereParameterCondition.LessThan:
                    result = SourceColumn + " < " + qprefix + val + qpostfix;
                    break;
                case WhereParameterCondition.GreaterThan:
                    result = SourceColumn + " > " + qprefix + val + qpostfix;
                    break;
                case WhereParameterCondition.LessThanOrEqual:
                    result = SourceColumn + " <= " + qprefix + val + qpostfix;
                    break;
                case WhereParameterCondition.GreaterThanOrEqual:
                    result = SourceColumn + " >= " + qprefix + val + qpostfix;
                    break;
                case WhereParameterCondition.BeginsWith:
                    result = SourceColumn + " like " + qprefix + val + wildcard + qpostfix;
                    break;
                case WhereParameterCondition.NotBeginsWith:
                    result = SourceColumn + " not like " + qprefix + val + wildcard + qpostfix;
                    break;
                case WhereParameterCondition.EndsWith:
                    result = SourceColumn + " like " + qprefix + wildcard + val + qpostfix;
                    break;
                case WhereParameterCondition.NotEndsWith:
                    result = SourceColumn + " not like " + qprefix + wildcard + val + qpostfix;
                    break;
                case WhereParameterCondition.Contains:
                    result = SourceColumn + " like " + qprefix + wildcard + val + wildcard + qpostfix;
                    break;
                case WhereParameterCondition.NotContains:
                    result = SourceColumn + " not like " + qprefix + wildcard + val + wildcard + qpostfix;
                    break;
                case WhereParameterCondition.NotNull:
                    result = SourceColumn + " IS NOT NULL";
                    break;
                case WhereParameterCondition.IsNull:
                    result = SourceColumn + " IS NULL";
                    break;
                case WhereParameterCondition.In:
                    result = SourceColumn + " IN(" + val + ")";
                    break;
                case WhereParameterCondition.NotIn:
                    result = SourceColumn + " NOT IN(" + val + ")";
                    break;
                case WhereParameterCondition.Between:
                    result = SourceColumn + " BETWEEN " + val + " AND " + val1;
                    break;
                case WhereParameterCondition.NotBetween:
                    result = SourceColumn + " NOT BETWEEN " + val + " AND " + val1;
                    break;
            }


            return result;
        }

        private void EscapeValue(Connection conn, ref string val, ref string qprefix, ref string qpostfix)
        {
            if (conn.Server == "MSSQLServer")
            {
                qprefix = "N'";
                qpostfix = "'";
            }
            else
            {
                qprefix = "'";
                qpostfix = "'";
            }
            switch (Type)
            {
                case DataType.Text:
                case DataType.Memo:
                    if (conn.Server == "MySQL")
                        val = val.Replace(@"\", @"\\");
                    val = val.Replace("'", "''");
                    break;
                case DataType.Integer:
                case DataType.Float:
                case DataType.Single:
                    val = val.Replace(",", ".");
                    qprefix = "";
                    qpostfix = "";
                    break;
                case DataType.Date:
                    qprefix = conn.DateLeftDelimiter;
                    qpostfix = conn.DateRightDelimiter;
                    break;
                case DataType.Boolean:
                    qprefix = "";
                    qpostfix = "";
                    break;

            }
        }
    }
}
