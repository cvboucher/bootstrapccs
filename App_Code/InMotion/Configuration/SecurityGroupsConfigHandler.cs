//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Xml;
using System.Configuration;
using System.Collections.Generic;
using InMotion.Security;

namespace InMotion.Configuration
{
    /// <summary>
    /// This class support framework infrastructure and is not 
    /// intended to be used directly from your code. 
    /// </summary>
    public class SecurityGroupsConfigHandler : IConfigurationSectionHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityGroupsConfigHandler"/> class.
        /// </summary>
        public SecurityGroupsConfigHandler() { }

        /// <summary>
        /// Creates a configuration section handler.
        /// </summary>
        /// <param name="parent">Parent object.</param>
        /// <param name="configContext">Configuration context object.</param>
        /// <param name="section">Section XML node.</param>
        /// <returns>The created section handler object.</returns>
        public object Create(object parent, object configContext, XmlNode section)
        {
            Dictionary<int, SecurityGroup> groups = new Dictionary<int, SecurityGroup>();
            foreach (XmlNode node in section.SelectNodes("*"))
            {
							int gid = 0;
							bool result = Int32.TryParse(node.SelectSingleNode("@id").Value, out gid);
							SecurityGroup sg = new SecurityGroup((result ? gid : 0), node.SelectSingleNode("@name").Value);
                groups.Add(sg.Id, sg);
            }
            return groups;
        }
    }
}