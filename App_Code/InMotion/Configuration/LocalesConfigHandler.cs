//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Xml;
using System.Configuration;
using System.Collections.Generic;
using InMotion.Globalization;

namespace InMotion.Configuration
{
    /// <summary>
    /// This class support framework infrastructure and is not 
    /// intended to be used directly from your code. 
    /// Handles locales sections that are represented by a 
    /// locale section under locales sectionGroup in the .config file.
    /// </summary>
    public class LocalesConfigHandler : IConfigurationSectionHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LocalesConfigHandler"/> class.
        /// </summary>
        public LocalesConfigHandler() { }

        /// <summary>
        /// Creates a configuration section handler.
        /// </summary>
        /// <param name="parent">Parent object.</param>
        /// <param name="configContext">Configuration context object.</param>
        /// <param name="section">Section XML node.</param>
        /// <returns>The created section handler object.</returns>
        public object Create(object parent, object configContext, XmlNode section)
        {
            Dictionary<string, MTCultureInfo> locales = new Dictionary<string, MTCultureInfo>();
            foreach (XmlNode node in section.SelectNodes("*"))
            {
                MTCultureInfo ci = new MTCultureInfo(node.SelectSingleNode("@name").Value);
                locales.Add(node.SelectSingleNode("@language").Value + (String.IsNullOrEmpty(node.SelectSingleNode("@country").Value) ? "" : ("-" + node.SelectSingleNode("@country").Value)), ci);
                ci.BooleanFormat = node.SelectSingleNode("@booleanFormat").Value;
                ci.DefaultCountry = node.SelectSingleNode("@defaultCountry").Value;
                ci.Encoding = node.SelectSingleNode("@encoding").Value;
                if (node.SelectSingleNode("@weekdayShortNames") != null)
                    ci.DateTimeFormat.AbbreviatedDayNames = node.SelectSingleNode("@weekdayShortNames").Value.Split(new char[] { ';' });
                if (node.SelectSingleNode("@weekdayNarrowNames") != null)
                    ci.WeekdayNarrowNames = node.SelectSingleNode("@weekdayNarrowNames").Value.Split(new char[] { ';' });
                if (node.SelectSingleNode("@weekdayNames") != null)
                    ci.DateTimeFormat.DayNames = node.SelectSingleNode("@weekdayNames").Value.Split(new char[] { ';' });
                if (node.SelectSingleNode("@monthShortNames") != null)
                    ci.DateTimeFormat.AbbreviatedMonthNames = node.SelectSingleNode("@monthShortNames").Value.Split(new char[] { ';' });
                if (node.SelectSingleNode("@monthNames") != null)
                    ci.DateTimeFormat.MonthNames = node.SelectSingleNode("@monthNames").Value.Split(new char[] { ';' });
                if (node.SelectSingleNode("@shortDate") != null)
                    ci.DateTimeFormat.ShortDatePattern = node.SelectSingleNode("@shortDate").Value;
                if (node.SelectSingleNode("@shortTime") != null)
                    ci.DateTimeFormat.ShortTimePattern = node.SelectSingleNode("@shortTime").Value;
                if (node.SelectSingleNode("@longDate") != null)
                    ci.DateTimeFormat.LongDatePattern = node.SelectSingleNode("@longDate").Value;
                if (node.SelectSingleNode("@longTime") != null)
                    ci.DateTimeFormat.LongTimePattern = node.SelectSingleNode("@longTime").Value;
                if (node.SelectSingleNode("@firstWeekDay") != null)
                    ci.DateTimeFormat.FirstDayOfWeek = (System.DayOfWeek)Int16.Parse(node.SelectSingleNode("@firstWeekDay").Value);

                if (node.SelectSingleNode("@decimalDigits") != null)
                    ci.NumberFormat.NumberDecimalDigits = int.Parse(node.SelectSingleNode("@decimalDigits").Value);
                if (node.SelectSingleNode("@decimalSeparator") != null)
                    ci.NumberFormat.NumberDecimalSeparator = node.SelectSingleNode("@decimalSeparator").Value;
                if (node.SelectSingleNode("@groupSeparator") != null)
                    ci.NumberFormat.NumberGroupSeparator = node.SelectSingleNode("@groupSeparator").Value;
                /*if(node.SelectSingleNode("@groupSizes")!=null)
                    ci.NumberFormat.NumberGroupSizes = node.SelectSingleNode("@groupSizes").Value.Split(new char[]{';'});*/
                if (node.SelectSingleNode("@zeroFormat") != null)
                    ci.NumberZeroFormat = node.SelectSingleNode("@zeroFormat").Value;
                if (node.SelectSingleNode("@nullFormat") != null)
                    ci.NumberNullFormat = node.SelectSingleNode("@nullFormat").Value;
            }
            return locales;
        }
    }

}
