//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace InMotion.Configuration
{
    /// <summary>
    /// Contains a collection of <see cref="ConnectionSettings"/> objects.
    /// </summary>
    public sealed class ConnectionSettingsCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// Creates a new instance of a ConnectionSettingsCollection class.
        /// </summary>
        public ConnectionSettingsCollection()
        {
            ConnectionSettings conn = (ConnectionSettings)CreateNewElement();
            Add(conn);
        }

        /// <summary>
        /// Gets the type of the ConfigurationElementCollection.
        /// </summary>
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }
        /// <summary>
        /// Create the new empty <see cref="ConfigurationElement"/> object.
        /// </summary>
        /// <returns>A ConfigurationElement object.</returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new ConnectionSettings();
        }
        /// <summary>
        /// Gets the element key for a specified configuration element
        /// </summary>
        /// <param name="element">The <see cref="ConfigurationElement"/> to return the key for. </param>
        /// <returns>An <see cref="Object"/> that acts as the key for the specified <b>ConfigurationElement</b>.</returns>
        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((ConnectionSettings)element).Name;
        }

        /// <summary>
        /// Gets or sets the ConnectionSettings object 
        /// at the specified index in the collection.
        /// </summary>
        /// <param name="index">The index of an element in the collection.</param>
        /// <returns>The ConnectionSettings object at the specified index. </returns>
        public ConnectionSettings this[int index]
        {
            get
            {
                return (ConnectionSettings)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }
        /// <summary>
        /// Gets or sets the ConnectionStringSettings object with the specified 
        /// name in the collection. 
        /// </summary>
        /// <param name="name">The name of a ConnectionSettings object in the collection.</param>
        /// <returns>The ConnectionSettings object with the specified name.</returns>
        new public ConnectionSettings this[string name]
        {
            get
            {
                return (ConnectionSettings)BaseGet(name);
            }
        }

        /// <summary>
        /// Returns the index in the collection of the passed 
        /// ConnectionSettings object. 
        /// </summary>
        /// <param name="connection">A ConnectionSettings object in the collection.</param>
        /// <returns>The collection index of the specified 
        /// ConnectionSettingsCollection object. </returns>
        public int IndexOf(ConnectionSettings connection)
        {
            return BaseIndexOf(connection);
        }
        /// <summary>
        /// Adds a ConnectionSettings object to the collection. 
        /// </summary>
        /// <param name="connection">The <b>ConnectionSettings</b> to add.</param>
        public void Add(ConnectionSettings connection)
        {
            BaseAdd(connection);
        }
        /// <summary>
        /// Adds a configuration element to the <see cref="ConfigurationElementCollection"/>. 
        /// </summary>
        /// <param name="element">The <b>ConfigurationElement</b> to add.</param>
        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }
        /// <summary>
        /// Removes a ConnectionSettings object from the collection. 
        /// </summary>
        /// <param name="connection">The <b>ConnectionSettings</b> to remove.</param>
        public void Remove(ConnectionSettings connection)
        {
            if (connection != null && BaseIndexOf(connection) >= 0)
                BaseRemove(connection.Name);
        }
        /// <summary>
        /// Removes the ConnectionSettings object at the 
        /// specified index in the collection. 
        /// </summary>
        /// <param name="index">The index location of the <b>ConnectionSettings</b> to remove.</param>
        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        /// <summary>
        /// Removes the specified ConnectionSettings 
        /// object from the collection. 
        /// </summary>
        /// <param name="name">The name of the <b>ConnectionSettings</b> to remove.</param>
        public void Remove(string name)
        {
            BaseRemove(name);
        }

        /// <summary>
        /// Removes all the ConnectionSettings 
        /// objects from the collection. 
        /// </summary>
        public void Clear()
        {
            BaseClear();
        }

    }
}