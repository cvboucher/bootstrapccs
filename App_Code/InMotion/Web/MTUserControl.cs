//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using InMotion.Resources;
using InMotion.Configuration;
using InMotion.Web.Controls;
using InMotion.Security;
using System.Collections.Specialized;

namespace InMotion.Web
{
    /// <summary>
    /// Represents a InMotion extension of <see cref="UserControl"/> class.
    /// </summary>
    public class MTUserControl : UserControl, IMTControlContainer
    {
        /// <summary>
        /// Occurs after the control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;
        /// <summary>
        /// Occurs after handling submitted data and before loading templates for control showing.
        /// </summary>
        public event EventHandler<EventArgs> InitializeView;
        /// <summary>
        /// Occurs after all components have been initialized, before processing submitted data or showing components.
        /// </summary>
        public event EventHandler<EventArgs> AfterInitialize;
        /// <summary>
        /// Occurs after control template is rendered and before output content to the client.
        /// </summary>
        public event EventHandler<EventArgs> BeforeOutput;
        /// <summary>
        /// Occurs before disposing controls components objects, after control output is completed.
        /// </summary>
        /// <remarks>
        /// It can be used for defining code that should be executed just before the control execution is complete. Another application is closing additional database connections or freeing other custom resources.
        /// </remarks>
        public event EventHandler<EventArgs> BeforeUnload;
        /// <summary>
        /// Occurs before caching controls components objects.
        /// </summary>
        public event EventHandler<EventArgs> Caching;
        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }
        /// <summary>
        /// Find all child controls of type <i>T</i>.
        /// </summary>
        /// <typeparam name="T">The type of controls to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <returns>the <see cref="ReadOnlyCollection&lt;T&gt;"/></returns>
        public ReadOnlyCollection<T> GetControls<T>()
            where T : IMTControl
        {
            List<T> result = new List<T>();
            ControlsHelper.PopulateControlCollection<T>(result, Controls);
            return new ReadOnlyCollection<T>(result);
        }

        /// <summary>
        /// Find a control of type <i>T</i> with specified <see cref="Control.ID"/>
        /// </summary>
        /// <typeparam name="T">The type of control to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>T</i>.</returns>
        public T GetControl<T>(string id)
            where T : Control
        {
            if (Controls.Count == 0) return null;
            Control res = this.FindControl(id);
            if (res != null)
                return (T)res;
            else
                return (T)FindControlIterative(id);
        }

        private Control FindControlIterative(string id)
        {
            Control ctl = this;
            LinkedList<Control> controls = new LinkedList<Control>();
            while (ctl != null)
            {
                if (ctl.ID == id)
                    return ctl;
                foreach (Control child in ctl.Controls)
                {
                    if (child.ID == id)
                        return child;
                    if (child.HasControls())
                        controls.AddLast(child);
                }
                ctl = controls.First.Value;
                controls.Remove(ctl);
            }
            return null;
        }
        /// <summary>
        /// Find a control with specified <see cref="Control.ID"/>
        /// </summary>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>Control</i></returns>
        public Control GetControl(string id)
        {
            return GetControl<Control>(id);
        }

        private FormSupportedOperations __UserRights;
        /// <summary>
        /// Gets the <see cref="FormSupportedOperations"/> for the current control.
        /// </summary>
        public FormSupportedOperations UserRights
        {
            get
            {
                if (__UserRights == null)
                    __UserRights = new FormSupportedOperations();
                return __UserRights;
            }
        }

        private NameValueCollection __variables;

        /// <summary>
        /// Gets the <see cref="NameValueCollection"/> of string variables for using in ASPX.
        /// </summary>
        public NameValueCollection Variables
        {
            get
            {
                if (__variables == null)
                    __variables = new NameValueCollection();
                return __variables;
            }
        }

        private string _accessDeniedPage;
        /// <summary>
        /// Get or sets the url of on which the unauthorized user is redirected. 
        /// </summary>
        public string AccessDeniedPage
        {
            get { return _accessDeniedPage; }
            set { _accessDeniedPage = value; }
        }
        
        private bool _restricted;
        /// <summary>
        /// Gets or sets the value indicating whether control will restrict the user access according to the <see cref="UserRights"/>.
        /// </summary>
        public bool Restricted
        {
            get
            {
                return _restricted;
            }
            set
            {
                _restricted = value;
            }
        }

        /// <summary>
        /// Gets or sets the value that indicating whether client script support is enabled for container page.
        /// </summary>
        public bool ScriptingSupport
        {
            get
            {
                return (this.Page is MTPage) && ((MTPage)this.Page).ScriptingSupport;
            }
            set
            {
                if (this.Page is MTPage) ((MTPage)this.Page).ScriptingSupport = value;
            }
        }

        private string _scriptIncludes = "";

        public string ScriptIncludes
        {
            get { return _scriptIncludes; }
            set
            {
                _scriptIncludes = value;
                string res = ((MTPage) Page).ScriptIncludes;
                string[] scripts = _scriptIncludes.Split('|');
                for (int i = 0; i < scripts.Length; i++)
                    if (scripts[i] != "" && res.IndexOf("|" + scripts[i] + "|") == -1)
                        res += scripts[i] + "|";
                ((MTPage) Page).ScriptIncludes = res;
            }
        }

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Raises the <see cref="AfterInitialize"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnAfterInitialize(EventArgs e)
        {
            if (AfterInitialize != null)
                AfterInitialize(this, e);
        }

        /// <summary>
        /// Raises the <see cref="InitializeView"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnInitializeView(EventArgs e)
        {
            if (InitializeView != null)
                InitializeView(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeOutput"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeOutput(EventArgs e)
        {
            if (BeforeOutput != null)
                BeforeOutput(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeUnload"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeUnload(EventArgs e)
        {
            if (BeforeUnload != null)
                BeforeUnload(this, e);
        }

        /// <summary>
        /// Raises the <see cref="Caching"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnCaching(EventArgs e)
        {
            if (Caching != null)
                Caching(this, e);
        }
        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            if (!Visible) return;
            base.OnPreRender(e);
            OnBeforeOutput(new EventArgs());
        }

        /// <summary>
        /// Raises the Unload event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnUnload(EventArgs e)
        {
            if (!Visible) return;
            OnBeforeUnload(new EventArgs());
            base.OnUnload(e);
        }

        /// <summary>
        /// Raises the Load event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            if (!Visible) return;
            OnCaching(new EventArgs());
            if (string.IsNullOrEmpty(Page.MasterPageFile))
                base.OnLoad(e);
            OnBeforeShow(new EventArgs());
        }

        /// <summary>
        /// Gets the reference on the <see cref="MTResourceManager"/> instance associated with this control.
        /// </summary>
        public MTResourceManager ResManager
        {
            get { return Common.Resources.ResManager; }
        }

        /// <summary>
        /// Raises the Init event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            if (Page.FindControl("___head_link_panel") != null)
            {
                MTPanel headLinkPanel = (MTPanel) Page.FindControl("___head_link_panel");
                for (int i=0; i<Controls.Count;i++)
                    if (Controls[i] is MTPanel && !string.IsNullOrEmpty(Controls[i].ID) && Controls[i].ID.StartsWith("___link_panel_"))
                        headLinkPanel.Controls.Add(Controls[i]);
            }
            base.OnInit(e);

            this.Visible = !(Restricted && !UserRights.AllowRead);
            if (Restricted && !UserRights.AllowRead)
            {
                if (!String.IsNullOrEmpty(AccessDeniedPage))
                {
                    Url u = new Url(this.ResolveClientUrl(AccessDeniedPage));
                    u.Parameters.Add("ReturnUrl", Request.RawUrl);
                    Response.Redirect(u.ToString());
                }
                else
                     FormsAuthentication.RedirectToLoginPage();
                HttpContext.Current.Response.End();
                return;
            }

            OnAfterInitialize(EventArgs.Empty);

            OnInitializeView(EventArgs.Empty);
        }
        /// <summary>
        /// Gets or sets the name of current style that used by the container page.
        /// </summary>
        public virtual string StyleName
        {
            get
            {
                if (Page is MTPage)
                {
                    return (Page as MTPage).StyleName;
                }
                else
                {
                    if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                        System.Diagnostics.Trace.TraceError("The StyleName is avaiable only into MTPage class.\n{0}", Environment.StackTrace);
                    throw new NotSupportedException("The StyleName is avaiable only into MTPage class");
                }
            }
            set
            {
                if (Page is MTPage)
                {
                    (Page as MTPage).StyleName = value;
                }
                else
                {
                    if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                        System.Diagnostics.Trace.TraceError("The StyleName is avaiable only into MTPage class.\n{0}", Environment.StackTrace);
                    throw new NotSupportedException("The StyleName is avaiable only into MTPage class");
                }
            }

        }

        public string PathToCurrentPage
        {
            get
            {
                Control parentPanel = this.Parent;
                while (!(parentPanel is MTPage))
                {
                    if (parentPanel is MTPanel && !string.IsNullOrEmpty(((MTPanel)parentPanel).MasterPageFile))
                        return this.Page.ResolveClientUrl(((MTPanel)parentPanel).PathToMasterPage);
                    parentPanel = parentPanel.Parent;
                }
                return "";
            }
        }

        public string PathToMasterPage
        {
            get
            {
                string res = PathToCurrentPage;
                if (string.IsNullOrEmpty(res) && Page is MTPage)
                    res = ((MTPage)Page).PathToMasterPage;
                return res;
            }
        }
    }
}
