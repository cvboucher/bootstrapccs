//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Collections.Specialized;
using System.Collections;
using System.Diagnostics;
using InMotion.Configuration;

namespace InMotion.Web
{
    /// <summary>
    /// Defines methods for managing client-side scripts in Web applications.
    /// </summary>
    public sealed class MTClientScriptManager
    {
        private Page _owner;
        private IDictionary _registeredArrayDeclares;
        private StringBuilder _code = new StringBuilder();
        public StringBuilder Code
        {
            get 
            {
                _code = _code.Remove(0, _code.Length);
                IDictionaryEnumerator enumArrays = this._registeredArrayDeclares.GetEnumerator();
                while (enumArrays.MoveNext())
                {
                    _code.Append("var ");
                    _code.Append(enumArrays.Key);
                    _code.Append(" =  new Array(");
                    IEnumerator enumArrayValues = ((ArrayList)enumArrays.Value).GetEnumerator();
                    bool flag1 = true;
                    while (enumArrayValues.MoveNext())
                    {
                        if (flag1)
                        {
                            flag1 = false;
                        }
                        else
                        {
                            _code.Append(", ");
                        }
                        _code.Append(enumArrayValues.Current);
                    }
                    _code.AppendLine(");");
                }
                return _code; 
            }
        }
        /// <summary>
        /// Initializes the new instance of the <see cref="MTClientScriptManager"/>.
        /// </summary>
        /// <param name="owner">The owner Page.</param>
        public MTClientScriptManager(Page owner)
        {
            _owner = owner;
            _owner.PreRenderComplete += new EventHandler(OnPageRender);
        }

        private void OnPageRender(object sender, EventArgs e)
        {
            StringBuilder code = new StringBuilder();
            if ((_owner is MTPage) && ((MTPage)_owner).ScriptingSupport == false) return;
            if ((this._registeredArrayDeclares != null) && (this._registeredArrayDeclares.Count != 0))
            {
                //code.Append("\r\n<script type=\"text/javascript\">\r\n<!--\r\n");
                code = Code;
                //code.Append("// -->\r\n</script>\r\n");
            }
			if (!AppConfig.UseJQuery)
			{
				if (!_owner.ClientScript.IsClientScriptIncludeRegistered("InMotionPageHelper"))
					_owner.ClientScript.RegisterClientScriptInclude("InMotionPageHelper", _owner.ResolveClientUrl("~/InMotionPageHelper.js"));
				if (!_owner.ClientScript.IsClientScriptIncludeRegistered("CommonScript"))
				{
					if (_owner is MTPage)
						_owner.ClientScript.RegisterClientScriptInclude("CommonScript", _owner.ResolveClientUrl("~/ClientI18N.aspx?file=Functions.js&locale=" + ((MTPage)_owner).ResManager.GetString("CCS_LocaleID")));
					else
						_owner.ClientScript.RegisterClientScriptInclude("CommonScript", _owner.ResolveClientUrl("~/ClientI18N.aspx?file=Functions.js&locale=en"));
				}
			}
			
            _owner.ClientScript.RegisterClientScriptBlock(_owner.GetType(), _owner.UniqueID, code.ToString(), true);
        }

        /// <summary>
        /// Registers a JavaScript array declaration with the Page object using an array name and array value.
        /// </summary>
        /// <param name="arrayName">The array name to register.</param>
        /// <param name="arrayValue">The array value or values to register.</param>
        public void RegisterArrayDeclaration(string arrayName, string arrayValue)
        {
            if (arrayName == null)
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("Argument 'arrayName' can not be Null.\n{0}", Environment.StackTrace);
                throw new ArgumentNullException("arrayName");
            }
            if (this._registeredArrayDeclares == null)
            {
                this._registeredArrayDeclares = new ListDictionary();
            }
            if (!this._registeredArrayDeclares.Contains(arrayName))
            {
                this._registeredArrayDeclares[arrayName] = new ArrayList();
            }
            ArrayList list1 = (ArrayList)this._registeredArrayDeclares[arrayName];
            list1.Add(arrayValue);
        }
    }
}
