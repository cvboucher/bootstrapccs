//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using InMotion.Resources;
using InMotion.Configuration;
using System.Web;
using InMotion.Globalization;
using InMotion.Web.Controls;
using InMotion.Security;
using System.Web.Security;
using System.Collections.ObjectModel;
using System.Security.Permissions;
using System.Collections.Specialized;
using System.ComponentModel;

namespace InMotion.Web
{
    /// <summary>
    /// Represents a InMotion extension of <see cref="Page"/> class.
    /// </summary>
    public class MTMasterPage : MasterPage, IMTControlContainer
    {
        /// <summary>
        /// Occurs after the Page control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;

        /// <summary>
        /// Occurs after handling submitted data and before loading templates for page showing.
        /// </summary>
        public event EventHandler<EventArgs> InitializeView;

        /// <summary>
        /// Occurs after all page components have been initialized, before processing submitted data or showing components.
        /// </summary>
        /// <remarks>
        /// It can be used for defining code that should be executed before any other event code. It can be used on pages that don't contain any objects and are used only for processing data, without outputting the rendered template (e.g. for pages that should output binary data, like images or files). 
        /// </remarks>
        public event EventHandler<EventArgs> AfterInitialize;

        /// <summary>
        /// Occurs after page template is rendered and before output content to the client.
        /// </summary>
        public event EventHandler<EventArgs> BeforeOutput;

        /// <summary>
        /// Occurs before disposing pages components objects, after page output is completed.
        /// </summary>
        /// <remarks>
        /// It can be used for defining code that should be executed just before the page execution is complete. Another application is closing additional database connections or freeing other custom resources.
        /// </remarks>
        public event EventHandler<EventArgs> BeforeUnload;

        /// <summary>
        /// Occurs before caching page's components objects.
        /// </summary>
        public event EventHandler<EventArgs> Caching;

        /// <summary>
        /// Occurs before initialize of components on page.
        /// </summary>
        public event EventHandler<EventArgs> BeforeInitialize;

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Find all child controls of type <i>T</i>.
        /// </summary>
        /// <typeparam name="T">The type of controls to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <returns>the <see cref="ReadOnlyCollection&lt;T&gt;"/></returns>
        [SecurityPermissionAttribute(SecurityAction.Demand, ControlEvidence = true)]
        public ReadOnlyCollection<T> GetControls<T>()
            where T : IMTControl
        {
            List<T> result = new List<T>();
            ControlsHelper.PopulateControlCollection<T>(result, Controls);
            return new ReadOnlyCollection<T>(result);
        }

        /// <summary>
        /// Find a control of type <i>T</i> with specified <see cref="Control.ID"/>
        /// </summary>
        /// <typeparam name="T">The type of control to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>T</i>.</returns>
        public T GetControl<T>(string id)
            where T : Control
        {
            if (Controls.Count == 0) return null;
            return (T)this.FindControl(id);
        }

        /// <summary>
        /// Find a control with specified <see cref="Control.ID"/>
        /// </summary>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>Control</i></returns>
        public Control GetControl(string id)
        {
            return GetControl<Control>(id);
        }

        private FormSupportedOperations __UserRights;
        /// <summary>
        /// Gets the <see cref="FormSupportedOperations"/> for the current page.
        /// </summary>
        public FormSupportedOperations UserRights
        {
            get
            {
                if (__UserRights == null)
                    __UserRights = new FormSupportedOperations();
                return __UserRights;
            }
        }

        private NameValueCollection __variables;
        /// <summary>
        /// Gets the <see cref="NameValueCollection"/> of string variables for using in ASPX.
        /// </summary>
        public NameValueCollection Variables
        {
            get
            {
                if (__variables == null)
                    __variables = new NameValueCollection();
                return __variables;
            }
        }

/*        private MTClientScriptManager _MTClientScriptManager;
        /// <summary>
        /// Gets the reference on the <see cref="MTClientScriptManager"/> instance associated with this page.
        /// </summary>
        public MTClientScriptManager MTClientScript
        {
            get
            {
                if (this._MTClientScriptManager == null)
                {
                    this._MTClientScriptManager = new MTClientScriptManager(this);
                }
                return this._MTClientScriptManager;
            }
        }*/

        private bool _restricted;
        /// <summary>
        /// Gets or sets the value indicating whether page will restrict the user access according to the <see cref="UserRights"/>.
        /// </summary>
        public bool Restricted
        {
            get
            {
                return _restricted;
            }
            set
            {
                _restricted = value;
            }
        }

        private bool __SSLOnly;
        /// <summary>
        /// Gets or sets the value indicating whether page can be accessed via SSL connection only.
        /// </summary>
        public bool IsSSLAccessOnly
        {
            get { return __SSLOnly; }
            set { __SSLOnly = value; }
        }

        /// <summary>
        /// Set value of attribute by the name.
        /// </summary>
        /// <param name="name">Name of attribute.</param>
        /// <param name="value">Value of attribute.</param>
        public void SetAttribute(String name, String value)
        {
            ViewState[name] = value;
        }

        /// <summary>
        /// Retrieve attribute value by the name.
        /// </summary>
        /// <param name="name">Name of attribute.</param>
        /// <returns>Value of attribute by the name.</returns>
        public String GetAttribute(String name)
        {
            return (String)ViewState[name];
        }

        private System.Web.UI.AttributeCollection _attributes = null;
        /// <summary>
        /// Gets the collection of arbitrary attributes (for rendering only) that do not correspond to properties on the control.
        /// </summary>
        public System.Web.UI.AttributeCollection Attributes
        {
            get
            {
                if (_attributes == null)
                    _attributes = new System.Web.UI.AttributeCollection(ViewState);
                return _attributes;
            }
        }

        private string _accessDeniedPage = "";
        /// <summary>
        /// Get or sets the url of on which the unauthorized user is redirected. 
        /// </summary>
        public string AccessDeniedPage
        {
            get { return _accessDeniedPage; }
            set { _accessDeniedPage = value; }
        }

/*        private bool _ScriptingSupport;
        /// <summary>
        /// Gets or sets the value that indicating whether client script support is enabled for this page.
        /// </summary>
        public bool ScriptingSupport
        {
            get
            {
                return _ScriptingSupport;
            }
            set
            {
                if (MTClientScript != null) _ScriptingSupport = value;
            }
        }*/

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Raises the <see cref="AfterInitialize"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnAfterInitialize(EventArgs e)
        {
            if (AfterInitialize != null)
                AfterInitialize(this, e);
        }

        /// <summary>
        /// Raises the <see cref="InitializeView"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnInitializeView(EventArgs e)
        {
            if (InitializeView != null)
                InitializeView(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeOutput"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeOutput(EventArgs e)
        {
            if (BeforeOutput != null)
                BeforeOutput(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeUnload"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeUnload(EventArgs e)
        {
            if (BeforeUnload != null)
                BeforeUnload(this, e);
        }

        /// <summary>
        /// Raises the <see cref="Caching"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnCaching(EventArgs e)
        {
            if (Caching != null)
                Caching(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeInitialize"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeInitialize(EventArgs e)
        {
            if (BeforeInitialize != null)
                BeforeInitialize(this, e);
        }

        /// <summary>
        /// Raises the Unload event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnUnload(EventArgs e)
        {
            OnBeforeUnload(new EventArgs());
            base.OnUnload(e);
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            ControlsHelper.RemoveMTAttributes(this.Controls);
            base.OnPreRender(e);
        }
        /*
        /// <summary>
        /// Raises the <see cref="BeforeOutput"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRenderComplete(EventArgs e)
        {
            base.OnPreRenderComplete(e);
            OnBeforeOutput(new EventArgs());
        }*/

        /// <summary>
        /// Raises the Load event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            OnCaching(new EventArgs());
            base.OnLoad(e);
            OnBeforeShow(new EventArgs());
        }

        /// <summary>
        /// Gets the reference on the <see cref="MTResourceManager"/> instance associated with this page.
        /// </summary>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public MTResourceManager ResManager
        {
            get { return Common.Resources.ResManager; }
        }

        private string _pageStyle = "";
        /// <summary>
        /// Gets or sets the name of current style that used by the page.
        /// </summary>
        public virtual string StyleName
        {
            get { return _pageStyle; }
            set { _pageStyle = value; }
        }

        private string m_ContentMeta = "";
        /// <summary>
        /// Gets or sets the value of Content meta-tag.
        /// </summary>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public string ContentMeta
        {
            get
            {
                if (String.IsNullOrEmpty(m_ContentMeta))
                    m_ContentMeta = "text/html; charset=" + ((MTCultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture).Encoding;
                return m_ContentMeta;
            }
            set { m_ContentMeta = value; }
        }

        private string _pathToMasterPage;

        public string PathToMasterPage
        {
            get
            {
                if (MasterPageFile == null)
                    MasterPageFile = "";
                if (_pathToMasterPage == null)
                    _pathToMasterPage = ResolveClientUrl(System.Text.RegularExpressions.Regex.Replace(this.MasterPageFile, @"\w*\.master$", ""));
                return _pathToMasterPage;
            }
        }
        /*
        /// <summary>
        /// Register a control instance in client script array.
        /// </summary>
        /// <param name="control">The control instance to register.</param>
        public void RegisterClientControl(Control control)
        {
            MTClientScript.RegisterArrayDeclaration("MTControlsControls", String.Format("'{0}','{1}'", control.ID, control.ClientID));
        }
        */
        private void SetPageStyle()
        {
            if (!AppConfig.UseDynamicStyles) return;
            if (AppConfig.UseUrlStyleParameter && Request.QueryString[AppConfig.UrlStyleParameterName] != null)
                StyleName = Request.QueryString[AppConfig.UrlStyleParameterName];
            if (String.IsNullOrEmpty(StyleName) && AppConfig.UseCookieStyleParameter && Request.Cookies[AppConfig.CookieStyleParameterName] != null)
                StyleName = Request.Cookies[AppConfig.CookieStyleParameterName].Value;
            if (String.IsNullOrEmpty(StyleName) && AppConfig.UseSessionStyleParameter && Session[AppConfig.SessionStyleParameterName] != null)
                StyleName = Session[AppConfig.SessionStyleParameterName].ToString();

            if (AppConfig.UseCookieStyleParameter)
            {
                HttpCookie cookie = new HttpCookie(AppConfig.CookieStyleParameterName, StyleName);
                if (AppConfig.StyleCookieExpiration != 0)
                    cookie.Expires = DateTime.Now.AddDays(AppConfig.StyleCookieExpiration);
                Response.Cookies.Add(cookie);
            }
            if (AppConfig.UseSessionStyleParameter)
                Session[AppConfig.SessionStyleParameterName] = StyleName;

            StyleName = StyleName.Trim();
            if (StyleName.Length > 0)
            {
                string root = Server.MapPath("~/Styles/" + StyleName + "/Style.css");
                if (!System.IO.File.Exists(root))
                    StyleName = "";
            }
            if (String.IsNullOrEmpty(StyleName))
                StyleName = AppConfig.DefaultStyleName;
        }
/*
        /// <summary>
        /// Sets the page style, culture info and encoding..
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            SetPageStyle();
            MTCultureInfo ci = InMotion.Globalization.Utility.SetThreadCulture();
            Encoding enc = Encoding.GetEncoding(ci.Encoding);
            CodePage = enc.CodePage;
        }
        */
        /// <summary>
        /// Performs the security checks and raises the Init event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            OnBeforeInitialize(EventArgs.Empty);
            if (IsSSLAccessOnly && !Request.IsSecureConnection)
            {
                Response.Write("This page can be accessed only via secured connection.");
                Response.End();
            }

            if (Restricted && !UserRights.AllowRead)
            {
                if (!String.IsNullOrEmpty(AccessDeniedPage))
                {
                    Url u = new Url(AccessDeniedPage);
                    u.Parameters.Add("ReturnUrl", Request.RawUrl);
                    Response.Redirect(u.ToString());
                }
                else
                    FormsAuthentication.RedirectToLoginPage();
                HttpContext.Current.Response.End();
            }

            OnAfterInitialize(EventArgs.Empty);
        }
        /*
        /// <summary>
        /// Raises InitComplete event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnInitComplete(EventArgs e)
        {
            base.OnInitComplete(e);
            OnInitializeView(EventArgs.Empty);
        }*/
        public string PathToCurrentPage
        {
            get 
            {
                return ((MTPage)this.Parent).PathToMasterPage;
            }
        }
    }
}