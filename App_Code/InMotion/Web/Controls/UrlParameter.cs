//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using InMotion.Common;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Specifies the source type of a parameter
    /// </summary>
    public enum UrlParameterSourceType
    {
        /// <summary>
        /// Get the value of parameter from the HttpApplicationState collection.
        /// </summary>
        Application,
        /// <summary>
        /// Get the value of parameter from the specified page control.
        /// </summary>
        Control,
        /// <summary>
        /// Get the value of parameter from the client-side HTTP cookie.
        /// </summary>
        Cookie,
        /// <summary>
        /// Get the value of parameter from the Datasource Column.
        /// </summary>
        DataSourceColumn,
        /// <summary>
        /// Get the value of parameter from the Datasource Parameter.
        /// </summary>
        DataSourceParameter,
        /// <summary>
        /// Get the value of parameter from the custom code expression.
        /// </summary>
        Expression,
        /// <summary>
        /// Get the value of parameter from the HttpRequest.Forms collection.
        /// </summary>
        Form,
        /// <summary>
        /// Get the value of parameter from the HttpSessionState of the current session.
        /// </summary>
        Session,
        /// <summary>
        /// Get the value of parameter from the HttpRequest.QueryStrings collection.
        /// </summary>
        Url,
        /// <summary>
        /// Get the value of parameter from the collection of special values of owner form.
        /// </summary>
        SpecialValue
    }
    /// <summary>
    /// Represents a parameter of the <see cref="Url"/> class.
    /// </summary>
    [Serializable]
    public class UrlParameter : Parameter
    {
        /// <summary>
        /// Gets or sets the <see cref="UrlParameterSourceType"/> of the parameter.
        /// </summary>
        public UrlParameterSourceType SourceType
        {
            get
            {
                if (ViewState["__SourceType"] == null)
                    ViewState["__SourceType"] = UrlParameterSourceType.Expression;
                return (UrlParameterSourceType)ViewState["__SourceType"];
            }
            set { ViewState["__SourceType"] = value; }
        }

        private bool _IsPreserved;
        /// <summary>
        /// Shows that parameter is preserved and RemoveParameters filter will be applied.
        /// </summary>
        public bool IsPreserved
        {
            get { return _IsPreserved; }
            set { _IsPreserved = value; }
        }

        /// <summary>
        /// Updates and returns the value of the <see cref="UrlParameter"/> object.
        /// </summary>
        /// <returns>An object that represents the updated and current value of the parameter.</returns>
        public override object Evaluate()
        {
            switch (SourceType)
            {
                case UrlParameterSourceType.Application:
                    SetValue(HttpContext.Current.Application[Source]);
                    break;
                case UrlParameterSourceType.Control:
                    break;
                case UrlParameterSourceType.Cookie:
                    if (HttpContext.Current.Request.Cookies[Source] != null)
                        SetValue(HttpContext.Current.Request.Cookies[Source].Value);
                    else
                        SetValue(null);
                    break;
                case UrlParameterSourceType.DataSourceColumn:
                case UrlParameterSourceType.DataSourceParameter:
                case UrlParameterSourceType.SpecialValue:
                    break;
                case UrlParameterSourceType.Expression:
                    if (Source != null && Source.Length > 0) SetValue(Source);
                    break;
                case UrlParameterSourceType.Form:
                    SetValue(HttpContext.Current.Request.Form[Source]);
                    break;
                case UrlParameterSourceType.Session:
                    SetValue(HttpContext.Current.Session[Source]);
                    break;
                case UrlParameterSourceType.Url:
                    SetValue(HttpContext.Current.Request.QueryString[Source]);
                    break;
                default:
                    break;
            }
            if (Value == null) SetValue(DefaultValue);
            return Value;
        }


        internal void DataBind(IMTControl sender)
        {
            if (SourceType == UrlParameterSourceType.DataSourceColumn && Source.Length > 0 && sender.OwnerForm != null && sender.OwnerForm.DataItem.Value != null)
            {
                SetValue(DataSourceHelper.GetFieldValue(Source, sender.OwnerForm.DataItem.Value));
            }
            if (SourceType == UrlParameterSourceType.DataSourceParameter && Source.Length > 0 && sender.OwnerForm != null)
            {
                MTDataSourceView dv = sender.OwnerForm.GetDataSourceView() as MTDataSourceView;
                if (dv != null)
                {
                    SetValue(dv.SelectParameters[Source].Value);
                }
            }
            if (SourceType == UrlParameterSourceType.SpecialValue && Source.Length > 0)
            {
                ISpecialValueProvider provider = sender.OwnerForm as ISpecialValueProvider;
                if (sender.OwnerForm is Calendar && (Source == "CurrentProcessingDate" || Source == "NextProcessingDate" || Source == "PrevProcessingDate"))
                    DataType = DataType.Date;
                if (provider != null && provider.SpecialValues.ContainsKey(Source))
                    SetValue(provider.SpecialValues[Source]);
            }
        }
    }
}