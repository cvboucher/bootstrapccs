//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.Diagnostics;
using InMotion.Data;
using InMotion.Configuration;
using InMotion.Common;


namespace InMotion.Web.Controls
{
    /// <summary>
    /// Implements DataSourceView abstarct class. 
    /// Encapsulates the capabilities of the MTDataSource class.
    /// </summary>
    public class MTDataSourceView : DataSourceView, IStateManager
    {

        #region Events
        /// <summary>
        /// Occurs before the parameters collection of inner <see cref="DataCommand"/> populated.
        /// </summary>
        public event EventHandler InitializeSelectParameters;
        /// <summary>
        /// Occurs before the parameters collection of inner <see cref="DataCommand"/> populated.
        /// </summary>
        public event EventHandler InitializeInsertParameters;
        /// <summary>
        /// Occurs before the parameters collection of inner <see cref="DataCommand"/> populated.
        /// </summary>
        public event EventHandler InitializeUpdateParameters;
        /// <summary>
        /// Occurs before the parameters collection of inner <see cref="DataCommand"/> populated.
        /// </summary>
        public event EventHandler InitializeDeleteParameters;

        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for select operation has been created but before its parameters populated and initialized.
        /// </summary>
        public event EventHandler<SelectDataOperationEventArgs> BeforeBuildSelect;
        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for count rows operation has been created but before its parameters populated and initialized.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> BeforeBuildCount;
        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for insert operation has been created but before its parameters populated and initialized.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> BeforeBuildInsert;
        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for update operation has been created but before its parameters populated and initialized.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> BeforeBuildUpdate;
        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for delete operation has been created but before its parameters populated and initialized.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> BeforeBuildDelete;
        /// <summary>
        /// Occurs before the inner <see cref="DataCommand"/> for count rows operation is executed but after the query has been composed.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> BeforeExecuteCount;
        /// <summary>
        /// Occurs before the inner <see cref="DataCommand"/> for select operation is executed but after the query has been composed.
        /// </summary>
        public event EventHandler<SelectDataOperationEventArgs> BeforeExecuteSelect;
        /// <summary>
        /// Occurs before the inner <see cref="DataCommand"/> for update operation is executed but after the query has been composed.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> BeforeExecuteUpdate;
        /// <summary>
        /// Occurs before the inner <see cref="DataCommand"/> for insert operation is executed but after the query has been composed.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> BeforeExecuteInsert;
        /// <summary>
        /// Occurs before the inner <see cref="DataCommand"/> for delete operation is executed but after the query has been composed.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> BeforeExecuteDelete;
        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for select operation has been executed.
        /// </summary>
        public event EventHandler<SelectDataOperationEventArgs> AfterExecuteSelect;
        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for count rows operation has been executed.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> AfterExecuteCount;
        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for insert operation has been executed.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> AfterExecuteInsert;
        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for update operation has been executed.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> AfterExecuteUpdate;
        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for delete operation has been executed.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> AfterExecuteDelete;

        private Control _owner;

        /// <summary>
        /// Gets or sets the data source control that the MTDataSourceView is associated with.
        /// </summary>
        public Control Owner
        {
            get { return _owner; }
            set { _owner = value; }
        }

        private bool __IsInitilaizeSelectParametersRaised = true;
        /// <summary>
        /// Gets or sets value which indicates whether <see cref="InitializeSelectParameters"/> event should be raised.
        /// </summary>
        public bool IsInitilaizeSelectParametersRaised
        {
            get { return __IsInitilaizeSelectParametersRaised; }
            set { __IsInitilaizeSelectParametersRaised = value; }
        }

        /// <summary>
        /// Raises InitializeSelectParameters event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnInitializeSelectParameters(EventArgs e)
        {
            if (InitializeSelectParameters != null && IsInitilaizeSelectParametersRaised)
                InitializeSelectParameters(this, e);
        }

        /// <summary>
        /// Raises InitializeInsertParameters event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnInitializeInsertParameters(EventArgs e)
        {
            if (InitializeInsertParameters != null)
                InitializeInsertParameters(this, e);
        }

        /// <summary>
        /// Raises InitializeUpdateParameters event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnInitializeUpdateParameters(EventArgs e)
        {
            if (InitializeUpdateParameters != null)
                InitializeUpdateParameters(this, e);
        }

        /// <summary>
        /// Raises InitializeDeleteParameters event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnInitializeDeleteParameters(EventArgs e)
        {
            if (InitializeDeleteParameters != null)
                InitializeDeleteParameters(this, e);
        }

        /// <summary>
        /// Raises BeforeBuildSelect event.
        /// </summary>
        /// <param name="e">An <see cref="DataOperationEventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeBuildSelect(SelectDataOperationEventArgs e)
        {
            if (BeforeBuildSelect != null)
                BeforeBuildSelect(this, e);
        }

        /// <summary>
        /// Raises BeforeExecuteSelect event.
        /// </summary>
        /// <param name="e">An <see cref="DataOperationEventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeExecuteSelect(SelectDataOperationEventArgs e)
        {
            if (BeforeExecuteSelect != null)
                BeforeExecuteSelect(this, e);
        }

        /// <summary>
        /// Raises AfterExecuteSelect event.
        /// </summary>
        /// <param name="e">An <see cref="DataOperationEventArgs"/> object that contains the event data.</param>
        protected virtual void OnAfterExecuteSelect(SelectDataOperationEventArgs e)
        {
            if (AfterExecuteSelect != null)
                AfterExecuteSelect(this, e);
        }

        /// <summary>
        /// Raises BeforeBuildCount event.
        /// </summary>
        /// <param name="e">An <see cref="DataOperationEventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeBuildCount(DataOperationEventArgs e)
        {
            if (BeforeBuildCount != null)
                BeforeBuildCount(this, e);
        }

        /// <summary>
        /// Raises BeforeExecuteCount event.
        /// </summary>
        /// <param name="e">An <see cref="DataOperationEventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeExecuteCount(DataOperationEventArgs e)
        {
            if (BeforeExecuteCount != null)
                BeforeExecuteCount(this, e);
        }

        /// <summary>
        /// Raises AfterExecuteCount event.
        /// </summary>
        /// <param name="e">An <see cref="DataOperationEventArgs"/> object that contains the event data.</param>
        protected virtual void OnAfterExecuteCount(DataOperationEventArgs e)
        {
            if (AfterExecuteCount != null)
                AfterExecuteCount(this, e);
        }

        /// <summary>
        /// Raises BeforeBuildInsert event.
        /// </summary>
        /// <param name="e">An <see cref="DataOperationEventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeBuildInsert(DataOperationEventArgs e)
        {
            if (BeforeBuildInsert != null)
                BeforeBuildInsert(this, e);
        }

        /// <summary>
        /// Raises BeforeExecuteInsert event.
        /// </summary>
        /// <param name="e">An <see cref="DataOperationEventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeExecuteInsert(DataOperationEventArgs e)
        {
            if (BeforeExecuteInsert != null)
                BeforeExecuteInsert(this, e);
        }

        /// <summary>
        /// Raises AfterExecuteInsert event.
        /// </summary>
        /// <param name="e">An <see cref="DataOperationEventArgs"/> object that contains the event data.</param>
        protected virtual void OnAfterExecuteInsert(DataOperationEventArgs e)
        {
            if (AfterExecuteInsert != null)
                AfterExecuteInsert(this, e);
        }

        /// <summary>
        /// Raises BeforeBuildUpdate event.
        /// </summary>
        /// <param name="e">An <see cref="DataOperationEventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeBuildUpdate(DataOperationEventArgs e)
        {
            if (BeforeBuildUpdate != null)
                BeforeBuildUpdate(this, e);
        }

        /// <summary>
        /// Raises BeforeExecuteUpdate event.
        /// </summary>
        /// <param name="e">An <see cref="DataOperationEventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeExecuteUpdate(DataOperationEventArgs e)
        {
            if (BeforeExecuteUpdate != null)
                BeforeExecuteUpdate(this, e);
        }

        /// <summary>
        /// Raises AfterExecuteUpdate event.
        /// </summary>
        /// <param name="e">An <see cref="DataOperationEventArgs"/> object that contains the event data.</param>
        protected virtual void OnAfterExecuteUpdate(DataOperationEventArgs e)
        {
            if (AfterExecuteUpdate != null)
                AfterExecuteUpdate(this, e);
        }

        /// <summary>
        /// Raises BeforeBuildDelete event.
        /// </summary>
        /// <param name="e">An <see cref="DataOperationEventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeBuildDelete(DataOperationEventArgs e)
        {
            if (BeforeBuildDelete != null)
                BeforeBuildDelete(this, e);
        }

        /// <summary>
        /// Raises BeforeExecuteDelete event.
        /// </summary>
        /// <param name="e">An <see cref="DataOperationEventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeExecuteDelete(DataOperationEventArgs e)
        {
            if (BeforeExecuteDelete != null)
                BeforeExecuteDelete(this, e);
        }

        /// <summary>
        /// Raises AfterExecuteDelete event.
        /// </summary>
        /// <param name="e">An <see cref="DataOperationEventArgs"/> object that contains the event data.</param>
        protected virtual void OnAfterExecuteDelete(DataOperationEventArgs e)
        {
            if (AfterExecuteDelete != null)
                AfterExecuteDelete(this, e);
        }
        #endregion


        /// <summary>
        /// The MTDataSource
        /// only supports one view, so the name is ignored, and the
        /// default name used instead.
        /// </summary>
        public static readonly string DefaultViewName = "MTDataSourceView";

        ///<summary>
        ///Initializes a new instance of the MTDataSourceView class.
        ///</summary>
        ///<param name="owner">The data source control that the MTDataSourceView is 
        ///associated with.</param>
        ///<param name="name">The name of the MTDataSourceView object. 
        ///The <see cref="MTDataSource"/> only supports one view, so the name is ignored, 
        ///and the default name "MTDataSourceView" used instead.</param>
        public MTDataSourceView(IDataSource owner, string name)
            : base(owner, DefaultViewName)
        {

        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="MTDataSourceView"/> object associated with the current <see cref="MTDataSource"/> object supports retrieving the total number of data rows, instead of the data.
        /// </summary>
        public override bool CanRetrieveTotalRowCount
        {
            get
            {
                return !String.IsNullOrEmpty(CountCommand);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="MTDataSourceView"/> object associated with the current <see cref="MTDataSource"/> object supports paging through the data retrieved by the <see cref="Select"/> method.
        /// </summary>
        public override bool CanPage
        {
            get
            {
                return base.CanPage;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="MTDataSourceView"/> object associated with the current <see cref="MTDataSource"/> object supports a sorted view on the underlying data source.
        /// </summary>
        public override bool CanSort
        {
            get
            {
                return base.CanSort;
            }
        }


        #region Select command

        private Collection<PrimaryKeyInfo> _pkFields;
        /// <summary>
        /// Gets or sets list of primary keys fields names.
        /// </summary>
        public Collection<PrimaryKeyInfo> PrimaryKeys
        {
            get
            {
                if (_pkFields == null) _pkFields = new Collection<PrimaryKeyInfo>();
                return _pkFields;
            }
            set
            {
                _pkFields = value;
            }
        }

        private bool _enableCaching;
        /// <summary>
        /// Gets or sets the value that indicating whether result of select operation will be cached for repeating requests.
        /// </summary>
        public bool EnableCaching
        {
            get { return _enableCaching; }
            set { _enableCaching = value; }
        }

        /// <summary>
        /// Gets or sets the SQL string that the <see cref="MTDataSourceView"/> object 
        /// uses to retrieve data from the underlying database.
        /// </summary>
        public string SelectCommand
        {
            get
            {
                if (ViewState["__SelectCommand"] == null)
                    ViewState["__SelectCommand"] = String.Empty;
                return (string)ViewState["__SelectCommand"];
            }
            set { ViewState["__SelectCommand"] = value; }
        }

        /// <summary>
        /// Gets or sets the index of resultset that will be used as datasource.
        /// </summary>
        public int ActiveResultset
        {
            get
            {
                if (ViewState["__AR"] == null)
                    ViewState["__AR"] = 0;
                return (int)ViewState["__AR"];
            }
            set { ViewState["__AR"] = value; }
        }

        /// <summary>
        /// Gets or sets the how <see cref="SelectCommand"/> property will be interpreted.
        /// </summary>
        public MTCommandType SelectCommandType
        {
            get
            {
                if (ViewState["__SelectCommandType"] == null)
                    ViewState["__SelectCommandType"] = MTCommandType.Sql;
                return (MTCommandType)ViewState["__SelectCommandType"];
            }
            set { ViewState["__SelectCommandType"] = value; }
        }

        /// <summary>
        /// Gets the parameters collection containing the parameters that are used by the SelectCommand property.
        /// </summary>
        public DataParameterCollection SelectParameters
        {
            get
            {
                if (ViewState["__SelectParameters"] == null)
                    ViewState["__SelectParameters"] = new DataParameterCollection();
                return (DataParameterCollection)ViewState["__SelectParameters"];
            }
            set { ViewState["__SelectParameters"] = value; }
        }

        private string _selectOrderBy;

        /// <summary>
        /// Gets or sets the sort expression that append to the select command.
        /// </summary>
        public string SelectOrderBy
        {
            get { return _selectOrderBy; }
            set { _selectOrderBy = value; }
        }

        /// <summary>
        /// Gets a list of data from the underlying data storage.
        /// </summary>
        /// <param name="arguments">A <see cref="DataSourceSelectArguments"/> that is used to request operations on the data beyond basic data retrieval.</param>
        /// <returns>An <see cref="IEnumerable"/> list of data from the underlying data storage.</returns>
        public virtual IEnumerable Select(DataSourceSelectArguments arguments)
        {
            return ExecuteSelect(arguments);
        }

        DataSourceSelectArguments __cachedArguments;
        IEnumerable __cachedResult;

        /// <summary>
        /// Gets a list of data from the underlying data storage.
        /// </summary>
        /// <param name="arguments">A <see cref="DataSourceSelectArguments"/> that is used to request operations on the data beyond basic data retrieval.</param>
        /// <returns>An <see cref="IEnumerable"/> list of data from the underlying data storage.</returns>
        protected override IEnumerable ExecuteSelect(DataSourceSelectArguments arguments)
        {
            Connection conn = GetConnection();
            DataCommand dc = (DataCommand)conn.CreateCommand();
            object countResult = null;
            dc.CommandText = SelectCommand;
            dc.MTCommandType = SelectCommandType;
            DataCommand dcount = null;
            OnInitializeSelectParameters(new EventArgs());
            if (!String.IsNullOrEmpty(CountCommand) && arguments.RetrieveTotalRowCount)
            {
                dcount = (DataCommand)conn.CreateCommand();
                dcount.CommandText = CountCommand;
                dcount.MTCommandType = CountCommandType;
            }

            bool isParametersChanged;
            try
            {
                InitializeParameters(dc, SelectParameters, out isParametersChanged);
            }
            catch (ParametersInitializationException e)
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError(e.ToString());
                arguments.TotalRowCount = 0;
                throw;
            }
            if (EnableCaching && !isParametersChanged)
            {
                arguments = __cachedArguments;
                return __cachedResult;
            }

            int maxRows = arguments.MaximumRows < 0 ? 0 : arguments.MaximumRows;
            dc.SortExpression = arguments.SortExpression;
            if (String.IsNullOrEmpty(dc.SortExpression))
                dc.SortExpression = SelectOrderBy;

            OnBeforeBuildSelect(new SelectDataOperationEventArgs(DataOperationType.Select, null, null, dc, dcount));

            conn.Open();
            Exception OperationException = null;
            //Execute Count query
            if (!String.IsNullOrEmpty(CountCommand) && arguments.RetrieveTotalRowCount)
            {
                if (CountParameters.Count > 0)
                {
                    InitializeParameters(dcount, CountParameters, out isParametersChanged);
                }
                else
                {
                    foreach (object o in dc.Parameters)
                        dcount.Parameters.Add(o);
                    foreach (object o in dc.WhereParameters)
                        dcount.WhereParameters.Add(o);
                    foreach (object o in dc.SPParameters)
                        dcount.SPParameters.Add(o);
                }
                OnBeforeBuildCount(new DataOperationEventArgs(DataOperationType.Select, null, dcount));
                dcount.BuildCommand();
                OnBeforeExecuteCount(new DataOperationEventArgs(DataOperationType.Count, null, dcount));
            }
            if (!String.IsNullOrEmpty(CountCommand) && arguments.RetrieveTotalRowCount)
            {
                try
                {
                    countResult = Convert.ToInt32(dcount.ExecuteScalar());
                }
                catch (Exception e)
                {
                    OperationException = e;
                }
                finally
                {
                    DataOperationEventArgs ea = new DataOperationEventArgs(DataOperationType.Count, null, dcount, OperationException);
                    OnAfterExecuteCount(ea);
                    if (ea.OperationException != null)
                    {
                        conn.Close();
                        if (AppConfig.IsMTErrorHandlerUse("critical"))
                            Trace.TraceError(ea.OperationException.ToString());
                        throw ea.OperationException;
                    }
                }
                arguments.TotalRowCount = (int)countResult;
                if (arguments.TotalRowCount <= arguments.StartRowIndex && arguments.MaximumRows > 0)
                {
                    int pages = arguments.TotalRowCount / arguments.MaximumRows;
                    arguments.StartRowIndex = pages * arguments.MaximumRows;
                }
            }

            dc.BuildCommand(arguments.StartRowIndex + maxRows);
            OnBeforeExecuteSelect(new SelectDataOperationEventArgs(DataOperationType.Select, null, null, dc, dcount));

            IEnumerable result = null;
            try
            {
                if (SelectCommandType == MTCommandType.StoredProcedure)
                {
                    DataSet TempResult = dc.Execute(arguments.StartRowIndex, maxRows);
                    if (TempResult.Tables.Count > 0)
                        result = (System.Collections.IEnumerable)TempResult.Tables[ActiveResultset].DefaultView;

                    foreach (SPParameter p in SelectParameters)
                    {
                        if (p.Direction == ParameterDirection.Output || p.Direction == ParameterDirection.InputOutput)
                            p.Value = dc.SPParameters[p.Name].Value;
                    }
                }
                else
                {
                    result = (System.Collections.IEnumerable)dc.Execute(arguments.StartRowIndex, maxRows).Tables[ActiveResultset].DefaultView;
                }
            }
            catch (Exception e)
            {
                OperationException = e;
            }
            finally
            {
                SelectDataOperationEventArgs ea = new SelectDataOperationEventArgs(DataOperationType.Select, result, countResult, dc, dcount, OperationException);
                OnAfterExecuteSelect(ea);
                conn.Close();
                if (ea.OperationException != null)
                {
                    if (AppConfig.IsMTErrorHandlerUse("critical"))
                        Trace.TraceError(ea.OperationException.ToString());
                    throw ea.OperationException;
                }
            }
            if (EnableCaching)
            {
                __cachedArguments = arguments;
                __cachedResult = result;
            }
            return result;
        }

        private Dictionary<string, Int32> parametersHash;
        private void InitializeParameters(DataCommand dc, DataParameterCollection col, out bool isParametersChanged)
        {
            isParametersChanged = false;
            if (EnableCaching && parametersHash == null)
                parametersHash = new Dictionary<string, int>();
            foreach (DataParameter p in col)
            {
                InMotion.Data.DataParameter dp = null;
                if (p is SqlParameter) dp = MapSqlParameter((SqlParameter)p);
                if (p is WhereParameter) dp = MapWhereParameter((WhereParameter)p);
                if (p is SPParameter) dp = MapSPParameter((SPParameter)p);
                if (EnableCaching)
                {
                    if (parametersHash.ContainsKey(dp.ParameterName))
                        isParametersChanged = parametersHash[dp.ParameterName] != dp.GetHashCode() || isParametersChanged;
                    else
                        parametersHash.Add(dp.ParameterName, dp.GetHashCode());
                }
                if (p is SqlParameter) dc.Parameters.Add(dp);
                if (p is WhereParameter) dc.WhereParameters.Add(dp);
                if (p is SPParameter) dc.SPParameters.Add(dp);
            }
        }
        #endregion

        #region Count command

        /// <summary>
        /// Gets or sets the SQL string that the <see cref="MTDataSourceView"/> object 
        /// uses to retrieve record count from the underlying database.
        /// </summary>
        public string CountCommand
        {
            get
            {
                if (ViewState["__CountCommand"] == null)
                    ViewState["__CountCommand"] = String.Empty;
                return (string)ViewState["__CountCommand"];
            }
            set { ViewState["__CountCommand"] = value; }
        }

        /// <summary>
        /// Gets or sets the how <see cref="CountCommand"/> property will be interpreted.
        /// </summary>
        public MTCommandType CountCommandType
        {
            get
            {
                if (ViewState["__CountCommandType"] == null)
                    ViewState["__CountCommandType"] = MTCommandType.Sql;
                return (MTCommandType)ViewState["__CountCommandType"];
            }
            set { ViewState["__CountCommandType"] = value; }
        }

        /// <summary>
        /// Gets the parameters collection containing the parameters that are used by the CountCommand property.
        /// </summary>
        public DataParameterCollection CountParameters
        {
            get
            {
                if (ViewState["__CountParameters"] == null)
                    ViewState["__CountParameters"] = new DataParameterCollection();
                return (DataParameterCollection)ViewState["__CountParameters"];
            }
            set { ViewState["__CountParameters"] = value; }
        }
        #endregion

        #region Insert command

        /// <summary>
        /// Gets a value indicating whether the <see cref="MTDataSourceView"/> object associated with the current <see cref="MTDataSource"/> object supports the ExecuteInsert operation.
        /// </summary>
        public override bool CanInsert
        {
            get
            {
                return InsertCommand.Length > 0;
            }
        }

        /// <summary>
        /// Gets or sets the SQL string that the <see cref="MTDataSourceView"/> object 
        /// uses to insert data to the underlying database.
        /// </summary>
        public string InsertCommand
        {
            get
            {
                if (ViewState["__InsertCommand"] == null)
                    ViewState["__InsertCommand"] = String.Empty;
                return (string)ViewState["__InsertCommand"];
            }
            set { ViewState["__InsertCommand"] = value; }
        }

        /// <summary>
        /// Gets or sets the how <see cref="InsertCommand"/> property will be interpreted.
        /// </summary>
        public MTCommandType InsertCommandType
        {
            get
            {
                if (ViewState["__InsertCommandType"] == null)
                    ViewState["__InsertCommandType"] = MTCommandType.Sql;
                return (MTCommandType)ViewState["__InsertCommandType"];
            }
            set { ViewState["__InsertCommandType"] = value; }
        }

        /// <summary>
        /// Gets the parameters collection containing the parameters that are used by the InsertCommand property.
        /// </summary>
        public DataParameterCollection InsertParameters
        {
            get
            {
                if (ViewState["__InsertParameters"] == null)
                    ViewState["__InsertParameters"] = new DataParameterCollection();
                return (DataParameterCollection)ViewState["__InsertParameters"];
            }
            set { ViewState["__InsertParameters"] = value; }
        }

        /// <summary>
        /// Performs an insert operation on the list of data that the <see cref="MTDataSourceView"/> object represents. 
        /// </summary>
        /// <param name="values">An <see cref="IDictionary"/> of name/value pairs used during an insert operation.</param>
        /// <returns>The number of items that were inserted into the underlying data storage.</returns>
        public virtual int Insert(System.Collections.IDictionary values)
        {
            return ExecuteInsert(values);
        }

        /// <summary>
        /// Performs an insert operation on the list of data that the <see cref="MTDataSourceView"/> object represents. 
        /// </summary>
        /// <param name="values">An <see cref="IDictionary"/> of name/value pairs used during an insert operation.</param>
        /// <returns>The number of items that were inserted into the underlying data storage.</returns>
        protected override int ExecuteInsert(System.Collections.IDictionary values)
        {
            Connection conn = GetConnection();
            DataCommand dc = (DataCommand)conn.CreateCommand();
            DataOperationEventArgs ea;

            dc.CommandText = InsertCommand;
            dc.MTCommandType = InsertCommandType;

            ea = new DataOperationEventArgs(DataOperationType.Insert, null, dc);
            OnInitializeInsertParameters(ea);
            if (ea.Cancel) return 0;

            Exception OperationException = null;
            try
            {
                ArrayList emptyParams = new ArrayList();
                foreach (Parameter p in InsertParameters)
                {
                    if (p is SqlParameter) dc.Parameters.Add(MapSqlParameter((SqlParameter)p));
                    if (p is WhereParameter) dc.WhereParameters.Add(MapWhereParameter((WhereParameter)p));
                    if (p is SPParameter) dc.SPParameters.Add(MapSPParameter((SPParameter)p));
                    if (p.IsValueNotPassed && (AppConfig.ExcludeMissingDataParameters || p.IsOmitEmptyValue))
                        emptyParams.Add(p.Name);
                }

                bool IsCustomInsert = InsertParameters.Count != 0;
                if (InsertCommandType == MTCommandType.Table && !IsCustomInsert)
                {
                    foreach (DictionaryEntry p in values)
                    {
                        Data.SqlParameter sp = new Data.SqlParameter();
                        sp.AddQuotes = true;
                        if (p.Value is TypedValue)
                        {
                            TypedValue v = (TypedValue)p.Value;
                            if (v.IsEmpty)
                            {
                                emptyParams.Add(p.Key as string);
                                continue;
                            }
                            sp.ParameterName = p.Key as string;
                            sp.Type = v.DataType;
                            sp.Value = v;
                        }
                        else
                        {
                            sp.ParameterName = p.Key as string;
                            sp.Type = InMotion.Common.DataType.Text;
                            sp.Value = p.Value;
                        }
                        dc.Parameters.Add(sp);
                    }
                }

                ea = new DataOperationEventArgs(DataOperationType.Insert, null, dc);
                OnBeforeBuildInsert(ea);
                if (ea.Cancel) return 0;

                if (InsertCommandType == MTCommandType.Table)
                    dc.CommandText = OmitEmptyInsertParameters(dc.CommandText, emptyParams);
            }
            catch (ParametersInitializationException e) // Parameters are not passed.
            {
                OperationException = e;
            }
            dc.Connection.Open();

            dc.BuildCommand();
            ea = new DataOperationEventArgs(DataOperationType.Insert, null, dc);
            ea.OperationException = OperationException;
            ea.Cancel = OperationException != null;
            OnBeforeExecuteInsert(ea);
            if (ea.Cancel)
            {
                dc.Connection.Close();
                if (ea.OperationException != null)
                {
                    if (AppConfig.IsMTErrorHandlerUse("critical"))
                        Trace.TraceError(ea.OperationException.ToString());
                    throw ea.OperationException;
                }
                return 0;
            }

            int result = 0;
            try
            {
                result = dc.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                OperationException = e;
            }
            finally
            {
                ea = new DataOperationEventArgs(DataOperationType.Insert, result, dc, OperationException);
                OnAfterExecuteInsert(ea);
                dc.Connection.Close();
                if (ea.OperationException != null)
                {
                    if (AppConfig.IsMTErrorHandlerUse("critical"))
                        Trace.TraceError(ea.OperationException.ToString());
                    throw ea.OperationException;
                }
            }

            return result;
        }

        private static string OmitEmptyInsertParameters(string query, ArrayList omitedParameters)
        {
            Regex re = new Regex("(INSERT\\s+INTO\\s+[^\\(]+\\()(([^\\)\\s,]+)[,\\s]*)+(\\)\\s+VALUES\\s+\\()(\\{([^\\)\\s,\\}]+)\\}[,\\s]*)+", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            MatchCollection mc = re.Matches(query);
            StringBuilder sql = new StringBuilder(mc[0].Groups[1].Captures[0].Value);
            StringBuilder sql_end = new StringBuilder(mc[0].Groups[4].Captures[0].Value);
            int clauses = 0;
            for (int i = 0; i < mc[0].Groups[2].Captures.Count; i++)
            {
                string name = mc[0].Groups[6].Captures[i].Value;
                string colname = mc[0].Groups[3].Captures[i].Value;
                if (omitedParameters.Contains(name)) continue;
                if (clauses > 0)
                {
                    sql.Append(", ");
                    sql_end.Append(", ");
                }
                sql.Append(colname);
                sql_end.Append("{" + name + "}");
                clauses++;
            }
            return clauses == 0 ? "" : sql.ToString() + sql_end.ToString() + ")";
        }


        private static void CheckIfParameterPassed(InMotion.Data.DataParameter dp, bool IsRequired)
        {
            //Parameters is not passed if is
            //- required
            //- has empty value
            //- is not where parameter with USE IS NULL condition.
            if (
                IsRequired && (dp.Value == null) &&
                !(dp is InMotion.Data.WhereParameter && ((InMotion.Data.WhereParameter)dp).UseIsNullIfEmpty)
                )
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("Error: Parameter {0} is not passed.", dp.ParameterName);
                throw new ParametersInitializationException(Common.Resources.ResManager.GetString("CCS_CustomOperationError_MissingParameters"));
            }
        }

        private InMotion.Data.WhereParameter MapWhereParameter(WhereParameter p)
        {
            InMotion.Data.WhereParameter wp = new InMotion.Data.WhereParameter();
            wp.IsExpression = p.SourceType == DataParameterSourceType.DBExpression;
            wp.ParameterName = p.Name;
            wp.Format = ControlsHelper.EvaluateDbFormat(p.DBFormat, p.DataType, GetConnection());
            wp.Type = p.DataType;
            wp.Condition = p.Condition;
            wp.Operation = p.Operation;
            wp.Prefix = p.Prefix;
            wp.Postfix = p.Postfix;
            wp.Value = TypeFactory.ToSimpleType(p.Evaluate(Owner));
            wp.SourceColumn = p.SourceColumn;
            wp.UseIsNullIfEmpty = p.UseIsNullIfEmpty;
            CheckIfParameterPassed(wp, p.Required);
            return wp;
        }

        private Data.SqlParameter MapSqlParameter(SqlParameter p)
        {
            Data.SqlParameter sp = new InMotion.Data.SqlParameter();
            sp.ParameterName = p.Name;
            sp.Format = ControlsHelper.EvaluateDbFormat(p.DBFormat, p.DataType, GetConnection());
            sp.Type = p.DataType;
            sp.Value = TypeFactory.ToSimpleType(p.Evaluate(Owner));
            CheckIfParameterPassed(sp, p.Required);
            return sp;
        }

        private Data.SPParameter MapSPParameter(SPParameter p)
        {
            InMotion.Data.SPParameter sp = new InMotion.Data.SPParameter();

            sp.ParameterName = p.Name;
            sp.DbType = p.DbType;
            sp.Size = p.Size;
            sp.Direction = p.Direction;
            sp.Value = TypeFactory.ToSimpleType(p.Evaluate(Owner));
            CheckIfParameterPassed(sp, p.Required);
            return sp;
        }

        #endregion

        #region Update command
        /// <summary>
        /// Gets a value indicating whether the DataSourceView object associated with the current DataSourceControl object supports the ExecuteUpdate operation.
        /// </summary>
        public override bool CanUpdate
        {
            get
            {
                return UpdateCommand.Length > 0;
            }
        }

        /// <summary>
        /// Gets or sets the SQL string that the <see cref="MTDataSourceView"/> object 
        /// uses to update data in the underlying database.
        /// </summary>
        public string UpdateCommand
        {
            get
            {
                if (ViewState["__UpdateCommand"] == null)
                    ViewState["__UpdateCommand"] = String.Empty;
                return (string)ViewState["__UpdateCommand"];
            }
            set { ViewState["__UpdateCommand"] = value; }
        }

        /// <summary>
        /// Gets or sets the how <see cref="UpdateCommand"/> property will be interpreted.
        /// </summary>
        public MTCommandType UpdateCommandType
        {
            get
            {
                if (ViewState["__UpdateCommandType"] == null)
                    ViewState["__UpdateCommandType"] = MTCommandType.Sql;
                return (MTCommandType)ViewState["__UpdateCommandType"];
            }
            set { ViewState["__UpdateCommandType"] = value; }
        }

        /// <summary>
        /// Gets the parameters collection containing the parameters that are used by the UpdateCommand property.
        /// </summary>
        public DataParameterCollection UpdateParameters
        {
            get
            {
                if (ViewState["__UpdateParameters"] == null)
                    ViewState["__UpdateParameters"] = new DataParameterCollection();
                return (DataParameterCollection)ViewState["__UpdateParameters"];
            }
            set { ViewState["__UpdateParameters"] = value; }
        }

        /// <summary>
        /// Performs an update operation on the list of data that the <see cref="MTDataSourceView"/> object represents. 
        /// </summary>
        /// <param name="keys">An <see cref="IDictionary"/> of object or row keys to be updated by the update operation.</param>
        /// <param name="values">An IDictionary of name/value pairs that represent data elements and their new values.</param>
        /// <param name="oldValues">An IDictionary of name/value pairs that represent data elements and their original values.</param>
        /// <returns>The number of items that were update in the underlying data storage.</returns>
        public virtual int Update(System.Collections.IDictionary keys, System.Collections.IDictionary values, System.Collections.IDictionary oldValues)
        {
            return ExecuteUpdate(keys, values, oldValues);
        }

        /// <summary>
        /// Performs an update operation on the list of data that the <see cref="MTDataSourceView"/> object represents. 
        /// </summary>
        /// <param name="keys">An <see cref="IDictionary"/> of object or row keys to be updated by the update operation.</param>
        /// <param name="values">An IDictionary of name/value pairs that represent data elements and their new values.</param>
        /// <param name="oldValues">An IDictionary of name/value pairs that represent data elements and their original values.</param>
        /// <returns>The number of items that were updated in the underlying data storage.</returns>
        protected override int ExecuteUpdate(System.Collections.IDictionary keys, System.Collections.IDictionary values, System.Collections.IDictionary oldValues)
        {
            Connection conn = GetConnection();
            DataCommand dc = (DataCommand)conn.CreateCommand();
            DataOperationEventArgs ea;

            dc.CommandText = UpdateCommand;
            dc.MTCommandType = UpdateCommandType;

            ea = new DataOperationEventArgs(DataOperationType.Update, null, dc);
            OnInitializeUpdateParameters(ea);
            if (ea.Cancel) return 0;

            Exception OperationException = null;
            try
            {
                ArrayList emptyParams = new ArrayList();
                foreach (Parameter p in UpdateParameters)
                {
                    if (p is SqlParameter) dc.Parameters.Add(MapSqlParameter((SqlParameter)p));
                    if (p is WhereParameter) dc.WhereParameters.Add(MapWhereParameter((WhereParameter)p));
                    if (p is SPParameter) dc.SPParameters.Add(MapSPParameter((SPParameter)p));
                    if (p.IsValueNotPassed && (AppConfig.ExcludeMissingDataParameters || p.IsOmitEmptyValue) && !p.IsValueChanged)
                        emptyParams.Add(p.Name);
                }

                bool IsCustomUpdate = UpdateParameters.Count != 0;
                if (UpdateCommandType == MTCommandType.Table && !IsCustomUpdate)
                {
                    foreach (DictionaryEntry p in values)
                    {
                        Data.SqlParameter sp = new Data.SqlParameter();
                        sp.AddQuotes = true;
                        if (p.Value is TypedValue)
                        {
                            TypedValue v = (TypedValue)p.Value;
                            if (v.IsEmpty)
                            {
                                emptyParams.Add(p.Key as string);
                                continue;
                            }
                            sp.ParameterName = p.Key as string;
                            sp.Type = v.DataType;
                            sp.Value = v;
                        }
                        else
                        {
                            sp.ParameterName = p.Key as string;
                            sp.Type = InMotion.Common.DataType.Text;
                            sp.Value = p.Value;
                        }
                        dc.Parameters.Add(sp);
                    }
                    foreach (PrimaryKeyInfo pk in PrimaryKeys)
                    {
                        Data.WhereParameter wp = new InMotion.Data.WhereParameter();
                        wp.Condition = WhereParameterCondition.Equal;
                        wp.Type = pk.Type;
                        wp.ParameterName = "PK" + pk.FieldName;
                        wp.SourceColumn = pk.FieldName;
                        wp.Value = keys[pk.FieldName];
                        dc.WhereParameters.Add(wp);
                    }
                    OnInitializeSelectParameters(new EventArgs());
                    foreach (Parameter p in SelectParameters)
                    {
                        if (p is WhereParameter) dc.WhereParameters.Add(MapWhereParameter((WhereParameter)p));
                    }
                }

                ea = new DataOperationEventArgs(DataOperationType.Update, null, dc);
                OnBeforeBuildUpdate(ea);
                if (ea.Cancel) return 0;

                if (dc.MTCommandType == MTCommandType.Table)
                    dc.CommandText = OmitEmptyUpdateParameters(dc.CommandText, emptyParams);
            }
            catch (ParametersInitializationException e) // Parameters are not passed.
            {
                OperationException = e;
            }
            dc.Connection.Open();

            dc.BuildCommand();
            ea = new DataOperationEventArgs(DataOperationType.Update, null, dc);
            ea.OperationException = OperationException;
            ea.Cancel = OperationException != null || String.IsNullOrEmpty(dc.CommandText);
            OnBeforeExecuteUpdate(ea);
            if (ea.Cancel)
            {
                dc.Connection.Close();
                if (ea.OperationException != null)
                {
                    if (AppConfig.IsMTErrorHandlerUse("critical"))
                        Trace.TraceError(ea.OperationException.ToString());
                    throw (Exception)ea.OperationException;
                }
                return 0;
            }

            int result = 0;
            try
            {
                result = dc.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                OperationException = e;
            }
            finally
            {
                ea = new DataOperationEventArgs(DataOperationType.Update, result, dc, OperationException);
                OnAfterExecuteUpdate(ea);
                dc.Connection.Close();
                if (ea.OperationException != null)
                {
                    if (AppConfig.IsMTErrorHandlerUse("critical"))
                        Trace.TraceError(ea.OperationException.ToString());
                    throw ea.OperationException;
                }
            }

            return result;
        }

        private string OmitEmptyUpdateParameters(string commandText, ArrayList omitedParameters)
        {
            Regex re = new Regex("(UPDATE\\s+.+\\s+SET\\s+)(([^\\s=]+)\\s*\\=\\s*\\{([^\\s,\\}]+)\\},*\\s*)+", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            MatchCollection mc = re.Matches(commandText);
            StringBuilder sql = new StringBuilder(mc[0].Groups[1].Captures[0].Value);
            int clauses = 0;
            for (int i = 0; i < mc[0].Groups[2].Captures.Count; i++)
            {
                string name = mc[0].Groups[4].Captures[i].Value;
                string colname = mc[0].Groups[3].Captures[i].Value;
                if (omitedParameters.Contains(name)) continue;
                if (clauses > 0) sql.Append(", ");
                sql.Append(colname + "={" + name + "}");
                clauses++;
            }
            return clauses == 0 ? "" : sql.ToString();
        }

        #endregion

        #region Delete command
        /// <summary>
        /// Gets a value indicating whether the DataSourceView object associated with the current DataSourceControl object supports the ExecuteDelete operation.
        /// </summary>
        public override bool CanDelete
        {
            get
            {
                return DeleteCommand.Length > 0;
            }
        }

        /// <summary>
        /// Gets or sets the SQL string that the <see cref="MTDataSourceView"/> object 
        /// uses to delete data from the underlying database.
        /// </summary>
        public string DeleteCommand
        {
            get
            {
                if (ViewState["__DeleteCommand"] == null)
                    ViewState["__DeleteCommand"] = String.Empty;
                return (string)ViewState["__DeleteCommand"];
            }
            set { ViewState["__DeleteCommand"] = value; }
        }

        /// <summary>
        /// Gets or sets the how <see cref="DeleteCommand"/> property will be interpreted.
        /// </summary>
        public MTCommandType DeleteCommandType
        {
            get
            {
                if (ViewState["__DeleteCommandType"] == null)
                    ViewState["__DeleteCommandType"] = MTCommandType.Sql;
                return (MTCommandType)ViewState["__DeleteCommandType"];
            }
            set { ViewState["__DeleteCommandType"] = value; }
        }

        /// <summary>
        /// Gets the parameters collection containing the parameters that are used by the DeleteCommand property.
        /// </summary>
        public DataParameterCollection DeleteParameters
        {
            get
            {
                if (ViewState["__DeleteParameters"] == null)
                    ViewState["__DeleteParameters"] = new DataParameterCollection();
                return (DataParameterCollection)ViewState["__DeleteParameters"];
            }
            set { ViewState["__DeleteParameters"] = value; }
        }

        /// <summary>
        /// Performs an delete operation on the list of data that the <see cref="MTDataSourceView"/> object represents. 
        /// </summary>
        /// <param name="keys">An <see cref="IDictionary"/> of object or row keys to be updated by the update operation.</param>
        /// <param name="oldValues">An IDictionary of name/value pairs that represent data elements and their original values.</param>
        /// <returns>The number of items that were deleted from the underlying data storage.</returns>
        public virtual int Delete(System.Collections.IDictionary keys, System.Collections.IDictionary oldValues)
        {
            return ExecuteDelete(keys, oldValues);
        }

        /// <summary>
        /// Performs an delete operation on the list of data that the <see cref="MTDataSourceView"/> object represents. 
        /// </summary>
        /// <param name="keys">An <see cref="IDictionary"/> of object or row keys to be updated by the update operation.</param>
        /// <param name="oldValues">An IDictionary of name/value pairs that represent data elements and their original values.</param>
        /// <returns>The number of items that were deleted from the underlying data storage.</returns>
        protected override int ExecuteDelete(System.Collections.IDictionary keys, System.Collections.IDictionary oldValues)
        {
            Connection conn = GetConnection();
            DataCommand dc = (DataCommand)conn.CreateCommand();
            DataOperationEventArgs ea;

            dc.CommandText = DeleteCommand;
            dc.MTCommandType = DeleteCommandType;

            ea = new DataOperationEventArgs(DataOperationType.Delete, null, dc);
            OnInitializeDeleteParameters(ea);
            if (ea.Cancel) return 0;

            Exception OperationException = null;
            try
            {
                foreach (Parameter p in DeleteParameters)
                {
                    if (p is SqlParameter) dc.Parameters.Add(MapSqlParameter((SqlParameter)p));
                    if (p is WhereParameter) dc.WhereParameters.Add(MapWhereParameter((WhereParameter)p));
                    if (p is SPParameter) dc.SPParameters.Add(MapSPParameter((SPParameter)p));
                }

                bool IsCustomDelete = DeleteParameters.Count != 0;
                if (DeleteCommandType == MTCommandType.Table && !IsCustomDelete)
                {
                    foreach (PrimaryKeyInfo pk in PrimaryKeys)
                    {
                        Data.WhereParameter wp = new InMotion.Data.WhereParameter();
                        wp.Condition = WhereParameterCondition.Equal;
                        wp.Type = pk.Type;
                        wp.ParameterName = "PK" + pk.FieldName;
                        wp.SourceColumn = pk.FieldName;
                        wp.Value = keys[pk.FieldName];
                        dc.WhereParameters.Add(wp);
                    }
                    OnInitializeSelectParameters(new EventArgs());
                    foreach (Parameter p in SelectParameters)
                    {
                        if (p is WhereParameter) dc.WhereParameters.Add(MapWhereParameter((WhereParameter)p));
                    }
                }

                ea = new DataOperationEventArgs(DataOperationType.Delete, null, dc);
                OnBeforeBuildDelete(ea);
                if (ea.Cancel) return 0;
            }
            catch (ParametersInitializationException e) // Parameters are not passed.
            {
                OperationException = e;
            }
            dc.Connection.Open();

            dc.BuildCommand();
            ea = new DataOperationEventArgs(DataOperationType.Delete, null, dc);
            ea.OperationException = OperationException;
            ea.Cancel = OperationException != null || String.IsNullOrEmpty(dc.CommandText);
            OnBeforeExecuteDelete(ea);
            if (ea.Cancel)
            {
                dc.Connection.Close();
                if (ea.OperationException != null)
                {
                    if (AppConfig.IsMTErrorHandlerUse("critical"))
                        Trace.TraceError(ea.OperationException.ToString());
                    throw ea.OperationException;
                }
                return 0;
            }

            int result = 0;
            try
            {
                result = dc.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                OperationException = e;
            }
            finally
            {
                ea = new DataOperationEventArgs(DataOperationType.Delete, result, dc, OperationException);
                OnAfterExecuteDelete(ea);
                dc.Connection.Close();
                if (ea.OperationException != null)
                {
                    if (AppConfig.IsMTErrorHandlerUse("critical"))
                        Trace.TraceError(ea.OperationException.ToString());
                    throw ea.OperationException;
                }
            }

            return result;
        }
        #endregion

        #region Unique Validation command
        /// <summary>
        /// Gets a value indicating whether the <see cref="MTDataSourceView"/> object associated with the current <see cref="MTDataSource"/> object supports the ValidateUnique operation.
        /// </summary>
        public bool CanValidateUnique
        {
            get
            {
                return ValidateUniqueCommand.Length > 0;
            }
        }

        /// <summary>
        /// Gets or sets the SQL string that the <see cref="MTDataSourceView"/> object 
        /// uses to validate data uniquess against the underlying database.
        /// </summary>
        public string ValidateUniqueCommand
        {
            get
            {
                if (ViewState["__ValidateUniqueCommand"] == null)
                    ViewState["__ValidateUniqueCommand"] = String.Empty;
                return (string)ViewState["__ValidateUniqueCommand"];
            }
            set { ViewState["__ValidateUniqueCommand"] = value; }
        }

        /// <summary>
        /// Gets or sets the how <see cref="ValidateUniqueCommand"/> property will be interpreted.
        /// </summary>
        public MTCommandType ValidateUniqueCommandType
        {
            get
            {
                if (ViewState["__ValidateUniqueCommandType"] == null)
                    ViewState["__ValidateUniqueCommandType"] = MTCommandType.Sql;
                return (MTCommandType)ViewState["__ValidateUniqueCommandType"];
            }
            set { ViewState["__ValidateUniqueCommandType"] = value; }
        }

        /// <summary>
        /// Gets the parameters collection containing the parameters that are used by the ValidateUniqueCommand property.
        /// </summary>
        public DataParameterCollection ValidateUniqueParameters
        {
            get
            {
                if (ViewState["__ValidateUniqueParameters"] == null)
                    ViewState["__ValidateUniqueParameters"] = new DataParameterCollection();
                return (DataParameterCollection)ViewState["__ValidateUniqueParameters"];
            }
            set { ViewState["__ValidateUniqueParameters"] = value; }
        }

        /// <summary>
        /// Performs an unique validation of value on the list of data that the <see cref="MTDataSourceView"/> object represents.
        /// </summary>
        /// <param name="keys">An <see cref="IDictionary"/> of object or row keys to be updated by the update operation.</param>
        /// <param name="field">A name of the field.</param>
        /// <param name="value">A value for validate.</param>
        /// <returns>true if the validation was successfull; otherwise false.</returns>
        public virtual bool ValidateUnique(ICollection keys, string field, object value)
        {
            Connection conn = GetConnection();
            DataCommand dc = (DataCommand)conn.CreateCommand();
            dc.CommandText = ValidateUniqueCommand;
            dc.MTCommandType = ValidateUniqueCommandType;
            dc.Operation = "AND NOT";
            if (ValidateUniqueCommandType == MTCommandType.Table)
            {
                if (value is TypedValue)
                {
                    TypedValue v = (TypedValue)value;
                    dc.Where += field + "=" + conn.EncodeSqlValue(v.ToString(), v.DataType, true);
                }

                if (keys != null)
                    foreach (Dictionary<string, Object> dItem in keys)
                        if (dItem != null)
                        {
                            Data.WhereParameter wp = new InMotion.Data.WhereParameter();
                            wp.Prefix = "(";
                            for (int i = 0; i < PrimaryKeys.Count; i++)
                            {
                                wp.Condition = WhereParameterCondition.Equal;
                                wp.Operation = WhereParameterOperation.And;
                                wp.Type = PrimaryKeys[i].Type;
                                wp.ParameterName = "PK" + PrimaryKeys[i].FieldName;
                                wp.SourceColumn = PrimaryKeys[i].FieldName;
                                wp.Value = dItem[PrimaryKeys[i].FieldName];
                                dc.WhereParameters.Add(wp);
                                if (i < PrimaryKeys.Count - 1)
                                    wp = new InMotion.Data.WhereParameter();
                            }
                            wp.Postfix = ")";
                            wp.Operation = WhereParameterOperation.Or;

                        }
            }

            foreach (Parameter p in ValidateUniqueParameters)
            {
                if (p is SqlParameter) dc.Parameters.Add(MapSqlParameter((SqlParameter)p));
                if (p is WhereParameter) dc.WhereParameters.Add(MapWhereParameter((WhereParameter)p));
                if (p is SPParameter) dc.SPParameters.Add(MapSPParameter((SPParameter)p));
            }
            if (ValidateUniqueParameters.Count == 0)
            {
                OnInitializeSelectParameters(new EventArgs());
                foreach (Parameter p in SelectParameters)
                {
                    if (p is WhereParameter)
                    {
                        try
                        {
                            dc.WhereParameters.Add(MapWhereParameter((WhereParameter)p));
                        }
                        catch (ParametersInitializationException)
                        {
                            continue; //Insert mode - where parameters are not passed
                        }
                    }
                }
            }

            dc.Connection.Open();
            int result = Convert.ToInt32(dc.ExecuteScalar());
            dc.Connection.Close();
            return result == 0;
        }
        #endregion

        /// <summary>
        /// Gets or sets the connection string that will be used for connect to database.
        /// </summary>
        public string ConnectionString
        {
            get
            {
                if (ViewState["__ConnectionString"] == null)
                    ViewState["__ConnectionString"] = String.Empty;
                return (string)ViewState["__ConnectionString"];
            }
            set { ViewState["__ConnectionString"] = value; }
        }

        private Connection _Connection;
        /// <summary>
        /// Returns the <see cref="Connection"/> object based on <see cref="ConnectionString"/> value.
        /// </summary>
        /// <returns>A <see cref="Connection"/> instance.</returns>
        public Connection GetConnection()
        {
            if (_Connection == null)
            {
                string connName = ConnectionString;
                if (connName.StartsWith("InMotion:"))
                {
                    _Connection = AppConfig.GetConnection(connName.Substring(9));
                }
                else
                {
                    _Connection = new Connection("System.Data.OleDb");
                    _Connection.ConnectionString = ConnectionString;
                }
            }
            return _Connection;
        }

        #region IStateManager Members
        private bool _isTrackingViewState;
        /// <summary>
        /// Gets a value indicating whether a parameter is tracking its view state changes.
        /// </summary>
        public bool IsTrackingViewState
        {
            get { return _isTrackingViewState; }
        }

        /// <summary>
        /// Loads the server control's previously saved view state to the control.
        /// </summary>
        /// <param name="state">An Object that contains the saved view state values for the control.</param>
        public void LoadViewState(object state)
        {
            object[] states = (object[])state;

            if (states.Length != 1)
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("Invalid Parameter View State.\n{0}", Environment.StackTrace);
                throw new ArgumentException("Invalid Parameter View State");
            }

            ((IStateManager)ViewState).LoadViewState(states[0]);
        }

        /// <summary>
        /// Saves the changes to a server control's view state to an Object.
        /// </summary>
        /// <returns>The Object that contains the view state changes.</returns>
        public object SaveViewState()
        {
            object[] state = new object[1];

            if (viewState != null)
            {
                state[0] = ((IStateManager)viewState).SaveViewState();
            }
            return state;

        }

        /// <summary>
        /// Instructs the server control to track changes to its view state.
        /// </summary>
        public void TrackViewState()
        {
            _isTrackingViewState = true;
            if (viewState != null)
            {
                ((IStateManager)viewState).TrackViewState();
            }
        }

        private StateBag viewState;
        private StateBag ViewState
        {
            get
            {
                if (viewState == null)
                {
                    viewState = new StateBag(false);
                    if (_isTrackingViewState)
                    {
                        ((IStateManager)viewState).TrackViewState();
                    }
                }
                return viewState;
            }
        }

        internal void SetDirty(bool p)
        {
            ViewState.SetDirty(true);
        }
        #endregion
    }
}
