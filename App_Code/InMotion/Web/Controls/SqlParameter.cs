//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Summary description for SqlParameter
    /// </summary>
    public class SqlParameter : DataParameter
    {
        /// <summary>
        /// Initializes the new instance of the <see cref="SqlParameter"/> class.
        /// </summary>
        public SqlParameter()
        {
            
        }
    }
}
