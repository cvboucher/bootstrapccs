//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Defines a service implemented by objects to support client scripts.
    /// </summary>
    public interface IClientScriptHelper
    {
        /// <summary>
        /// Register a control instance in client script array.
        /// </summary>
        /// <param name="control">The control instance to register.</param>
        void RegisterClientControl(Control control);
    }
}
