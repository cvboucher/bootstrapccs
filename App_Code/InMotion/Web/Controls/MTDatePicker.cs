//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using InMotion.Configuration;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Specifies the display style of link for <see cref="MTDatePicker"/>
    /// </summary>
    public enum DatePickerType
    {
        /// <summary>
        /// The <see cref="MTDatePicker"/> will display as html link.
        /// </summary>
        Link,
        /// <summary>
        /// The <see cref="MTDatePicker"/> will display as image button.
        /// </summary>
        ImageButton,
        /// <summary>
        /// The <see cref="MTDatePicker"/> will display as push button.
        /// </summary>
        Button
    }
    /// <summary>
    /// Represents the control that allows the user to select a date from a pop-up calendar.
    /// </summary>
    public class MTDatePicker : WebControl, IMTControl, IMTAttributeAccessor
    {
        private DatePickerType _DisplayType = DatePickerType.Link;
        /// <summary>
        /// Gets or sets the display type of <see cref="MTDatePicker"/>
        /// </summary>
        public DatePickerType DisplayType
        {
            get { return _DisplayType; }
            set { _DisplayType = value; }
        }

        private string _DisplayStyle;
        /// <summary>
        /// Gets or sets the path to the style for the DatePicker window.
        /// </summary>
        public string DisplayStyle
        {
            get
            {
                if (_DisplayStyle == null)
                    _DisplayStyle = "";
                return _DisplayStyle;
            }
            set { _DisplayStyle = value; }
        }


        private string _control;
        /// <summary>
        /// Gets or sets the id of control to which the selected date value will be automatically entered.
        /// </summary>
        public string ControlId
        {
            get { return _control; }
            set { _control = value; }
        }

        private string _value;
        /// <summary>
        /// Gets or sets the current value.
        /// </summary>
        public string Value
        {
            get { return _value; }
            set
            {
                _value = value;
            }
        }

        private Control FindControlByUniqueId(Control ctrl, string id)
        {
            if (ctrl.UniqueID == id || ctrl.ClientID == id || ctrl.ID == id)
                return ctrl;
            for (int i = 0; i < ctrl.Controls.Count; i++)
            {
                Control result = FindControlByUniqueId(ctrl.Controls[i], id);
                if (result != null)
                    return result;
            }
            return null;
        }

        private IMTEditableControl __BoundControl;
        private IMTEditableControl GetBoundControl()
        {
            if (__BoundControl == null && OwnerForm != null)
                __BoundControl = (IMTEditableControl)OwnerForm.GetControl<Control>(ControlId);
            if (__BoundControl == null && OwnerForm is EditableGrid)
            {
                for (int i = 0; i < ((EditableGrid)OwnerForm).Items.Count; i++)
                    if (FindControlByUniqueId(((EditableGrid)OwnerForm).Items[i], UniqueID) != null)
                        return (IMTEditableControl)(FindControlByUniqueId(((EditableGrid)OwnerForm).Items[i], ControlId));
            }
            return __BoundControl;
        }

        /// <summary>
        /// Raises the Init event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!Page.ClientScript.IsClientScriptIncludeRegistered("DatePickerScript"))
            {
                string url, url1;
                MTPage p = Page as MTPage;
                if (p != null)
                {
                    url = "~/ClientI18N.aspx?file=DatePicker.js&locale=" + p.ResManager.GetString("CCS_LocaleID");
                    url1 = "~/ClientI18N.aspx?file=Functions.js&locale=" + p.ResManager.GetString("CCS_LocaleID");
                }
                else
                {
                    url = "~/ClientI18N.aspx?file=DatePicker.js&locale=en";
                    url1 = "~/ClientI18N.aspx?file=Functions.js&locale=en";
                }
                Page.ClientScript.RegisterClientScriptInclude("CommonScript", ResolveClientUrl(url1));
                Page.ClientScript.RegisterClientScriptInclude("DatePickerScript", ResolveClientUrl(url));
            }
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            StringBuilder code = new StringBuilder();
            code.Append("var ");
            code.Append(UniqueID);
            code.Append(" = new Object();\n");
            code.Append(UniqueID);
            code.Append(".format = \"");
            if (GetBoundControl() != null && GetBoundControl().Format.Length > 0)
                code.Append(ControlsHelper.GetMTFormat(GetBoundControl().Format));
            else
                code.Append(ControlsHelper.GetMTFormat(AppConfig.DefaultDateFormat));
            code.Append("\";\n");
            code.Append(UniqueID);
            DisplayStyle = ControlsHelper.ReplaceResourcesAndStyles(DisplayStyle, Page);

            code.Append(".style = \"" + ResolveUrl(DisplayStyle) + "\";\n");

            code.Append(UniqueID);
            code.Append(".relativePathPart = \"");
            code.Append(ResolveClientUrl("~"));
            code.Append("\";\n");
            code.Append(UniqueID);
            code.Append(".themeVersion = \"3.0\";\n");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), UniqueID, code.ToString(), true);
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            OnBeforeShow(EventArgs.Empty);
            GetBoundControl();
        }

        /// <summary>
        /// Renders the <see cref="MTDatePicker"/> control to the specified HtmlTextWriter object. 
        /// </summary>
        /// <param name="writer">
        /// The <see cref="HtmlTextWriter"/> that receives the rendered output.
        ///</param>
        protected override void Render(HtmlTextWriter writer)
        {
            if (!Visible) return;
            Value = ControlsHelper.ReplaceResourcesAndStyles(Value, Page);
            AddAttributesToRender(writer);

            switch (DisplayType)
            {
                case DatePickerType.Link:
                    if (!DesignMode)
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, String.Format("javascript:showDatePicker('{0}','{1}','{2}');", UniqueID, "forms[\\''+document.Form1.id+'\\']", ((Control)GetBoundControl()).UniqueID));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(Value);
                    writer.RenderEndTag();
                    break;
                case DatePickerType.ImageButton:
                    if (!DesignMode)
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, String.Format("javascript:showDatePicker('{0}','{1}','{2}');", UniqueID, "forms[\\''+document.Form1.id+'\\']", ((Control)GetBoundControl()).UniqueID));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.AddAttribute(HtmlTextWriterAttribute.Border, "0");
                    writer.AddAttribute(HtmlTextWriterAttribute.Src, Value);
                    writer.RenderBeginTag(HtmlTextWriterTag.Img);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                    break;
                case DatePickerType.Button:
                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "button");
                    writer.AddAttribute(HtmlTextWriterAttribute.Value, Value);
                    if (!DesignMode)
                        writer.AddAttribute(HtmlTextWriterAttribute.Onclick, String.Format("showDatePicker('{0}','{1}','{2}');", UniqueID, "forms[\\''+document.Form1.id+'\\']", ((Control)GetBoundControl()).UniqueID));
                    writer.RenderBeginTag(HtmlTextWriterTag.Input);
                    writer.RenderEndTag();
                    break;
                default:
                    break;
            }
        }

        #region IMTControl Members

        /// <summary>
        /// Occurs after the <see cref="MTDatePicker"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }
        #endregion

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }
    }
}