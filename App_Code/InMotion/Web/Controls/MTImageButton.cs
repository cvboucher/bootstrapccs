//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InMotion.Common;
using System.Collections;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// A control that displays an image and responds to mouse clicks on the image.
    /// </summary>
    public class MTImageButton : ImageButton, IMTButtonControl, IMTAttributeAccessor
    {
        #region IMTControl Members

        /// <summary>
        /// Occurs after the <see cref="MTImageButton"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        #endregion

        /// <summary>
        /// Gets or sets the page name to which the user is directed to after the form has been submitted successfully.
        /// </summary>
        public string ReturnPage
        {
            get
            {
                if (ViewState["__ReturnPage"] == null)
                    ViewState["__ReturnPage"] = String.Empty;
                return (string)ViewState["__ReturnPage"];
            }
            set { ViewState["__ReturnPage"] = value; }
        }

        private bool _PerformRedirect = true;
        /// <summary>
        /// Gets or sets the value indicating whether form will perform redirect after the successfull submit.
        /// </summary>
        public bool PerformRedirect
        {
            get { return _PerformRedirect; }
            set { _PerformRedirect = value; }
        }

        /// <summary>
        /// Gets or sets a semicolon-separated list of parameters that should be removed from the hyperlink.
        /// </summary>
        public string RemoveParameters
        {
            get
            {
                if (ViewState["__RemoveParameters"] == null)
                    ViewState["__RemoveParameters"] = String.Empty;
                return (string)ViewState["__RemoveParameters"];
            }
            set { ViewState["__RemoveParameters"] = value; }
        }

        /// <summary>
        /// Gets or sets whether URLs should be automatically converted to absolute URLs or secure URLs for the SSL protocol (https://).
        /// </summary>
        public UrlType ConvertUrl
        {
            get
            {
                if (ViewState["__ConvertUrl"] == null)
                    ViewState["__ConvertUrl"] = UrlType.None;
                return (UrlType)ViewState["__ConvertUrl"];
            }
            set { ViewState["__ConvertUrl"] = value; }
        }

        /// <summary>
        /// Gets or sets the value indicating whether form should validate user input when this button is pressed.
        /// </summary>
        public bool EnableValidation
        {
            get
            {
                if (ViewState["__EnableValidation"] == null)
                    ViewState["__EnableValidation"] = true;
                return (bool)ViewState["__EnableValidation"];
            }
            set { ViewState["__EnableValidation"] = value; }
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            if (OwnerForm is IClientScriptHelper && Visible)
                (OwnerForm as IClientScriptHelper).RegisterClientControl(this);
            ImageUrl = ControlsHelper.ReplaceResource(ImageUrl);
            if (Page is MTPage)
                ImageUrl = ImageUrl.Replace("{CCS_Style}", (Page as MTPage).StyleName);
            if (!string.IsNullOrEmpty(AlternateText))
                AlternateText = InMotion.Web.Controls.ControlsHelper.ReplaceResource(AlternateText);
            base.OnPreRender(e);
            if (NamingContainer is RepeaterItem && !string.IsNullOrEmpty(Attributes["data-id"]))
                Attributes["data-id"] = Regex.Replace(Attributes["data-id"], @"\{\w+:rowNumber\}", (((RepeaterItem)NamingContainer).ItemIndex + 1).ToString());
        }
        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            if (DesignMode) return;
            if (OwnerForm != null && OwnerForm is Record)
            {
                Record r = (Record)OwnerForm;
                if (String.Compare(CommandName, "insert", StringComparison.OrdinalIgnoreCase) == 0 && !r.IsInsertMode)
                    Visible = false;
                if ((String.Compare(CommandName, "update", StringComparison.OrdinalIgnoreCase) == 0 || String.Compare(CommandName, "delete", StringComparison.OrdinalIgnoreCase) == 0) && r.IsInsertMode)
                    Visible = false;
                if (String.Compare(CommandName, "insert", StringComparison.OrdinalIgnoreCase) == 0 && (!r.UserRights.AllowInsert && r.Restricted || !r.AllowInsert))
                    Visible = false;
                if (String.Compare(CommandName, "delete", StringComparison.OrdinalIgnoreCase) == 0 && (!r.UserRights.AllowDelete && r.Restricted || !r.AllowDelete))
                    Visible = false;
                if (String.Compare(CommandName, "update", StringComparison.OrdinalIgnoreCase) == 0 && (!r.UserRights.AllowUpdate && r.Restricted || !r.AllowUpdate))
                    Visible = false;
            }
            else if (OwnerForm != null && OwnerForm is EditableGrid)
            {
                EditableGrid r = (EditableGrid)OwnerForm;
                if (String.Compare(CommandName, "submit", StringComparison.OrdinalIgnoreCase) == 0 &&
                    (!(r.UserRights.AllowUpdate || r.UserRights.AllowInsert || r.UserRights.AllowDelete) && r.Restricted || !r.AllowUpdate && !r.AllowDelete && !r.AllowInsert))
                    Visible = false;

            }
            OnBeforeShow(EventArgs.Empty);
        }
    }
}
