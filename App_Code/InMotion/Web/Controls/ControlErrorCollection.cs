//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents the collection of control's errors.
    /// </summary>
    public class ControlErrorCollection:Collection<string>
    {
        public List<string> keys = new List<string>(); 
        /// <summary>
        /// Occurs when error is removed from collection.
        /// </summary>
        public event EventHandler<EventArgs> ErrorRemoved;
        /// <summary>
        /// Occurs when error is added to collection.
        /// </summary>
        public event EventHandler<EventArgs> ErrorAdded;

        /// <summary>
        /// Raises the <see cref="ErrorRemoved"/> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> of event</param>
        protected void OnErrorRemoved(EventArgs e)
        {
            if (ErrorRemoved != null)
                ErrorRemoved(this, e);
        }

        /// <summary>
        /// Raises the <see cref="ErrorAdded"/> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> of event</param>
        protected void OnErrorAdded(EventArgs e)
        {
            if (ErrorAdded!= null)
                ErrorAdded(this, e);
        }

        /// <summary>
        /// Inserts an element into the Collection at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which item should be inserted.</param>
        /// <param name="item">The object to insert. The value can be a null reference (Nothing in Visual Basic) for reference types.</param>
        protected override void InsertItem(int index, string item)
        {
            item = ControlsHelper.ReplaceResource(item);
            if (!keys.Contains(item))
            {
                base.InsertItem(index, item);
                keys.Add(item);
                OnErrorAdded(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Removes the element at the specified index of the Collection.
        /// </summary>
        /// <param name="index">The zero-based index of the element to remove.</param>
        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
            OnErrorRemoved(EventArgs.Empty);
        }

        /// <summary>
        /// Removes all elements from the Collection.
        /// </summary>
        protected override void ClearItems()
        {
            base.ClearItems();
            OnErrorRemoved(EventArgs.Empty);
        }

        /// <summary>
        /// Replaces the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to replace.</param>
        /// <param name="item">The new value for the element at the specified index. The value can be a null reference (Nothing in Visual Basic) for reference types.</param>
        protected override void SetItem(int index, string item)
        {
            item = ControlsHelper.ReplaceResource(item);
            base.SetItem(index, item);
            OnErrorAdded(EventArgs.Empty);
        }

        public void Add(string value, string key)
        {
            if (!keys.Contains(key))
            {
                this.Add(value);
                keys.Add(key);
            }
        }
    }
}
