//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using InMotion.Common;
using InMotion.Security;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents a web report form.
    /// </summary>
    [PersistChildren(true), Designer(typeof(ReportDesigner))]
    public class Report : DataBoundControl, INamingContainer, IForm, ISortable, INavigable, IMTAttributeAccessor, IClientScriptHelper
    {
        private ITemplate _layoutheader;
        private ITemplate _layoutfooter;
        private Collection<ReportGroup> _groups = new Collection<ReportGroup>();
        private NameValueCollection _values = new NameValueCollection();
        private ArrayList sections = new ArrayList();
        private ArrayList specialValues = new ArrayList();
        private List<ReportSectionTemplate> controls = new List<ReportSectionTemplate>();
        private List<Control> navigators = new List<Control>();
        private Hashtable groupsData = new Hashtable();
        private bool isPageCreated;

        private ReportViewMode _viewMode = ReportViewMode.Web;
        private float _pageSize, _webPageSize, _pageSizeLimit;
        private float currPageSize;
        private int _totalPages;
        private uint _rowNumber;

        private int _CurrentItemIndex = -1;
        /// <summary>
        /// Gets the index of the current processed <see cref="ReportSection"/>.
        /// </summary>
        public int CurrentItemIndex
        {
            get { return _CurrentItemIndex; }
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        private ReportSectionTemplate __CurrentItem;
        /// <summary>
        /// Gets the current processed <see cref="ReportSectionTemplate"/>.
        /// </summary>
        public ReportSectionTemplate CurrentItem
        {
            get
            {
                if (__CurrentItem != null) return __CurrentItem;
                if (_CurrentItemIndex < 0 || _CurrentItemIndex > controls.Count - 1)
                    return null;
                return controls[_CurrentItemIndex];
            }
        }

        internal class TotalValue
        {
            private float firstS;
            public int second;
            private double firstD;
            private bool isInitialized;
            public bool isDouble;
            public string source = "";
            public SourceType sourceType = SourceType.DatabaseColumn;
            public TotalFunction function = TotalFunction.None;
            public string resetAt = "Report";
            public string percentOf = "";
            public bool percentOfService;

            public TotalValue(TotalFunction func, bool isDouble)
            {
                function = func;
                Reset();
                this.isDouble = isDouble;
            }

            public object Value
            {
                get
                {
                    if (isDouble)
                        return firstD;
                    else
                        return firstS;
                }
                set
                {
                    if (value == DBNull.Value) value = null;
                    if (isDouble)
                        firstD = Convert.ToDouble(value);
                    else
                        firstS = Convert.ToSingle(value);
                }
            }

            public object Average()
            {
                if (isDouble)
                    return firstD / second;
                else
                    return firstS / second;
            }

            public void AddFirst(object val)
            {
                if (val == DBNull.Value) val = null;
                if (isDouble)
                {
                    if (val != null && !isInitialized)
                    {
                        isInitialized = true;
                        firstD = 0;
                    }
                    firstD += Convert.ToDouble(val);
                }
                else
                {
                    if (val != null && !isInitialized)
                    {
                        isInitialized = true;
                        firstS = 0;
                    }
                    firstS += Convert.ToSingle(val);
                }
            }

            public void SetMin(object val)
            {
                if (val == DBNull.Value) val = null;
                if (isDouble)
                    firstD = Convert.ToDouble(val) < firstD || double.IsNaN(firstD) ? Convert.ToDouble(val) : firstD;
                else
                    firstS = Convert.ToSingle(val) < firstS || float.IsNaN(firstS) ? Convert.ToSingle(val) : firstS;
            }

            public void SetMax(object val)
            {
                if (val == DBNull.Value) val = null;
                if (isDouble)
                    firstD = Convert.ToDouble(val) > firstD || double.IsNaN(firstD) ? Convert.ToDouble(val) : firstD;
                else
                    firstS = Convert.ToSingle(val) > firstS || float.IsNaN(firstS) ? Convert.ToSingle(val) : firstS;
            }

            public object GetPercent(object val)
            {
                if (val == DBNull.Value) val = null;
                if (isDouble)
                {
                    if (val == null || firstD == 0) return Double.NaN;
                    return Convert.ToDouble(val) / firstD * (function == TotalFunction.Avg ? second : 1);
                }
                else
                {
                    if (val == null || firstS == 0) return Single.NaN;
                    return Convert.ToSingle(val) / firstS * (function == TotalFunction.Avg ? second : 1);
                }
            }

            public void Reset()
            {
                isInitialized = false;
                if (function == TotalFunction.Min || function == TotalFunction.Max || function == TotalFunction.Sum)
                {
                    this.firstS = float.NaN;
                    this.firstD = double.NaN;
                    this.second = 0;
                }
                else
                {
                    this.firstS = 0;
                    this.firstD = 0;
                    this.second = 0;
                }
            }
        }

        /// <summary>
        /// Occurs after an item in the <see cref="Report"/> is data-bound but before it is rendered on the page.
        /// </summary>
        public event EventHandler<BeforeItemDataBoundEventArgs> BeforeItemDataBound;

        /// <summary>
        /// Occurs when a button is clicked in the <see cref="Report"/> control.
        /// </summary>
        public event EventHandler<RepeaterCommandEventArgs> ItemCommand;

        /// <summary>
        /// Occurs after the <see cref="Report"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;

        /// <summary>
        /// Occurs before the Report send request to the datasource control for data.
        /// </summary>
        public event EventHandler<EventArgs> BeforeSelect;

        private Dictionary<string, TotalValue> calculationTable = new Dictionary<string, TotalValue>();
        private Dictionary<string, Dictionary<MTReportLabel, object>> percentValues = new Dictionary<string, Dictionary<MTReportLabel, object>>();
        private Hashtable headersLinks = new Hashtable();
        private Dictionary<string, ReportSection> templates = new Dictionary<string, ReportSection>();
        private float minPageSize;
        private float maxSectionSize;

        #region Properties

        private int _currentProcessingPage;
        /// <summary>
        /// Get or sets the number of currently processed page.
        /// </summary>
        public virtual int CurrentProcessingPage
        {
            get
            {
                return _currentProcessingPage;
            }
            set
            {
                _currentProcessingPage = value;
            }
        }

        /// <summary>
        /// Gets value indicates whethere use client paging.
        /// </summary>
        public bool UseClientPaging
        {
            get
            {
                if (ViewState["_ucp"] != null)
                    return (int)ViewState["_ucp"] == 1;
                else
                    return false;
            }
            set
            {
                ViewState["_ucp"] = value ? 1 : 0;
            }
        }

        /// <summary>
        /// Gets the total number of pages.
        /// </summary>
        public virtual int TotalPages
        {
            get
            {
                return _totalPages;
            }
        }

        /// <summary>
        /// Gets or sets the total number of records.
        /// </summary>
        public int TotalRecords
        {
            get { return SelectArguments.TotalRowCount; }
        }

        /// <summary>
        /// Gets the current row number.
        /// </summary>
        public virtual uint RowNumber
        {
            get
            {
                return _rowNumber;
            }
        }

        /// <summary>
        /// Gets value indicates whethere grid datasource is empty.
        /// </summary>
        public bool IsEmpty
        {
            get { return SelectArguments.TotalRowCount <= 0; }
        }

        /// <summary>
        /// Gets or sets the size of page in the Print view mode, measured in lines.
        /// </summary>
        public virtual float PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of page in the Web show mode, measured in lines. 
        /// </summary>
        public virtual float WebpageSize
        {
            get
            {
                return _webPageSize;
            }
            set
            {
                _webPageSize = value;
            }
        }

        /// <summary>
        /// Gets or sets the maximum number of lines per page for Web view mode.
        /// </summary>
        public virtual float PageSizeLimit
        {
            get
            {
                return _pageSizeLimit;
            }
            set
            {
                _pageSizeLimit = value;
            }
        }

        private bool __enablePrint = true;
        /// <summary>
        /// Gets or sets the value which indicates whether print mode could be enabled.
        /// </summary>
        public bool EnablePrintMode
        {
            get { return __enablePrint; }
            set { __enablePrint = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="ReportViewMode"/> which will be used for report.
        /// </summary>
        public virtual ReportViewMode ViewMode
        {
            get
            {
                return _viewMode;
            }
            set
            {
                _viewMode = value;
            }
        }

        private float ActivePageSize
        {
            get
            {
                if (ViewMode == ReportViewMode.Print)
                    return PageSize;
                else
                    return WebpageSize;
            }
        }

        /// <summary>
        /// This property supports the framework infrastructure and is not intended to be used directly from your code.
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        ]
        public virtual ReportSection Section
        {
            get
            {
                return null;
            }
            set
            {
                ReportSection section = new ReportSection();
                value.Template.InstantiateIn(section);
                if (value.Visible && (value.Name == "Page_Header" || value.Name == "Page_Footer"))
                    minPageSize += value.Height;
                else if (value.Visible)
                    if (value.Height > maxSectionSize) maxSectionSize = value.Height;
                PopulateCalculationTable(section.Controls, value.Name);
                templates.Add(value.Name, value);
            }
        }

        /// <summary>
        /// Gets sections the name of <see cref="Report"/> control. 
        /// </summary>
        [
        Browsable(false),
        PersistenceMode(PersistenceMode.InnerProperty),
        ]
        public ArrayList Sections
        {
            get
            {
                return sections;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the header section of <see cref="Report"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(ReportSectionTemplate))
        ]
        public virtual ITemplate LayoutHeaderTemplate
        {
            get
            {
                return _layoutheader;
            }
            set
            {
                _layoutheader = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the footer section of <see cref="Report"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(ReportSectionTemplate))
        ]
        public virtual ITemplate LayoutFooterTemplate
        {
            get
            {
                return _layoutfooter;
            }
            set
            {
                _layoutfooter = value;
            }
        }

        /// <summary>
        /// Gets or sets the Dictionary that defined sections of <see cref="Report"/> control. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(ReportSectionTemplate))
        ]
        public virtual Dictionary<string, ReportSection> Templates
        {
            get
            {
                return templates;
            }
            set
            {
                templates = value;
            }
        }

        /// <summary>
        /// Gets the list of defined report groups.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public virtual Collection<ReportGroup> Groups
        {
            get
            {
                return _groups;
            }

        }
        #endregion

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeSelect"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeSelect(EventArgs e)
        {
            if (BeforeSelect != null)
                BeforeSelect(this, e);
        }

        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code. 
        /// Assigns any sources of the event and its information to the parent control.
        /// </summary>
        /// <param name="source">The source of the event. </param>
        /// <param name="args">An <see cref="EventArgs"/> that contains the event data.</param>
        /// <returns><b>true</b> if the event assigned to the parent was raised, otherwise <b>false</b>.</returns>
        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            bool handled = false;
            if (ItemCommand != null)
            {
                ItemCommand(this, new RepeaterCommandEventArgs(null, source, (CommandEventArgs)args));
                handled = true;
            }
            return handled;
        }

        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code.
        /// </summary>
        /// <param name="col">A <see cref="ControlCollection"/> of calculated Item.</param>
        /// <param name="sectionName">The section name.</param>
        protected void PopulateCalculationTable(ControlCollection col, string sectionName)
        {
            foreach (Control control in col)
            {
                if (control.Controls.Count > 0)
                    PopulateCalculationTable(control.Controls, sectionName);
                if (control is MTReportLabel)
                {
                    MTReportLabel rl = (MTReportLabel)control;
                    if (rl.Function != TotalFunction.None)
                    {
                        TotalValue val = new TotalValue(rl.Function, rl.DataType == DataType.Float);
                        val.source = rl.Source;
                        val.sourceType = rl.SourceType;
                        val.resetAt = rl.ResetAt;
                        val.percentOf = rl.PercentOf;
                        calculationTable.Add(rl.ID, val);
                    }
                    if (rl.PercentOf.Length > 0)
                    {
                        TotalValue val = new TotalValue(rl.Function != TotalFunction.None ? rl.Function : TotalFunction.Sum, rl.DataType == DataType.Float);
                        val.source = rl.Source;
                        val.sourceType = rl.SourceType;
                        val.resetAt = rl.PercentOf;
                        val.percentOf = rl.PercentOf;
                        val.percentOfService = true;
                        calculationTable.Add("__PercentOf" + rl.ID, val);
                        percentValues.Add("__PercentOf" + rl.ID, new Dictionary<MTReportLabel, object>());
                    }
                }
            }
        }

        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code.
        /// </summary>
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            Controls.Clear();
            if (ViewState["sections"] != null)
            {

                if (LayoutHeaderTemplate != null)
                {
                    ReportSectionTemplate head = new ReportSectionTemplate(this);
                    LayoutHeaderTemplate.InstantiateIn(head);
                    Controls.Add(head);

                }

                sections = (ArrayList)ViewState["sections"];
                foreach (object name in sections)
                {
                    ReportSectionTemplate item = new ReportSectionTemplate(this);
                    templates[name.ToString()].Template.InstantiateIn(item);
                    item.Name = name.ToString();
                    Controls.Add(item);
                }
                if (LayoutFooterTemplate != null)
                {
                    ReportSectionTemplate footer = new ReportSectionTemplate(this);
                    LayoutFooterTemplate.InstantiateIn(footer);
                    Controls.Add(footer);
                }
            }
        }

        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code.
        /// </summary>
        /// <param name="col">A <see cref="ControlCollection"/> of calculated Item.</param>
        protected void CalculateSpecialControls(ControlCollection col)
        {
            foreach (Control control in col)
            {
                if (!(control is MTReportLabel)) continue;
                MTReportLabel rl = (MTReportLabel)control;
                if (rl.SourceType == SourceType.SpecialValue)
                {
                    specialValues.Add(rl);
                    switch (rl.Source)
                    {
                        case "RowNumber":
                            rl.Value = RowNumber;
                            break;
                        case "PageNumber":
                            rl.Value = CurrentProcessingPage;
                            break;
                        case "CurrentDate":
                            rl.DataType = DataType.Text;
                            rl.Value = DateTime.Now.ToString("d");
                            break;
                        case "CurrentDateTime":
                            rl.DataType = DataType.Text;
                            rl.Value = DateTime.Now.ToString("G");
                            break;
                        case "CurrentTime":
                            rl.DataType = DataType.Text;
                            rl.Value = DateTime.Now.ToString("t");
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code.
        /// </summary>
        /// <param name="di">A DataItem object.</param>
        /// <param name="col">A <see cref="ControlCollection"/> of calculated Item.</param>
        /// <param name="sectionName">The section name.</param>
        /// <param name="isHeader">true if calculated item is header item; otherwise false.</param>
        /// <param name="isTotalOnly">Indicates whethere execute only total calculation.</param>
        protected void CalculateControls(object di, ControlCollection col, string sectionName, bool isHeader, bool isTotalOnly)
        {
            CalculateSpecialControls(col);
            foreach (Control control in col)
            {
                if (control.Controls.Count > 0)
                    CalculateControls(di, control.Controls, sectionName, isHeader, isTotalOnly);

                if (control is MTNavigator) navigators.Add(control);
                if (control is MTReportLabel)
                {
                    MTReportLabel rl = (MTReportLabel)control;
                    object currentValue = null;
                    bool isInitialized = false;
                    if (rl.Function != TotalFunction.None)
                    {
                        if (rl.Function == TotalFunction.Avg)
                            currentValue = ((TotalValue)(calculationTable[rl.ID])).Average();
                        else
                            currentValue = ((TotalValue)(calculationTable[rl.ID])).Value;
                        if (rl.DataType != DataType.Float || rl.DataType != DataType.Single || rl.DataType != DataType.Integer)
                            rl.DataType = DataType.Float;
                        rl.Value = currentValue;
                        isInitialized = true;

                    }
                    else if (di != null && rl.SourceType != SourceType.SpecialValue && !isTotalOnly && rl.Source.Length > 0)
                    {
                        currentValue = DataSourceHelper.GetFieldValue(rl.Source, di);
                        rl.Value = currentValue;
                        isInitialized = true;
                    }

                    if (rl.PercentOf.Length > 0 && isInitialized)
                    {
                        percentValues["__PercentOf" + rl.ID][rl] = currentValue;

                    }
                    if (rl.HideDuplicates)
                        if (_values[rl.ID] != null)
                        {
                            rl.Visible = _values[rl.ID] != rl.Text;
                            _values[rl.ID] = rl.Text;
                        }
                        else
                        {
                            _values.Add(rl.ID, rl.Text);
                        }
                }
            }
        }

        private ReportSectionTemplate InstantiateSection(string name, bool isHeader, object di)
        {
            return InstantiateSection(name, isHeader, di, false, false, true);
        }

        private ReportSectionTemplate InstantiateSection(string name, bool isHeader, object di, bool noRecordsFlag)
        {
            return InstantiateSection(name, isHeader, di, noRecordsFlag, false, true);
        }

        private ReportSectionTemplate InstantiateSection(string name, bool isHeader, object di, bool noRecordsFlag, bool lastSection, bool addToCollection)
        {
            ReportSectionTemplate item = null;
            string sName = name;
            float sectionHeight = 0, footerHeight = 0;
            if (name != "Detail")
                sName += isHeader ? "_Header" : "_Footer";
            if (templates.ContainsKey(sName) && templates[sName].Visible)
                sectionHeight = templates[sName].Height;
            if (templates.ContainsKey("Page_Footer") && templates["Page_Footer"].Visible)
                footerHeight = templates["Page_Footer"].Height;

            if (name == "Page" && isHeader)
            {
                groupsData["Page"] = di;
                _currentProcessingPage++;
                _totalPages++;
            }
            if (ActivePageSize != 0 && currPageSize + footerHeight + sectionHeight > ActivePageSize && name != "Page" && addToCollection)
            {
                InstantiateSection("Page", false, groupsData["Page"]);
                if (ViewMode == ReportViewMode.Web &&
                    CurrentPage != CurrentProcessingPage && !isPageCreated && (!lastSection || name == "Report"))
                    controls.Clear();
                else if (ViewMode == ReportViewMode.Web &&
                    CurrentPage == CurrentProcessingPage)
                    isPageCreated = true;
                currPageSize = 0;
                InstantiateSection("Page", true, di);
            }
            if (addToCollection)
                currPageSize += sectionHeight;

            if (BeforeItemDataBound != null)
                BeforeItemDataBound(this, new BeforeItemDataBoundEventArgs(di));
            if (name == "Detail" && !DesignMode)
            {
                _rowNumber++;
                foreach (KeyValuePair<string, TotalValue> v in calculationTable)
                {
                    object val = null;
                    if (!String.IsNullOrEmpty(v.Value.source))
                        val = DataSourceHelper.GetFieldValue(v.Value.source, di);
                    if (val == DBNull.Value) val = null;
                    switch (v.Value.function)
                    {
                        case TotalFunction.Avg:
                            if (val != null)
                            {
                                v.Value.AddFirst(val);
                                v.Value.second++;
                            }
                            break;
                        case TotalFunction.Sum:
                            v.Value.AddFirst(val);
                            break;
                        case TotalFunction.Min:
                            if (val != null)
                                v.Value.SetMin(val);
                            break;
                        case TotalFunction.Max:
                            if (val != null)
                                v.Value.SetMax(val);
                            break;
                        case TotalFunction.Count:
                            if (String.IsNullOrEmpty(v.Value.source))
                                v.Value.AddFirst(1);
                            else
                                v.Value.AddFirst(val == null ? 0 : 1);
                            break;
                    }
                }
            }

            if (templates.ContainsKey(sName))
            {
                item = new ReportSectionTemplate(this);
                templates[sName].Template.InstantiateIn(item);
                item.Name = sName;
                item.Visible = templates[sName].Visible;
                item.Height = templates[sName].Height;
                item.DataItem = di;
                _dataItem = new DataItem(di);
                CalculateControls(di, item.Controls, name, isHeader, false);
                if (controls.Count > 0 && controls[controls.Count - 1].Name == "Detail" &&
                    controls[controls.Count - 1].FindControl("Separator") != null) controls[controls.Count - 1].FindControl("Separator").Visible = sName == "Detail";
                if (item.FindControl("NoRecords") != null) item.FindControl("NoRecords").Visible = noRecordsFlag;
                if ((lastSection || ViewMode == ReportViewMode.Web) && item.FindControl("PageBreak") != null) item.FindControl("PageBreak").Visible = false;
                item.ID = "tempID";
                Controls.Add(item);
                Controls.Remove(item);
                item.ID = "";
                if (addToCollection && !isPageCreated)
                {
                    _CurrentItemIndex++;
                    controls.Add(item);
                }
                __CurrentItem = item;
                templates[sName].RaiseCalculate(new OnCalculateEventArgs(di, sName, item));
                __CurrentItem = null;
                if (ViewMode == ReportViewMode.Print ||
                    ViewMode == ReportViewMode.Web &&
                    CurrentPage == CurrentProcessingPage &&
                    sName != "Page_Footer" ||
                    sName == "Page_Footer" &&
                    lastSection ||
                    sName == "Report_Header" &&
                    CurrentPage == 1 ||
                    sName == "Report_Footer" &&
                    CurrentPage == TotalPages
                    )
                {
                    __CurrentItem = item;
                    item.DataBind();
                    __CurrentItem = null;
                }

                if (isHeader && !headersLinks.Contains(name)) headersLinks.Add(name, item);
            }

            if (headersLinks.Contains(name) && !isHeader)
            {
                CalculateControls(di, ((ReportSectionTemplate)headersLinks[name]).Controls, name, isHeader, true);
                headersLinks.Remove(name);
            }
            if (!isHeader)
                foreach (KeyValuePair<string, TotalValue> val in calculationTable)
                {
                    if (val.Value.percentOf == name && val.Value.percentOfService)
                    {
                        foreach (KeyValuePair<MTReportLabel, object> pair in percentValues[val.Key])
                            pair.Key.Value = val.Value.GetPercent(pair.Value);
                        percentValues[val.Key].Clear();
                        if (!(name == "Page" && lastSection))
                            val.Value.Reset();
                    }
                    if (val.Value.resetAt == name && name != "Report" && !(name == "Page" && lastSection))
                        val.Value.Reset();
                }
            return item;
        }

        private void OpenGroup(string name, object di)
        {
            bool flag = false;
            foreach (ReportGroup rg in Groups)
                if (rg.Name == name || flag)
                {
                    groupsData[rg.Name] = di;
                    InstantiateSection(rg.Name, true, di);
                    flag = true;
                }
        }

        private void CloseGroup(string name, object di)
        {
            for (int i = Groups.Count - 1; i >= 0; i--)
            {
                InstantiateSection(Groups[i].Name, false, groupsData[Groups[i].Name]);
                if (Groups[i].Name == name) break;
            }
        }

        /// <summary>
        /// Raises the Init event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            minPageSize += maxSectionSize;
            if (!DesignMode)
            {
                if (Page.Request["ViewMode"] != null)
                {
                    ReportViewMode vm = (ReportViewMode)Enum.Parse(typeof(ReportViewMode), Page.Request["ViewMode"], true);
                    if (EnablePrintMode || vm == ReportViewMode.Web)
                        ViewMode = vm;
                }

                if (Page.Request[ID + "Page"] != null)
                {
                    short val;
                    Int16.TryParse(Page.Request[ID + "Page"], out val);
                    CurrentPage = val;
                    if (CurrentPage <= 0) CurrentPage = 1;
                }

                if (Page.Request[ID + "PageSize"] != null)
                {
                    float tempval;
                    Single.TryParse(Page.Request[ID + "PageSize"], out tempval);
                    if (tempval < 0) return;
                    if (tempval < minPageSize && tempval != 0) tempval = minPageSize;
                    if (ViewMode == ReportViewMode.Print)
                        PageSize = tempval;
                    else
                    {
                        WebpageSize = tempval;
                        if (PageSizeLimit > 0 && WebpageSize > PageSizeLimit || WebpageSize == 0)
                            WebpageSize = PageSizeLimit;
                    }
                }

                if (Page.Request[ID + "Order"] != null)
                {
                    if (templates.ContainsKey("Page_Header"))
                    {
                        ReportSectionTemplate ri = new ReportSectionTemplate(this);
                        templates["Page_Header"].Template.InstantiateIn(ri);

                        MTSorter c = ri.FindControl(Page.Request.QueryString[ID + "Order"]) as MTSorter;
                        if (c == null && templates.ContainsKey("Page_Footer"))
                        {
                            templates["Page_Footer"].Template.InstantiateIn(ri);
                            c = ri.FindControl(Page.Request.QueryString[ID + "Order"]) as MTSorter;
                        }
                        if (c != null)
                            if (Page.Request.QueryString[ID + "Dir"] != null
                             && String.Compare(Page.Request.QueryString[ID + "Dir"], "desc", StringComparison.OrdinalIgnoreCase) == 0)
                                SortExpression = c.ReverseOrder;
                            else
                                SortExpression = c.SortOrder;
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves data from the associated data source.
        /// </summary>
        protected override void PerformSelect()
        {
            OnBeforeSelect(EventArgs.Empty);
            SelectArguments.RetrieveTotalRowCount = false;
            if (DesignMode)
            {
                groupsData.Clear();
                controls.Clear();
                OnDataSourceViewSelectCallback(null);
            }
            else
            {
                string staticOrder = "";
                foreach (ReportGroup rg in Groups)
                {
                    staticOrder += rg.SqlField + " " + rg.SortOrder + ",";
                }
                if (SortExpression.Length > 0)
                    staticOrder += SortExpression;
                else
                    staticOrder = staticOrder.TrimEnd(',');
                SelectArguments.SortExpression = staticOrder;
                GetData().Select(SelectArguments, OnDataSourceViewSelectCallback);
            }
            RequiresDataBinding = false;
            MarkAsDataBound();

            OnDataBound(EventArgs.Empty);
        }

        private void OnDataSourceViewSelectCallback(IEnumerable retrievedData)
        {
            if (IsBoundUsingDataSourceID)
            {
                OnDataBinding(EventArgs.Empty);
            }
            if (retrievedData != null && retrievedData.GetEnumerator().MoveNext()) __hasData = true;
            PerformDataBinding(retrievedData);
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="HtmlTextWriter"/> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the control content.</param>
        public override void RenderControl(HtmlTextWriter writer)
        {
            if (!DesignMode && (!Visible || Restricted && !UserRights.AllowRead)) return;
            RenderContents(writer);
        }

        /// <summary>
        /// Binds the specified data source to the <see cref="Report"/> control.
        /// </summary>
        /// <param name="data">An object that represents the data source; the object must implement the <see cref="IEnumerable"/> interface.</param>
        protected override void PerformDataBinding(IEnumerable data)
        {
            IEnumerable dataCollection;
            if (data == null)
                dataCollection = new System.Data.DataView();
            else
                dataCollection = data;
            if (!DesignMode && (!Visible || Restricted && !UserRights.AllowRead)) return;

            base.PerformDataBinding(dataCollection);
            ClearChildViewState();
            Controls.Clear();
            sections.Clear();
            if (LayoutHeaderTemplate != null)
            {
                ReportSectionTemplate head = new ReportSectionTemplate(this);
                LayoutHeaderTemplate.InstantiateIn(head);
                Controls.Add(head);
                head.DataBind();
            }
            object item = null;
            if (!DesignMode)
            {
                IEnumerator en = dataCollection.GetEnumerator();
                if (en.MoveNext())
                    item = en.Current;
            }
            groupsData.Add("Report", item);
            groupsData.Add("Page", null);
            for (int i = Groups.Count - 1; i >= 0; i--)
                groupsData.Add(Groups[i].Name, null);
            InstantiateSection("Report", true, item);
            InstantiateSection("Page", true, item);
            object previousDi = null;
            if (!DesignMode)
            {
                SelectArguments.TotalRowCount = ((System.Data.DataView)dataCollection).Count;
                int k = 0;
                foreach (object di in dataCollection)
                {
                    for (int i = 0; i < Groups.Count; i++)
                        if (k > 0 &&
                            !DataSourceHelper.GetFieldValue(Groups[i].Field, di).Equals(
                            DataSourceHelper.GetFieldValue(Groups[i].Field, previousDi)))
                        {
                            CloseGroup(Groups[i].Name, di);
                            break;
                        }

                    for (int i = 0; i < Groups.Count; i++)
                        if (k == 0 ||
                            !DataSourceHelper.GetFieldValue(Groups[i].Field, di).Equals(
                            DataSourceHelper.GetFieldValue(Groups[i].Field, previousDi)))
                        {
                            OpenGroup(Groups[i].Name, di);
                            break;
                        }
                    InstantiateSection("Detail", false, di, false);
                    k++;
                    previousDi = di;
                }
            }
            else
            {
                for (int i = 0; i < Groups.Count; i++)
                    OpenGroup(Groups[i].Name, null);

                InstantiateSection("Detail", false, null, false);

                for (int i = Groups.Count - 1; i > 0; i--)
                    CloseGroup(Groups[i].Name, null);
            }
            if (Groups.Count > 0)
                CloseGroup(Groups[0].Name, previousDi);
            ReportSectionTemplate lastFooter = InstantiateSection("Page", false, groupsData["Page"], false, true, false);
            InstantiateSection("Report", false, groupsData["Report"], previousDi == null, true, true);
            if (lastFooter != null)
                CalculateSpecialControls(lastFooter.Controls);
            if (TotalPages < CurrentPage) CurrentPage = TotalPages;
            if (lastFooter != null && (ViewMode == ReportViewMode.Print ||
                (ViewMode == ReportViewMode.Web && CurrentPage == TotalPages)))
            {
                controls.Add(lastFooter);
                _CurrentItemIndex++;
            }

            foreach (ReportSectionTemplate c in controls)
            {
                if (UseClientPaging && c.Name == "Page_Header")
                {
                    Controls.Add(c);
                    sections.Add(c.Name);
                    foreach (ReportSectionTemplate c1 in controls)
                        if (c1.Name == "Page_Footer")
                        {
                            Controls.Add(c1);
                            sections.Add(c1.Name);
                            break;
                        }
                }
                else if (!UseClientPaging || c.Name != "Page_Footer")
                {
                    Controls.Add(c);
                    sections.Add(c.Name);
                }
            }
            if (LayoutFooterTemplate != null)
            {
                ReportSectionTemplate footer = new ReportSectionTemplate(this);
                LayoutFooterTemplate.InstantiateIn(footer);
                Controls.Add(footer);
                footer.DataBind();
            }

            foreach (Control nav in navigators)
                if (ViewMode != ReportViewMode.Web)
                    nav.Visible = false;

            foreach (object rl in specialValues)
            {
                if (((MTReportLabel)rl).Source == "TotalPages")
                    ((MTReportLabel)rl).Value = TotalPages;
            }

            ChildControlsCreated = true;
            ViewState["sections"] = sections;
            _CurrentItemIndex = 0;
            for (int i = 0; i < Controls.Count; i++)
                if (Controls[i] is ReportSectionTemplate && ((ReportSectionTemplate)Controls[i]).Name != "")
                {
                    templates[((ReportSectionTemplate)Controls[i]).Name].RaiseBeforeShow
                        (new BeforeShowSectionEventArgs((ReportSectionTemplate)Controls[i]));
                    _CurrentItemIndex++;
                }

            OnBeforeShow(EventArgs.Empty);
        }

        #region IForm Members

        private DataItem _dataItem;
        /// <summary>
        /// Gets the current <see cref="DataItem"/>.
        /// </summary>
        public DataItem DataItem
        {
            get
            {
                return _dataItem;
            }
        }

        private FormSupportedOperations __UserRights;
        /// <summary>
        /// Gets the <see cref="FormSupportedOperations"/> for the current form.
        /// </summary>
        public FormSupportedOperations UserRights
        {
            get
            {
                if (__UserRights == null)
                    __UserRights = new FormSupportedOperations();
                return __UserRights;
            }
        }

        /// <summary>
        /// Find all child controls of type <i>T</i>.
        /// </summary>
        /// <typeparam name="T">The type of controls to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <returns>the <see cref="ReadOnlyCollection&lt;T&gt;"/></returns>
        public ReadOnlyCollection<T> GetControls<T>()
            where T : IMTControl
        {
            List<T> result = new List<T>();
            List<int> rows = new List<int>(new int[] { 0, Controls.Count - 1 });
            if (_CurrentItemIndex != 0 && _CurrentItemIndex != Controls.Count - 1) rows.Insert(0, _CurrentItemIndex);
            for (int i = 0; i < rows.Count && rows[i] >= 0; i++)
            {
                ControlsHelper.PopulateControlCollection<T>(result, Controls[i].Controls);
            }
            return new ReadOnlyCollection<T>(result);
        }

        /// <summary>
        /// Find a control of type <i>T</i> with specified <see cref="Control.ID"/>
        /// </summary>
        /// <typeparam name="T">The type of control to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>T</i>.</returns>
        public T GetControl<T>(string id)
            where T : Control
        {
            if (CurrentItem == null) return null;

            T ctrl = (T)CurrentItem.FindControl(id);
            if (ctrl == null)
                ctrl = (T)InMotion.Common.Controls.FindControlIterative(this.CurrentItem, id);
            return ctrl;
        }

        /// <summary>
        /// Find a control with specified <see cref="Control.ID"/>
        /// </summary>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>Control</i></returns>
        public Control GetControl(string id)
        {
            return GetControl<Control>(id);
        }

        private bool _restricted;
        /// <summary>
        /// Gets or sets the value indicating whether form will restrict the user access according to the <see cref="UserRights"/>.
        /// </summary>
        public bool Restricted
        {
            get
            {
                return _restricted;
            }
            set
            {
                _restricted = value;
            }
        }
        #endregion

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            RegisterArrayDeclaration();
        }

        /// <summary>
        /// Registers a JavaScript array declaration with the page object.
        /// </summary>
        protected virtual void RegisterArrayDeclaration()
        {
            if (Page is MTPage)
            {
                ((MTPage)Page).MTClientScript.RegisterArrayDeclaration("MTForms", String.Format("'{0}','{1}','{2}','{3}'", ID, Controls.Count, "", ""));
            }
            else
            {
                Page.ClientScript.RegisterArrayDeclaration("MTForms", String.Format("'{0}','{1}','{2}','{3}'", ID, Controls.Count, "", ""));
            }
        }

        private static ReportSectionTemplate GetControlContainer(Control control)
        {
            Control c = control.NamingContainer;
            while (!(c is ReportSectionTemplate) && c != null)
                c = c.NamingContainer;
            return c as ReportSectionTemplate;
        }

        /// <summary>
        /// Register a control instance in client script array.
        /// </summary>
        /// <param name="control">The control instance to register.</param>
        public void RegisterClientControl(Control control)
        {
            ReportSectionTemplate ri = GetControlContainer(control);
            string arrayName = ID + "Controls_Row" + this.Controls.IndexOf(ri);
            if (Page is MTPage)
            {
                ((MTPage)Page).MTClientScript.RegisterArrayDeclaration(arrayName, String.Format("'{0}','{1}'", control.ID, control.ClientID));
            }
            else
            {
                Page.ClientScript.RegisterArrayDeclaration(arrayName, String.Format("'{0}','{1}'", control.ID, control.ClientID));
            }
        }

        private string _sortExpression = "";
        /// <summary>
        /// Gets or sets the current sort expression.
        /// </summary>
        public string SortExpression
        {
            get
            {
                return _sortExpression;
            }
            set
            {
                _sortExpression = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of currently displayed page.
        /// </summary>
        public int CurrentPage
        {
            get
            {
                if (ViewState["_pageNumber"] != null)
                    return (int)ViewState["_pageNumber"];
                else
                    return 1;
            }
            set
            {
                ViewState["_pageNumber"] = value;
            }
        }

        private bool __hasData;
        /// <summary>
        /// Gets value indicating whether form has non-empty data object.
        /// </summary>
        public virtual bool HasData
        {
            get { return __hasData; }
        }

        /// <summary>
        /// Retrieves the <see cref="DataSourceView"/> object that is associated with the current form.
        /// </summary>
        /// <returns>The <see cref="DataSourceView"/> object that is associated with the current form.</returns>
        public DataSourceView GetDataSourceView()
        {
            Control c1 = Parent;
            IDataSource dataSource = null;
            while (dataSource == null && c1 != Page)
            {
                dataSource = c1.FindControl(DataSourceID) as IDataSource;
                c1 = c1.Parent;
            }
            return dataSource != null ? dataSource.GetView(DataMember) : null;
        }
    }

    /// <summary>
    /// Provides a control designer class for extending the design-mode behavior of a <see cref="Report"/> control.
    /// </summary>
    class ReportDesigner : MTFormDesigner
    {
        /// <summary>
        /// Gets a collection of template groups, each containing one or more template definitions.
        /// </summary>
        public override TemplateGroupCollection TemplateGroups
        {
            get
            {
                if (__TemplateGroups == null)
                {
                    Report ctrl = (Report)Component;
                    __TemplateGroups = base.TemplateGroups;

                    TemplateGroup tempGroup;
                    TemplateDefinition tempDef;

                    tempGroup = new TemplateGroup("Common Templates");
                    tempDef = new TemplateDefinition(this, "LayoutHeaderTemplate", ctrl, "LayoutHeaderTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);

                    tempDef = new TemplateDefinition(this, "LayoutFooterTemplate", ctrl, "LayoutFooterTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);


                    tempGroup = new TemplateGroup("Item Templates");
                    for (int i = 0; i < ctrl.Sections.Count; i++)
                    {
                        tempDef = new TemplateDefinition(this, ctrl.Sections[i].ToString(), ctrl.Templates[ctrl.Sections[i].ToString()], "Template", false);
                        tempGroup.AddTemplateDefinition(tempDef);
                    }
                    __TemplateGroups.Add(tempGroup);
                }
                return __TemplateGroups;
            }
        }
    }
}