//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.ComponentModel;
using InMotion.Security;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// The directory control is specifically intended for displaying hierarchical content.
    /// </summary>
    [Designer(typeof(DirectoryDesigner))]
    public class Directory : DataBoundControl, INamingContainer, IForm, IMTAttributeAccessor, IClientScriptHelper
    {
        [Serializable()]
        internal class DummySource
        {
            private string m_categoryID;
            private string m_subcatID;

            public string CategoryId
            {
                get
                {
                    return m_categoryID;
                }
                set
                {
                    m_categoryID = value;
                }
            }

            public string SubcategoryId
            {
                get
                {
                    return m_subcatID;
                }
                set
                {
                    m_subcatID = value;
                }
            }

            private string _CatIdName;

            public string CategoryIdName
            {
                get { return _CatIdName; }
                set { _CatIdName = value; }
            }
            private string _subcatIdName;

            public string SubcategoryIdName
            {
                get { return _subcatIdName; }
                set { _subcatIdName = value; }
            }
        }

        #region Events
        /// <summary>
        /// Occurs before the <see cref="Directory"/> send request to the datasource control for data.
        /// </summary>
        public event EventHandler<EventArgs> BeforeSelect;
        /// <summary>
        /// Occurs after the <see cref="Directory"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;
        /// <summary>
        /// Occurs when an item is created in the <see cref="Directory"/> control. 
        /// </summary>
        public event EventHandler<DirectoryItemEventArgs> ItemCreated;
        /// <summary>
        /// Occurs after an item in the <see cref="Directory"/> is data-bound but before it is rendered on the page.
        /// </summary>
        public event EventHandler<DirectoryItemEventArgs> ItemDataBound;
        /// <summary>
        /// Occurs when a button is clicked in the <see cref="Directory"/> control.
        /// </summary>
        public event EventHandler<DirectoryCommandEventArgs> ItemCommand;

        /// <summary>
        /// Occurs before showing (populating template with data) each category. 
        /// </summary>
        public event EventHandler<EventArgs> BeforeShowCategory;
        /// <summary>
        /// Occurs before showing (populating template with data) each subcategory. 
        /// </summary>
        public event EventHandler<EventArgs> BeforeShowSubcategory;
        #endregion

        #region Member variables
        private ITemplate footerTemplate;
        private ITemplate headerTemplate;
        private ITemplate categoryHeaderTemplate;
        private ITemplate categoryFooterTemplate;
        private ITemplate categorySeparatorTemplate;
        private ITemplate subcategoryTemplate;
        private ITemplate subcategorySeparatorTemplate;
        private ITemplate subcategoriesTailTemplate;
        private ITemplate columnSeparatorTemplate;
        private ITemplate noRecordsTemplate;
        #endregion

        #region Properties

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        private int _CurrentItemIndex = -1;
        /// <summary>
        /// Gets the index of the current processed <see cref="DirectoryItem"/>.
        /// </summary>
        public int CurrentItemIndex
        {
            get { return _CurrentItemIndex; }
        }

        /// <summary>
        /// Gets the current processed <see cref="DirectoryItem"/>.
        /// </summary>
        public DirectoryItem CurrentItem
        {
            get
            {
                if (_CurrentItemIndex < 0 || _CurrentItemIndex > Controls.Count - 1)
                    return null;
                return (DirectoryItem)Controls[_CurrentItemIndex];
            }
        }

        /// <summary>
        /// Gets or sets the number of columns which should be shown in the Directory form.
        /// </summary>
        [
        Bindable(true),
        Category("Data"),
        DefaultValue(null),
        Description("The No. of colums"),
        ]
        public int NumberOfColumns
        {
            get
            {
                if (ViewState["columnCount"] == null) return 1;
                return (int)ViewState["columnCount"];
            }
            set
            {
                ViewState["columnCount"] = value;
            }
        }
        /// <summary>
        /// Get or sets the number of subcategories shown for each category. 
        /// </summary>
        [
        Bindable(true),
        Category("Data"),
        DefaultValue(null),
        Description("The No. of subcategories"),
        ]
        public int NumberOfSubcategories
        {
            get
            {
                if (ViewState["subcategoryCount"] == null) return -1;
                return (int)ViewState["subcategoryCount"];
            }
            set
            {
                ViewState["subcategoryCount"] = value;
            }
        }
        /// <summary>
        /// Gets or sets database field which contains the ID of the categories displayed in the Directory.
        /// </summary>
        public string CategoryIdField
        {
            get
            {
                if (ViewState["__CategoryIdField"] == null)
                    ViewState["__CategoryIdField"] = String.Empty;
                return (string)ViewState["__CategoryIdField"];
            }
            set { ViewState["__CategoryIdField"] = value; }
        }
        /// <summary>
        /// Gets or sets database field which contains the ID of the subcategories. This is essentially the same as the Category ID field but the field comes from an alias table.
        /// </summary>
        public string SubcategoryIdField
        {
            get
            {
                if (ViewState["__SubcategoryIdField"] == null)
                    ViewState["__SubcategoryIdField"] = String.Empty;
                return (string)ViewState["__SubcategoryIdField"];
            }
            set { ViewState["__SubcategoryIdField"] = value; }
        }
        #endregion

        #region Templates
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the header section of <see cref="Directory"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(DirectoryItem))
        ]
        public virtual ITemplate HeaderTemplate
        {
            get
            {
                return headerTemplate;
            }
            set
            {
                headerTemplate = value;
            }
        }
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the footer section of <see cref="Directory"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(DirectoryItem))
        ]
        public virtual ITemplate FooterTemplate
        {
            get
            {
                return footerTemplate;
            }
            set
            {
                footerTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the category header section of <see cref="Directory"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(DirectoryItem))
        ]
        public virtual ITemplate CategoryHeaderTemplate
        {
            get
            {
                return categoryHeaderTemplate;
            }
            set
            {
                categoryHeaderTemplate = value;
            }
        }
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the category footer section of <see cref="Directory"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(DirectoryItem))
        ]
        public virtual ITemplate CategoryFooterTemplate
        {
            get
            {
                return categoryFooterTemplate;
            }
            set
            {
                categoryFooterTemplate = value;
            }
        }
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the categories separator section of <see cref="Directory"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(DirectoryItem))
        ]
        public virtual ITemplate CategorySeparatorTemplate
        {
            get
            {
                return categorySeparatorTemplate;
            }
            set
            {
                categorySeparatorTemplate = value;
            }
        }
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the subcategory section of <see cref="Directory"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(DirectoryItem))
        ]
        public virtual ITemplate SubcategoryTemplate
        {
            get
            {
                return subcategoryTemplate;
            }
            set
            {
                subcategoryTemplate = value;
            }
        }
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the subcategory separator section of <see cref="Directory"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(DirectoryItem))
        ]
        public virtual ITemplate SubcategorySeparatorTemplate
        {
            get
            {
                return subcategorySeparatorTemplate;
            }
            set
            {
                subcategorySeparatorTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the subcategory tail section of <see cref="Directory"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(DirectoryItem))
        ]
        public virtual ITemplate SubcategoriesTailTemplate
        {
            get
            {
                return subcategoriesTailTemplate;
            }
            set
            {
                subcategoriesTailTemplate = value;
            }
        }
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the column separator section of <see cref="Directory"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(DirectoryItem))
        ]
        public virtual ITemplate ColumnSeparatorTemplate
        {
            get
            {
                return columnSeparatorTemplate;
            }
            set
            {
                columnSeparatorTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the No Records section of <see cref="Directory"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(DirectoryItem))
        ]
        public virtual ITemplate NoRecordsTemplate
        {
            get
            {
                return noRecordsTemplate;
            }
            set
            {
                noRecordsTemplate = value;
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Raises the <see cref="ItemCommand"/> event. This allows you to provide a custom handler for the event.
        /// </summary>
        /// <param name="e">A <see cref="DirectoryCommandEventArgs"/> that contains event data. </param>
        protected virtual void OnItemCommand(DirectoryCommandEventArgs e)
        {
            if (ItemCommand != null)
                ItemCommand(this, e);
        }
        /// <summary>
        /// Raises the <see cref="ItemCreated"/> event. This allows you to provide a custom handler for the event.
        /// </summary>
        /// <param name="e">A <see cref="DirectoryItemEventArgs"/> that contains event data.</param>
        protected virtual void OnItemCreated(DirectoryItemEventArgs e)
        {
            if (ItemCreated != null)
                ItemCreated(this, e);
        }
        /// <summary>
        /// Raises the <see cref="ItemDataBound"/> event. This allows you to provide a custom handler for the event.
        /// </summary>
        /// <param name="e">A <see cref="DirectoryItemEventArgs"/> that contains event data.</param>
        protected virtual void OnItemDataBound(DirectoryItemEventArgs e)
        {
            _CurrentItemIndex++;
            if (ItemDataBound != null)
                ItemDataBound(this, e);
        }
        /// <summary>
        /// Raises the <see cref="BeforeSelect"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeSelect(EventArgs e)
        {
            if (BeforeSelect != null)
                BeforeSelect(this, e);
        }
        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }
        /// <summary>
        /// Raises the <see cref="BeforeShowCategory"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShowCategory(EventArgs e)
        {
            if (BeforeShowCategory != null)
                BeforeShowCategory(this, e);
        }
        /// <summary>
        /// Raises the <see cref="BeforeShowSubcategory"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShowSubcategory(EventArgs e)
        {
            if (BeforeShowSubcategory != null)
                BeforeShowSubcategory(this, e);
        }


        #endregion

        #region Methods and Implementation
        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code.
        /// </summary>
        protected override void CreateChildControls()
        {
            Controls.Clear();

            if (ViewState["ItemCount"] != null)
            {
                CreateControlHierarchy(false, null);
            }
        }

        private void CreateControlHierarchy(bool useDataSource, object data)
        {
            IEnumerable dataSource = null;
            List<DummySource> categories = null;
            int subindex = 0;
            int count = -1, elementCount = 1, columnElements = 0;

            if (!DesignMode)
            {
                if (useDataSource == false)
                {
                    count = (int)ViewState["ItemCount"];
                    if (count != -1)
                        dataSource = (List<DummySource>)ViewState["Categories"];
                }
                else
                {
                    dataSource = (IEnumerable)data;
                    categories = new List<DummySource>();
                }
            }

            int index = 0;
            object previousDataItem = null;
            CreateItem(ref index, DirectoryItemType.Header, false, null);
            if (dataSource != null)
            {
                count = 0; int subcatCount = 1;
                bool CreateCategory = true;
                foreach (object dataItem in dataSource)
                {
                    __hasData = true;
                    if (count != 0 &&
                        !DataSourceHelper.GetFieldValue(CategoryIdField, previousDataItem).Equals(
                        DataSourceHelper.GetFieldValue(CategoryIdField, dataItem)))
                        elementCount++;
                    previousDataItem = dataItem;
                    count++;
                }
                columnElements = (int)Math.Ceiling((double)elementCount / (double)NumberOfColumns);

                previousDataItem = null; count = 0; elementCount = 0;
                foreach (object dataItem in dataSource)
                {
                    if (count != 0 &&
                        !DataSourceHelper.GetFieldValue(CategoryIdField, previousDataItem).Equals(
                        DataSourceHelper.GetFieldValue(CategoryIdField, dataItem)))
                    {
                        CreateItem(ref index, DirectoryItemType.CategoryFooter, useDataSource, previousDataItem);
                        if (count == 0 || elementCount < columnElements)
                            CreateItem(ref index, DirectoryItemType.CategorySeparator, useDataSource, null);
                        CreateCategory = true;
                    }
                    else if (count != 0 && ((NumberOfSubcategories != 0 && subcatCount <= NumberOfSubcategories + 1) || NumberOfSubcategories == -1))
                        CreateItem(ref index, DirectoryItemType.SubcategorySeparator, useDataSource, null);

                    if (CreateCategory)
                    {
                        if (count != 0 && elementCount >= columnElements)
                        {
                            CreateItem(ref index, DirectoryItemType.ColumnSeparator, useDataSource, null);
                            elementCount = 0;
                        }
                        CreateItem(ref index, DirectoryItemType.CategoryHeader, useDataSource, dataItem);
                        CreateCategory = false;
                        subcatCount = 1;
                        elementCount++;
                    }
                    if ((NumberOfSubcategories == -1 ||
                        subcatCount <= NumberOfSubcategories) &&
                        !DataSourceHelper.IsFieldNull(SubcategoryIdField, dataItem))
                        CreateItem(ref index, DirectoryItemType.Subcategory, useDataSource, dataItem);
                    if (NumberOfSubcategories != 0 &&
                        subcatCount == NumberOfSubcategories + 1 &&
                        !DataSourceHelper.IsFieldNull(SubcategoryIdField, dataItem))
                        CreateItem(ref index, DirectoryItemType.SubcategoriesTail, useDataSource, dataItem);
                    previousDataItem = dataItem;
                    count++;
                    subcatCount++;
                    if (useDataSource)
                    {
                        DummySource ds = new DummySource();
                        ds.CategoryId = Convert.ToString(DataSourceHelper.GetFieldValue(CategoryIdField, dataItem));
                        ds.SubcategoryId = Convert.ToString(DataSourceHelper.GetFieldValue(SubcategoryIdField, dataItem) as string);
                        ds.CategoryIdName = CategoryIdField;
                        ds.SubcategoryIdName = SubcategoryIdField;
                        categories.Add(ds);
                    }
                }
                subindex++;
            }

            if (DesignMode)
            {
                CreateItem(ref index, DirectoryItemType.CategoryHeader, useDataSource, null);
                CreateItem(ref index, DirectoryItemType.Subcategory, useDataSource, null);
                CreateItem(ref index, DirectoryItemType.SubcategorySeparator, useDataSource, null);
                CreateItem(ref index, DirectoryItemType.SubcategoriesTail, useDataSource, null);
                CreateItem(ref index, DirectoryItemType.CategorySeparator, useDataSource, null);
                CreateItem(ref index, DirectoryItemType.CategoryFooter, useDataSource, null);

            }

            if (count <= 0)
                CreateItem(ref index, DirectoryItemType.NoRecords, false, null);
            else
                CreateItem(ref index, DirectoryItemType.CategoryFooter, useDataSource, previousDataItem);

            CreateItem(ref index, DirectoryItemType.Footer, false, null);
            if (useDataSource)
            {
                ViewState["ItemCount"] = ((dataSource != null) ? count : -1);
                ViewState["Categories"] = categories;
                OnBeforeShow(EventArgs.Empty);
            }
        }

        private DirectoryItem CreateItem(ref int itemIndex, DirectoryItemType itemType, bool dataBind, object dataItem)
        {
            DirectoryItem item = new DirectoryItem(itemIndex, itemType);
            DirectoryItemEventArgs e = new DirectoryItemEventArgs(item);
            _dataItem = new DataItem(dataItem);
            switch (itemType)
            {
                case DirectoryItemType.CategoryFooter:
                    if (categoryFooterTemplate != null) { CategoryFooterTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case DirectoryItemType.CategoryHeader:
                    if (CategoryHeaderTemplate != null) { CategoryHeaderTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case DirectoryItemType.CategorySeparator:
                    if (CategorySeparatorTemplate != null) { CategorySeparatorTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case DirectoryItemType.Footer:
                    if (FooterTemplate != null) { FooterTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case DirectoryItemType.Header:
                    if (HeaderTemplate != null) { HeaderTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case DirectoryItemType.NoRecords:
                    if (NoRecordsTemplate != null) { NoRecordsTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case DirectoryItemType.Subcategory:
                    if (SubcategoryTemplate != null) { SubcategoryTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case DirectoryItemType.SubcategorySeparator:
                    if (SubcategorySeparatorTemplate != null) { SubcategorySeparatorTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case DirectoryItemType.SubcategoriesTail:
                    if (SubcategoriesTailTemplate != null) { SubcategoriesTailTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case DirectoryItemType.ColumnSeparator:
                    if (ColumnSeparatorTemplate != null) { ColumnSeparatorTemplate.InstantiateIn(item); itemIndex++; }
                    break;
            }
            if (dataBind)
            {
                item.DataItem = dataItem;
            }
            OnItemCreated(e);
            Controls.Add(item);

            if (dataBind)
            {
                item.DataBind();
                OnItemDataBound(e);
                item.DataItem = null;
                if (DirectoryItemType.Subcategory == itemType && SubcategoryTemplate != null)
                    OnBeforeShowSubcategory(EventArgs.Empty);
                if (DirectoryItemType.CategoryFooter == itemType && CategoryFooterTemplate != null ||
                    DirectoryItemType.CategoryHeader == itemType && CategoryFooterTemplate == null)
                    OnBeforeShowCategory(EventArgs.Empty);
            }

            return item;
        }

        /// <summary>
        /// Binds the specified data source to the <see cref="Directory"/> control.
        /// </summary>
        /// <param name="data">An object that represents the data source; the object must implement the <see cref="IEnumerable"/> interface.</param>
        protected override void PerformDataBinding(System.Collections.IEnumerable data)
        {
            if (!DesignMode && Restricted && !UserRights.AllowRead)
            {
                Visible = false;
                return;
            }
            OnBeforeSelect(EventArgs.Empty);
            base.PerformDataBinding(data);
            Controls.Clear();
            if (HasChildViewState)
                ClearChildViewState();

            CreateControlHierarchy(true, data);
            ChildControlsCreated = true;
        }

        /// <summary>
        /// Retrieves data from the associated data source.
        /// </summary>
        protected override void PerformSelect()
        {
            if (DesignMode)
            {
                DataSourceID = "";
                DataSource = null;
            }
            base.PerformSelect();
        }

        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code. 
        /// Assigns any sources of the event and its information to the parent control, if the EventArgs parameter is an instance of <see cref="DirectoryCommandEventArgs"/>.
        /// </summary>
        /// <param name="source">The source of the event. </param>
        /// <param name="args">An <see cref="EventArgs"/> that contains the event data.</param>
        /// <returns><b>true</b> if the event assigned to the parent was raised, otherwise <b>false</b>.</returns>
        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            bool handled = false;

            if (args is DirectoryCommandEventArgs)
            {
                DirectoryCommandEventArgs ce = (DirectoryCommandEventArgs)args;

                OnItemCommand(ce);
                handled = true;

            }

            return handled;
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="HtmlTextWriter"/> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the control content.</param>
        public override void RenderControl(HtmlTextWriter writer)
        {
            if (!DesignMode && (!Visible || Restricted && !UserRights.AllowRead)) return;
            RenderContents(writer);
        }
        #endregion

        #region IForm Members

        private DataItem _dataItem;
        /// <summary>
        /// Gets the current <see cref="DataItem"/>.
        /// </summary>
        public DataItem DataItem
        {
            get
            {
                return _dataItem;
            }
        }

        /// <summary>
        /// Gets or sets the current sort expression.
        /// </summary>
        public string SortExpression
        {
            get
            {
                return ViewState["__se"] as string;
            }
            set
            {
                ViewState["__se"] = value;
                SelectArguments.SortExpression = value;
            }
        }

        /// <summary>
        /// Gets the <see cref="FormSupportedOperations"/> for the current form.
        /// </summary>
        public FormSupportedOperations UserRights
        {
            get
            {
                if (ViewState["__UserRights"] == null)
                    ViewState["__UserRights"] = new FormSupportedOperations();
                return (FormSupportedOperations)ViewState["__UserRights"];
            }
        }
        /// <summary>
        /// Find all child controls of type <i>T</i>.
        /// </summary>
        /// <typeparam name="T">The type of controls to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <returns>the <see cref="ReadOnlyCollection&lt;T&gt;"/></returns>
        public ReadOnlyCollection<T> GetControls<T>()
            where T : IMTControl
        {
            List<T> result = new List<T>();
            List<int> rows = new List<int>(new int[] { 0, Controls.Count - 1 });
            if (_CurrentItemIndex != 0 && _CurrentItemIndex != Controls.Count - 1)
            {
                rows.Insert(0, _CurrentItemIndex);
                if (((DirectoryItem)Controls[_CurrentItemIndex]).ItemType == DirectoryItemType.CategoryFooter)
                    for (int i = _CurrentItemIndex; i >= 0; i--)
                        if (((DirectoryItem)Controls[i]).ItemType == DirectoryItemType.CategoryFooter)
                        {
                            rows.Insert(1, i);
                            break;
                        }
            }
            for (int i = 0; i < rows.Count && rows[i] >= 0; i++)
            {
                ControlsHelper.PopulateControlCollection<T>(result, Controls[i].Controls);
            }
            return new ReadOnlyCollection<T>(result);
        }
        /// <summary>
        /// Find a control of type <i>T</i> with specified <see cref="Control.ID"/>
        /// </summary>
        /// <typeparam name="T">The type of control to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>T</i>.</returns>
        public T GetControl<T>(string id)
            where T : Control
        {
            if (Controls.Count == 0) return null;
            List<int> rows = new List<int>(new int[] { 0, Controls.Count - 1 });
            if (_CurrentItemIndex != 0 && _CurrentItemIndex != Controls.Count - 1)
            {
                rows.Insert(0, _CurrentItemIndex);
                if (((DirectoryItem)Controls[_CurrentItemIndex]).ItemType == DirectoryItemType.CategoryFooter)
                    for (int i = _CurrentItemIndex; i >= 0; i--)
                        if (((DirectoryItem)Controls[i]).ItemType == DirectoryItemType.CategoryFooter)
                        {
                            rows.Insert(1, i);
                            break;
                        }
            }
            for (int i = 0; i < rows.Count && rows[i] >= 0; i++)
            {
                T ctrl = (T)Controls[rows[i]].FindControl(id);
                if (ctrl != null) return ctrl;
            }

            for (int i = 0; i < rows.Count && rows[i] >= 0; i++)
            {
                T ctrl = (T)InMotion.Common.Controls.FindControlIterative(Controls[rows[i]], id);
                if (ctrl != null) return ctrl;
            }
            return (T)InMotion.Common.Controls.FindControlIterative(this, id);
        }
        /// <summary>
        /// Find a control with specified <see cref="Control.ID"/>
        /// </summary>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>Control</i></returns>
        public Control GetControl(string id)
        {
            return GetControl<Control>(id);
        }

        #endregion

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            RegisterArrayDeclaration();
        }

        /// <summary>
        /// Registers a JavaScript array declaration with the page object.
        /// </summary>
        protected virtual void RegisterArrayDeclaration()
        {
            if (Page is MTPage)
            {
                ((MTPage)Page).MTClientScript.RegisterArrayDeclaration("MTForms", String.Format("'{0}','{1}','{2}','{3}'", ID, Controls.Count, "", ""));
            }
            else
            {
                Page.ClientScript.RegisterArrayDeclaration("MTForms", String.Format("'{0}','{1}','{2}','{3}'", ID, Controls.Count, "", ""));
            }
        }

        private static DirectoryItem GetControlContainer(Control control)
        {
            Control c = control.NamingContainer;
            while (!(c is DirectoryItem) && c != null)
                c = c.NamingContainer;
            return c as DirectoryItem;
        }

        /// <summary>
        /// Register a control instance in client script array.
        /// </summary>
        /// <param name="control">The control instance to register.</param>
        public void RegisterClientControl(Control control)
        {
            DirectoryItem ri = GetControlContainer(control);
            string arrayName = "";
            switch (ri.ItemType)
            {
                case DirectoryItemType.Footer:
                    arrayName = ID + "Controls_Footer";
                    break;
                case DirectoryItemType.Header:
                    arrayName = ID + "Controls_Header";
                    break;
                default:
                    arrayName = ID + "Controls_Row" + ri.ItemIndex.ToString();
                    break;
            }
            if (Page is MTPage)
            {
                ((MTPage)Page).MTClientScript.RegisterArrayDeclaration(arrayName, String.Format("'{0}','{1}'", control.ID, control.ClientID));
            }
            else
            {
                Page.ClientScript.RegisterArrayDeclaration(arrayName, String.Format("'{0}','{1}'", control.ID, control.ClientID));
            }
        }

        private bool _restricted;
        /// <summary>
        /// Gets or sets the value indicating whether form will restrict the user access according to the <see cref="UserRights"/>.
        /// </summary>
        public bool Restricted
        {
            get
            {
                return _restricted;
            }
            set
            {
                _restricted = value;
            }
        }

        private bool __hasData;
        /// <summary>
        /// Gets value indicating whether form has non-empty data object.
        /// </summary>
        public virtual bool HasData
        {
            get { return __hasData; }
        }

        /// <summary>
        /// Retrieves the <see cref="DataSourceView"/> object that is associated with the current form.
        /// </summary>
        /// <returns>The <see cref="DataSourceView"/> object that is associated with the current form.</returns>
        public DataSourceView GetDataSourceView()
        {
            Control c1 = Parent;
            IDataSource dataSource = null;
            while (dataSource == null && c1 != Page)
            {
                dataSource = c1.FindControl(DataSourceID) as IDataSource;
                c1 = c1.Parent;
            }
            return dataSource != null ? dataSource.GetView(DataMember) : null;
        }
    }

    /// <summary>
    /// Provides a control designer class for extending the design-mode behavior of a <see cref="Directory"/> control.
    /// </summary>
    class DirectoryDesigner : MTFormDesigner
    {
        /// <summary>
        /// Gets a collection of template groups, each containing one or more template definitions.
        /// </summary>
        public override TemplateGroupCollection TemplateGroups
        {
            get
            {
                if (__TemplateGroups == null)
                {
                    Directory ctrl = (Directory)Component;
                    __TemplateGroups = base.TemplateGroups;

                    TemplateGroup tempGroup;
                    TemplateDefinition tempDef;

                    tempGroup = new TemplateGroup("Common Templates");
                    tempDef = new TemplateDefinition(this, "HeaderTemplate", ctrl, "HeaderTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);

                    tempDef = new TemplateDefinition(this, "FooterTemplate", ctrl, "FooterTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);

                    tempDef = new TemplateDefinition(this, "ColumnSeparatorTemplate", ctrl, "ColumnSeparatorTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);

                    tempDef = new TemplateDefinition(this, "NoRecordsTemplate", ctrl, "NoRecordsTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);

                    tempGroup = new TemplateGroup("Category Templates");
                    tempDef = new TemplateDefinition(this, "CategoryHeaderTemplate", ctrl, "CategoryHeaderTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);

                    tempDef = new TemplateDefinition(this, "CategoryFooterTemplate", ctrl, "CategoryFooterTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);

                    tempDef = new TemplateDefinition(this, "CategorySeparatorTemplate", ctrl, "CategorySeparatorTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);

                    tempGroup = new TemplateGroup("Subcategory Templates");
                    tempDef = new TemplateDefinition(this, "SubcategoryTemplate", ctrl, "SubcategoryTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);

                    tempDef = new TemplateDefinition(this, "SubcategorySeparatorTemplate", ctrl, "SubcategorySeparatorTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);

                    tempDef = new TemplateDefinition(this, "SubcategoriesTailTemplate", ctrl, "SubcategoriesTailTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);
                }
                return __TemplateGroups;
            }
        }
    }
}