//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Defines a service implemented by objects to interact with <see cref="MTSorter"/> control.
    /// </summary>
    public interface ISortable
    {
        /// <summary>
        /// Gets or sets the current sort expression.
        /// </summary>
        string SortExpression { get; set;}
    }
}