//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InMotion.Common;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Diagnostics;
using InMotion.Configuration;

namespace InMotion.Web.Controls
{

    /// <summary>
    /// Represents a control that displays a link to another Web page.
    /// </summary>
    [ParseChildren(true, "Text")]
    [PersistChildren(false)]
    public class MTLink : System.Web.UI.WebControls.HyperLink, IMTControlWithValue, IMTAttributeAccessor
    {
        #region Events
        /// <summary>
        /// Occurs after the <see cref="MTLink"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a semicolon-separated list of parameters that should be removed from the hyperlink.
        /// </summary>
        public string RemoveParameters
        {
            get { return Url.RemoveParameters; }
            set { Url.RemoveParameters = value; }
        }

        /// <summary>
        /// Gets or sets whether URLs should be automatically converted to absolute URLs or secure URLs for the SSL protocol (https://).
        /// </summary>
        public UrlType ConvertUrl
        {
            get { return Url.Type; }
            set { Url.Type = value; }
        }

        /// <summary>
        /// Gets or sets whether Get or Post parameters should be preserved.
        /// </summary>
        public PreserveParameterType PreserveParameters
        {
            get { return Url.PreserveParameters; }
            set { Url.PreserveParameters = value; }
        }

        /// <summary>
        /// Gets the <see cref="UrlParameterCollection"/> that contains the parameters of url.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public UrlParameterCollection Parameters
        {
            get { return Url.Parameters; }
        }

        private Url _url;
        /// <summary>
        /// Gets or sets the Url of this <see cref="MTLink"/>.
        /// </summary>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public Url Url
        {
            get
            {
                if (_url == null) _url = new Url("");
                return _url;
            }
            set 
            { 
                _url = value; 
            }
        }

        /// <summary>
        /// Gets or sets the name of data source field to be used as source of href attribute.
        /// </summary>
        [
        Category("Behavior"),
        Editor(typeof(System.Web.UI.Design.UrlEditor), typeof(string))
        ]
        public string HrefSource
        {
            get
            {
                if (ViewState["__HrefSource"] == null)
                    ViewState["__HrefSource"] = String.Empty;
                return (string)ViewState["__HrefSource"];
            }
            set
            {
                ViewState["__HrefSource"] = value;
                Url.Address = value;
            }
        }

        HrefType _hrefType = HrefType.Page;
        /// <summary>
        /// Gets or sets the type of source where the URL value will come from.
        /// </summary>
        public HrefType HrefType
        {
            get { return _hrefType; }
            set { _hrefType = value; }
        }

        private SourceType _sourceType = SourceType.DatabaseColumn;
        /// <summary>
        /// Gets or sets the type of data source that will provide data for the control.
        /// </summary>
        public SourceType SourceType
        {
            get
            {
                return _sourceType;
            }
            set { _sourceType = value; }
        }

        private string _source = String.Empty;
        /// <summary>
        /// Gets or sets the source of data for the control e.g. the name of a database column.
        /// </summary>
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        private DataType _dataType = DataType.Text;
        /// <summary>
        /// Gets or sets the DataType of the control value.
        /// </summary>
        public DataType DataType
        {
            get { return _dataType; }
            set
            {
                _dataType = value;
                if (_value != null) _value = TypeFactory.CreateTypedField(DataType, Value, Format);
            }
        }

        private ContentType _content_Type = ContentType.Text;
        /// <summary>
        /// Gets or sets whether the content of the control is plain text or HTML code.
        /// </summary>
        public ContentType ContentType
        {
            get { return _content_Type; }
            set { _content_Type = value; }
        }

        private string _format = String.Empty;
        /// <summary>
        /// Gets or sets the format depending on the Data Type property in which control's value will be displayed.
        /// </summary>
        public string Format
        {
            get { return _format; }
            set { _format = value; }
        }

        private string _dBformat = string.Empty;
        /// <summary>
        /// Format that will be used to extract as well as place the control value into the database.
        /// </summary>
        public string DBFormat
        {
            get { return _dBformat; }
            set { _dBformat = value; }
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        private bool _valUpdated;

        private bool _textUpdated
        {
            get
            {
                return ViewState["_textUpdated"] != null && (bool)ViewState["_textUpdated"];
            }
            set
            {
                ViewState["_textUpdated"] = value;
            }
        }

        private object _value;
        /// <summary>
        /// Gets or sets the control value.
        /// </summary>
        /// <remarks>
        /// The assigned value will be automatically converted into one of IMTField types according to the Data Type property (MTText will be used by default).
        /// Retrieved value is guaranteed is not null IMTField object of type specified in Data Type property.
        /// </remarks>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public object Value
        {
            get
            {
                if (_value == null) return DefaultValue;
                return _value;
            }
            set
            {
                object val = value;
                try
                {
                    val = TypeFactory.CreateTypedField(DataType, val, Format);
                    _value = val;
                    _valUpdated = true;
                }
                catch (FormatException e)
                {
                    if (AppConfig.IsMTErrorHandlerUse("all"))
                        Trace.TraceError(e.ToString());
                    throw;
                }
            }
        }

        private object __DefaultValue;
        /// <summary>
        /// Gets or sets the default value of the control.
        /// </summary>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public object DefaultValue
        {
            get
            {
                if (__DefaultValue == null)
                    __DefaultValue = TypeFactory.CreateTypedField(DataType, null, Format);
                return __DefaultValue;
            }
            set
            {
                object val = value;
                val = TypeFactory.CreateTypedField(DataType, val, Format);
                __DefaultValue = val;

            }
        }

        /// <summary>
        /// Gets the value indicating whether <see cref="Value"/> of control is null.
        /// </summary>
        public virtual bool IsEmpty
        {
            get { return ((IMTField)Value).IsNull; }
        }

        private string DisplayedText;
        /// <summary>
        /// Gets or sets the string representation of <see cref="Value"/>.
        /// </summary>
        public override string Text
        {
            get
            {
                if (DisplayedText != null)
                {
                    return DisplayedText;
                }
                if (DesignMode && Value == null && String.IsNullOrEmpty(base.Text))
                {
                    return ID;
                }
                if (!_textUpdated && _valUpdated)
                {
                    return ((IMTField)Value).ToString(Format);
                }
                else
                {
                    return base.Text;
                }
            }
            set
            {
                _textUpdated = true;
                base.Text = value;
            }
        }

        private bool _textUpdatedUrl
        {
            get
            {
                return ViewState["_tU"] != null && (bool)ViewState["_tU"];
            }
            set
            {
                ViewState["_tU"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the Url to link to when the link control is clicked.
        /// </summary>
        public new string NavigateUrl
        {
            get
            {
                if (!_textUpdatedUrl && Page is MTPage)
                {
                    return Url.ToString((MTPage)Page);
                }
                else
                {
                    return base.NavigateUrl;
                }
            }
            set
            {
                base.NavigateUrl = value;
                _textUpdatedUrl = true;
            }
        }

        private OperationMode __OperationMode = OperationMode.Url;
        /// <summary>
        /// Gets or sets the operation mode of the <see cref="MTLink"/> control.
        /// </summary>
        /// <value>
        /// One of the <see cref="OperationMode"/> enumeration. 
        /// The default is [b]Url[/b]
        /// </value>
        public OperationMode OperationMode
        {
            get
            {
                return __OperationMode;
            }
            set
            {
                __OperationMode = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            IMTField val = (IMTField)Value;
            if (!Page.IsPostBack || _valUpdated || string.IsNullOrEmpty(Source))
            {
                if (!_textUpdated)
                {
                    if (Text != Value.ToString()) DisplayedText = Text;
                    if (String.IsNullOrEmpty(DisplayedText)) DisplayedText = "{0}";
                }
                else
                {
                    DisplayedText = Text;
                }
                DisplayedText = DisplayedText.Replace("{0}", ControlsHelper.EncodeText(val.ToString(Format), ContentType));
                base.Text = DisplayedText = ControlsHelper.ReplaceResourcesAndStyles(Text, Page);
                if (base.Text.IndexOf("{CCS_PathToMasterPage}") != -1 || base.Text.IndexOf("{page:pathToCurrentPage}") != -1)
                {
                    Control parent = Parent;
                    while (parent != null)
                    {
                        if (parent is MTPanel && !string.IsNullOrEmpty(((MTPanel)parent).MasterPageFile))
                        {
                            base.Text = DisplayedText = base.Text.Replace("{CCS_PathToMasterPage}", ((MTPanel)parent).PathToMasterPage);
                            base.Text = DisplayedText = base.Text.Replace("{page:pathToCurrentPage}", ((MTPanel)parent).PathToMasterPage);
                            break;
                        }
                        else if (parent is MTPage && !string.IsNullOrEmpty(((MTPage)parent).MasterPageFile))
                        {
                            base.Text = DisplayedText = base.Text.Replace("{CCS_PathToMasterPage}", ((MTPage)parent).PathToMasterPage);
                            base.Text = DisplayedText = base.Text.Replace("{page:pathToCurrentPage}", ((MTPage)parent).PathToMasterPage);
                            break;
                        }
                        parent = parent.Parent;
                    }
                }
            }
            if (OwnerForm is IClientScriptHelper && Visible)
                (OwnerForm as IClientScriptHelper).RegisterClientControl(this);

            if (HrefSource.StartsWith("~/"))
                Url.Address = this.HrefSource;
            if (String.IsNullOrEmpty(base.NavigateUrl) || (!_textUpdatedUrl && !Page.IsPostBack)) base.NavigateUrl = "{0}";

            ToolTip = ControlsHelper.ReplaceResourcesAndStyles(ToolTip, Page);

            if (base.NavigateUrl.IndexOf("{0}") > -1)
                base.NavigateUrl = base.NavigateUrl.Replace("{0}", Url.ToString((MTPage)Page));
            if (this.OperationMode == OperationMode.Postback && (String.IsNullOrEmpty(Url.Address) || String.Compare(ResolveUrl(Url.Address), Page.Request.Path, true) == 0 || String.Compare(ResolveUrl(Url.Address), ResolveUrl((new Url()).Address), true) == 0))
                base.NavigateUrl = Page.ClientScript.GetPostBackClientHyperlink(this, ResolveUrl(Page.AppRelativeVirtualPath + "?" + (new Url(NavigateUrl)).Parameters.ToString()));
            if (NamingContainer is RepeaterItem && !string.IsNullOrEmpty(Attributes["data-id"]))
                Attributes["data-id"] = Regex.Replace(Attributes["data-id"], @"\{\w+:rowNumber\}", (((RepeaterItem)NamingContainer).ItemIndex + 1).ToString());
        }
        
        /// <summary>
        /// Outputs server control content to a provided <see cref="HtmlTextWriter"/> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            if (DesignMode)
            {
                Visible = true;
                if (String.IsNullOrEmpty(Text))
                    Text = ((System.Web.UI.Control)this).ID;
                writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "1px");
                writer.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "solid");
                writer.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "#AAAAAA");
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                base.Render(writer);
                writer.RenderEndTag();
            }
            else
            {
                if (TemplateControl.Parent != null && TemplateControl.Parent is MTPanel && !string.IsNullOrEmpty(((MTPanel)this.TemplateControl.Parent).MasterPageFile))
                    AppRelativeTemplateSourceDirectory = Page.AppRelativeTemplateSourceDirectory;
                base.Render(writer);
            }
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            if (DesignMode)
            {
                DataType = DataType.Text;
                Value = ID;
            }
            else
            {
                base.OnDataBinding(e);
                object val = ControlsHelper.DataBindControl(SourceType, Source, OwnerForm, DataType, DBFormat);
                if (val != null || val is IMTField && !((IMTField)val).IsNull)
                    Value = val;
                if (HrefType == HrefType.Database && HrefSource.Length > 0 && OwnerForm != null && OwnerForm.DataItem != null)
                {
                    this.Url.Address = MTText.Parse(OwnerForm.DataItem[HrefSource]).ToString();
                }

                Url.DataBind(this);
                OnBeforeShow(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Raises the Load event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!_valUpdated)
            {
                if (Page.Request.QueryString[ID] != null && !Page.IsPostBack)
                    Value = Page.Request.QueryString[ID];
                if (ID != UniqueID && Page.Request.Form[ID] != null)
                    Value = Page.Request.Form[ID];
            }
            if (HrefType != HrefType.Page || String.IsNullOrEmpty(HrefSource))
                AppRelativeTemplateSourceDirectory = Page.AppRelativeTemplateSourceDirectory;
        }
        #endregion

        /// <summary>
        /// Gets the value of the control as a <see cref="MTFloat"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTFloat GetFloat()
        {
            return (MTFloat)TypeFactory.CreateTypedField(DataType.Float, Value, "");
        }

        /// <summary>
        /// Gets the value of the control as a <see cref="MTInteger"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTInteger GetInteger()
        {
            return (MTInteger)TypeFactory.CreateTypedField(DataType.Integer, Value, "");
        }

        /// <summary>
        /// Gets the value of the control as a <see cref="MTSingle"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTSingle GetSingle()
        {
            return (MTSingle)TypeFactory.CreateTypedField(DataType.Single, Value, "");
        }

        /// <summary>
        /// Gets the value of the control as a <see cref="MTBoolean"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTBoolean GetBoolean()
        {
            return (MTBoolean)TypeFactory.CreateTypedField(DataType.Boolean, Value, "");
        }

        /// <summary>
        /// Gets the value of the control as a <see cref="MTDate"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTDate GetDate()
        {
            return (MTDate)TypeFactory.CreateTypedField(DataType.Date, Value, "");
        }

        /// <summary>
        /// Gets the value of the control as a <see cref="MTText"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTText GetText()
        {
            return (MTText)TypeFactory.CreateTypedField(DataType.Text, Value, "");
        }

        /// <summary>
        /// Gets the value of the control as a <see cref="MTMemo"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTMemo GetMemo()
        {
            return (MTMemo)TypeFactory.CreateTypedField(DataType.Memo, Value, "");
        }
    }
}