//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;
using System.ComponentModel;
using System.Collections;
using InMotion.Security;
using System.Collections.ObjectModel;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Path component is mainly used in conjunction with the Directory form to provide a sequential list of links leading up to a category within a hierarchy.
    /// </summary>
    [Designer(typeof(PathDesigner))]
    public class Path : DataBoundControl, INamingContainer, IForm, IMTAttributeAccessor, IClientScriptHelper
    {
        /// <summary>
        /// Occurs before the <see cref="Path"/> send request to the datasource control for data.
        /// </summary>
        public event EventHandler<EventArgs> BeforeSelect;
        /// <summary>
        /// Occurs after the <see cref="Path"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;

        /// <summary>
        /// Occurs after the category section is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShowCategory;
        #region Statics and Constants
        private static readonly object EventItemCreated = new object();
        private static readonly object EventItemDataBound = new object();
        private static readonly object EventItemCommand = new object();
        #endregion

        #region Member variables
        private ITemplate footerTemplate;
        private ITemplate headerTemplate;
        private ITemplate pathComponentTemplate;
        private ITemplate currentCategoryTemplate;
        #endregion

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }
        /// <summary>
        /// Gets or sets the name of  the field with primary key values of the parent categories.
        /// </summary>
        public string ParentIdFieldName
        {
            get
            {
                if (ViewState["__ParentIDFieldName"] == null)
                    ViewState["__ParentIDFieldName"] = "";
                return (string)ViewState["__ParentIDFieldName"];
            }
            set { ViewState["__ParentIDFieldName"] = value; }
        }
        /// <summary>
        /// Gets or sets the name of the field containing the unique ID that identifies the categories.
        /// </summary>
        public string CategoryIdParameterName
        {
            get
            {
                if (ViewState["__CategoryIDParameterName"] == null)
                    ViewState["__CategoryIDParameterName"] = "";
                return (string)ViewState["__CategoryIDParameterName"];
            }
            set { ViewState["__CategoryIDParameterName"] = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the header section of <see cref="Path"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(PathItem))
        ]
        public virtual ITemplate HeaderTemplate
        {
            get
            {
                return headerTemplate;
            }
            set
            {
                headerTemplate = value;
            }
        }
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the footer section of <see cref="Path"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(PathItem))
        ]
        public virtual ITemplate FooterTemplate
        {
            get
            {
                return footerTemplate;
            }
            set
            {
                footerTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the path component section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(PathItem))
        ]
        public virtual ITemplate PathComponentTemplate
        {
            get
            {
                return pathComponentTemplate;
            }
            set
            {
                pathComponentTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the current category section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(PathItem))
        ]
        public virtual ITemplate CurrentCategoryTemplate
        {
            get
            {
                return currentCategoryTemplate;
            }
            set
            {
                currentCategoryTemplate = value;
            }
        }

        #region Events
        /// <summary>
        /// Raises the <see cref="ItemCommand"/> event. This allows you to provide a custom handler for the event.
        /// </summary>
        /// <param name="e">A <see cref="PathCommandEventArgs"/> that contains event data. </param>
        protected virtual void OnItemCommand(PathCommandEventArgs e)
        {
            EventHandler<PathCommandEventArgs> onItemCommandHandler = (EventHandler<PathCommandEventArgs>)Events[EventItemCommand];
            if (onItemCommandHandler != null) onItemCommandHandler(this, e);
        }

        /// <summary>
        /// Raises the <see cref="ItemCreated"/> event. This allows you to provide a custom handler for the event.
        /// </summary>
        /// <param name="e">A <see cref="PathItemEventArgs"/> that contains event data.</param>
        protected virtual void OnItemCreated(PathItemEventArgs e)
        {
            EventHandler<PathItemEventArgs> onItemCreatedHandler = (EventHandler<PathItemEventArgs>)Events[EventItemCreated];
            if (onItemCreatedHandler != null) onItemCreatedHandler(this, e);
        }

        /// <summary>
        /// Raises the <see cref="ItemDataBound"/> event. This allows you to provide a custom handler for the event.
        /// </summary>
        /// <param name="e">A <see cref="PathItemEventArgs"/> that contains event data.</param>
        protected virtual void OnItemDataBound(PathItemEventArgs e)
        {
            _CurrentItemIndex++;

            EventHandler<PathItemEventArgs> onItemDataBoundHandler = (EventHandler<PathItemEventArgs>)Events[EventItemDataBound];
            if (onItemDataBoundHandler != null) onItemDataBoundHandler(this, e);
        }
        /// <summary>
        /// Occurs when a button is clicked in the <see cref="Path"/> control.
        /// </summary>
        [
        Category("Action"),
        Description("Raised when a CommandEvent ours within an item.")
        ]
        public event EventHandler<PathCommandEventArgs> ItemCommand
        {
            add
            {
                Events.AddHandler(EventItemCommand, value);
            }
            remove
            {
                Events.RemoveHandler(EventItemCommand, value);
            }
        }

        /// <summary>
        /// Occurs when an item is created in the <see cref="Path"/> control. 
        /// </summary>
        [
        Category("Behavior"),
        Description("Raised when an item is created and is ready for customization.")
        ]
        public event EventHandler<PathItemEventArgs> ItemCreated
        {
            add
            {
                Events.AddHandler(EventItemCreated, value);
            }
            remove
            {
                Events.RemoveHandler(EventItemCreated, value);
            }
        }

        /// <summary>
        /// Occurs after an item in the <see cref="Path"/> is data-bound but before it is rendered on the page.
        /// </summary>
        [
        Category("Behavior"),
        Description("Raised when an item is data-bound.")
        ]
        public event EventHandler<PathItemEventArgs> ItemDataBound
        {
            add
            {
                Events.AddHandler(EventItemDataBound, value);
            }
            remove
            {
                Events.RemoveHandler(EventItemDataBound, value);
            }
        }

        #endregion

        #region Methods and Implementation
        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code.
        /// </summary>
        protected override void CreateChildControls()
        {
            Controls.Clear();

            if (ViewState["ItemCount"] != null)
            {
                // Create the control hierarchy using the view state, 
                // not the data source.
                CreateControlHierarchy(null, false);
            }
        }

        private void CreateControlHierarchy(IEnumerable data, bool useDataSource)
        {
            IEnumerable dataSource = null;
            int count = -1;

            if (useDataSource == false)
            {
                count = (int)ViewState["ItemCount"];
                if (count != -1)
                {
                    dataSource = new ArrayList(count);
                }
            }
            else
            {
                dataSource = data;
            }

            int index = 0;
            CreateItem(ref index, PathItemType.Header, useDataSource, null);
            if (dataSource != null)
            {
                count = 0;
                object lastDataItem = null;
                foreach (object dataItem in dataSource)
                {
                    if (count != 0)
                    {
                        CreateItem(ref index, PathItemType.PathComponent, useDataSource, lastDataItem);
                    }
                    lastDataItem = dataItem;
                    count++;
                }
                if (lastDataItem != null)
                {
                    index--;
                    CreateItem(ref index, PathItemType.CurrentCategory, useDataSource, lastDataItem);
                }
            }
            CreateItem(ref index, PathItemType.Footer, useDataSource, null);

            if (useDataSource)
            {
                ViewState["ItemCount"] = ((dataSource != null) ? count : -1);
                OnBeforeShow(EventArgs.Empty);
            }
        }

        private PathItem CreateItem(ref int itemIndex, PathItemType itemType, bool dataBind, object dataItem)
        {
            PathItem item = new PathItem(itemIndex, itemType);
            PathItemEventArgs e = new PathItemEventArgs(item);
            switch (itemType)
            {
                case PathItemType.Footer:
                    if (FooterTemplate != null) FooterTemplate.InstantiateIn(item);
                    break;
                case PathItemType.Header:
                    if (HeaderTemplate != null) HeaderTemplate.InstantiateIn(item);
                    break;
                case PathItemType.CurrentCategory:
                    if (CurrentCategoryTemplate != null) CurrentCategoryTemplate.InstantiateIn(item);
                    break;
                case PathItemType.PathComponent:
                    if (PathComponentTemplate != null) PathComponentTemplate.InstantiateIn(item);
                    break;
            }
            if (dataBind)
            {
                item.DataItem = dataItem;
                _dataItem = new DataItem(dataItem);
            }
            OnItemCreated(e);
            Controls.Add(item);

            if (dataBind)
            {
                item.DataBind();
                OnItemDataBound(e);
                if (itemType == PathItemType.CurrentCategory)
                    OnBeforeShowCategory(EventArgs.Empty);
                item.DataItem = null;
            }
            itemIndex++;
            return item;
        }

        /// <summary>
        /// Retrieves data from the associated data source.
        /// </summary>
        protected override void PerformSelect()
        {
            if (DesignMode)
            {
                DataSourceID = "";
                DataSource = null;
            }
            else
            {
                if (list.Count == 0)
                {
                    if (Restricted && !UserRights.AllowRead)
                    {
                        Visible = false;
                        return;
                    }
                    OnBeforeSelect(EventArgs.Empty);
                }
            }
            base.PerformSelect();
        }

        ArrayList list = new ArrayList();

        private int recurseCounter = 0;

        /// <summary>
        /// Binds the specified data source to the <see cref="Path"/> control.
        /// </summary>
        /// <param name="data">An object that represents the data source; the object must implement the <see cref="IEnumerable"/> interface.</param>
        protected override void PerformDataBinding(System.Collections.IEnumerable data)
        {
            IEnumerable dataCollection;
            if (data == null)
                dataCollection = new System.Data.DataView();
            else
                dataCollection = data;
            if (DesignMode)
            {
                Controls.Clear();
                if (HasChildViewState)
                    ClearChildViewState();
                list.Add("PathCategory");
                list.Add("CurrentCategory");
                recurseCounter++;
                CreateControlHierarchy(list, true);
                ChildControlsCreated = true;
                list.Clear();
            }
            else
            {
                MTDataSource ds = GetDataSource() as MTDataSource;
                IEnumerator en = dataCollection.GetEnumerator();

                if (en.MoveNext())
                {
                    __hasData = true;
                    object d = en.Current;
                    list.Add(d);
                    if (DataSourceHelper.GetFieldValue(ParentIdFieldName, d) != null && recurseCounter < 40)
                    {
                        recurseCounter++;

                        if (ds != null)
                        {
                            ds.IsInitilaizeSelectParametersRaised = false;
                            ds.SelectParameters[CategoryIdParameterName].Value =
                                DataSourceHelper.GetFieldValue(ParentIdFieldName, d);
                            ((DataParameter)ds.SelectParameters[CategoryIdParameterName]).SourceType = DataParameterSourceType.Expression;
                        }
                        base.PerformSelect();
                    }
                }
                else
                {
                    Controls.Clear();
                    if (HasChildViewState)
                        ClearChildViewState();
                    list.Reverse();
                    CreateControlHierarchy(list, true);
                    ChildControlsCreated = true;
                    list.Clear();
                }
            }
        }

        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code. 
        /// Assigns any sources of the event and its information to the parent control, if the EventArgs parameter is an instance of <see cref="PathCommandEventArgs"/>.
        /// </summary>
        /// <param name="source">The source of the event. </param>
        /// <param name="args">An <see cref="EventArgs"/> that contains the event data.</param>
        /// <returns><b>true</b> if the event assigned to the parent was raised, otherwise <b>false</b>.</returns>
        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            bool handled = false;

            if (args is PathCommandEventArgs)
            {
                PathCommandEventArgs ce = (PathCommandEventArgs)args;

                OnItemCommand(ce);
                handled = true;
            }

            return handled;
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="HtmlTextWriter"/> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the control content.</param>
        public override void RenderControl(HtmlTextWriter writer)
        {
            if (!DesignMode && (!Visible || Restricted && !UserRights.AllowRead)) return;
            RenderContents(writer);
        }
        #endregion

        /// <summary>
        /// Raises the <see cref="BeforeSelect"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeSelect(EventArgs e)
        {
            if (BeforeSelect != null)
                BeforeSelect(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }
        /// <summary>
        /// Raises the <see cref="BeforeShowCategory"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShowCategory(EventArgs e)
        {
            if (BeforeShowCategory != null)
                BeforeShowCategory(this, e);
        }

        #region IForm Members

        private DataItem _dataItem;
        /// <summary>
        /// Gets the current <see cref="DataItem"/>.
        /// </summary>
        public DataItem DataItem
        {
            get
            {
                return _dataItem;
            }
        }

        /// <summary>
        /// Gets the <see cref="FormSupportedOperations"/> for the current form.
        /// </summary>
        public FormSupportedOperations UserRights
        {
            get
            {
                if (ViewState["__UserRights"] == null)
                    ViewState["__UserRights"] = new FormSupportedOperations();
                return (FormSupportedOperations)ViewState["__UserRights"];
            }
        }

        private int _CurrentItemIndex = -1;
        /// <summary>
        /// Gets the index of the current processed <see cref="PathItem"/>.
        /// </summary>
        public int CurrentItemIndex
        {
            get { return _CurrentItemIndex; }
        }

        /// <summary>
        /// Gets the current processed <see cref="PathItem"/>.
        /// </summary>
        public PathItem CurrentItem
        {
            get
            {
                if (_CurrentItemIndex < 0 || _CurrentItemIndex > Controls.Count - 1)
                    return null;
                return (PathItem)Controls[_CurrentItemIndex];
            }
        }
        /// <summary>
        /// Find all child controls of type <i>T</i>.
        /// </summary>
        /// <typeparam name="T">The type of controls to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <returns>the <see cref="ReadOnlyCollection&lt;T&gt;"/></returns>
        public ReadOnlyCollection<T> GetControls<T>()
            where T : IMTControl
        {
            List<T> result = new List<T>();
            List<int> rows = new List<int>(new int[] { 0, Controls.Count - 1 });
            if (_CurrentItemIndex != 0 && _CurrentItemIndex != Controls.Count - 1) rows.Insert(0, _CurrentItemIndex);
            for (int i = 0; i < rows.Count && rows[i] >= 0; i++)
            {
                ControlsHelper.PopulateControlCollection<T>(result, Controls[i].Controls);
            }
            return new ReadOnlyCollection<T>(result);
        }
        /// <summary>
        /// Find a control of type <i>T</i> with specified <see cref="Control.ID"/>
        /// </summary>
        /// <typeparam name="T">The type of control to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>T</i>.</returns>
        public T GetControl<T>(string id)
            where T : Control
        {
            if (Controls.Count == 0) return null;
            List<int> rows = new List<int>(new int[] { 0, Controls.Count - 1 });
            if (_CurrentItemIndex != 0 && _CurrentItemIndex != Controls.Count - 1) rows.Insert(0, _CurrentItemIndex);
            for (int i = 0; i < rows.Count && rows[i] >= 0; i++)
            {
                T ctrl = (T)Controls[rows[i]].FindControl(id);
                if (ctrl != null) return ctrl;
            }
            for (int i = 0; i < rows.Count && rows[i] >= 0; i++)
            {
                T ctrl = (T)InMotion.Common.Controls.FindControlIterative(Controls[rows[i]], id);
                if (ctrl != null) return ctrl;
            }
            return (T)InMotion.Common.Controls.FindControlIterative(this, id);
        }
        /// <summary>
        /// Find a control with specified <see cref="Control.ID"/>
        /// </summary>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>Control</i></returns>
        public Control GetControl(string id)
        {
            return GetControl<Control>(id);
        }

        private bool _restricted;
        /// <summary>
        /// Gets or sets the value indicating whether form will restrict the user access according to the <see cref="UserRights"/>.
        /// </summary>
        public bool Restricted
        {
            get
            {
                return _restricted;
            }
            set
            {
                _restricted = value;
            }
        }

        private bool __hasData;
        /// <summary>
        /// Gets value indicating whether form has non-empty data object.
        /// </summary>
        public virtual bool HasData
        {
            get { return __hasData; }
        }

        /// <summary>
        /// Retrieves the <see cref="DataSourceView"/> object that is associated with the current form.
        /// </summary>
        /// <returns>The <see cref="DataSourceView"/> object that is associated with the current form.</returns>
        public DataSourceView GetDataSourceView()
        {
            Control c1 = Parent;
            IDataSource dataSource = null;
            while (dataSource == null && c1 != Page)
            {
                dataSource = c1.FindControl(DataSourceID) as IDataSource;
                c1 = c1.Parent;
            }
            return dataSource != null ? dataSource.GetView(DataMember) : null;
        }
        #endregion

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            RegisterArrayDeclaration();
        }

        /// <summary>
        /// Registers a JavaScript array declaration with the page object.
        /// </summary>
        protected virtual void RegisterArrayDeclaration()
        {
            if (Page is MTPage)
            {
                ((MTPage)Page).MTClientScript.RegisterArrayDeclaration("MTForms", String.Format("'{0}','{1}','{2}','{3}'", ID, Controls.Count, "", ""));
            }
            else
            {
                Page.ClientScript.RegisterArrayDeclaration("MTForms", String.Format("'{0}','{1}','{2}','{3}'", ID, Controls.Count, "", ""));
            }
        }

        private static PathItem GetControlContainer(Control control)
        {
            Control c = control.NamingContainer;
            while (!(c is PathItem) && c != null)
                c = c.NamingContainer;
            return c as PathItem;
        }

        /// <summary>
        /// Register a control instance in client script array.
        /// </summary>
        /// <param name="control">The control instance to register.</param>
        public void RegisterClientControl(Control control)
        {
            PathItem ri = GetControlContainer(control);
            string arrayName = "";
            switch (ri.ItemType)
            {
                case PathItemType.Footer:
                    arrayName = ID + "Controls_Footer";
                    break;
                case PathItemType.Header:
                    arrayName = ID + "Controls_Header";
                    break;
                default:
                    arrayName = ID + "Controls_Row" + ri.ItemIndex.ToString();
                    break;
            }
            if (Page is MTPage)
            {
                ((MTPage)Page).MTClientScript.RegisterArrayDeclaration(arrayName, String.Format("'{0}','{1}'", control.ID, control.ClientID));
            }
            else
            {
                Page.ClientScript.RegisterArrayDeclaration(arrayName, String.Format("'{0}','{1}'", control.ID, control.ClientID));
            }
        }
    }

    /// <summary>
    /// Provides a control designer class for extending the design-mode behavior of a <see cref="Path"/> control.
    /// </summary>
    class PathDesigner : MTFormDesigner
    {
        /// <summary>
        /// Gets a collection of template groups, each containing one or more template definitions.
        /// </summary>
        public override TemplateGroupCollection TemplateGroups
        {
            get
            {
                if (__TemplateGroups == null)
                {
                    Path ctrl = (Path)Component;
                    __TemplateGroups = base.TemplateGroups;

                    TemplateGroup tempGroup;
                    TemplateDefinition tempDef;

                    tempGroup = new TemplateGroup("Common Templates");
                    tempDef = new TemplateDefinition(this, "HeaderTemplate", ctrl, "HeaderTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);

                    tempDef = new TemplateDefinition(this, "FooterTemplate", ctrl, "FooterTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);

                    tempGroup = new TemplateGroup("Path Templates");
                    tempDef = new TemplateDefinition(this, "PathComponentTemplate", ctrl, "PathComponentTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);

                    tempDef = new TemplateDefinition(this, "CurrentCategoryTemplate", ctrl, "CurrentCategoryTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);
                }
                return __TemplateGroups;
            }
        }
    }
}