//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using InMotion.Common;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Specifies the source type of a parameter
    /// </summary>
    public enum DataParameterSourceType
    {
        /// <summary>
        /// Get the value of parameter from the HttpApplicationState collection.
        /// </summary>
        Application,
        /// <summary>
        /// Get the value of parameter from the specified page control.
        /// </summary>
        Control,
        /// <summary>
        /// Get the value of parameter from the client-side HTTP cookie.
        /// </summary>
        Cookie,
        /// <summary>
        /// Get the value of parameter from the Datasource Column.
        /// </summary>
        DataSourceColumn,
        /// <summary>
        /// Get the value of parameter as string from the Source property.
        /// </summary>
        DBExpression,
        /// <summary>
        /// Get the value of parameter from the custom code expression.
        /// </summary>
        Expression,
        /// <summary>
        /// Get the value of parameter from the HttpRequest.Forms collection.
        /// </summary>
        Form,
        /// <summary>
        /// Get the value of parameter from the HttpSessionState of the current session.
        /// </summary>
        Session,
        /// <summary>
        /// Get the value of parameter from the HttpRequest.QueryStrings collection.
        /// </summary>
        Url,
        /// <summary>
        /// Get the value of parameter from the collection of special values of owner form.
        /// </summary>
        SpecialValue
    }

    /// <summary>
    /// Represents the parameter for <see cref="MTDataSourceView"/> operations.
    /// </summary>
    public class DataParameter : InMotion.Web.Controls.Parameter
    {
        private string _dbFormat;

        /// <summary>
        /// Format that will be used to place the control value into the database.
        /// </summary>
        public string DBFormat
        {
            get { return _dbFormat; }
            set { _dbFormat = value; }
        }

        /// <summary>
        /// Initializes the new instance of the <b>Parameter</b> class using supplied source type and source.
        /// </summary>
        /// <param name="sourceType">The <see cref="SourceType"/> of new parameter.</param>
        /// <param name="source">The Source of parameter.</param>
        public DataParameter(DataParameterSourceType sourceType, string source)
        {
            this.SourceType = sourceType;
            this.Source = source;
        }

        /// <summary>
        /// Initializes the new instance of the <b>Parameter</b> class using supplied source type and source.
        /// </summary>
        /// <param name="sourceType">The <see cref="SourceType"/> of new parameter.</param>
        /// <param name="source">The Source of parameter.</param>
        public DataParameter(DataParameterSourceType sourceType, object source)
        {
            this.SourceType = sourceType;
            if (source != null)
                this.Source = source.ToString();
        }

        /// <summary>
        /// Initializes the new instance of the <b>DataParameter</b> class.
        /// </summary>
        public DataParameter()
        {

        }
        /// <summary>
        /// Gets or sets the <see cref="DataParameterSourceType"/> of the parameter.
        /// </summary>
        public DataParameterSourceType SourceType
        {
            get
            {
                object o = ViewState["SourceType"];
                return (null == o) ? DataParameterSourceType.Expression : (DataParameterSourceType)o;
            }
            set
            {
                ViewState["SourceType"] = value;
            }
        }

        /// <summary>
        /// Updates and returns the value of the Parameter object.
        /// </summary>
        /// <returns>An object that represents the updated and current value of the parameter.</returns>
        public override object Evaluate()
        {
            switch (SourceType)
            {
                case DataParameterSourceType.Application:
                    if (HttpContext.Current.Application[Source] != null)
                    {
                        SetValue(HttpContext.Current.Application[Source]);
                        IsValueNotPassed = false;
                    }
                    else
                        IsValueNotPassed = true;
                    break;
                case DataParameterSourceType.Control:
                    if (_owner != null)
                    {
                        Control c = null;
                        if (_owner is IMTControlContainer)
                            c = ((IMTControlContainer)_owner).GetControl<Control>(Source);
                        if (c == null && _owner.ID == Source) c = _owner;
                        if (c is IMTControlWithValue)
                            SetValue(((IMTControlWithValue)c).Value);
                        if (c is IMTEditableControl)
                            IsValueNotPassed = ((IMTEditableControl)c).IsValueNotPassed;
                    }
                    else
                        IsValueNotPassed = true;
                    break;
                case DataParameterSourceType.DataSourceColumn:
                    if (_owner != null)
                    {
                        IForm c = _owner as IForm;

                        if (c != null && c.DataItem[Source] != null && c.DataItem[Source] != DBNull.Value)
                        {
                            SetValue(c.DataItem[Source]);
                            IsValueNotPassed = false;
                        }
                        else
                            IsValueNotPassed = true;
                    }
                    else
                        IsValueNotPassed = true;
                    break;
                case DataParameterSourceType.Cookie:
                    if (HttpContext.Current.Request.Cookies[Source] != null)
                    {
                        object[] temp = new object[HttpContext.Current.Request.Cookies[Source].Values.Count];
                        HttpContext.Current.Request.Cookies[Source].Values.CopyTo(temp, 0);
                        SetValue(temp);
                        IsValueNotPassed = false;
                    }
                    else
                    {
                        IsValueNotPassed = true;
                    }
                    break;
                case DataParameterSourceType.Expression:
                    if (Source != null && Source.Length > 0)
                        SetValue(Source);
                    else
                        SetValue(DefaultValue);
                    break;
                case DataParameterSourceType.Form:
                    if (HttpContext.Current.Request.Form[Source] != null)
                    {
                        SetValue(HttpContext.Current.Request.Form.GetValues(Source));
                        IsValueNotPassed = false;
                    }
                    else
                        IsValueNotPassed = true;
                    break;
                case DataParameterSourceType.Session:
                    if (HttpContext.Current.Session[Source] != null)
                    {
                        SetValue(HttpContext.Current.Session[Source]);
                        IsValueNotPassed = false;
                    }
                    else
                        IsValueNotPassed = true;
                    break;
                case DataParameterSourceType.Url:
                    if (HttpContext.Current.Request.QueryString[Source] != null)
                    {
                        SetValue(HttpContext.Current.Request.QueryString.GetValues(Source));
                        IsValueNotPassed = false;
                    }
                    else
                        IsValueNotPassed = true;
                    break;
                case DataParameterSourceType.DBExpression:
                    SetValue(Source);
                    break;
                case DataParameterSourceType.SpecialValue:
                    ISpecialValueProvider provider = _owner as ISpecialValueProvider;
                    if (provider != null && provider.SpecialValues.ContainsKey(Source))
                    {
                        SetValue(provider.SpecialValues[Source]);
                        IsValueNotPassed = false;
                    }
                    else
                        IsValueNotPassed = true;
                    break;
                default:
                    break;
            }
            if (IsValueNotPassed)
            {
                SetValue(HttpContext.Current.Items[Source]);
                IsValueNotPassed = HttpContext.Current.Items[Source] == null;
            }
            if (Value == null || Value is IMTField && (Value as IMTField).IsNull) SetValue(DefaultValue);
            if (!IsMultiple)
                return Value;
            else
                return Values;
        }
    }
}
