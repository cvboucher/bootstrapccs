//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Provides data for ItemCommand event of the <see cref="Directory"/> component.
    /// </summary>
    public sealed class DirectoryCommandEventArgs : CommandEventArgs
    {

        private DirectoryItem item;
        private object commandSource;

        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryCommandEventArgs"/> class. 
        /// </summary>
        /// <param name="item">A <see cref="DirectoryItem"/> that represents an item in the <see cref="Directory"/>. The <see cref="Item"/> property is set to this value.</param>
        /// <param name="commandSource">The command source. The <see cref="CommandSource"/> property is set to this value.</param>
        /// <param name="originalArgs">The original event arguments.</param>
        public DirectoryCommandEventArgs(DirectoryItem item, object commandSource, CommandEventArgs originalArgs)
            :
            base(originalArgs)
        {
            this.item = item;
            this.commandSource = commandSource;
        }

        /// <summary>
        /// Gets the <see cref="DirectoryItem"/> associated with the event.
        /// </summary>
        public DirectoryItem Item
        {
            get
            {
                return item;
            }
        }

        /// <summary>
        /// Gets the source of the command.
        /// </summary>
        public object CommandSource
        {
            get
            {
                return commandSource;
            }
        }
    }
   
}
