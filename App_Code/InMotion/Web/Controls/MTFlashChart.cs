//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Xml;
using System.Web;
using System.Data;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Collections.ObjectModel;
using InMotion.Common;
using InMotion.Security;
using InMotion.Configuration;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents a list box control that allows single or multiple item selection.
    /// </summary>
    public class MTFlashChart : DataBoundControl, IMTControl, IMTAttributeAccessor
    {
        #region Events
        /// <summary>
        /// Occurs after the <see cref="MTFlashChart"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;
        #endregion

        #region Properties
        private Collection<MTParameter> _paremeters = null;
        /// <summary>
        /// Gets collection of flashchart rendering parameters.
        /// </summary>
        [
        DefaultValue(null),
        Editor("System.Drawing.Design.UITypeEditor,System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor)),
        PersistenceMode(PersistenceMode.InnerProperty),
        MergableProperty(false)
        ]
        public Collection<MTParameter> Parameters
        {
            get
            {
                if (_paremeters == null)
                    _paremeters = new Collection<MTParameter>();
                return _paremeters;
            }
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>.
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        private string __title;
        /// <summary>
        /// Gets or sets a control Title.
        /// </summary>
        public string Title
        {
            get
            {
                return __title;
            }
            set
            {
                __title = value;
            }
        }

        private int __maximumRows;
        /// <summary>
        /// Gets or sets a value that represents the maximum number of data rows that a data source control returns for a data retrieval operation.
        /// </summary>
        public int MaximumRows
        {
            get
            {
                return __maximumRows;
            }
            set
            {
                __maximumRows = value;
            }
        }

        private bool _allowRead = true;
        /// <summary>
        /// Gets or sets the value that indicating whether current user have Read permission.
        /// </summary>
        public bool AllowRead
        {
            get
            {
                return _allowRead;
            }
            set
            {
                _allowRead = value;
            }
        }

        private FormSupportedOperations _userRights;
        /// <summary>
        /// Gets the <see cref="FormSupportedOperations"/> for the current form.
        /// </summary>
        public FormSupportedOperations UserRights
        {
            get
            {
                if (_userRights == null)
                    _userRights = new FormSupportedOperations();
                return _userRights;
            }
        }

        private bool _restricted;
        /// <summary>
        /// Gets or sets the value indicating whether form will restrict the user access according to the <see cref="UserRights"/>.
        /// </summary>
        public bool Restricted
        {
            get
            {
                return _restricted;
            }
            set
            {
                _restricted = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// The path to the web service 
        /// </summary>
        public virtual string ServicePath
        {
            get
            {
                return (string)ViewState["ServicePath"];
            }
            set
            {
                ViewState["ServicePath"] = value;
            }
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            if (!DesignMode && Page.Request["callbackControl"] == ClientID)
            {
                ExecuteService();
            }
            OnBeforeShow(EventArgs.Empty);
        }

        /// <summary>
        /// Retrieves the <see cref="DataSourceView"/> object that is associated with the current form.
        /// </summary>
        /// <returns>The <see cref="DataSourceView"/> object that is associated with the current form.</returns>
        public DataSourceView GetDataSourceView()
        {
            Control c1 = this as Control;
            IDataSource dataSource = null;
            while (dataSource == null && c1 != Page)
            {
                dataSource = c1.FindControl(DataSourceID) as IDataSource;
                c1 = c1.Parent;
            }
            return dataSource != null ? dataSource.GetView(DataMember) : null;
        }

        /// <summary>
        /// Execute flashchart service
        /// </summary>
        private void ExecuteService()
        {
            if (!AllowRead || Restricted && !UserRights.AllowRead)
                Visible = false;
            if (!DesignMode && Visible)
            {
                XmlDocument serviceDoc = new XmlDocument();
                serviceDoc.Load(System.IO.Path.Combine(System.IO.Path.Combine(Page.Request.PhysicalApplicationPath, AppRelativeTemplateSourceDirectory.Substring(2).Replace("/", "\\")), ServicePath));
                XmlNode titleNode = serviceDoc.SelectSingleNode("/root/chartarea/title/@text");
                if (titleNode != null)
                    titleNode.Value = InMotion.Web.Controls.ControlsHelper.ReplaceResource(titleNode.Value);
                XmlNode TemplateNode = serviceDoc.SelectSingleNode("/root/data/rows/row").Clone();
                XmlNode ParrentNode = serviceDoc.SelectSingleNode("/root/data/rows");
                ParrentNode.RemoveAll();

                DataSourceView dsw = GetDataSourceView();
                DataView dv = ((MTDataSourceView)GetData()).Select(new DataSourceSelectArguments(0, MaximumRows)) as DataView;

                for (int i = 0; i < dv.Count; i++)
                {
                    XmlNode CurrentNode = TemplateNode.Clone();
                    for (int j = 0; j < CurrentNode.Attributes.Count; j++)
                    {
                        string[] DSFields = CurrentNode.Attributes[j].Value.Split(new char[] { '}', '{' }, StringSplitOptions.RemoveEmptyEntries);
                        if (DSFields.Length == 1 && dv.Table.Columns.Contains(DSFields[0]))
                            CurrentNode.Attributes[j].Value = dv[i][DSFields[0]].ToString();
                    }
                    ParrentNode.AppendChild(CurrentNode);
                }
                Page.Response.Cache.AppendCacheExtension("cache, must-revalidate");
                Page.Response.AddHeader("Pragma", "public");
                Page.Response.AddHeader("Content-type", "text/xml");
                Page.Response.Write(serviceDoc.InnerXml);
                Page.Response.End();
            }
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="HtmlTextWriter"/> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            if (!DesignMode && AllowRead && (!Restricted || UserRights.AllowRead) && Visible)
            {
                Title = InMotion.Web.Controls.ControlsHelper.ReplaceResource(Title);
                Url src = new Url(ResolveClientUrl("~/FlashChart.swf"));
                src.Parameters.Add("XMLDataFile", "?callbackControl=" + ClientID + "&" + Page.Request.QueryString.ToString());

                AddAttributesToRender(writer);
                writer.AddAttribute("classid", "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000");
                writer.AddAttribute("codebase", "http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0");
                writer.AddAttribute(HtmlTextWriterAttribute.Width, Width.ToString());
                writer.AddAttribute(HtmlTextWriterAttribute.Height, Height.ToString());
                writer.AddAttribute(HtmlTextWriterAttribute.Accesskey, "q");
                writer.AddAttribute(HtmlTextWriterAttribute.Tabindex, "1");
                writer.AddAttribute(HtmlTextWriterAttribute.Title, Title);
                writer.RenderBeginTag(HtmlTextWriterTag.Object);

                for (int i =0; i < Parameters.Count; i++)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Name, Parameters[i].Name);
                    writer.AddAttribute(HtmlTextWriterAttribute.Value, Parameters[i].Value);
                    writer.RenderBeginTag(HtmlTextWriterTag.Param);
                    writer.RenderEndTag();
                }
                writer.AddAttribute(HtmlTextWriterAttribute.Name, "movie");
                writer.AddAttribute(HtmlTextWriterAttribute.Value, src.ToString(true));
                writer.RenderBeginTag(HtmlTextWriterTag.Param);
                writer.RenderEndTag();
                
                writer.AddAttribute(HtmlTextWriterAttribute.Type, "application/x-shockwave-flash");
                writer.AddAttribute("pluginspage", "http://www.macromedia.com/go/getflashplayer");
                writer.AddAttribute(HtmlTextWriterAttribute.Src, src.ToString(true));
                writer.AddAttribute(HtmlTextWriterAttribute.Height, Height.ToString());
                writer.AddAttribute(HtmlTextWriterAttribute.Width, Width.ToString());

                for (int i = 0; i < Parameters.Count; i++)
                {
                    writer.AddAttribute(Parameters[i].Name, Parameters[i].Value);
                }

                writer.RenderBeginTag(HtmlTextWriterTag.Embed);
                writer.RenderEndTag();

                writer.RenderEndTag();
            }
        }
        #endregion
    }
}