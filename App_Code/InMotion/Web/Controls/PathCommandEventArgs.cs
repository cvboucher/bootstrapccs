//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Provides data for ItemCommand event of the <see cref="Path"/> component.
    /// </summary>
    public sealed class PathCommandEventArgs : CommandEventArgs
    {

        private PathItem item;
        private object commandSource;

        /// <summary>
        /// Initializes a new instance of the <see cref="PathCommandEventArgs"/> class. 
        /// </summary>
        /// <param name="item">A <see cref="PathItem"/> that represents an item in the <see cref="Path"/>. The <see cref="Item"/> property is set to this value.</param>
        /// <param name="commandSource">The command source. The <see cref="CommandSource"/> property is set to this value.</param>
        /// <param name="originalArgs">The original event arguments.</param>
        public PathCommandEventArgs(PathItem item, object commandSource, CommandEventArgs originalArgs)
            :
            base(originalArgs)
        {
            this.item = item;
            this.commandSource = commandSource;
        }

        /// <summary>
        /// Gets the <see cref="PathItem"/> associated with the event.
        /// </summary>
        public PathItem Item
        {
            get
            {
                return item;
            }
        }

        /// <summary>
        /// Gets the source of the command.
        /// </summary>
        public object CommandSource
        {
            get
            {
                return commandSource;
            }
        }
    }

}
