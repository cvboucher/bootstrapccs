//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Provides data for ItemDataBound event of the <see cref="Report"/> component.
    /// </summary>
    public class BeforeItemDataBoundEventArgs : EventArgs
    {
        
        private object m_DataItem;

        /// <summary>
        /// Gets the data item associated with the event.
        /// </summary>
        public object DataItem
        {
            get { return m_DataItem; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BeforeItemDataBoundEventArgs"/> class. 
        /// </summary>
        /// <param name="data">The data item associated with the event. The <see cref="DataItem"/> property is set to this value. </param>
        public BeforeItemDataBoundEventArgs(object data)
        {
            this.m_DataItem = data;
        }
    }

}
