//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Specifies the type of an item in a <see cref="Menu"/> control.
    /// </summary>
    public class MenuItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuItem"/> class.
        /// </summary>
        public MenuItem()
        {
            _name = "";
            _parent = "";
            _caption = "";
            _hrefSource = null;
            _target = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuItem"/> class.
        /// </summary>
        /// <param name="name">The name of the item.</param>
        /// <param name="caption">The text value that is rendered as a caption for the item.</param>
        public MenuItem(string name, string caption)
        {
            _name = name;
            _parent = "";
            _caption = caption;
            _hrefSource = null;
            _target = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuItem"/> class.
        /// </summary>
        /// <param name="name">The name of the item.</param>
        /// <param name="parent">The name of parent item in the menu hierarchy.</param>
        /// <param name="caption">The text value that is rendered as a caption for the item.</param>
        public MenuItem(string name, string parent, string caption)
        {
            _name = name;
            _parent = parent;
            _caption = caption;
            _hrefSource = null;
            _target = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuItem"/> class.
        /// </summary>
        /// <param name="name">The name of the item.</param>
        /// <param name="parent">The name of parent item in the menu hierarchy.</param>
        /// <param name="caption">The text value that is rendered as a caption for the item.</param>
        /// <param name="hrefSource">The URL to link to when the item control is clicked.</param>
        public MenuItem(string name, string parent, string caption, string hrefSource)
        {
            _name = name;
            _parent = parent;
            _caption = caption;
            _hrefSource = hrefSource;
            _target = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuItem"/> class.
        /// </summary>
        /// <param name="name">The name of the item.</param>
        /// <param name="parent">The name of parent item in the menu hierarchy.</param>
        /// <param name="caption">The text value that is rendered as a caption for the item.</param>
        /// <param name="hrefSource">The URL to link to when the item control is clicked.</param>
        /// <param name="target">Gets or sets the target window or frame in which to display the Web page content linked to when the MenuItem link control is clicked.</param>
        public MenuItem(string name, string parent, string caption, string hrefSource, string target)
        {
            _name = name;
            _parent = parent;
            _caption = caption;
            _hrefSource = hrefSource;
            _target = target;
        }

        private string _name;
        /// <summary>
        /// Gets or sets the name of the item.
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        private string _parent;
        /// <summary>
        /// Gets or sets name of parent item in the menu hierarchy. Can be empty if item is main.
        /// </summary>
        public string Parent
        {
            get
            {
                return _parent;
            }
            set
            {
                _parent = value;
            }
        }

        private string _caption;
        /// <summary>
        /// Gets or sets a text value that is rendered as a caption for the item.
        /// </summary>
        public string Caption
        {
            get
            {
                if (String.IsNullOrEmpty(_caption))
                    return Name;
                return _caption;
            }
            set
            {
                _caption = value;
            }
        }

        private string _hrefSource;
        /// <summary>
        /// Gets or sets the URL to link to when the item control is clicked.
        /// </summary>
        public string HrefSource
        {
            get
            {
                return _hrefSource;
            }
            set
            {
                _hrefSource = value;
            }
        }

        private string _target;
        /// <summary>
        /// Gets or sets the target window or frame in which to display the Web page content linked to when the MenuItem link control is clicked.
        /// </summary>
        public string Target
        {
            get
            {
                return _target;
            }
            set
            {
                _target = value;
            }
        }

        private string _title;
        /// <summary>
        /// Gets or sets the text displayed when the mouse pointer hovers over the Web server control.
        /// </summary>
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }
    }
}