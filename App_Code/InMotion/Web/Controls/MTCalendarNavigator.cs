//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.ComponentModel;
using InMotion.Configuration;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Specifies the order to which elements of navigator will appear.
    /// </summary>
    public enum CalendarNavigatorOrder
    {
        /// <summary>
        /// Years selector, Quarters selector, Month selector.
        /// </summary>
        YearsQuartersMonths,
        /// <summary>
        /// Years selector, Quarters selector, Month selector.
        /// </summary>
        YearsMonthsQuarters,
        /// <summary>
        /// Month selector, Quarters selector, Years selector.
        /// </summary>
        MonthsQuartersYears,
        /// <summary>
        /// Month selector, Years selector, Quarters selector.
        /// </summary>
        MonthsYearsQuarters,
        /// <summary>
        /// Quarters selector, Years selector, Month selector.
        /// </summary>
        QuartersYearsMonths,
        /// <summary>
        /// Quarters selector, Month selector, Years selector.
        /// </summary>
        QuartersMonthsYears
    }

    /// <summary>
    /// Specifies how the current date selector will displayed.
    /// </summary>
    public enum CurrentDateElementStyle
    {
        /// <summary>
        /// The selctor is not displayed.
        /// </summary>
        None,
        /// <summary>
        /// The selector is displayed as text.
        /// </summary>
        Text,
        /// <summary>
        /// The selector is displayed as listbox.
        /// </summary>
        ListBox,
        /// <summary>
        /// The selector is displayed as set of links.
        /// </summary>
        Link
    }

    /// <summary>
    /// Specify how month names displayed in <see cref="MTCalendarNavigator"/>.
    /// </summary>
    public enum MonthNameStyle
    {
        /// <summary>
        /// The months names is displayed in short form, i.e. Jun, Jul.
        /// </summary>
        Short,
        /// <summary>
        /// The months names is displayed in full form, i.e. June, July.
        /// </summary>
        Full
    }

    /// <summary>
    /// Calendar Navigator controls the navigation area where users can select the year and month to be displayed by the calendar.
    /// </summary>
    public class MTCalendarNavigator : WebControl, IPostBackEventHandler, IMTControl, IMTAttributeAccessor
    {
        /// <summary>
        /// Occurs after the <see cref="MTCalendarNavigator"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;

        /// <summary>
        /// Gets or sets the text for the previous year element of navigator.
        /// </summary>
        public string PrevYear
        {
            get
            {
                if (ViewState["__PrevYear"] == null)
                    ViewState["__PrevYear"] = "<<";
                return (string)ViewState["__PrevYear"];
            }
            set { ViewState["__PrevYear"] = value; }
        }

        /// <summary>
        /// Gets or sets the text for the previous month element of navigator.
        /// </summary>
        public string PrevMonth
        {
            get
            {
                if (ViewState["__PrevMonth"] == null)
                    ViewState["__PrevMonth"] = "<";
                return (string)ViewState["__PrevMonth"];
            }
            set { ViewState["__PrevMonth"] = value; }
        }

        /// <summary>
        /// Gets or sets the text for the previous quarter element of navigator.
        /// </summary>
        public string PrevQuarter
        {
            get
            {
                if (ViewState["__PrevQuarter"] == null)
                    ViewState["__PrevQuarter"] = "<";
                return (string)ViewState["__PrevQuarter"];
            }
            set { ViewState["__PrevQuarter"] = value; }
        }

        /// <summary>
        /// Gets or sets the text for the next year element of navigator.
        /// </summary>
        public string NextYear
        {
            get
            {
                if (ViewState["__NextYear"] == null)
                    ViewState["__NextYear"] = ">>";
                return (string)ViewState["__NextYear"];
            }
            set { ViewState["__NextYear"] = value; }
        }

        /// <summary>
        /// Gets or sets the text for the next month element of navigator.
        /// </summary>
        public string NextMonth
        {
            get
            {
                if (ViewState["__NextMonth"] == null)
                    ViewState["__NextMonth"] = ">";
                return (string)ViewState["__NextMonth"];
            }
            set { ViewState["__NextMonth"] = value; }
        }

        /// <summary>
        /// Gets or sets the text for the next quarter element of navigator.
        /// </summary>
        public string NextQuarter
        {
            get
            {
                if (ViewState["__NextQuarter"] == null)
                    ViewState["__NextQuarter"] = ">";
                return (string)ViewState["__NextQuarter"];
            }
            set { ViewState["__NextQuarter"] = value; }
        }

        private OperationMode __OperationMode = OperationMode.Url;
        /// <summary>
        /// Gets or sets the operation mode of the <see cref="MTSorter"/> control.
        /// </summary>
        /// <value>
        /// One of the <see cref="OperationMode"/> enumeration. 
        /// The default is [b]Url[/b]
        /// </value>
        public OperationMode OperationMode
        {
            get
            {
                return __OperationMode;
            }
            set { __OperationMode = value; }
        }

        /// <summary>
        /// Gets or sets the text for the previous year alt attribute of image link.
        /// </summary>
        public string PrevYearAlt
        {
            get
            {
                if (ViewState["__PrevYearAlt"] == null)
                    ViewState["__PrevYearAlt"] = "<<";
                return (string)ViewState["__PrevYearAlt"];
            }
            set { ViewState["__PrevYearAlt"] = value; }
        }

        /// <summary>
        /// Gets or sets the text for the previous month alt attribute of image link.
        /// </summary>
        public string PrevMonthAlt
        {
            get
            {
                if (ViewState["__PrevMonthAlt"] == null)
                    ViewState["__PrevMonthAlt"] = "<";
                return (string)ViewState["__PrevMonthAlt"];
            }
            set { ViewState["__PrevMonthAlt"] = value; }
        }

        /// <summary>
        /// Gets or sets the text for the previous quarter alt attribute of image link.
        /// </summary>
        public string PrevQuarterAlt
        {
            get
            {
                if (ViewState["__PrevQuarterAlt"] == null)
                    ViewState["__PrevQuarterAlt"] = "<";
                return (string)ViewState["__PrevQuarterAlt"];
            }
            set { ViewState["__PrevQuarterAlt"] = value; }
        }

        /// <summary>
        /// Gets or sets the text for the next year element alt attribute of image link.
        /// </summary>
        public string NextYearAlt
        {
            get
            {
                if (ViewState["__NextYearAlt"] == null)
                    ViewState["__NextYearAlt"] = ">>";
                return (string)ViewState["__NextYearAlt"];
            }
            set { ViewState["__NextYearAlt"] = value; }
        }

        /// <summary>
        /// Gets or sets the text for the next month element alt attribute of image link.
        /// </summary>
        public string NextMonthAlt
        {
            get
            {
                if (ViewState["__NextMonthAlt"] == null)
                    ViewState["__NextMonthAlt"] = ">";
                return (string)ViewState["__NextMonthAlt"];
            }
            set { ViewState["__NextMonthAlt"] = value; }
        }

        /// <summary>
        /// Gets or sets the text for the next quarter alt attribute of image link.
        /// </summary>
        public string NextQuarterAlt
        {
            get
            {
                if (ViewState["__NextQuarterAlt"] == null)
                    ViewState["__NextQuarterAlt"] = ">";
                return (string)ViewState["__NextQuarterAlt"];
            }
            set { ViewState["__NextQuarterAlt"] = value; }
        }

        private string __submitButtonText = "Submit";
        /// <summary>
        /// Gets or sets the text for the psubmit button of navigator.
        /// </summary>
        public string SubmitButtonText
        {
            get { return __submitButtonText; }
            set { __submitButtonText = value; }
        }

        private string __submitButtonImg = "";
        /// <summary>
        /// Gets or sets the text for the image submit button of navigator.
        /// </summary>
        public string SubmitButtonImg
        {
            get { return __submitButtonImg; }
            set { __submitButtonImg = value; }
        }

        /// <summary>
        /// Gets or sets value that indicating whether navigator will display previous year element.
        /// </summary>
        public bool PrevYearEnabled
        {
            get
            {
                return PrevYear.Length > 0;
            }
        }

        /// <summary>
        /// Gets or sets value that indicating whether navigator will display previous month element.
        /// </summary>
        public bool PrevMonthEnabled
        {
            get
            {
                return PrevMonth.Length > 0;
            }
        }

        /// <summary>
        /// Gets or sets value that indicating whether navigator will display previous quarter element.
        /// </summary>
        public bool PrevQuarterEnabled
        {
            get
            {
                return PrevQuarter.Length > 0;
            }
        }

        /// <summary>
        /// Gets or sets value that indicating whether navigator will display next year element.
        /// </summary>
        public bool NextYearEnabled
        {
            get
            {
                return NextYear.Length > 0;
            }
        }

        /// <summary>
        /// Gets or sets value that indicating whether navigator will display next month element.
        /// </summary>
        public bool NextMonthEnabled
        {
            get
            {
                return NextMonth.Length > 0;
            }
        }

        /// <summary>
        /// Gets or sets value that indicating whether navigator will display next quarter element.
        /// </summary>
        public bool NextQuarterEnabled
        {
            get
            {
                return NextQuarter.Length > 0;
            }
        }

        bool __useImaageLinks;
        /// <summary>
        /// Gets or sets value that indicating whether navigator will use Image Links for its elements.
        /// </summary>
        public bool UseImageLinks
        {
            get
            {
                return __useImaageLinks;
            }
            set { __useImaageLinks = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="CurrentDateElementStyle"/> for current year element.
        /// </summary>
        public CurrentDateElementStyle CurrentYear
        {
            get
            {
                if (ViewState["__CurrentYear"] == null)
                    ViewState["__CurrentYear"] = (int)CurrentDateElementStyle.None;
                return (CurrentDateElementStyle)ViewState["__CurrentYear"];
            }
            set { ViewState["__CurrentYear"] = (int)value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="CurrentDateElementStyle"/> for current month element.
        /// </summary>
        public CurrentDateElementStyle CurrentMonth
        {
            get
            {
                if (ViewState["__CurrentMonth"] == null)
                    ViewState["__CurrentMonth"] = (int)CurrentDateElementStyle.None;
                return (CurrentDateElementStyle)ViewState["__CurrentMonth"];
            }
            set { ViewState["__CurrentMonth"] = (int)value; }
        }

        private MonthNameStyle ___monthNameStyle;
        /// <summary>
        /// Gets or sets the <see cref="MonthNameStyle"/> for month name element.
        /// </summary>
        public MonthNameStyle MonthNameStyle
        {
            get { return ___monthNameStyle; }
            set { ___monthNameStyle = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="CurrentDateElementStyle"/> for current quarter element.
        /// </summary>
        public CurrentDateElementStyle CurrentQuarter
        {
            get
            {
                if (ViewState["__CurrentQuarter"] == null)
                    ViewState["__CurrentQuarter"] = (int)CurrentDateElementStyle.None;
                return (CurrentDateElementStyle)ViewState["__CurrentQuarter"];
            }
            set { ViewState["__CurrentQuarter"] = (int)value; }
        }

        /// <summary>
        /// Gets or sets the order of elements of the navigator.
        /// </summary>
        public CalendarNavigatorOrder Order
        {
            get
            {
                if (ViewState["__Order"] == null)
                    ViewState["__Order"] = (int)CalendarNavigatorOrder.MonthsQuartersYears;
                return (CalendarNavigatorOrder)ViewState["__Order"];
            }
            set { ViewState["__Order"] = (int)value; }
        }

        /// <summary>
        /// Gets or sets value that indicating whether navigator will use Image Buttons for button elements.
        /// </summary>
        public bool UseImageButton
        {
            get
            {
                if (ViewState["__UseImageButton"] == null)
                    ViewState["__UseImageButton"] = false;
                return (bool)ViewState["__UseImageButton"];
            }
            set { ViewState["__UseImageButton"] = value; }
        }

        /// <summary>
        /// Gets or sets the number of displayed years.
        /// </summary>
        public int YearsRange
        {
            get
            {
                if (ViewState["yearsRange"] == null) return 0;
                return (int)ViewState["yearsRange"];
            }
            set
            {
                ViewState["yearsRange"] = value;
            }
        }

        private Url _url = new Url();
        /// <summary>
        /// Gets or sets the url which will be used as target page 
        /// for Url operation mode.
        /// </summary>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public Url Url
        {
            get { return _url; }
            set { _url = value; }
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        private Calendar owner
        {
            get { return (Calendar)OwnerForm; }
        }

        /// <summary>
        /// Gets the currently selected date.
        /// </summary>
        [Browsable(false)]
        public DateTime Date
        {
            get
            {
                return owner.Date;
            }
        }

        /// <summary>
        /// Gets the currently selected month.
        /// </summary>
        [Browsable(false)]
        public int Month
        {
            get
            {
                return Date.Month;
            }
        }

        /// <summary>
        /// Gets the currently selected year.
        /// </summary>
        [Browsable(false)]
        public int Year
        {
            get
            {
                return Date.Year;
            }
        }

        /// <summary>
        /// Gets the currently selected quarter.
        /// </summary>
        [Browsable(false)]
        public int Quarter
        {
            get
            {
                return (Month - 1) / 3 + 1;
            }
        }

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Gets or sets the name of query string parameter which will
        /// be used for Url operation mode.
        /// </summary>
        /// <value>
        /// The string. The default is owner ID.
        /// </value>
        public string ParameterName
        {
            get
            {
                if (ViewState["__ParameterName"] == null && owner != null)
                    ViewState["__ParameterName"] = owner.ID;
                return (string)ViewState["__ParameterName"];
            }
            set { ViewState["__ParameterName"] = value; }
        }

        /// <summary>
        /// Renders the <see cref="MTCalendarNavigator"/> control to the specified HtmlTextWriter object. 
        /// </summary>
        /// <param name="writer">
        /// The <see cref="HtmlTextWriter"/> that receives the rendered output.
        ///</param>
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            if (!Visible) return;
            writer.RenderBeginTag("table");
            writer.RenderBeginTag("tr");
            DateTime CDate = DateTime.Now.Date;
            if (!DesignMode || owner != null)
                CDate = Date;
            int CQuarter = (CDate.Month - 1) / 3 + 1;

            if (PrevYearEnabled)
            {
                writer.RenderBeginTag("td");
                CreateLink(writer, PrevYear, PrevYearAlt, CDate.AddYears(-1));
                writer.RenderEndTag();
            }

            if (PrevQuarterEnabled && (DesignMode || owner.Mode == CalendarMode.Quarter))
            {
                writer.RenderBeginTag("td");
                CreateLink(writer, PrevQuarter, PrevQuarterAlt, CDate.AddMonths(-(CDate.Month - 1) % 3 - 3));
                writer.RenderEndTag();
            }
            else if (PrevMonthEnabled && (DesignMode || owner.Mode != CalendarMode.Full))
            {
                writer.RenderBeginTag("td");
                CreateLink(writer, PrevMonth, PrevMonthAlt, CDate.AddMonths(-1));
                writer.RenderEndTag();
            }

            switch (Order)
            {
                case CalendarNavigatorOrder.YearsQuartersMonths:
                    CreateSelector(writer, CurrentYear, CDate.Year - YearsRange, YearsRange + CDate.Year, CDate.Year, SelectorType.Year);
                    CreateSelector(writer, CurrentQuarter, 1, 4, CQuarter, SelectorType.Quarter);
                    CreateSelector(writer, CurrentMonth, 1, 12, CDate.Month, SelectorType.Month);
                    break;
                case CalendarNavigatorOrder.YearsMonthsQuarters:
                    CreateSelector(writer, CurrentYear, CDate.Year - YearsRange, YearsRange + CDate.Year, CDate.Year, SelectorType.Year);
                    CreateSelector(writer, CurrentMonth, 1, 12, CDate.Month, SelectorType.Month);
                    CreateSelector(writer, CurrentQuarter, 1, 4, CQuarter, SelectorType.Quarter);
                    break;
                case CalendarNavigatorOrder.MonthsQuartersYears:
                    CreateSelector(writer, CurrentMonth, 1, 12, CDate.Month, SelectorType.Month);
                    CreateSelector(writer, CurrentQuarter, 1, 4, CQuarter, SelectorType.Quarter);
                    CreateSelector(writer, CurrentYear, CDate.Year - YearsRange, YearsRange + CDate.Year, CDate.Year, SelectorType.Year);
                    break;
                case CalendarNavigatorOrder.MonthsYearsQuarters:
                    CreateSelector(writer, CurrentMonth, 1, 12, CDate.Month, SelectorType.Month);
                    CreateSelector(writer, CurrentYear, CDate.Year - YearsRange, YearsRange + CDate.Year, CDate.Year, SelectorType.Year);
                    CreateSelector(writer, CurrentQuarter, 1, 4, CQuarter, SelectorType.Quarter);
                    break;
                case CalendarNavigatorOrder.QuartersYearsMonths:
                    CreateSelector(writer, CurrentQuarter, 1, 4, CQuarter, SelectorType.Quarter);
                    CreateSelector(writer, CurrentYear, CDate.Year - YearsRange, YearsRange + CDate.Year, CDate.Year, SelectorType.Year);
                    CreateSelector(writer, CurrentMonth, 1, 12, CDate.Month, SelectorType.Month);
                    break;
                case CalendarNavigatorOrder.QuartersMonthsYears:
                    CreateSelector(writer, CurrentQuarter, 1, 4, CQuarter, SelectorType.Quarter);
                    CreateSelector(writer, CurrentMonth, 1, 12, CDate.Month, SelectorType.Month);
                    CreateSelector(writer, CurrentYear, CDate.Year - YearsRange, YearsRange + CDate.Year, CDate.Year, SelectorType.Year);
                    break;
            }

            if (CurrentQuarter == CurrentDateElementStyle.ListBox ||
                CurrentMonth == CurrentDateElementStyle.ListBox ||
                CurrentYear == CurrentDateElementStyle.ListBox)
            {
                writer.RenderBeginTag("td");
                if (UseImageButton)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "image");
                    writer.AddAttribute(HtmlTextWriterAttribute.Src, ControlsHelper.ReplaceResourcesAndStyles(SubmitButtonImg, Page));
                    writer.AddAttribute(HtmlTextWriterAttribute.Alt, SubmitButtonText);
                    writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0");
                }
                else
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "button");
                    writer.AddAttribute(HtmlTextWriterAttribute.Value, SubmitButtonText);
                }

                writer.AddAttribute(HtmlTextWriterAttribute.Onclick, Page.ClientScript.GetPostBackEventReference(this, "navigate", false));
                writer.RenderBeginTag(HtmlTextWriterTag.Input);
                writer.RenderEndTag();
                writer.RenderEndTag();
            }

            if (NextQuarterEnabled && (DesignMode || owner.Mode == CalendarMode.Quarter))
            {
                writer.RenderBeginTag("td");
                CreateLink(writer, NextQuarter, NextQuarterAlt, CDate.AddMonths(3 - (CDate.Month - 1) % 3));
                writer.RenderEndTag();
            }
            else if (NextMonthEnabled && (DesignMode || owner.Mode != CalendarMode.Full))
            {
                writer.RenderBeginTag("td");
                CreateLink(writer, NextMonth, NextMonthAlt, CDate.AddMonths(1));
                writer.RenderEndTag();
            }

            if (NextYearEnabled)
            {
                writer.RenderBeginTag("td");
                CreateLink(writer, NextYear, NextYearAlt, CDate.AddYears(1));
                writer.RenderEndTag();
            }

            writer.RenderEndTag();
            writer.RenderEndTag();
        }

        private enum SelectorType { Month, Year, Quarter }
        private void CreateSelector(System.Web.UI.HtmlTextWriter writer, CurrentDateElementStyle style, int rangeStart, int rangeEnd, int current, SelectorType type)
        {
            if (style == CurrentDateElementStyle.None) return;
            writer.RenderBeginTag("td");
            if (style == CurrentDateElementStyle.ListBox)
            {
                writer.WriteBeginTag("select");
                writer.WriteAttribute("name", owner.ID + type.ToString());
                writer.Write(HtmlTextWriter.TagRightChar);
            }
            if (style == CurrentDateElementStyle.Text)
            {
                rangeEnd = current;
                rangeStart = current;

            }

            for (int i = rangeStart; i <= rangeEnd; i++)
            {
                string textValue = "";
                if (SelectorType.Month == type)
                    if (MonthNameStyle == MonthNameStyle.Full)
                        textValue = (new DateTime(1, i, 1)).ToString("MMMM");
                    else
                        textValue = (new DateTime(1, i, 1)).ToString("MMM");
                else
                    textValue = i.ToString();
                DateTime param = DateTime.Now.Date;
                if (!DesignMode || owner != null)
                    param = Date;
                if (style == CurrentDateElementStyle.Link && i != current)
                {
                    switch (type)
                    {
                        case SelectorType.Month:
                            param = new DateTime(param.Year, i, 1);
                            break;
                        case SelectorType.Year:
                            param = new DateTime(i, param.Month, 1);
                            break;
                        case SelectorType.Quarter:
                            param = new DateTime(param.Year, (i - 1) * 3 + 1, 1);
                            break;
                        default:
                            break;
                    }
                    CreateLink(writer, textValue, null, param, true);
                }
                if (style == CurrentDateElementStyle.ListBox)
                {
                    writer.WriteBeginTag("option");
                    writer.WriteAttribute("value", i.ToString());
                    if (current == i)
                        writer.WriteAttribute("selected", "selected");
                    writer.Write(HtmlTextWriter.TagRightChar);
                }
                if (i == current || style == CurrentDateElementStyle.ListBox)
                    writer.Write(textValue);
                if (style == CurrentDateElementStyle.Text || style == CurrentDateElementStyle.Link)
                    writer.WriteLine(HtmlTextWriter.SpaceChar);
                if (style == CurrentDateElementStyle.ListBox)
                    writer.WriteEndTag("option");
            }
            if (style == CurrentDateElementStyle.ListBox)
            {
                writer.WriteEndTag("select");
            }
            writer.RenderEndTag();
        }

        private void CreateLink(System.Web.UI.HtmlTextWriter writer, string text, DateTime date)
        {
            CreateLink(writer, text, null, date, false);
        }

        private void CreateLink(System.Web.UI.HtmlTextWriter writer, string text, string altText, DateTime date)
        {
            CreateLink(writer, text, altText, date, false);
        }

        private void CreateLink(System.Web.UI.HtmlTextWriter writer, string text, string altText, DateTime date, bool useText)
        {
            text = ControlsHelper.ReplaceResourcesAndStyles(text, Page);
            writer.WriteBeginTag("a");
            CreateHref(writer, date.ToString("yyyy-MM"));
            writer.Write(HtmlTextWriter.TagRightChar);

            if (!UseImageLinks || useText)
            {
                writer.Write(text);
            }
            else
            {
                writer.WriteBeginTag("img");
                writer.WriteAttribute("border", "0");
                writer.WriteAttribute("src", text);
                if (!string.IsNullOrEmpty(altText))
                    writer.WriteAttribute("alt", altText);
                writer.Write(HtmlTextWriter.SelfClosingTagEnd);
            }
            writer.WriteEndTag("a");
        }

        private void CreateHref(System.Web.UI.HtmlTextWriter writer, string reqstate)
        {
            if (OperationMode == OperationMode.Postback)
            {
                writer.WriteAttribute("href",
                    Page.ClientScript.GetPostBackClientHyperlink(this, reqstate));
            }
            else
            {
                Url tempUrl = new Url(Url.ToString());
                tempUrl.PreserveParameters = PreserveParameterType.Get;
                tempUrl.Type = UrlType.None;
                tempUrl.RemoveParameters = ParameterName + "Date";
                tempUrl.Parameters.Add(ParameterName + "Date", reqstate);
                writer.WriteAttribute("href", tempUrl.ToString());
            }
        }

        /// <summary>
        /// Raises the Init event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        /// <exception cref="NotSupportedException">Thrown when a CalendarNavigator is placed not inside <see cref="Calendar"/> control.</exception>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!(OwnerForm is Calendar || DesignMode))
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("CalendarNavigator can be placed only inside Calendar component.\n{0}", Environment.StackTrace);
                throw new NotSupportedException("CalendarNavigator can be placed only inside Calendar component");
            }
        }

        /// <summary>
        /// Raises events for the <see cref="MTCalendarNavigator"/> control when a form is posted back to the server.
        /// </summary>
        /// <param name="eventArgument">The string representation of the requested state.</param>
        public void RaisePostBackEvent(string eventArgument)
        {
            if (OperationMode == OperationMode.Url)
            {
                Url.PreserveParameters = PreserveParameterType.Get;
                Url.Type = UrlType.None;
                Url.RemoveParameters = String.Format("{0}Year;{0}Month;{0}Date", ParameterName);
                if (Page.Request.Form[ParameterName + "Year"] != null)
                    Url.Parameters.Add(ParameterName + "Year", Page.Request.Form[ParameterName + "Year"]);
                if (Page.Request.Form[ParameterName + "Month"] != null)
                    Url.Parameters.Add(ParameterName + "Month", Page.Request.Form[ParameterName + "Month"]);
                Page.Response.Redirect(Url.ToString());
            }
            else if (OwnerForm is Calendar)
            {
                string[] parts = null;
                if (!String.IsNullOrEmpty(eventArgument))
                    parts = eventArgument.Split('-');
                if (parts != null && parts.Length == 2)
                {
                    DateTime dummyDate;
                    if (DateTime.TryParseExact(parts[0], "yyyy", System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat, System.Globalization.DateTimeStyles.None, out dummyDate))
                        ((Calendar)OwnerForm).Year = dummyDate.Year;
                    if (DateTime.TryParseExact(parts[1], "MM", System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat, System.Globalization.DateTimeStyles.None, out dummyDate))
                        ((Calendar)OwnerForm).Month = dummyDate.Month;
                }
                else
                {
                    int val = 0;
                    if (Page.Request.Form[ParameterName + "Month"] != null && int.TryParse(Page.Request.Form[ParameterName + "Month"], out val) && val > 0 && val < 13)
                        ((Calendar)OwnerForm).Month = val;
                    if (Page.Request.Form[ParameterName + "Year"] != null && int.TryParse(Page.Request.Form[ParameterName + "Year"], out val) && val > 0 && val < 10000)
                        ((Calendar)OwnerForm).Year = val;
                }
                OwnerForm.DataBind();
            }
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            OnBeforeShow(EventArgs.Empty);
        }
    }
}