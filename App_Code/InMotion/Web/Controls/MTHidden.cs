//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InMotion.Common;
using System.Collections;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.ComponentModel;
using InMotion.Configuration;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents a hidden field used to store a non-displayed value.
    /// </summary>
    [ParseChildren(true, "Text")]
    public class MTHidden : HiddenField, IMTEditableControl, IErrorProducer, IMTAttributeAccessor
    {
        #region Events
        /// <summary>
        /// Occurs after the <see cref="MTHidden"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;
        #endregion

        #region Properties
        private SourceType _sourceType = SourceType.DatabaseColumn;
        /// <summary>
        /// Gets or sets the type of data source that will provide data for the control.
        /// </summary>
        public SourceType SourceType
        {
            get
            {
                return _sourceType;
            }
            set { _sourceType = value; }
        }

        private string _source = String.Empty;
        /// <summary>
        /// Gets or sets the source of data for the control e.g. the name of a database column.
        /// </summary>
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        private DataType _dataType = DataType.Text;
        /// <summary>
        /// Gets or sets the DataType of the control value.
        /// </summary>
        public DataType DataType
        {
            get { return _dataType; }
            set
            {
                _dataType = value;
                if (_value != null) _value = TypeFactory.CreateTypedField(DataType, Value, Format);
            }
        }

        string _caption;
        /// <summary>
        /// Gets or sets the caption of the control that will be used in validation messages.
        /// </summary>
        public string Caption
        {
            get
            {
                if (String.IsNullOrEmpty(_caption)) _caption = ID;
                return _caption;
            }
            set { _caption = value; }
        }

        private bool _required;
        /// <summary>
        /// Gets or sets the value that indicating whether control must have non-empty value.
        /// </summary>
        public bool Required
        {
            get { return _required; }
            set { _required = value; }
        }

        private bool _Unique;
        /// <summary>
        /// Gets or sets the value that indicating whether value of control must be unique.
        /// </summary>
        public bool Unique
        {
            get { return _Unique; }
            set { _Unique = value; }
        }

        private string __ValidationMask;
        /// <summary>
        /// Gets or sets regular expression pattern for validating data entry.
        /// </summary>
        public string ValidationMask
        {
            get
            {
                if (__ValidationMask == null)
                    __ValidationMask = "";
                return __ValidationMask;
            }
            set { __ValidationMask = value; }
        }

        private string __ErrorControl;
        /// <summary>
        /// Gets or sets the ID of the control to be used for displaying error messages.
        /// </summary>
        public string ErrorControl
        {
            get
            {
                if (__ErrorControl == null)
                    __ErrorControl = "";
                return __ErrorControl;
            }
            set { __ErrorControl = value; }
        }

        private string _format = String.Empty;

        /// <summary>
        /// Gets or sets the format depending on the Data Type property in which control's value will be displayed.
        /// </summary>
        public string Format
        {
            get { return _format; }
            set { _format = value; }
        }

        private string _dBformat = string.Empty;

        /// <summary>
        /// Format that will be used to extract as well as place the control value into the database.
        /// </summary>
        public string DBFormat
        {
            get { return _dBformat; }
            set { _dBformat = value; }
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        private bool _valUpdated;

        private bool _textUpdated
        {
            get
            {
                return ViewState["_textUpdated"] != null && (bool)ViewState["_textUpdated"];
            }
            set
            {
                ViewState["_textUpdated"] = value;
            }
        }

        private object _value;
        /// <summary>
        /// Gets or sets the control value.
        /// </summary>
        /// <remarks>
        /// The assigned value will be automatically converted into one of IMTField types according to the Data Type property (MTText will be used by default).
        /// Retrieved value is guaranteed is not null IMTField object of type specified in Data Type property.
        /// </remarks>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public new object Value
        {
            get
            {
                if (_value == null) return DefaultValue;
                return _value;
            }
            set
            {
                object val = value;
                try
                {
                    val = TypeFactory.CreateTypedField(DataType, val, Format);
                    _value = val;
                    _valUpdated = true;
                }
                catch (FormatException e)
                {
                    if (OwnerForm is IErrorHandler)
                    {
                        if (String.IsNullOrEmpty(Format))
                            Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_IncorrectValue"), ControlsHelper.ReplaceResource(Caption)));
                        else
                        {
                            string _format = Format;
                            if (DataType == DataType.Date)
                                _format = ControlsHelper.GetMTFormat(_format);
                            Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_IncorrectFormat"), ControlsHelper.ReplaceResource(Caption), _format));
                        }
                        ((IErrorHandler)OwnerForm).RegisterError(this, e);
                    }
                    else
                    {
                        if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                            Trace.TraceError(e.ToString());
                        throw;
                    }
                }
            }
        }

        private object __DefaultValue;
        /// <summary>
        /// Gets or sets the default value of the control.
        /// </summary>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public object DefaultValue
        {
            get
            {
                if (__DefaultValue == null)
                    __DefaultValue = TypeFactory.CreateTypedField(DataType, null, Format);
                return __DefaultValue;
            }
            set
            {
                object val = value;
                try
                {
                    val = TypeFactory.CreateTypedField(DataType, val, Format);
                    __DefaultValue = val;
                }
                catch (FormatException e)
                {
                    if (OwnerForm is IErrorHandler)
                    {
                        if (String.IsNullOrEmpty(Format))
                            Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_IncorrectValue"), ControlsHelper.ReplaceResource(Caption)));
                        else
                        {
                            string _format = Format;
                            if (DataType == DataType.Date)
                                _format = ControlsHelper.GetMTFormat(_format);
                            Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_IncorrectFormat"), ControlsHelper.ReplaceResource(Caption), _format));
                        }
                        ((IErrorHandler)OwnerForm).RegisterError(this, e);
                    }
                    else
                    {
                        if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                            Trace.TraceError(e.ToString());
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the value indicating whether <see cref="Value"/> of control is null.
        /// </summary>
        public virtual bool IsEmpty
        {
            get { return ((IMTField)Value).IsNull; }
        }

        /// <summary>
        /// Gets or sets the string representation of <see cref="Value"/>.
        /// </summary>
        public string Text
        {
            get
            {
                return GetText(Format);
            }
            set
            {
                _textUpdated = true;
                base.Value = value;
            }
        }

        /// <summary>
        /// Gets a formatted <see cref="Value"/> representation to be placed into the database.
        /// </summary>
        public TypedValue DBValue
        {
            get
            {
                return ControlsHelper.CreateTypedValue((IMTField)this.Value, this.DataType, GetText(DBFormat), DBFormat, this.Format);
            }
        }

        private string DisplayedText;
        private string GetText(string _format)
        {
            if (DisplayedText != null)
            {
                return DisplayedText;
            }
            if (DesignMode && Value == null && String.IsNullOrEmpty(base.Value))
            {
                return ID;
            }
            if (!_textUpdated && _valUpdated)
            {
                return ((IMTField)Value).ToString(_format);
            }
            else
            {
                return base.Value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            if (!DesignMode)
            {
                object val = ControlsHelper.DataBindControl(SourceType, Source, OwnerForm, DataType, DBFormat);
                if (val != null || val is IMTField && !((IMTField)val).IsNull)
                    Value = val;
            }
            OnBeforeShow(EventArgs.Empty);
        }
        #endregion

        #region IMTAttributeAccessor Members
        /// <summary>
        /// Set value of attribute by the name.
        /// </summary>
        /// <param name="name">Name of attribute.</param>
        /// <param name="value">Value of attribute.</param>
        public void SetAttribute(String name, String value)
        {
            Attributes[name] = value;
        }

        /// <summary>
        /// Retrieve attribute value by the name.
        /// </summary>
        /// <param name="name">Name of attribute.</param>
        /// <returns>Value of attribute by the name.</returns>
        public String GetAttribute(String name)
        {
            return (String)Attributes[name];
        }

        System.Web.UI.AttributeCollection _attributes = null;
        StateBag _attributeState;
        /// <summary>
        /// Gets the collection of arbitrary attributes (for rendering only) that do not correspond to properties on the control.
        /// </summary>
        public System.Web.UI.AttributeCollection Attributes
        {
            get
            {
                if (_attributes == null)
                {
                    if (_attributeState == null)
                        _attributeState = new StateBag(true);
                    if (IsTrackingViewState)
                        ((IStateManager)_attributeState).TrackViewState();
                    _attributes = new System.Web.UI.AttributeCollection(_attributeState);
                }
                return _attributes;
            }
        }
        #endregion

        /// <summary>
        /// Saves any state that was modified after the TrackViewState method was invoked.
        /// </summary>
        /// <returns>An object that contains the current view state of the control; otherwise, if there is no view state associated with the control, a null reference (Nothing in Visual Basic).</returns>
        protected override object SaveViewState()
        {
            object view_state;
            object attr_view_state = null;

            view_state = base.SaveViewState();
            if (_attributeState != null)
                attr_view_state = ((IStateManager)_attributeState).SaveViewState();

            if (view_state == null && attr_view_state == null)
                return null;
            return new Pair(view_state, attr_view_state);
        }

        /// <summary>
        /// Restores view-state information from a previous request that was saved with the <see cref="SaveViewState"/> method.
        /// </summary>
        /// <param name="savedState">An object that represents the control state to restore.</param>
        protected override void LoadViewState(object savedState)
        {
            if (savedState == null)
            {
                base.LoadViewState(null);
                return;
            }

            Pair pair = (Pair)savedState;
            base.LoadViewState(pair.First);

            if (pair.Second != null)
            {
                if (_attributeState == null)
                {
                    _attributeState = new StateBag();
                    if (IsTrackingViewState)
                        ((IStateManager)_attributeState).TrackViewState();
                }
                ((IStateManager)_attributeState).LoadViewState(pair.Second);
                _attributes = new System.Web.UI.AttributeCollection(_attributeState);
            }
        }

        /// <summary>
        /// Raises the <see cref="Validating"/> event.
        /// </summary>
        /// <param name="e">A <see cref="ValidatingEventArgs"/> that contains event data.</param>
        protected virtual void OnValidating(ValidatingEventArgs e)
        {
            if (Validating != null)
                Validating(this, e);
        }

        /// <summary>
        /// Perform the value validation.
        /// </summary>
        /// <returns><b>true</b> if the <see cref="Value"/> is valid; otherwise <b>false</b>.</returns>
        public bool Validate()
        {
            ValidatingEventArgs args = new ValidatingEventArgs();
            if (Required && String.IsNullOrEmpty(Text))
            {
                Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_RequiredField"), ControlsHelper.ReplaceResource(Caption)));
                args.HasErrors = true;
            }
            if ((!String.IsNullOrEmpty(Text) || Required) && !ValidationHelper.ValidateMask(ValidationMask, Text))
            {
                args.HasErrors = true;
                Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_MaskValidation"), ControlsHelper.ReplaceResource(Caption)));
            }
            if (Unique && SourceType == SourceType.DatabaseColumn && Source.Length > 0 && Text.Length > 0)
            {
                if (OwnerForm is IValidator)
                    if (!((IValidator)OwnerForm).ValidateUnique(this))
                    {
                        args.HasErrors = true;
                        Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_UniqueValue"), ControlsHelper.ReplaceResource(Caption)));
                    }
            }
            OnValidating(args);
            if (args.HasErrors)
            {
                if (OwnerForm is IErrorHandler)
                    ((IErrorHandler)OwnerForm).RegisterError(this, null);
                return false;
            }
            return true;
        }

        private ControlErrorCollection _errors;
        /// <summary>
        /// Gets the Collection&lt;string&gt; which is used to collect error messages generated during the controls execution.
        /// </summary>
        public ControlErrorCollection Errors
        {
            get
            {
                if (_errors == null)
                {
                    _errors = new ControlErrorCollection();
                    _errors.ErrorAdded += new EventHandler<EventArgs>(OnErrorAdded);
                    _errors.ErrorRemoved += new EventHandler<EventArgs>(OnErrorRemoved);
                }
                return _errors;
            }
        }

        private void OnErrorAdded(object sender, EventArgs e)
        {
            if (OwnerForm is IErrorHandler)
                ((IErrorHandler)OwnerForm).RegisterError(this, null);
        }

        private void OnErrorRemoved(object sender, EventArgs e)
        {
            if (Errors.Count == 0 && OwnerForm is IErrorHandler)
                ((IErrorHandler)OwnerForm).RemoveError(this);
        }

        /// <summary>
        /// Occurs after the control performed the value validation.
        /// </summary>
        public event EventHandler<ValidatingEventArgs> Validating;

        private void OwnerForm_Validating(object sender, ValidatingEventArgs e)
        {
            e.HasErrors = !Validate();
        }

        /// <summary>
        /// Raises the Init event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (OwnerForm is IValidator)
                (OwnerForm as IValidator).ControlsValidating += new EventHandler<ValidatingEventArgs>(OwnerForm_Validating);
        }

        /// <summary>
        /// Raises the Load event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (Page.IsPostBack && Page.Request.Form[UniqueID] == null)
                _IsValueNotPassed = true;
            if (!_valUpdated)
            {
                if (Page.Request.QueryString[ID] != null && !Page.IsPostBack)
                    Value = Page.Request.QueryString[ID];
                if (ID != UniqueID && Page.Request.Form[ID] != null)
                {
                    Value = Page.Request.Form[ID];
                    base.Value = Page.Request.Form[ID];
                }
                string txt = Page.IsPostBack ? base.Value : Text;
                if (txt.Length > 0)
                {
                    if (Page.IsPostBack || Text.IndexOf("{0}") == -1) Value = txt; else DisplayedText = txt;
                    _textUpdated = Errors.Count != 0;
                }
            }
        }

        /// <summary>
        /// This method support framework infrastructure and is not 
        /// intended to be used directly from your code. 
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnValueChanged(EventArgs e)
        {
            base.OnValueChanged(e);
            _IsValueChanged = true;
        }

        private bool _IsValueChanged;
        /// <summary>
        /// Gets or sets the value indicating whether value of control changed by the user.
        /// </summary>
        public bool IsValueChanged
        {
            get { return _IsValueChanged; }
        }

        /// <summary>
        /// Gets the value of the control as a <see cref="MTFloat"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTFloat GetFloat()
        {
            return (MTFloat)TypeFactory.CreateTypedField(DataType.Float, Value, "");
        }

        /// <summary>
        /// Gets the value of the control as a <see cref="MTInteger"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTInteger GetInteger()
        {
            return (MTInteger)TypeFactory.CreateTypedField(DataType.Integer, Value, "");
        }

        /// <summary>
        /// Gets the value of the control as a <see cref="MTSingle"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTSingle GetSingle()
        {
            return (MTSingle)TypeFactory.CreateTypedField(DataType.Single, Value, "");
        }

        /// <summary>
        /// Gets the value of the control as a <see cref="MTBoolean"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTBoolean GetBoolean()
        {
            return (MTBoolean)TypeFactory.CreateTypedField(DataType.Boolean, Value, "");
        }

        /// <summary>
        /// Gets the value of the control as a <see cref="MTDate"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTDate GetDate()
        {
            return (MTDate)TypeFactory.CreateTypedField(DataType.Date, Value, "");
        }

        /// <summary>
        /// Gets the value of the control as a <see cref="MTText"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTText GetText()
        {
            return (MTText)TypeFactory.CreateTypedField(DataType.Text, Value, "");
        }

        /// <summary>
        /// Gets the value of the control as a <see cref="MTMemo"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTMemo GetMemo()
        {
            return (MTMemo)TypeFactory.CreateTypedField(DataType.Memo, Value, "");
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (OwnerForm is IClientScriptHelper && Visible)
                (OwnerForm as IClientScriptHelper).RegisterClientControl(this);
            if (!Page.IsPostBack || _valUpdated)
            {
                IMTField val = (IMTField)Value;
                if (!_textUpdated)
                {
                    if (Text != Value.ToString()) DisplayedText = Text;
                    if (String.IsNullOrEmpty(DisplayedText)) DisplayedText = "{0}";
                    DisplayedText = DisplayedText.Replace("{0}", val.ToString(Format));
                }
                base.Value = Text;
            }
            if (NamingContainer is RepeaterItem && !string.IsNullOrEmpty(Attributes["data-id"]))
                Attributes["data-id"] = Regex.Replace(Attributes["data-id"], @"\{\w+:rowNumber\}", (((RepeaterItem)NamingContainer).ItemIndex + 1).ToString());
        }

        protected override void Render(HtmlTextWriter writer)
        {
            System.Web.UI.AttributeCollection atrColl = Attributes;
            IEnumerator keys = atrColl.Keys.GetEnumerator();
            while (keys.MoveNext())
            {
                string attrName = (string) (keys.Current);
                writer.AddAttribute(attrName, atrColl[attrName]);
            }
            base.Render(writer);
        }

        private bool _IsValueNotPassed;
        /// <summary>
        /// Indicates whethere value is empty, i.e. not passed by the client request.
        /// </summary>
        public bool IsValueNotPassed
        {
            get { return _IsValueNotPassed; }
            set { _IsValueNotPassed = value; }
        }

        private bool _IsOmitEmptyValue;
        /// <summary>
        /// Indicates whethere control value will be excluded from data update operation when <see cref="IsValueNotPassed"/> is true;
        /// </summary>
        public bool IsOmitEmptyValue
        {
            get
            {
                return _IsOmitEmptyValue;
            }
            set
            {
                _IsOmitEmptyValue = value;
            }
        }
    }
}