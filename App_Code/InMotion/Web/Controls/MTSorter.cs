//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.ComponentModel;
using System.Drawing.Design;
using InMotion.Configuration;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Specify the display style for <see cref="MTSorter"/>
    /// </summary>
    public enum SorterStyle
    {
        /// <summary>
        /// The <see cref="MTSorter"/> will display as link.
        /// </summary>
        Link,
        /// <summary>
        /// The <see cref="MTSorter"/> will display as combination
        /// of the link and images.
        /// </summary>
        LinkAndImages,
        /// <summary>
        /// The <see cref="MTSorter"/> will display as combination
        /// of the text and clickable images.
        /// </summary>
        Images
    }

    /// <summary>
    /// Specify the state of the <see cref="MTSorter"/>
    /// </summary>
    public enum SorterState
    {
        /// <summary>
        /// The <see cref="MTSorter"/> is inactive.
        /// </summary>
        Inactive,
        /// <summary>
        /// The <see cref="MTSorter"/> is in ascending mode.
        /// </summary>
        Ascending,
        /// <summary>
        /// The <see cref="MTSorter"/> is in descending mode.
        /// </summary>
        Descending
    }

    /// <summary>
    /// Displays a sorter control for manage state of sortable forms.
    /// </summary>
    public class MTSorter : WebControl, IPostBackEventHandler, IMTControl, IStateUrlParameterProvider, IMTAttributeAccessor
    {
        /// <summary>
        /// Occurs after the <see cref="MTSorter"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }
        private SorterStyle __DisplayStyle = SorterStyle.Link;
        /// <summary>
        /// Gets or sets the display style of the <see cref="MTSorter"/> control.
        /// </summary>
        /// <value>
        /// One of the <see cref="SorterStyle"/> enumeration. 
        /// The default is [b]Link[/b]
        /// </value>
        public SorterStyle DisplayStyle
        {
            get
            {
                return __DisplayStyle;
            }
            set { __DisplayStyle = value; }
        }

        /// <summary>
        /// Gets or sets the curent state of the <see cref="MTSorter"/> control.
        /// </summary>
        /// <value>
        /// One of the <see cref="SorterState"/> enumeration. 
        /// The default is [b]Inactive[/b]
        /// </value>
        public SorterState State
        {
            get
            {
                if (ViewState["__State"] == null)
                    ViewState["__State"] = (int)SorterState.Inactive;
                return (SorterState)ViewState["__State"];
            }
            set { ViewState["__State"] = (int)value; }
        }
        private OperationMode __OperationMode = OperationMode.Url;
        /// <summary>
        /// Gets or sets the operation mode of the <see cref="MTSorter"/> control.
        /// </summary>
        /// <value>
        /// One of the <see cref="OperationMode"/> enumeration. 
        /// The default is [b]Url[/b]
        /// </value>

        public OperationMode OperationMode
        {
            get
            {
                return __OperationMode;
            }
            set { __OperationMode = value; }
        }
        private string __ParameterName;
        /// <summary>
        /// Gets or sets the name of query string parameter which will
        /// be used for Url operation mode.
        /// </summary>
        /// <value>
        /// The string. The default is owner form (if any) ID, or control ID.
        /// </value>
        public string ParameterName
        {
            get
            {
                if (__ParameterName == null && OwnerForm != null)
                    __ParameterName = OwnerForm.ID;
                else if (__ParameterName == null && OwnerForm == null)
                    __ParameterName = ID;
                return __ParameterName;
            }
            set { __ParameterName = value; }
        }

        private Url _url = new Url("");
        /// <summary>
        /// Gets or sets the url which will be used as target page 
        /// for Url operation mode.
        /// </summary>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public Url Url
        {
            get { return _url; }
            set { _url = value; }
        }

        private string __SortOrder;
        /// <summary>
        /// Gets or sets the sorting order value.
        /// </summary>

        public string SortOrder
        {
            get
            {
                if (__SortOrder == null)
                    __SortOrder = "";

                return (string)__SortOrder;
            }
            set { __SortOrder = value; }
        }

        private string __ReverseOrder;
        /// <summary>
        /// Gets or sets the reverse sorting order value.
        /// </summary>
        public string ReverseOrder
        {
            get
            {
                if (String.IsNullOrEmpty(__ReverseOrder))
                    __ReverseOrder = SortOrder + " desc";

                return (string)__ReverseOrder;
            }
            set { __ReverseOrder = value; }
        }


        private string __AscOnImage;
        /// <summary>
        /// Gets or sets the path to the image, that represents a active ascending state.
        /// </summary>
        [EditorAttribute(typeof(System.Web.UI.Design.ImageUrlEditor), typeof(UITypeEditor))]
        public string AscOnImage
        {
            get
            {
                if (__AscOnImage == null)
                    __AscOnImage = "";
                return (string)__AscOnImage;
            }
            set { __AscOnImage = value; }
        }

        private string __AscOffImage;
        /// <summary>
        /// Gets or sets the path to the image, that represents a inactive ascending state.
        /// </summary>
        public string AscOffImage
        {

            get
            {
                if (__AscOffImage == null)
                    __AscOffImage = "";
                return (string)__AscOffImage;
            }
            set { __AscOffImage = value; }
        }

        private string __DescOnImage;
        /// <summary>
        /// Gets or sets the path to the image, that represents a active descending state.
        /// </summary>
        public string DescOnImage
        {
            get
            {
                if (__DescOnImage == null)
                    __DescOnImage = "";
                return (string)__DescOnImage;
            }
            set { __DescOnImage = value; }
        }

        private string __DesOffImage;
        /// <summary>
        /// Gets or sets the path to the image, that represents a inactive descending state.
        /// </summary>
        public string DescOffImage
        {
            get
            {
                if (__DesOffImage == null)
                    __DesOffImage = "";
                return (string)__DesOffImage;
            }
            set { __DesOffImage = value; }
        }
        private string __AscOnAlt;
        /// <summary>
        /// Gets or sets the alt attribute of the image, that represents a active ascending state.
        /// </summary>
        public string AscOnAlt
        {
            get
            {
                if (__AscOnAlt == null)
                    __AscOnAlt = "";
                return (string)__AscOnAlt;
            }
            set { __AscOnAlt = value; }
        }

        private string __AscOffAlt;
        /// <summary>
        /// Gets or sets the alt attribute of the image, that represents a inactive ascending state.
        /// </summary>
        public string AscOffAlt
        {

            get
            {
                if (__AscOffAlt == null)
                    __AscOffAlt = "";
                return (string)__AscOffAlt;
            }
            set { __AscOffAlt = value; }
        }

        private string __DescOnAlt;
        /// <summary>
        /// Gets or sets the alt attribute of the image, that represents a active descending state.
        /// </summary>
        public string DescOnAlt
        {
            get
            {
                if (__DescOnAlt == null)
                    __DescOnAlt = "";
                return (string)__DescOnAlt;
            }
            set { __DescOnAlt = value; }
        }

        private string __DesOffAlt;
        /// <summary>
        /// Gets or sets the alt attribute of the image, that represents a inactive descending state.
        /// </summary>
        public string DescOffAlt
        {
            get
            {
                if (__DesOffAlt == null)
                    __DesOffAlt = "";
                return (string)__DesOffAlt;
            }
            set { __DesOffAlt = value; }
        }
        private string __Text;
        /// <summary>
        /// Gets or sets the text which will displayed in <see cref="MTSorter"/> 
        /// </summary>
        public string Text
        {
            get { return __Text; }
            set { __Text = value; }
        }

        private string __SpanClassName;

        public string SpanClassName
        {
            get { return __SpanClassName; }
            set { __SpanClassName = value; }
        }

        /// <summary>
        /// Renders the <see cref="MTSorter"/> control to the specified HtmlTextWriter object. 
        /// </summary>
        /// <param name="writer">
        /// The <see cref="HtmlTextWriter"/> that receives the rendered output.
        /// </param>
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            if (!Visible) return;
            AddAttributesToRender(writer);
            if (!string.IsNullOrEmpty(__SpanClassName))
                writer.AddAttribute(HtmlTextWriterAttribute.Class, __SpanClassName);
            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            if (DesignMode)
            {
                switch (DisplayStyle)
                {
                    case SorterStyle.Link:
                        WriteLink(writer);
                        break;
                    case SorterStyle.Images:
                    case SorterStyle.LinkAndImages:
                        WriteLink(writer);
                        writer.WriteBeginTag("img");
                        writer.WriteAttribute("border", "0");
                        writer.WriteAttribute("src", ControlsHelper.ReplaceResourcesAndStyles(AscOnImage, Page, this));
                        if (AppConfig.HTMLTemplateType == "xHTML" || AscOnAlt != "")
                            writer.WriteAttribute("alt", AscOnAlt);
                        writer.Write(HtmlTextWriter.SelfClosingTagEnd);

                        writer.WriteBeginTag("img");
                        writer.WriteAttribute("border", "0");
                        writer.WriteAttribute("src", ControlsHelper.ReplaceResourcesAndStyles(DescOnImage, Page, this));
                        if (AppConfig.HTMLTemplateType == "xHTML" || DescOnAlt != "")
                            writer.WriteAttribute("alt", DescOnAlt);
                        writer.Write(HtmlTextWriter.SelfClosingTagEnd);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (DisplayStyle)
                {
                    case SorterStyle.Link:
                        WriteLink(writer);
                        break;
                    case SorterStyle.LinkAndImages:
                        WriteLink(writer);
                        if (State == SorterState.Ascending)
                        {
                            writer.WriteBeginTag("img");
                            writer.WriteAttribute("border", "0");
                            writer.WriteAttribute("src", ControlsHelper.ReplaceResourcesAndStyles(AscOnImage, Page, this));
                            if (AppConfig.HTMLTemplateType == "xHTML" || AscOnAlt != "")
                                writer.WriteAttribute("alt", AscOnAlt);
                            writer.Write(HtmlTextWriter.SelfClosingTagEnd);
                        }
                        if (State == SorterState.Descending)
                        {
                            writer.WriteBeginTag("img");
                            writer.WriteAttribute("border", "0");
                            writer.WriteAttribute("src", ControlsHelper.ReplaceResourcesAndStyles(DescOnImage, Page, this));
                            if (AppConfig.HTMLTemplateType == "xHTML" || DescOnAlt != "")
                                writer.WriteAttribute("alt", DescOnAlt);
							writer.Write(HtmlTextWriter.SelfClosingTagEnd);
                        }
                        break;
                    case SorterStyle.Images:
                        writer.Write(ControlsHelper.ReplaceResourcesAndStyles(Text, Page, this));
                        if (State != SorterState.Ascending)
                        {
                            writer.WriteBeginTag("a");
                            CreateHref(writer, "Asc");
                            writer.Write(HtmlTextWriter.TagRightChar);
                        }
                        WriteAscImage(writer);
                        if (State != SorterState.Ascending)
                        {
                            writer.WriteEndTag("a");
                        }
                        if (State != SorterState.Descending)
                        {
                            writer.WriteBeginTag("a");
                            CreateHref(writer, "Desc");
                            writer.Write(HtmlTextWriter.TagRightChar);
                        }
                        WriteDescImage(writer);
                        if (State != SorterState.Descending)
                        {
                            writer.WriteEndTag("a");
                        }
                        break;
                    default:
                        break;
                }
            }
            writer.RenderEndTag();
        }

        private void WriteDescImage(System.Web.UI.HtmlTextWriter writer)
        {
            writer.WriteBeginTag("img");
            writer.WriteAttribute("border", "0");
            if (State == SorterState.Inactive || State == SorterState.Ascending)
            {
                writer.WriteAttribute("src", ControlsHelper.ReplaceResourcesAndStyles(DescOffImage, Page, this));
                if (AppConfig.HTMLTemplateType == "xHTML" || DescOffAlt != "")
                    writer.WriteAttribute("alt", DescOffAlt);
            }
            else
            {
                writer.WriteAttribute("src", ControlsHelper.ReplaceResourcesAndStyles(DescOnImage, Page, this));
                if (AppConfig.HTMLTemplateType == "xHTML" || DescOnAlt != "")
                    writer.WriteAttribute("alt", DescOnAlt);
            }
            writer.Write(HtmlTextWriter.SelfClosingTagEnd);
        }

        private void WriteAscImage(System.Web.UI.HtmlTextWriter writer)
        {
            writer.WriteBeginTag("img");
            writer.WriteAttribute("border", "0");
            if (State == SorterState.Inactive || State == SorterState.Descending)
            {
                writer.WriteAttribute("src", ControlsHelper.ReplaceResourcesAndStyles(AscOffImage, Page, this));
                if (AppConfig.HTMLTemplateType == "xHTML" || AscOffAlt != "")
                    writer.WriteAttribute("alt", AscOffAlt);
            }
            else
            {
                writer.WriteAttribute("src", ControlsHelper.ReplaceResourcesAndStyles(AscOnImage, Page, this));
                if (AppConfig.HTMLTemplateType == "xHTML" || AscOnAlt != "")
                    writer.WriteAttribute("alt", AscOnAlt);
            }
            writer.Write(HtmlTextWriter.SelfClosingTagEnd);
        }

        private void WriteLink(System.Web.UI.HtmlTextWriter writer)
        {
            writer.WriteBeginTag("a");
            string reqstate;
            if (State == SorterState.Inactive || State == SorterState.Descending)
                reqstate = "Asc";
            else
                reqstate = "Desc";

            CreateHref(writer, reqstate);
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write(ControlsHelper.ReplaceResourcesAndStyles(Text, Page, this));
            writer.WriteEndTag("a");
        }

        private void CreateHref(System.Web.UI.HtmlTextWriter writer, string reqstate)
        {
            if (OperationMode == OperationMode.Postback)
            {
                writer.WriteAttribute("href",
                    Page.ClientScript.GetPostBackClientHyperlink(this, reqstate));
            }
            else
            {
                Url u = new Url(Url.Address, Url.RemoveParameters, PreserveParameterType.Get, Url.Type);
                if (u.RemoveParameters.Length > 0) u.RemoveParameters += ";";
                u.RemoveParameters += ParameterName + "Page";
                u.Parameters.Add(ParameterName + "Dir", reqstate);
                u.Parameters.Add(ParameterName + "Order", ID);
                if (!DesignMode)
                    writer.WriteAttribute("href", u.ToString(true));
            }
        }


        #region IPostBackEventHandler Members
        /// <summary>
        /// Raises events for the <see cref="MTSorter"/> control when a form is posted back to the server.
        /// </summary>
        /// <param name="eventArgument">The string representation of the requested state.</param>
        public void RaisePostBackEvent(string eventArgument)
        {

            if (State == SorterState.Inactive)
                State = SorterState.Ascending;
            else if (State == SorterState.Ascending)
                State = SorterState.Descending;
            else if (State == SorterState.Descending)
                State = SorterState.Ascending;
            if (String.Compare(eventArgument, "desc", StringComparison.OrdinalIgnoreCase) == 0)
                State = SorterState.Descending;
            if (String.Compare(eventArgument, "asc", StringComparison.OrdinalIgnoreCase) == 0)
                State = SorterState.Ascending;
            if (OwnerForm is ISortable)
            {
                if (State == SorterState.Ascending)
                    ((ISortable)OwnerForm).SortExpression = SortOrder;
                else if (State == SorterState.Descending)
                    ((ISortable)OwnerForm).SortExpression = ReverseOrder;
                if (OwnerForm is INavigable)
                    ((INavigable)OwnerForm).CurrentPage = 1;
                OwnerForm.DataBind();
            }
        }

        #endregion

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (OwnerForm is ISortable)
            {
                if (((ISortable)OwnerForm).SortExpression == SortOrder)
                    State = SorterState.Ascending;
                else if (((ISortable)OwnerForm).SortExpression == ReverseOrder)
                    State = SorterState.Descending;
            }
        }

        /// <summary>
        /// Raises the Init event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            if (!Page.IsPostBack && !DesignMode)
            {
                if (Page.Request.QueryString[ParameterName + "Order"] == ID)
                {
                    if (Page.Request.QueryString[ParameterName + "Dir"] != null
                        && String.Compare(Page.Request.QueryString[ParameterName + "Dir"], "desc", StringComparison.OrdinalIgnoreCase) == 0)
                        State = SorterState.Descending;
                    else
                        State = SorterState.Ascending;
                }
                else if (Page.Request.QueryString[ParameterName + "Order"] != null)
                {
                    State = SorterState.Inactive;
                }
            }
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            OnBeforeShow(EventArgs.Empty);
        }

        /// <summary>
        /// Gets the name of state parameter for remove from query string.
        /// </summary>
        public string RemoveStateParameters
        {
            get
            {
                string result = "";
                if (OperationMode == OperationMode.Postback && State != SorterState.Inactive)
                {
                    result = ParameterName + "Page";
                }
                return result;
            }
        }

        /// <summary>
        /// Gets the <see cref="UrlParameterCollection"/> of state parameters for preserving in query string.
        /// </summary>
        public UrlParameterCollection PreserveStateParameters
        {
            get
            {
                UrlParameterCollection p = new UrlParameterCollection();
                if (OperationMode == OperationMode.Postback && State != SorterState.Inactive)
                {
                    p.Add(ParameterName + "Order", ID);
                    if (State == SorterState.Descending)
                        p.Add(ParameterName + "Dir", "desc", true);
                    else
                        p.Add(ParameterName + "Dir", "asc", true);
                }
                return p;
            }
        }
    }
}