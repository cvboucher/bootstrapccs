//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Provides data for the ItemCreated and ItemDataBound events of a <see cref="Directory"/>.
    /// </summary>
    public sealed class DirectoryItemEventArgs : EventArgs
    {

        private DirectoryItem item;
        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryItemEventArgs"/> class.
        /// </summary>
        /// <param name="item">The <see cref="DirectoryItem"/> associated with the event. The Item property is set to this value.</param>
        public DirectoryItemEventArgs(DirectoryItem item)
        {
            this.item = item;
        }
        /// <summary>
        /// Gets the <see cref="DirectoryItem"/> associated with the event.
        /// </summary>
        public DirectoryItem Item
        {
            get
            {
                return item;
            }
        }
    }
}
