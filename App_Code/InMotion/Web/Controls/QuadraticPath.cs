using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Summary description for QuadraticPath
    /// </summary>
    class QuadraticPath
    {
        private int _x1;
        private int _y1;
        private int _x2;
        private int _y2;
        private int _x3;
        private int _y3;
        public int X1
        {
            get { return _x1; }
            set { _x1 = value; }
        }
        public int Y1
        {
            get { return _y1; }
            set { _y1 = value; }
        }
        public int X2
        {
            get { return _x2; }
            set { _x2 = value; }
        }
        public int Y2
        {
            get { return _y2; }
            set { _y2 = value; }
        }
        public int X3
        {
            get { return _x3; }
            set { _x3 = value; }
        }
        public int Y3
        {
            get { return _y3; }
            set { _y3 = value; }
        }
        public QuadraticPath(int x1, int y1, int x2, int y2, int x3, int y3)
        {
            _x1 = x1;
            _y1 = y1;
            _x2 = x2;
            _y2 = y2;
            _x3 = x3;
            _y3 = y3;
        }
        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3},{4},{5}", new object[] { _x1, _y1, _x2, _y2, _x3, _y3 });
        }
    }
}
