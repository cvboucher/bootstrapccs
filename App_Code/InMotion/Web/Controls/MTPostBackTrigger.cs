//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Web.UI;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Defines a control inside a <see cref="MTUpdatePanel"/> control as a postback control.
    /// </summary>
    public class MTPostBackTrigger : PostBackTrigger
    {
        #region Properties
        private string _formID;
        /// <summary>
        /// Gets or sets the name of the control form that is an <see cref="MTPostBackTrigger"/> control for an <see cref="MTUpdatePanel"/> control.
        /// </summary>
        public string FormID
        {
            get
            {
                return _formID;
            }
            set
            {
                _formID = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Initializes the <see cref="MTPostBackTrigger"/> object.
        /// </summary>
        protected override void Initialize()
        {
            if (string.IsNullOrEmpty(FormID))
            {
                Control ctrl = Owner.FindControl(ControlID);
                if (ctrl == null)
                    ctrl = Owner.Page.FindControl(ControlID);
                if (ctrl != null)
                    base.Initialize();
            }
            else
            {
                Control ctrl = Owner.Page.FindControl(FormID);
                if (ctrl != null)
                {
                    ControlCollection CtrlCollect = ctrl.Controls;
                    for (int i = 0; i < CtrlCollect.Count; i++)
                    {
                        ctrl = CtrlCollect[i].FindControl(ControlID);
                        if (ctrl != null)
                            base.Initialize();
                    }
                }
            }
        }
        #endregion
    }
}