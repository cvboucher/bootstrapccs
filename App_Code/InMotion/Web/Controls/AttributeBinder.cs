//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using InMotion.Web;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Binding control attributes.
    /// </summary>
    public class AttributeBinder : Control
    {
        private string _name = string.Empty;
        /// <summary>
        /// Name of the control attribute.
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        private string _controlID = null;
        /// <summary>
        /// Gets or sets control ID.
        /// </summary>
        public string ControlID
        {
            get
            {
                if (string.IsNullOrEmpty(_controlID))
                    return Page.ClientID;
                return _controlID;
            }
            set
            {
                _controlID = value;
            }
        }

        private string _containerID = null;
        /// <summary>
        /// Gets or sets container ID of the control.
        /// </summary>
        public string ContainerID
        {
            get
            {
                return _containerID;
            }
            set
            {
                _containerID = value;
            }
        }

        /// <summary>
        /// Renders the attribute control to the specified HTML writer.
        /// </summary>
        /// <param name="writer">The HtmlTextWriter object that receives the control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            if (!string.IsNullOrEmpty(Name))
            {
                Control ctrl = null;
                if (string.IsNullOrEmpty(ControlID))
                {
                    if (Page is MTPage)
                    {
                        writer.WriteLine(((MTPage)Page).Attributes[Name]);
                        return;
                    }
                }
                else if (string.IsNullOrEmpty(ContainerID))
                {
                    ctrl = Page.FindControl(ControlID);
                }
                else
                {
                    Control container = Page.FindControl(ContainerID);
                    if (container == null)
                        throw new Exception("The specified control can not be finded.");
                    else if (container is IForm)
                        ctrl = ((IForm)container).GetControl(ControlID);
                    else
                        ctrl = container.FindControl(ControlID);
                }

                if (ctrl == null)
                    throw new Exception("The specified control can not be finded.");
                else if (ctrl is WebControl)
                    writer.WriteLine(((WebControl)ctrl).Attributes[Name]);
                else if (!(ctrl is IMTAttributeAccessor))
                    throw new Exception("The specified control does not provide attributes.");
                else if (ctrl is Repeater)
                    writer.WriteLine(((IMTAttributeAccessor)ctrl).Attributes[((System.Web.UI.Control)(this)).BindingContainer.UniqueID + "$" + Name]);
                else
                    writer.WriteLine(((IMTAttributeAccessor)ctrl).Attributes[Name]);
            }
        }
    }
}