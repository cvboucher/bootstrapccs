//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System.Web.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Defines the interface to all InMotion controls that can work as container for other controls.
    /// </summary>
    public interface IMTControlContainer : IMTControl
    {
        /// <summary>
        /// Find all child controls of type <i>T</i>.
        /// </summary>
        /// <typeparam name="T">The type of controls to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <returns>the <see cref="ReadOnlyCollection&lt;T&gt;"/></returns>
        ReadOnlyCollection<T> GetControls<T>() where T : IMTControl;
        /// <summary>
        /// Find a control of type <i>T</i> with specified <see cref="Control.ID"/>
        /// </summary>
        /// <typeparam name="T">The type of control to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>T</i>.</returns>
        T GetControl<T>(string id) where T : Control;
        /// <summary>
        /// Find a control with specified <see cref="Control.ID"/>
        /// </summary>
        /// <param name="id">The ID of the control to find.</param>
        /// <returns>The instance of the <i>Control</i> if any; otherwise the <b>null</b> reference.</returns>
        Control GetControl(string id);
    }
}