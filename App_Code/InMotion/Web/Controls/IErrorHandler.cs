//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Defines a service implemented by objects to register and handle errors.
    /// </summary>
    public interface IErrorHandler
    {
        /// <summary>
        /// Performs an error registration.
        /// </summary>
        /// <param name="control">The <see cref="IErrorProducer"/> of error.</param>
        /// <param name="innerException">The initial exception, if any.</param>
        void RegisterError(IErrorProducer control, Exception innerException);

        /// <summary>
        /// Remove an error from a collection.
        /// </summary>
        /// <param name="control">The <see cref="IErrorProducer"/> which will be removed from collection.</param>
        void RemoveError(IErrorProducer control);
    }
}
