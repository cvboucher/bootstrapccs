//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using InMotion.Common;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Defines the interface to a InMotion control that have a value.
    /// </summary>
    public interface IMTControlWithValue : IMTControl
    {
        /// <summary>
        /// Gets or sets the string representation of <see cref="Value"/>.
        /// </summary>
        string Text { get;set; }
        /// <summary>
        /// Gets or sets the <see cref="DataType"/> of <see cref="Value"/>.
        /// </summary>
        DataType DataType { get;set; }
        /// <summary>
        /// Gets or sets the value of control.
        /// </summary>
        object Value { get;set; }
        /// <summary>
        /// Gets or sets the <see cref="SourceType"/> of <see cref="Value"/>.
        /// </summary>
        SourceType SourceType { get;set;}
        /// <summary>
        /// Gets or sets the name of <see cref="Value"/> source.
        /// </summary>
        string Source { get;set; }
        /// <summary>
        /// Gets or sets the format string that will be used for format <see cref="Value"/> of control.
        /// </summary>
        string Format { get;set; }

        /// <summary>
        /// Gets the value indicating whether <see cref="Value"/> is null.
        /// </summary>
        bool IsEmpty { get; }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTFloat"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        MTFloat GetFloat();
        /// <summary>
        /// Gets the value of the control as a <see cref="MTInteger"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        MTInteger GetInteger();
        /// <summary>
        /// Gets the value of the control as a <see cref="MTSingle"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        MTSingle GetSingle();
        /// <summary>
        /// Gets the value of the control as a <see cref="MTBoolean"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        MTBoolean GetBoolean();
        /// <summary>
        /// Gets the value of the control as a <see cref="MTDate"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        MTDate GetDate();
        /// <summary>
        /// Gets the value of the control as a <see cref="MTText"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        MTText GetText();
        /// <summary>
        /// Gets the value of the control as a <see cref="MTMemo"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        MTMemo GetMemo();

    }


}
