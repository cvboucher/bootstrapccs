//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InMotion.Common;
using System.Collections;
using System.Globalization;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Displays a hyperlink-style button control on a Web page.
    /// </summary>
    public class MTLinkButton : LinkButton, IMTButtonControl, IMTAttributeAccessor
    {
        #region IMTControl Members

        /// <summary>
        /// Occurs after the <see cref="MTLinkButton"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        #endregion
        private Url __RedirectUrl = new Url("");
        /// <summary>
        /// Gets or sets the url to which the user is directed to after the form has been submitted successfully.
        /// </summary>
        public Url RedirectUrl
        {
            get
            {
                return __RedirectUrl;
            }
            set { __RedirectUrl = value; }
        }

        private bool _PerformRedirect = true;
        /// <summary>
        /// Gets or sets the value indicating whether form will perform redirect after the successfull submit.
        /// </summary>
        public bool PerformRedirect
        {
            get { return _PerformRedirect; }
            set { _PerformRedirect = value; }
        }

        /// <summary>
        /// Gets or sets the page name to which the user is directed to after the form has been submitted successfully.
        /// </summary>
        public string ReturnPage
        {
            get
            {
                return RedirectUrl.ToString();
            }
            set
            {
                RedirectUrl.Reset();
                RedirectUrl.Address = value;
            }
        }

        /// <summary>
        /// Gets or sets a semicolon-separated list of parameters that should be removed from the hyperlink.
        /// </summary>
        public string RemoveParameters
        {
            get
            {
                return RedirectUrl.RemoveParameters;
            }
            set { RedirectUrl.RemoveParameters = value; }
        }

        /// <summary>
        /// Gets or sets whether URLs should be automatically converted to absolute URLs or secure URLs for the SSL protocol (https://).
        /// </summary>
        public UrlType ConvertUrl
        {
            get
            {
                return RedirectUrl.Type;
            }
            set { RedirectUrl.Type = value; }
        }

        private bool __EnableValidation = true;
        /// <summary>
        /// Gets or sets the value indicating whether form should validate user input when this button is pressed.
        /// </summary>
        public bool EnableValidation
        {
            get
            {
                return __EnableValidation;
            }
            set { __EnableValidation = value; }
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            if (OwnerForm != null && OwnerForm is Record)
            {
                Record r = (Record)OwnerForm;
                if (String.Compare(CommandName, "insert", StringComparison.OrdinalIgnoreCase) == 0 && !r.IsInsertMode)
                    Visible = false;
                if ((String.Compare(CommandName, "update", StringComparison.OrdinalIgnoreCase) == 0 || String.Compare(CommandName, "delete", StringComparison.OrdinalIgnoreCase) == 0) && r.IsInsertMode)
                    Visible = false;
            }
            OnBeforeShow(EventArgs.Empty);
        }
    }
}
