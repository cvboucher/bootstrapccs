//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Collections;
using System.Web.UI.WebControls;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Specifies the type of an item in a <see cref="Directory"/> control.
    /// </summary>
    public enum DirectoryItemType
    {
        /// <summary>
        /// A header for the <see cref="Directory"/> control.
        /// </summary>
        Header,
        /// <summary>
        /// A footer for the <see cref="Directory"/> control.
        /// </summary>
        Footer,
        /// <summary>
        /// A category header for the <see cref="Directory"/> control.
        /// </summary>
        CategoryHeader,
        /// <summary>
        /// A category footer for the <see cref="Directory"/> control.
        /// </summary>
        CategoryFooter,
        /// <summary>
        /// A category separator for the <see cref="Directory"/> control.
        /// </summary>
        CategorySeparator,
        /// <summary>
        /// A subcategory for the <see cref="Directory"/> control.
        /// </summary>
        Subcategory,
        /// <summary>
        /// A subcategory separator for the <see cref="Directory"/> control.
        /// </summary>
        SubcategorySeparator,
        /// <summary>
        /// A subcategory tail for the <see cref="Directory"/> control.
        /// </summary>
        SubcategoriesTail,
        /// <summary>
        /// A column separator for the <see cref="Directory"/> control.
        /// </summary>
        ColumnSeparator,
        /// <summary>
        /// A No records item for the <see cref="Directory"/> control.
        /// </summary>
        NoRecords 
    }
    /// <summary>
    /// Represents an item in the <see cref="Directory"/> control.
    /// </summary>
    public class DirectoryItem : Control, INamingContainer
    {
        private int itemIndex;
        private DirectoryItemType itemType;
        private object dataItem;
        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryItem"/> class.
        /// </summary>
        /// <param name="itemIndex">The index of the item in the <see cref="Directory"/> control.</param>
        /// <param name="itemType">One of the <see cref="DirectoryItemType"/> values.</param>
        public DirectoryItem(int itemIndex, DirectoryItemType itemType)
        {
            this.itemIndex = itemIndex;
            this.itemType = itemType;
        }

        /// <summary>
        /// Gets or sets a data item associated with the <see cref="DirectoryItem"/> object in the <see cref="Directory"/> control.
        /// </summary>
        public virtual object DataItem
        {
            get
            {
                return dataItem;
            }
            set
            {
                dataItem = value;
            }
        }
        /// <summary>
        /// Gets the index of the item in the <see cref="Directory"/> control.
        /// </summary>
        public virtual int ItemIndex
        {
            get
            {
                return itemIndex;
            }
        }

        /// <summary>
        /// Gets the type of the item in the Directory control.
        /// </summary>
        public virtual DirectoryItemType ItemType
        {
            get
            {
                return itemType;
            }
        }

        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code. 
        /// Assigns any sources of the event and its information to the parent <see cref="Directory"/> control, if the EventArgs parameter is an instance of <see cref="DirectoryCommandEventArgs"/>.
        /// </summary>
        /// <param name="source">The source of the event. </param>
        /// <param name="args">An <see cref="EventArgs"/> that contains the event data.</param>
        /// <returns><b>true</b> if the event assigned to the parent was raised, otherwise <b>false</b>.</returns>
        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            if (args is CommandEventArgs)
            {
                DirectoryCommandEventArgs dargs =
                    new DirectoryCommandEventArgs(this, source, (CommandEventArgs)args);

                RaiseBubbleEvent(this, dargs);
                return true;
            }
            return false;
        }

        internal void SetItemType(DirectoryItemType itemType)
        {
            this.itemType = itemType;
        }
    }

}
