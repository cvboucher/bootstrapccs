//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Defines the interface to a editable InMotion control wich can contain multiple values, such as <see cref="MTListBox"/> or <see cref="MTCheckBoxList"/>.
    /// </summary>
    public interface IMTMultipleEditableControl : IMTEditableControl
    {
        /// <summary>
        /// Gets or sets the value indicating whether control contains multiple values.
        /// </summary>
        bool IsMultiple { get;}
        /// <summary>
        /// Returns a <see cref="string"/> representation of value at the specified index.
        /// </summary>
        /// <param name="index">The index of value.</param>
        /// <returns>A <see cref="string"/> representation of value at the specified index.</returns>
        string ToString(int index);
        /// <summary>
        /// Gets the array of the selected values; 
        /// </summary>
        object[] SelectedValues { get;}
    }
}
