//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace InMotion.Web.Controls
{
    internal static class ValidationHelper
    {
        public static bool ValidateMask(string mask, string text)
        {
            if (mask.Length > 0)
            {
                Regex re = new Regex(mask);
                return re.IsMatch(text);
            }
            return true;
        }
    }
}
