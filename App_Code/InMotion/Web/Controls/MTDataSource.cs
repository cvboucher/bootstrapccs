//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Web.UI;
using System.Web;
using System.ComponentModel;
using InMotion.Data;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents an Data Source for InMotion data-bound controls.
    /// </summary>
    [ParseChildren(true)]
    [PersistChildren(false)]
    public class MTDataSource : DataSourceControl
    {
        #region Events
        /// <summary>
        /// Occurs immediately before <see cref="BeforeBuildSelect"/> event.
        /// </summary>
        public event EventHandler InitializeSelectParameters
        {
            add
            {
                ((MTDataSourceView)GetView("")).InitializeSelectParameters += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).InitializeSelectParameters -= value;
            }
        }

        /// <summary>
        /// Occurs immediately before <see cref="BeforeBuildUpdate"/> event.
        /// </summary>
        public event EventHandler InitializeUpdateParameters
        {
            add
            {
                ((MTDataSourceView)GetView("")).InitializeUpdateParameters += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).InitializeUpdateParameters -= value;
            }
        }

        /// <summary>
        /// Occurs immediately before <see cref="BeforeBuildDelete"/> event.
        /// </summary>
        public event EventHandler InitializeDeleteParameters
        {
            add
            {
                ((MTDataSourceView)GetView("")).InitializeDeleteParameters += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).InitializeDeleteParameters -= value;
            }
        }

        /// <summary>
        /// Occurs immediately before <see cref="BeforeBuildInsert"/> event.
        /// </summary>
        public event EventHandler InitializeInsertParameters
        {
            add
            {
                ((MTDataSourceView)GetView("")).InitializeInsertParameters += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).InitializeInsertParameters -= value;
            }
        }

        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for select operation has been created but before its parameters populated and initialized.
        /// </summary>
        public event EventHandler<SelectDataOperationEventArgs> BeforeBuildSelect
        {
            add
            {
                ((MTDataSourceView)GetView("")).BeforeBuildSelect += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).BeforeBuildSelect -= value;
            }
        }

        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for count rows operation has been created but before its parameters populated and initialized.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> BeforeBuildCount
        {
            add
            {
                ((MTDataSourceView)GetView("")).BeforeBuildCount += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).BeforeBuildCount -= value;
            }
        }

        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for insert operation has been created but before its parameters populated and initialized.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> BeforeBuildInsert
        {
            add
            {
                ((MTDataSourceView)GetView("")).BeforeBuildInsert += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).BeforeBuildInsert -= value;
            }
        }

        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for update operation has been created but before its parameters populated and initialized.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> BeforeBuildUpdate
        {
            add
            {
                ((MTDataSourceView)GetView("")).BeforeBuildUpdate += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).BeforeBuildUpdate -= value;
            }
        }

        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for delete operation has been created but before its parameters populated and initialized.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> BeforeBuildDelete
        {
            add
            {
                ((MTDataSourceView)GetView("")).BeforeBuildDelete += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).BeforeBuildDelete -= value;
            }
        }

        /// <summary>
        /// Occurs before the inner <see cref="DataCommand"/> for count rows operation is executed but after the query has been composed.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> BeforeExecuteCount
        {
            add
            {
                ((MTDataSourceView)GetView("")).BeforeExecuteCount += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).BeforeExecuteCount -= value;
            }
        }

        /// <summary>
        /// Occurs before the inner <see cref="DataCommand"/> for select operation is executed but after the query has been composed.
        /// </summary>
        public event EventHandler<SelectDataOperationEventArgs> BeforeExecuteSelect
        {
            add
            {
                ((MTDataSourceView)GetView("")).BeforeExecuteSelect += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).BeforeExecuteSelect -= value;
            }
        }

        /// <summary>
        /// Occurs before the inner <see cref="DataCommand"/> for update operation is executed but after the query has been composed.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> BeforeExecuteUpdate
        {
            add
            {
                ((MTDataSourceView)GetView("")).BeforeExecuteUpdate += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).BeforeExecuteUpdate -= value;
            }
        }

        /// <summary>
        /// Occurs before the inner <see cref="DataCommand"/> for insert operation is executed but after the query has been composed.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> BeforeExecuteInsert
        {
            add
            {
                ((MTDataSourceView)GetView("")).BeforeExecuteInsert += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).BeforeExecuteInsert -= value;
            }
        }

        /// <summary>
        /// Occurs before the inner <see cref="DataCommand"/> for delete operation is executed but after the query has been composed.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> BeforeExecuteDelete
        {
            add
            {
                ((MTDataSourceView)GetView("")).BeforeExecuteDelete += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).BeforeExecuteDelete -= value;
            }
        }

        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for select operation has been executed.
        /// </summary>
        public event EventHandler<SelectDataOperationEventArgs> AfterExecuteSelect
        {
            add
            {
                ((MTDataSourceView)GetView("")).AfterExecuteSelect += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).AfterExecuteSelect -= value;
            }
        }

        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for count rows operation has been executed.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> AfterExecuteCount
        {
            add
            {
                ((MTDataSourceView)GetView("")).AfterExecuteCount += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).AfterExecuteCount -= value;
            }
        }

        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for insert operation has been executed.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> AfterExecuteInsert
        {
            add
            {
                ((MTDataSourceView)GetView("")).AfterExecuteInsert += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).AfterExecuteInsert -= value;
            }
        }

        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for update operation has been executed.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> AfterExecuteUpdate
        {
            add
            {
                ((MTDataSourceView)GetView("")).AfterExecuteUpdate += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).AfterExecuteUpdate -= value;
            }
        }

        /// <summary>
        /// Occurs after the inner <see cref="DataCommand"/> for delete operation has been executed.
        /// </summary>
        public event EventHandler<DataOperationEventArgs> AfterExecuteDelete
        {
            add
            {
                ((MTDataSourceView)GetView("")).AfterExecuteDelete += value;
            }
            remove
            {
                ((MTDataSourceView)GetView("")).AfterExecuteDelete -= value;
            }
        }
        #endregion

        private MTDataSourceView _view;
        /// <summary>
        /// Initialize a new instance of MTDataSource class.
        /// </summary>
        public MTDataSource()
            : base()
        {
        }

        #region IDataSource Members

        /// <summary>
        /// Gets the named data source view that is associated with the data source control.
        /// </summary>
        /// <param name="viewName">The name of the view to retrieve. Because the <see cref="MTDataSource"/> supports only one view, viewName is ignored.</param>
        /// <returns>The <see cref="MTDataSourceView"/> named "MTDataSourceView" that is associated with the MTDataSource.</returns>
        protected override DataSourceView GetView(string viewName)
        {
            if (null == _view)
                _view = new MTDataSourceView(this, String.Empty);
            return _view;
        }

        /// <summary>
        /// Gets a collection of names representing the list of view objects that are associated with the <see cref="MTDataSource"/> control.
        /// </summary>
        /// <returns>An <see cref="System.Collections.ICollection"/> that contains the names of the views associated with the MTDataSource.</returns>
        protected override global::System.Collections.ICollection GetViewNames()
        {
            global::System.Collections.ArrayList names = new System.Collections.ArrayList(1);
            names.Add(MTDataSourceView.DefaultViewName);
            return names as System.Collections.ICollection;
        }

        #endregion

        /// <summary>
        /// Gets or sets the connection string that will be used for connect to database.
        /// </summary>
        public string ConnectionString
        {
            get { return ((MTDataSourceView)GetView("")).ConnectionString; }
            set { ((MTDataSourceView)GetView("")).ConnectionString = value; }
        }

        /// <summary>
        /// Gets or sets the value that indicating whether result of select operation will be cached for repeating requests.
        /// </summary>
        public bool EnableCaching
        {
            get { return ((MTDataSourceView)GetView("")).EnableCaching; }
            set { ((MTDataSourceView)GetView("")).EnableCaching = value; }
        }

        /// <summary>
        /// Gets or sets value which indicates whether <see cref="InitializeSelectParameters"/> event should be raised.
        /// </summary>
        public bool IsInitilaizeSelectParametersRaised
        {
            get { return ((MTDataSourceView)GetView("")).IsInitilaizeSelectParametersRaised; }
            set { ((MTDataSourceView)GetView("")).IsInitilaizeSelectParametersRaised = value; }
        }

        /// <summary>
        /// Gets or sets the SQL string that the <see cref="MTDataSourceView"/> object 
        /// uses to retrieve data from the underlying database.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string SelectCommand
        {
            get { return ((MTDataSourceView)GetView("")).SelectCommand; }
            set { ((MTDataSourceView)GetView("")).SelectCommand = value; }
        }

        /// <summary>
        /// Gets or sets the index of resultset that will be used as datasource.
        /// </summary>
        [PersistenceMode(PersistenceMode.Attribute)]
        public int ActiveResultset
        {
            get { return ((MTDataSourceView)GetView("")).ActiveResultset; }
            set { ((MTDataSourceView)GetView("")).ActiveResultset = value; }
        }

        /// <summary>
        /// Gets or sets the list of primary keys of returned result.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public Collection<PrimaryKeyInfo> PrimaryKeys
        {
            get { return ((MTDataSourceView)GetView("")).PrimaryKeys; }
        }

        /// <summary>
        /// Gets or sets the how <see cref="SelectCommand"/> property will be interpreted.
        /// </summary>
        public InMotion.Data.MTCommandType SelectCommandType
        {
            get { return ((MTDataSourceView)GetView("")).SelectCommandType; }
            set { ((MTDataSourceView)GetView("")).SelectCommandType = value; }
        }

        /// <summary>
        /// Gets or sets the sort expression that append to the select command.
        /// </summary>
        public string SelectOrderBy
        {
            get { return ((MTDataSourceView)GetView("")).SelectOrderBy; }
            set { ((MTDataSourceView)GetView("")).SelectOrderBy = value; }
        }

        /// <summary>
        /// Gets the parameters collection containing the parameters that are used by the SelectCommand property.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public DataParameterCollection SelectParameters
        {
            get { return ((MTDataSourceView)GetView(null)).SelectParameters; }
        }

        /// <summary>
        /// Gets or sets the SQL string that the <see cref="MTDataSourceView"/> object 
        /// uses to retrieve record count from the underlying database.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string CountCommand
        {
            get { return ((MTDataSourceView)GetView("")).CountCommand; }
            set { ((MTDataSourceView)GetView("")).CountCommand = value; }
        }

        /// <summary>
        /// Gets or sets the how <see cref="CountCommand"/> property will be interpreted.
        /// </summary>
        public InMotion.Data.MTCommandType CountCommandType
        {
            get { return ((MTDataSourceView)GetView("")).CountCommandType; }
            set { ((MTDataSourceView)GetView("")).CountCommandType = value; }
        }

        /// <summary>
        /// Gets the parameters collection containing the parameters that are used by the CountCommand property.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public DataParameterCollection CountParameters
        {
            get { return ((MTDataSourceView)GetView(null)).CountParameters; }
        }

        /// <summary>
        /// Gets or sets the SQL string that the <see cref="MTDataSourceView"/> object 
        /// uses to validate data uniquess against the underlying database.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string ValidateUniqueCommand
        {
            get { return ((MTDataSourceView)GetView("")).ValidateUniqueCommand; }
            set { ((MTDataSourceView)GetView("")).ValidateUniqueCommand = value; }
        }

        /// <summary>
        /// Gets or sets the how <see cref="ValidateUniqueCommand"/> property will be interpreted.
        /// </summary>
        public InMotion.Data.MTCommandType ValidateUniqueCommandType
        {
            get { return ((MTDataSourceView)GetView("")).ValidateUniqueCommandType; }
            set { ((MTDataSourceView)GetView("")).ValidateUniqueCommandType = value; }
        }

        /// <summary>
        /// Gets the parameters collection containing the parameters that are used by the ValidateUniqueCommand property.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public DataParameterCollection ValidateUniqueParameters
        {
            get { return ((MTDataSourceView)GetView(null)).ValidateUniqueParameters; }
        }

        /// <summary>
        /// Gets or sets the SQL string that the <see cref="MTDataSourceView"/> object 
        /// uses to insert data to the underlying database.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string InsertCommand
        {
            get { return ((MTDataSourceView)GetView("")).InsertCommand; }
            set { ((MTDataSourceView)GetView("")).InsertCommand = value; }
        }

        /// <summary>
        /// Gets or sets the how <see cref="InsertCommand"/> property will be interpreted.
        /// </summary>
        public InMotion.Data.MTCommandType InsertCommandType
        {
            get { return ((MTDataSourceView)GetView("")).InsertCommandType; }
            set { ((MTDataSourceView)GetView("")).InsertCommandType = value; }
        }

        /// <summary>
        /// Gets the parameters collection containing the parameters that are used by the InsertCommand property.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public DataParameterCollection InsertParameters
        {
            get { return ((MTDataSourceView)GetView(null)).InsertParameters; }
        }

        /// <summary>
        /// Gets or sets the SQL string that the <see cref="MTDataSourceView"/> object 
        /// uses to update data in the underlying database.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string UpdateCommand
        {
            get { return ((MTDataSourceView)GetView("")).UpdateCommand; }
            set { ((MTDataSourceView)GetView("")).UpdateCommand = value; }
        }

        /// <summary>
        /// Gets or sets the how <see cref="UpdateCommand"/> property will be interpreted.
        /// </summary>
        public InMotion.Data.MTCommandType UpdateCommandType
        {
            get { return ((MTDataSourceView)GetView("")).UpdateCommandType; }
            set { ((MTDataSourceView)GetView("")).UpdateCommandType = value; }
        }
        /// <summary>
        /// Gets the parameters collection containing the parameters that are used by the UpdateCommand property.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public DataParameterCollection UpdateParameters
        {
            get { return ((MTDataSourceView)GetView(null)).UpdateParameters; }
        }

        /// <summary>
        /// Gets or sets the SQL string that the <see cref="MTDataSourceView"/> object 
        /// uses to delete data from the underlying database.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string DeleteCommand
        {
            get { return ((MTDataSourceView)GetView("")).DeleteCommand; }
            set { ((MTDataSourceView)GetView("")).DeleteCommand = value; }
        }

        /// <summary>
        /// Gets or sets the how <see cref="DeleteCommand"/> property will be interpreted.
        /// </summary>
        public InMotion.Data.MTCommandType DeleteCommandType
        {
            get { return ((MTDataSourceView)GetView("")).DeleteCommandType; }
            set { ((MTDataSourceView)GetView("")).DeleteCommandType = value; }
        }

        /// <summary>
        /// Gets the parameters collection containing the parameters that are used by the DeleteCommand property.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public DataParameterCollection DeleteParameters
        {
            get { return ((MTDataSourceView)GetView(null)).DeleteParameters; }
        }
    }
}