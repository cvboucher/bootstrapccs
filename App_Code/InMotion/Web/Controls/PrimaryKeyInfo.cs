//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using InMotion.Common;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents the information about datasource primary key.
    /// </summary>
    public class PrimaryKeyInfo
    {
        private string _fieldName;

        /// <summary>
        /// Gets or sets the name of the field that used as primary key.
        /// </summary>
        public string FieldName
        {
            get { return _fieldName; }
            set { _fieldName = value; }
        }

        private DataType _Type = DataType.Text;
        /// <summary>
        /// Gets or sets the <see cref="DataType"/> of primary key.
        /// </summary>
        public DataType Type
        {
            get { return _Type; }
            set { _Type = value; }

        }

    }
}
