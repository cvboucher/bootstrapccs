//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Provides data for DataOperationCompleting event.
    /// </summary>
    public class DataOperationCompletingEventArgs : EventArgs
    {

        private DataOperationType _operationType;
        /// <summary>
        /// Gets the <see cref="DataOperationType"/> of operation.
        /// </summary>
        public DataOperationType OperationType
        {
            get { return _operationType; }
            set { _operationType = value; }
        }
        private object _operationResult;

        /// <summary>
        /// Gets or sets the result of operation execution. 
        /// </summary>
        public object OperationResult
        {
            get { return _operationResult; }
            set { _operationResult = value; }
        }

        private Exception _ex;

        /// <summary>
        /// Gets or sets the <see cref="Exception"/> bound with operation, if it is thrown during execution.
        /// </summary>
        public Exception OperationException
        {
            get { return _ex; }
            set 
            {
                _ex = value;
				if (_ex == null)
					_exMessage = null;
				else
					_exMessage = _ex.Message;
            }
        }

        private string _exMessage = null;

        /// <summary>
        /// Gets or sets the <see cref="Exception"/> text bound with operation, if it is thrown during execution.
        /// </summary>
        public string OperationExceptionMessage
        {
            get { return _exMessage; }
            set { _exMessage = value; }
        }

        private Url _redirectUrl;

        /// <summary>
        /// Gets or sets the redirect url for current operation.
        /// </summary>
        public Url RedirectUrl
        {
            get { return _redirectUrl; }
            set { _redirectUrl = value; }
        }

        private bool _redirect = true;

        /// <summary>
        /// Gets or sets value indicating whether form will perform redirect after completing current operation.
        /// </summary>
        public bool PerformRedirect
        {
            get { return _redirect = true; }
            set { _redirect = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataOperationEventArgs"/>
        /// </summary>
        /// <param name="type">The <see cref="DataOperationType"/> of operation.</param>
        /// <param name="result">The result of operation execution.</param>
        /// <param name="redirectUrl">The redirect url for operation.</param>
        public DataOperationCompletingEventArgs(DataOperationType type, object result, Url redirectUrl)
        {
            OperationType = type;
            OperationResult = result;
            RedirectUrl = redirectUrl;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataOperationEventArgs"/>
        /// </summary>
        /// <param name="type">The <see cref="DataOperationType"/> of operation.</param>
        /// <param name="result">The result of operation execution.</param>
        /// <param name="redirectUrl">The redirect url for operation.</param>
        /// <param name="ex">The <see cref="Exception"/> bound with operation, if it is thrown during execution.</param>
        public DataOperationCompletingEventArgs(DataOperationType type, object result, Url redirectUrl, Exception ex)
            : this(type, result,redirectUrl)
        {
            OperationException = ex;
        }



    }
}
