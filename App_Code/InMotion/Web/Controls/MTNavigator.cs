//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.ComponentModel;
using System.Drawing.Design;
using System.Globalization;
using System.Diagnostics;
using InMotion.Configuration;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Specifies how command elements of the <see cref="MTNavigator"/> control will be rendered.
    /// </summary>
    public enum NavigatorStyle
    {
        /// <summary>
        /// The command elements will be rendered as html links.
        /// </summary>
        Links,
        /// <summary>
        /// The command elements will be rendered as image buttons.
        /// </summary>
        ImageButtons
    }
    /// <summary>
    /// Specifies how page numbers section of the <see cref="MTNavigator"/> control will be rendered.
    /// </summary>
    public enum PageNumbersStyle
    {
        /// <summary>
        /// The page numbers will not be rendered.
        /// </summary>
        NoPageNumbers,
        /// <summary>
        /// Only the current page and the Number of Pages are displayed in the <see cref="MTNavigator"/> control. 
        /// </summary>
        CurrentPageOnly,
        /// <summary>
        /// The Number of Pages is shown as it is set in the Page Links property and the current page is displayed in the center of the <see cref="MTNavigator"/> control.
        /// </summary>
        CurrentPageCentered,
        /// <summary>
        /// The Number of Pages is shown as it is set in the Page Links property and the current page is displayed moving on the page list of the <see cref="MTNavigator"/> control.
        /// </summary>
        CurrentPageMoving,
        /// <summary>
        /// The Number of showed page change manually by textbox value input of the <see cref="MTNavigator"/> control.
        /// </summary>
        ManualPageChanger
    }
    /// <summary>
    /// A navigator provides functionality that allows the database records within a form to be browsed.
    /// </summary>
    public class MTNavigator : WebControl, IPostBackEventHandler, IMTControl, IStateUrlParameterProvider, IMTAttributeAccessor
    {
        #region IMTControl Members

        /// <summary>
        /// Occurs after the <see cref="MTNavigator"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }
        #endregion

        /// <summary>
        /// Gets or sets the display style of the <see cref="MTNavigator"/> control.
        /// </summary>
        /// <value>
        /// One of the <see cref="NavigatorStyle"/> enumeration. 
        /// The default is [b]Links[/b]
        /// </value>
        public NavigatorStyle DisplayStyle
        {
            get
            {
                if (ViewState["__DisplayStyle"] == null)
                    ViewState["__DisplayStyle"] = NavigatorStyle.Links;
                return (NavigatorStyle)ViewState["__DisplayStyle"];
            }
            set { ViewState["__DisplayStyle"] = value; }
        }

        /// <summary>
        /// Gets value that indicates whether Previous part of navigator enabled.
        /// </summary>
        /// <value>true if the Previous enabled; otherwise false.</value>
        public bool PreviousEnabled
        {
            get
            {

                return PreviousOnValue.Length > 0 || PreviousOffValue.Length > 0;
            }
        }

        /// <summary>
        /// Gets or sets value for the active state of Previous part of Navigator
        /// </summary>
        /// <value>The value of property is depend from the <see href="DisplayStyle"/> property. In case of Links, the value is text which will be displayed. Otherwise this property will contain the path to the corresponding image.</value>
        public string PreviousOnValue
        {
            get
            {
                if (ViewState["__PreviousOnText"] == null)
                    ViewState["__PreviousOnText"] = "";
                return (string)ViewState["__PreviousOnText"];
            }
            set { ViewState["__PreviousOnText"] = value; }
        }

        /// <summary>
        /// Gets or sets value for the inactive state of Previous part of Navigator
        /// </summary>
        /// <value>The value of property is depend from the <see href="DisplayStyle"/> property. In case of Links, the value is text which will be displayed. Otherwise this property will contain the path to the corresponding image.</value>
        public string PreviousOffValue
        {
            get
            {
                if (ViewState["__PreviousOffValue"] == null)
                    ViewState["__PreviousOffValue"] = "";
                return (string)ViewState["__PreviousOffValue"];
            }
            set { ViewState["__PreviousOffValue"] = value; }
        }

        /// <summary>
        /// Gets value that indicates whether Next part of navigator enabled.
        /// </summary>
        /// <value>true if the Next enabled; otherwise false.</value>
        public bool NextEnabled
        {
            get
            {
                return NextOnValue.Length > 0 || NextOffValue.Length > 0;
            }
        }

        /// <summary>
        /// Gets or sets value for the active state of Next part of Navigator
        /// </summary>
        /// <value>The value of property is depend from the <see href="DisplayStyle"/> property. In case of Links, the value is text which will be displayed. Otherwise this property will contain the path to the corresponding image.</value>
        public string NextOnValue
        {
            get
            {
                if (ViewState["__NextOnText"] == null)
                    ViewState["__NextOnText"] = "";
                return (string)ViewState["__NextOnText"];
            }
            set { ViewState["__NextOnText"] = value; }
        }

        /// <summary>
        /// Gets or sets value for the inactive state of Next part of Navigator
        /// </summary>
        /// <value>The value of property is depend from the <see href="DisplayStyle"/> property. In case of Links, the value is text which will be displayed. Otherwise this property will contain the path to the corresponding image.</value>
        public string NextOffValue
        {
            get
            {
                if (ViewState["__NextOffValue"] == null)
                    ViewState["__NextOffValue"] = "";
                return (string)ViewState["__NextOffValue"];
            }
            set { ViewState["__NextOffValue"] = value; }
        }

        /// <summary>
        /// Gets or sets value that indicates whether First part of navigator enabled.
        /// </summary>
        /// <value>true if the First enabled; otherwise false.</value>
        public bool FirstEnabled
        {
            get
            {
                return !String.IsNullOrEmpty(FirstOnValue);
            }
        }

        /// <summary>
        /// Gets or sets value for the active state of First part of Navigator
        /// </summary>
        /// <remarks></remarks>
        /// <value>The value of property is depend from the <see href="DisplayStyle"/> property. In case of Links, the value is text which will be displayed. Otherwise this property will contain the path to the corresponding image.</value>
        public string FirstOnValue
        {
            get
            {
                if (ViewState["__FirstOnText"] == null)
                    ViewState["__FirstOnText"] = "";
                return (string)ViewState["__FirstOnText"];
            }
            set { ViewState["__FirstOnText"] = value; }
        }

        /// <summary>
        /// Gets or sets value for the inactive state of First part of Navigator
        /// </summary>
        /// <remarks></remarks>
        /// <value>The value of property is depend from the <see href="DisplayStyle"/> property. In case of Links, the value is text which will be displayed. Otherwise this property will contain the path to the corresponding image.</value>
        public string FirstOffValue
        {
            get
            {
                if (ViewState["__FirstOffValue"] == null)
                    ViewState["__FirstOffValue"] = "";
                return (string)ViewState["__FirstOffValue"];
            }
            set { ViewState["__FirstOffValue"] = value; }
        }

        /// <summary>
        /// Gets or sets values of page size selector items
        /// </summary>
        /// <value>If the property value is null or empty listbox does not show. Page size values must separated by semicolon.</value>
        public string PageSizeItems
        {
            get
            {
                return (string)ViewState["__PageSizeValues"];
            }
            set
            {
                ViewState["__PageSizeValues"] = value;
            }
        }

        /// <summary>
        /// Gets or sets values of page size selector caption
        /// </summary>
        /// <value>The value was displayed before page size selector.</value>
        public string PageSizeCaption
        {
            get
            {
                if (ViewState["__PageSizeCaption"] == null)
                    ViewState["__PageSizeCaption"] = "";
                return (string)ViewState["__PageSizeCaption"];
            }
            set
            {
                ViewState["__PageSizeCaption"] = value;
            }
        }

        /// <summary>
        /// Gets or sets values of page number input caption
        /// </summary>
        /// <value>The value was displayed before page number input control.</value>
        public string PageNumberCaption
        {
            get
            {
                if (ViewState["__PageNumberCaption"] == null)
                    ViewState["__PageNumberCaption"] = "";
                return (string)ViewState["__PageNumberCaption"];
            }
            set
            {
                ViewState["__PageNumberCaption"] = value;
            }
        }

        /// <summary>
        /// Gets or sets values of page number input caption
        /// </summary>
        /// <value>The value was displayed after page number input control.</value>
        public string PostPageNumberCaption
        {
            get
            {
                if (ViewState["__PostPageNumberCaption"] == null)
                    ViewState["__PostPageNumberCaption"] = "";
                return (string)ViewState["__PostPageNumberCaption"];
            }
            set
            {
                ViewState["__PostPageNumberCaption"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the text for the psubmit button of navigator.
        /// </summary>
        public string SubmitButtonText
        {
            get
            {
                if (ViewState["__SubmitButtonText"] == null)
                    ViewState["__SubmitButtonText"] = "";
                return (string)ViewState["__SubmitButtonText"];
            }
            set
            {
                ViewState["__SubmitButtonText"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the text for the psubmit button of navigator.
        /// </summary>
        public string SubmitButtonImg
        {
            get
            {
                if (ViewState["__SubmitButtonImg"] == null)
                    ViewState["__SubmitButtonImg"] = "";
                return (string)ViewState["__SubmitButtonImg"];
            }
            set
            {
                ViewState["__SubmitButtonImg"] = value;
            }
        }

        /// <summary>
        /// Gets value that indicates whether Last part of navigator enabled.
        /// </summary>
        /// <value>true if the Last enabled; otherwise false.</value>
        public bool LastEnabled
        {
            get
            {
                return !String.IsNullOrEmpty(LastOnValue);
            }
        }

        /// <summary>
        /// Gets or sets value for the active state of Last part of Navigator
        /// </summary>
        /// <value>The value of property is depend from the <see href="DisplayStyle"/> property. In case of Links, the value is text which will be displayed. Otherwise this property will contain the path to the corresponding image.</value>
        public string LastOnValue
        {
            get
            {
                if (ViewState["__LastOnText"] == null)
                    ViewState["__LastOnText"] = "";
                return (string)ViewState["__LastOnText"];
            }
            set { ViewState["__LastOnText"] = value; }
        }

        /// <summary>
        /// Gets or sets value for the inactive state of Last  part of Navigator
        /// </summary>
        /// <value>The value of property is depend from the <see href="DisplayStyle"/> property. In case of Links, the value is text which will be displayed. Otherwise this property will contain the path to the corresponding image.</value>
        public string LastOffValue
        {
            get
            {
                if (ViewState["__LastOffValue"] == null)
                    ViewState["__LastOffValue"] = "";
                return (string)ViewState["__LastOffValue"];
            }
            set { ViewState["__LastOffValue"] = value; }
        }

        /// <summary>
        /// Gets or sets the type of links used in the <see cref="MTNavigator"/>.
        /// </summary>
        public PageNumbersStyle PageNumbersStyle
        {
            get
            {
                if (ViewState["__PN"] == null)
                    ViewState["__PN"] = (int)PageNumbersStyle.NoPageNumbers;
                return (PageNumbersStyle)ViewState["__PN"];
            }
            set { ViewState["__PN"] = (int)value; }
        }

        /// <summary>
        /// Gets or sets the number of page links, that displayed simultaneously.
        /// </summary>
        public int PageLinksNumber
        {
            get
            {
                if (ViewState["__PageLinksNumber"] == null)
                    ViewState["__PageLinksNumber"] = 10;
                return (int)ViewState["__PageLinksNumber"];
            }
            set
            {
                if (value < 1)
                {
                    if (AppConfig.IsMTErrorHandlerUse("all"))
                        Trace.TraceError("PageLinksNumber can not be less than 1.\n{0}", Environment.StackTrace);
                    throw new ArgumentOutOfRangeException("value", value, "PageLinksNumber can not be less than 1");
                }
                ViewState["__PageLinksNumber"] = value;
            }
        }

        /// <summary>
        /// Gets or sets value that indicates whether the total number of pages will be displayed.
        /// </summary>
        public bool ShowTotalPages
        {
            get
            {
                if (ViewState["__ShowTotalPages"] == null)
                    ViewState["__ShowTotalPages"] = false;
                return (bool)ViewState["__ShowTotalPages"];
            }
            set { ViewState["__ShowTotalPages"] = value; }
        }

        /// <summary>
        /// Gets or sets the text which will be displayed before the total numbers of pages.
        /// </summary>
        public string TotalPagesText
        {
            get
            {
                if (ViewState["__TotalPagesText"] == null)
                    ViewState["__TotalPagesText"] = "";
                return (string)ViewState["__TotalPagesText"];
            }
            set { ViewState["__TotalPagesText"] = value; }
        }

        /// <summary>
        /// Gets or sets the text which will be displayed before number of current page.
        /// </summary>
        public string PageOffPreValue
        {
            get
            {
                if (ViewState["__PageOffPreValue"] == null)
                    ViewState["__PageOffPreValue"] = "";
                return (string)ViewState["__PageOffPreValue"];
            }
            set { ViewState["__PageOffPreValue"] = value; }
        }

        /// <summary>
        /// Gets or sets the text which will be displayed after number of current page.
        /// </summary>
        public string PageOffPostValue
        {
            get
            {
                if (ViewState["__PageOffPostValue"] == null)
                    ViewState["__PageOffPostValue"] = "";
                return (string)ViewState["__PageOffPostValue"];
            }
            set { ViewState["__PageOffPostValue"] = value; }
        }
        
        /// <summary>
        /// Gets or sets the number of currently selected page.
        /// </summary>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden),
        Browsable(false)]
        public int SelectedPage
        {
            get
            {
                return ((INavigable)OwnerForm).CurrentPage;
            }
            set
            {
                if (value < 1)
                    value = 1;
                ((INavigable)OwnerForm).CurrentPage = value;
            }
        }

        /// <summary>
        /// Gets or sets the total number of pages.
        /// </summary>
        public int TotalPages
        {
            get
            {
                if (DesignMode) return 10;
                return ((INavigable)OwnerForm).TotalPages;
            }
        }

        private OperationMode __OperationMode = OperationMode.Url;
        /// <summary>
        /// Gets or sets the operation mode of the <see cref="MTNavigator"/> control.
        /// </summary>
        /// <value>
        /// One of the <see cref="OperationMode"/> enumeration. 
        /// The default is [b]Url[/b]
        /// </value>
        public OperationMode OperationMode
        {
            get
            {
                return __OperationMode;
            }
            set
            {
                __OperationMode = value;
            }
        }

        private string __ParameterName;
        /// <summary>
        /// Gets or sets the name of query string parameter which will
        /// be used for Url operation mode.
        /// </summary>
        /// <value>
        /// The string. The default is owner form (if any) ID, or control ID.
        /// </value>
        public string ParameterName
        {
            get
            {
                if (__ParameterName == null && OwnerForm != null)
                    __ParameterName = OwnerForm.ID;
                else if (__ParameterName == null && OwnerForm == null)
                    __ParameterName = ID;
                return __ParameterName;
            }
            set { __ParameterName = value; }
        }

        private Url _url = new Url("");
        /// <summary>
        /// Gets or sets the url which will be used as target page 
        /// for Url operation mode.
        /// </summary>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public Url Url
        {
            get { return _url; }
            set { _url = value; }
        }

        /// <summary>
        /// Renders the <see cref="MTNavigator"/> control to the specified HtmlTextWriter object. 
        /// </summary>
        /// <param name="writer">
        /// The <see cref="HtmlTextWriter"/> that receives the rendered output.
        ///</param>
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            if (!Visible) return;
            //writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID);
            //writer.RenderBeginTag(HtmlTextWriterTag.Div);

            int SelPage = 1;
            if (!DesignMode)
                SelPage = SelectedPage;

            if (!string.IsNullOrEmpty(PageSizeItems) && OwnerForm is Grid)
            {
                writer.WriteLine(InMotion.Web.Controls.ControlsHelper.ReplaceResource(PageSizeCaption));
                writer.AddAttribute(HtmlTextWriterAttribute.Name, OwnerForm.ID + "PageSize");
                writer.AddAttribute(HtmlTextWriterAttribute.Onchange, Page.ClientScript.GetPostBackEventReference(this, "navigate", false));
                writer.RenderBeginTag(HtmlTextWriterTag.Select);
                string[] items = PageSizeItems.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                bool renderCurentValue = false;
                for (int i = 0; i < items.Length; i++)
                {
                    int itemValue;
                    if (int.TryParse(items[i], NumberStyles.Integer, CultureInfo.CurrentCulture.NumberFormat, out itemValue))
                    {
                        if (!renderCurentValue && itemValue > ((Grid)OwnerForm).RecordsPerPage)
                        {
                            itemValue = ((Grid)OwnerForm).RecordsPerPage;
                            i--;
                        }

                        writer.AddAttribute(HtmlTextWriterAttribute.Value, itemValue.ToString());
                        if (itemValue == ((Grid)OwnerForm).RecordsPerPage)
                        {
                            renderCurentValue = true;
                            writer.AddAttribute(HtmlTextWriterAttribute.Selected, "selected");
                        }

                        writer.RenderBeginTag(HtmlTextWriterTag.Option);
                        writer.Write(itemValue);
                        writer.RenderEndTag();
                    }
                }
                writer.RenderEndTag();
                writer.Write("&nbsp;");
            }

            if (FirstEnabled)
                CreateNavigationLink(writer, SelPage > 1, SelPage > 1 ? ControlsHelper.ReplaceResourcesAndStyles(FirstOnValue.Trim(), Page, this) : ControlsHelper.ReplaceResourcesAndStyles(FirstOffValue.Trim(), Page, this), 1);
            if (PreviousEnabled)
                CreateNavigationLink(writer, SelPage > 1, SelPage > 1 ? ControlsHelper.ReplaceResourcesAndStyles(PreviousOnValue.Trim(), Page, this) : ControlsHelper.ReplaceResourcesAndStyles(PreviousOffValue.Trim(), Page, this), SelPage - 1);
            switch (PageNumbersStyle)
            {
                case PageNumbersStyle.NoPageNumbers: break;

                case PageNumbersStyle.ManualPageChanger:
                    writer.WriteLine(PageNumberCaption);
                    writer.AddAttribute(HtmlTextWriterAttribute.Name, OwnerForm.ID + "Page");
                    writer.AddAttribute(HtmlTextWriterAttribute.Size, "2");
                    writer.AddAttribute(HtmlTextWriterAttribute.Value, SelPage.ToString());
                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");
                    writer.RenderBeginTag(HtmlTextWriterTag.Input);
                    writer.RenderEndTag();

                    /*                    if (UseImageButton)
                                        {
                                            writer.AddAttribute(HtmlTextWriterAttribute.Type, "image");
                                            writer.AddAttribute(HtmlTextWriterAttribute.Src, ControlsHelper.ReplaceResourcesAndStyles(SubmitButtonImg, Page));
                                            writer.AddAttribute(HtmlTextWriterAttribute.Alt, SubmitButtonText);
                                            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0");
                                        }
                                        else
                                        {*/
                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "button");
                    writer.AddAttribute(HtmlTextWriterAttribute.Value, SubmitButtonText);
                    //                    }

                    writer.AddAttribute(HtmlTextWriterAttribute.Onclick, Page.ClientScript.GetPostBackEventReference(this, "navigate", false));
                    writer.RenderBeginTag(HtmlTextWriterTag.Input);
                    writer.RenderEndTag();
                    break;

                default: CreatePager(writer); break;
            }

            if (NextEnabled)
                CreateNavigationLink(writer, SelPage < TotalPages, SelPage < TotalPages ? ControlsHelper.ReplaceResourcesAndStyles(NextOnValue.Trim(), Page, this) : ControlsHelper.ReplaceResourcesAndStyles(NextOffValue.Trim(), Page, this), SelPage + 1);
            if (LastEnabled)
                CreateNavigationLink(writer, SelPage < TotalPages, SelPage < TotalPages ? ControlsHelper.ReplaceResourcesAndStyles(LastOnValue.Trim(), Page, this) : ControlsHelper.ReplaceResourcesAndStyles(LastOffValue.Trim(), Page, this), TotalPages);
            //writer.RenderEndTag();
        }

        private void CreatePager(HtmlTextWriter writer)
        {
            int start = 0, end = 0;
            int SelPage = 1;
            if (!DesignMode)
                SelPage = SelectedPage;

            switch (PageNumbersStyle)
            {
                case PageNumbersStyle.CurrentPageOnly:
                    if (!String.IsNullOrEmpty(PageNumberCaption))
                    {
                        writer.Write(PageNumberCaption);
                        writer.Write("&nbsp;");
                    }
                    start = SelPage;
                    end = start;
                    break;
                case PageNumbersStyle.CurrentPageCentered:
                    start = SelPage - (PageLinksNumber / 2);
                    if (start + PageLinksNumber > TotalPages)
                        start = TotalPages - PageLinksNumber + 1;
                    if (start < 1) start = 1;
                    end = start + PageLinksNumber - 1;
                    break;
                case PageNumbersStyle.CurrentPageMoving:
                    start = ((SelPage - 1) / PageLinksNumber) * PageLinksNumber + 1;
                    if (start != 1)
                    {
                        start--;
                        end++;
                    }
                    end = start + PageLinksNumber;
                    break;
                default:
                    break;
            }

            for (; start <= end && start <= TotalPages; start++)
            {
                if (start == SelPage)
                {
                    if (!string.IsNullOrEmpty(PageOffPreValue))
                        writer.Write(PageOffPreValue);
                    writer.Write(start.ToString());
                    if (!string.IsNullOrEmpty(PageOffPostValue))
                        writer.Write(PageOffPostValue);
                    else
                        writer.Write("&nbsp;");
                    if (PageNumbersStyle == PageNumbersStyle.CurrentPageOnly && !string.IsNullOrEmpty(PostPageNumberCaption) && string.IsNullOrEmpty(TotalPagesText))
                    {
                        writer.Write(PostPageNumberCaption);
                        writer.Write("&nbsp;");
                    }
                }
                else
                {
                    writer.WriteBeginTag("a");
                    CreateHref(writer, start.ToString());
                    writer.Write(HtmlTextWriter.TagRightChar);
                    writer.Write(start.ToString());
                    writer.WriteEndTag("a");
                    writer.Write("&nbsp;");
                }
            }

            if (ShowTotalPages)
            {
                writer.Write(ControlsHelper.ReplaceResourcesAndStyles(TotalPagesText, Page, this));
                writer.Write(HtmlTextWriter.SpaceChar);
                writer.Write(TotalPages.ToString());
                writer.Write("&nbsp;");
            }

        }

        private void CreateNavigationLink(System.Web.UI.HtmlTextWriter writer, bool IsActive, string val, int pageNumber)
        {
            if (IsActive)
            {
                writer.WriteBeginTag("a");
                CreateHref(writer, pageNumber.ToString());
                writer.Write(HtmlTextWriter.TagRightChar);
            }

            if (DisplayStyle == NavigatorStyle.Links)
            {
                writer.Write(val);
            }
            else
            {
                writer.WriteBeginTag("img");
                writer.WriteAttribute("border", "0");
                writer.WriteAttribute("src", val);
                writer.Write(HtmlTextWriter.SelfClosingTagEnd);
            }
            if (IsActive) writer.WriteEndTag("a");
            writer.Write("&nbsp;");
        }

        private void CreateHref(System.Web.UI.HtmlTextWriter writer, string reqstate)
        {
            if (OperationMode == OperationMode.Postback)
            {
                writer.WriteAttribute("href",
                    Page.ClientScript.GetPostBackClientHyperlink(this, reqstate));
            }
            else
            {
                Url u = new Url(Url.Address, Url.RemoveParameters, PreserveParameterType.Get, Url.Type);
                u.Parameters.Add(ParameterName + "Page", reqstate);
                if (!string.IsNullOrEmpty(PageSizeItems) && OwnerForm is Grid)
                    u.Parameters.Add(ParameterName + "PageSize", ((Grid)OwnerForm).RecordsPerPage.ToString());
                if (!DesignMode)
                    writer.WriteAttribute("href", u.ToString(true));
            }
        }

        #region IPostBackEventHandler Members
        /// <summary>
        /// Raises events for the <see cref="MTNavigator"/> control when a form is posted back to the server.
        /// </summary>
        /// <param name="eventArgument">The string representation of the requested state.</param>
        public void RaisePostBackEvent(string eventArgument)
        {
            if (OwnerForm is INavigable)
            {
                int pageNumber;
                if (OwnerForm is Grid && Page.Request.Form[OwnerForm.ID + "PageSize"] != null && int.TryParse(Page.Request.Form[OwnerForm.ID + "PageSize"], NumberStyles.Integer, CultureInfo.CurrentCulture.NumberFormat, out pageNumber))
                {
                    ((Grid)OwnerForm).RecordsPerPage = pageNumber;
                    if (OperationMode == OperationMode.Url)
                    {
                        Url r = new Url(Page.Request.Url.OriginalString, OwnerForm.ID + "PageSize", PreserveParameterType.GetAndPost, UrlType.None);
                        r.Parameters.Add(OwnerForm.ID + "PageSize", pageNumber.ToString());
                        Page.Response.Redirect(r.ToString());
                    }
                }

                if (int.TryParse(eventArgument, NumberStyles.Integer, CultureInfo.CurrentCulture.NumberFormat, out pageNumber) ||
                    Page.Request.Form[OwnerForm.ID + "Page"] != null && int.TryParse(Page.Request.Form[OwnerForm.ID + "Page"], NumberStyles.Integer, CultureInfo.CurrentCulture.NumberFormat, out pageNumber))
                    SelectedPage = pageNumber;
                OwnerForm.DataBind();
            }
        }
        #endregion

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            Visible = (TotalPages > 1);
            OnBeforeShow(EventArgs.Empty);
        }

        /// <summary>
        /// Gets the name of state parameter for remove from query string.
        /// </summary>
        public string RemoveStateParameters
        {
            get
            {
                if (SelectedPage == 1) return ParameterName + "Page";
                return "";
            }
        }

        /// <summary>
        /// Gets the <see cref="UrlParameterCollection"/> of state parameters for preserving in query string.
        /// </summary>
        public UrlParameterCollection PreserveStateParameters
        {
            get
            {
                UrlParameterCollection p = new UrlParameterCollection();
                if (OperationMode == OperationMode.Postback)
                {
                    if (HttpContext.Current.Request.Form["__EVENTTARGET"] == this.UniqueID)
                        p.Add(ParameterName + "Page", SelectedPage.ToString(), true);
                }
                return p;
            }
        }
    }
}
