//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Provides data for the OnCalculate event.
    /// </summary>
    public class OnCalculateEventArgs : EventArgs
    {
        private object _dataItem;

        /// <summary>
        /// Gets or sets the data item associated with current <see cref="ReportSection"/>.
        /// </summary>
        public object DataItem
        {
            get { return _dataItem; }
        }
        private string _sectionName;
        /// <summary>
        /// Gets the name of current <see cref="ReportSection"/>.
        /// </summary>
        public string SectionName
        {
            get { return _sectionName; }
        }
        private ReportSectionTemplate _item;

        /// <summary>
        /// Gets the current item.
        /// </summary>
        public ReportSectionTemplate Item
        {
            get { return _item; }
        }

        /// <summary>
        /// Initialize the new instance of <see cref="OnCalculateEventArgs"/>
        /// </summary>
        /// <param name="data">The data item associated with current <see cref="ReportSection"/>.</param>
        /// <param name="sectionName">The name of current <see cref="ReportSection"/>.</param>
        /// <param name="item">Gets the current item.</param>
        public OnCalculateEventArgs(object data, string sectionName, ReportSectionTemplate item)
        {
            this._dataItem = data;
            this._sectionName = sectionName;
            this._item = item;
        }
    }

}
