//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Defines a service implemented by objects to provide special values as data source of controls.
    /// </summary>
    public interface ISpecialValueProvider
    {
        /// <summary>
        /// Gets the <see cref="Dictionary&lt;TKey,TValue&gt;"/> that contain special values.
        /// </summary>
        Dictionary<string, object> SpecialValues
        {
            get;
        }
    }
}
