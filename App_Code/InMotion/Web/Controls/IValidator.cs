//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using InMotion.Common;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Defines the interface that used by InMotion forms for validating child controls.
    /// </summary>
    public interface IValidator
    {
        /// <summary>
        /// Occurs when the form perform child controls validation.
        /// </summary>
        event EventHandler<ValidatingEventArgs> ControlsValidating;
        /// <summary>
        /// Performs the validating uniqueness of supplied <see cref="IMTEditableControl"/> value against the database.
        /// </summary>
        /// <param name="control">The <see cref="IMTEditableControl"/> that value should be validated.</param>
        /// <returns>true if the validation was successfull; otherwise false.</returns>
        bool ValidateUnique(IMTEditableControl control);
    }
}