//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represent a template for <see cref="ReportSection"/> class.
    /// </summary>
    public class ReportSectionTemplate : Control, INamingContainer
    {
        private string _name = "";
        private float _height = 1;
        private object _dataItem;

        /// <summary>
        /// Initializes the new instance of the <see cref="ReportSectionTemplate"/>
        /// </summary>
        public ReportSectionTemplate()
        {

        }

        internal ReportSectionTemplate(Control parent)
        {
            _parent = parent;
        }

        private Control _parent;
        /// <summary>
        /// Gets a reference to the server control's parent control in the page control hierarchy.
        /// </summary>
        public override Control Parent
        {
            get
            {
                if (_parent == null)
                    return base.Parent;
                return _parent;
            }
        }


        /// <summary>
        /// Gets or sets the name of current section.
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        /// <summary>
        /// Gets or sets the height of the section in abstract "line" units.
        /// </summary>
        public float Height
        {
            get
            {
                return _height;
            }
            set
            {
                _height = value;
            }
        }
        /// <summary>
        /// Gets or sets the data item associated with current section.
        /// </summary>
        public object DataItem
        {
            get
            {
                return _dataItem;
            }
            set
            {
                _dataItem = value;
            }
        }
    }

}
