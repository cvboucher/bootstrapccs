//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using InMotion.Common;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Defines the interface to a editable InMotion control, such as <see cref="MTTextBox"/> or <see cref="MTListBox"/>.
    /// </summary>
    public interface IMTEditableControl:IMTControlWithValue
    {
        /// <summary>
        /// Gets or sets the caption of the control that will be used in validation messages.
        /// </summary>
        string Caption { get; set; }
        /// <summary>
        /// Gets or sets the <see cref="TypedValue"/> of the control for interact with data source.
        /// </summary>
        TypedValue DBValue { get; }
        /// <summary>
        /// Indicates whethere value is empty, i.e. not passed by the client request.
        /// </summary>
        bool IsValueNotPassed { get;set;}
        /// <summary>
        /// Indicates whethere control value will be excluded from data update operation when <see cref="IsValueNotPassed"/> is true;
        /// </summary>
        bool IsOmitEmptyValue { get;set;}
        /// <summary>
        /// Gets or sets the value indicating whether value of control changed by the user.
        /// </summary>
        bool IsValueChanged { get;}
        /// <summary>
        /// Occurs after the control performed the value validation.
        /// </summary>
        event EventHandler<ValidatingEventArgs> Validating;
        /// <summary>
        /// Perform the value validation.
        /// </summary>
        /// <returns><b>true</b> if the value is valid; otherwise <b>false</b>.</returns>
        bool Validate();

    }
}
