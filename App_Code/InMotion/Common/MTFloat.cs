//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Globalization;
using System.Diagnostics;
using InMotion.Configuration;

namespace InMotion.Common
{
    /// <summary>
    /// Represents a double-precision floating point number. 
    /// </summary>
    [Serializable]
    public struct MTFloat : IMTField
    {
        private bool _isNull;
        private double _value;

        /// <summary>
        /// Returns a <see cref="DataType"/> of the specified <see cref="IMTField"/> object.
        /// </summary>
        public DataType DataType
        {
            get { return DataType.Float; }
        }

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MTFloat"/> structure.
        /// </summary>
        /// <param name="IsNullable">The supplied <see cref="bool"/> IsNullable indicates that the value of the new <see cref="MTFloat"/> structure is null.</param>
        private MTFloat(bool IsNullable)
            : this(0)
        {
            _isNull = IsNullable;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MTFloat"/> structure.
        /// </summary>
        /// <param name="value">The supplied <see cref="double"/> value that will be used as the value of the new <see cref="MTFloat"/> structure.</param>
        public MTFloat(double value)
        {
            _isNull = false;
            _value = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MTFloat"/> structure.
        /// </summary>
        /// <param name="value">The supplied <see cref="double"/> value that will be used as the value of the new <see cref="MTFloat"/> structure.</param>
        public MTFloat(double? value)
        {
            _isNull = (value == null);
            _value = value.GetValueOrDefault(0);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Represents the largest possible value of an MTFloat. This field is constant.
        /// </summary>
        public static MTFloat MaxValue
        {
            get
            {
                return new MTFloat(double.MaxValue);
            }
        }

        /// <summary>
        /// Represents the smallest possible value of an MTFloat. This field is constant.
        /// </summary>
        public static MTFloat MinValue
        {
            get
            {
                return new MTFloat(double.MinValue);
            }
        }

        /// <summary>
        /// Gets the <see cref="MTFloat"/> structure's value.
        /// </summary>
        public double Value
        {
            get { return _value; }
        }

        /// <summary>
        /// Indicates whether a structure is null. This property is read-only.
        /// </summary>
        public bool IsNull
        {
            get { return _isNull; }
        }

        /// <summary>
        /// Represents a null value that can be assigned to the Value property of an instance of the <see cref="MTFloat"/> structure.
        /// </summary>
        public static MTFloat Null
        {
            get { return new MTFloat(true); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Converts the string representation of a number  in a specified format to its <see cref="MTFloat"/> equivalent.
        /// </summary>
        /// <param name="value">An object containing the value to convert.</param>
        /// <param name="format">A number format string.</param>
        /// <returns>The <see cref="MTFloat"/> value equivalent to the number contained in value.</returns>
        public static MTFloat Parse(object value, string format)
        {
            if (value is MTFloat) return (MTFloat)value;
            if (value == null || ((value is INullable) && ((INullable)value).IsNull)
                || value.Equals(DBNull.Value) || value.ToString().Length == 0)
                return MTFloat.Null;
            if (value is IMTField) value = ((IMTField)value).ToString("");

            try
            {
                if (value is string)
                {
                    char[] chDelim = { ';' };
                    if (String.IsNullOrEmpty(format))
                        return (MTFloat)Double.Parse((string)value);

                    NumberFormatInfo nfi = (NumberFormatInfo)System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.Clone();
                    string[] Tokens = format.Split(chDelim);
                    if (Tokens.Length > 4 && Tokens[4].Length > 0)
                    {
                        nfi.NumberDecimalSeparator = Tokens[4];
                        nfi.PercentDecimalSeparator = Tokens[4];
                    }

                    if (Tokens.Length > 5 && Tokens[5].Length > 0)
                    {
                        nfi.NumberGroupSeparator = Tokens[5];
                        nfi.PercentGroupSeparator = Tokens[5];
                    }

                    return (MTFloat)Double.Parse((string)value, NumberStyles.Any, nfi);
                }
                else return (MTFloat)Convert.ToDouble(value);
            }
            catch (Exception e)
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("Unable to parse Float.\n{0}", e);
                throw (new FormatException("Unable to parse Float: " + e.Message));
            }
        }

        /// <summary>
        /// Converts the string representation of a number to its <see cref="MTFloat"/> equivalent.
        /// </summary>
        /// <param name="value">A string containing the value to convert.</param>
        /// <returns>The <see cref="MTFloat"/> value equivalent to the number contained in value.</returns>
        public static MTFloat Parse(object value)
        {
            return Parse(value, "");
        }

        private static MTFloat ToMTFloat(object obj)
        {
            MTFloat result;
            try
            {
                result = (MTFloat)obj;
            }
            catch(Exception e)
            {
                if (obj is IConvertible && !(obj is string) && !(obj is DateTime) && !(obj is TimeSpan))
                    result = (MTFloat)Convert.ToDouble(obj);
                else
                {
                    if (AppConfig.IsMTErrorHandlerUse("all"))
                        Trace.TraceError(e.ToString());
                    throw new InvalidCastException();
                }
            }
            return result;
        }

        /// <summary>
        /// Tests for equality between two <see cref="MTFloat"/> objects.
        /// </summary>
        /// <param name="obj">An <see cref="object"/> value to compare to this instance.</param>
        /// <returns><b>true</b> if obj has the same value as this instance; otherwise, <b>false</b></returns>
        public override bool Equals(object obj)
        {
            MTFloat i;
            try
            {
                i = ToMTFloat(obj);
            }
            catch { return false; }
            if (i.IsNull || this.IsNull)
                return this.IsNull == i.IsNull;
            return i.Value == this.Value;
        }

        /// <summary>
        /// Serves as a hash function for a particular type. GetHashCode is suitable for use in hashing algorithms and data structures like a hash table.
        /// </summary>
        /// <returns>A hash code for the current <see cref="MTFloat"/>.</returns>
        public override int GetHashCode()
        {
            if (this.IsNull) return 0;
            return this.Value.GetHashCode();
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation.
        /// </summary>
        /// <returns>The string representation of the value of this instance.</returns>
        public override string ToString()
        {
            return this.ToString("");
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation, using the specified format. 
        /// </summary>
        /// <param name="format">A format string. </param>
        /// <returns>The string representation of the value of this instance as specified by format. </returns>
        public string ToString(string format)
        {
            return this.ToString(format, null);
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation using the specified format and culture-specific format information. 
        /// </summary>
        /// <param name="format">A format string.</param>
        /// <param name="formatProvider">An <see cref="IFormatProvider"/> that supplies culture-specific formatting information.</param>
        /// <returns>The string representation of the value of this instance as specified by format and provider.</returns>
        public string ToString(string format, IFormatProvider formatProvider)
        {
            if (this.IsNull || double.IsNaN(Value)) return "";
            return TypeFactory.FormatNumber(Value, format);
        }
        #endregion

        #region IComparable Members
        /// <summary>
        /// Compares this instance to a specified <see cref="MTFloat"/> and returns an indication of their relative values.
        /// </summary>
        /// <param name="op1">A <see cref="MTFloat"/> to compare to this instance.</param>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects
        /// being compared. The return value has these meanings: Value Meaning Less than
        /// zero This instance is less than obj. Zero This instance is equal to obj.
        /// Greater than zero This instance is greater than obj.
        /// </returns>
        public int CompareTo(MTFloat op1)
        {
            if (op1.IsNull && this.IsNull) return 0;
            if (op1.IsNull) return 1;
            if (this.IsNull) return -1;
            return this.Value.CompareTo(op1.Value);
        }

        /// <summary>
        /// Compares this instance to an <see cref="object"/> and returns an indication of their relative values.
        /// </summary>
        /// <param name="op1">A <see cref="object"/> to compare to this instance.</param>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects
        /// being compared. The return value has these meanings: Value Meaning Less than
        /// zero This instance is less than obj. Zero This instance is equal to obj.
        /// Greater than zero This instance is greater than obj.
        /// </returns>
        public int CompareTo(object op1)
        {
            if (op1 is MTFloat)
                return CompareTo((MTFloat)op1);
            else
                return CompareTo(Parse(op1));
        }
        #endregion

        #region Static operators
        /// <summary>
        /// Determines whether two specified instances of MTFloat are equal.
        /// </summary>
        /// <param name="op1">A <see cref="MTFloat"/>.</param>
        /// <param name="op2">A <b>MTFloat</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> and <i>op2</i> represent the same value; otherwise <b>false</b>.;</returns>
        public static bool operator ==(MTFloat op1, MTFloat op2)
        {
            return op1.Equals(op2);
        }

        /// <summary>
        /// Determines whether two specified instances of MTFloat are not equal.
        /// </summary>
        /// <param name="op1">A <see cref="MTFloat"/>.</param>
        /// <param name="op2">A <b>MTFloat</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> and <i>op2</i> do not represent the same value; otherwise <b>false</b>.;</returns>
        public static bool operator !=(MTFloat op1, MTFloat op2)
        {
            return !op1.Equals(op2);
        }

        /// <summary>
        /// Adds a specified MTFloat to another specified MTFloat.
        /// </summary>
        /// <param name="op1">A <see cref="MTFloat"/>.</param>
        /// <param name="op2">A <b>MTFloat</b>.</param>
        /// <returns>A <b>MTFloat</b> whose value is the value of <i>op1</i> plus the value of <i>op2</i>.</returns>
        public static MTFloat operator +(MTFloat op1, MTFloat op2)
        {
            if (op1.IsNull || op2.IsNull) return MTFloat.Null;
            return new MTFloat(op1.Value + op2.Value);
        }

        /// <summary>
        /// Subtract the second operand from the first.
        /// </summary>
        /// <param name="op1">A <see cref="MTFloat"/>.</param>
        /// <param name="op2">A <b>MTFloat</b>.</param>
        /// <returns>A <b>MTFloat</b> whose value is the value of <i>op1</i> minus the value of <i>op2</i>.</returns>
        public static MTFloat operator -(MTFloat op1, MTFloat op2)
        {
            if (op1.IsNull || op2.IsNull) return MTFloat.Null;
            return new MTFloat(op1.Value - op2.Value);
        }

        /// <summary>
        /// Computes the product of its operands.
        /// </summary>
        /// <param name="op1">A <see cref="MTFloat"/>.</param>
        /// <param name="op2">A <b>MTFloat</b>.</param>
        /// <returns>A <b>MTFloat</b> instance.</returns>
        public static MTFloat operator *(MTFloat op1, MTFloat op2)
        {
            if (op1.IsNull || op2.IsNull) return MTFloat.Null;
            return new MTFloat(op1.Value * op2.Value);
        }

        /// <summary>
        /// Divides its first operand by its second.
        /// </summary>
        /// <param name="op1">A <see cref="MTFloat"/>.</param>
        /// <param name="op2">A <b>MTFloat</b>.</param>
        /// <returns>A <b>MTFloat</b> instance.</returns>
        public static MTFloat operator /(MTFloat op1, MTFloat op2)
        {
            if (op1.IsNull || op2.IsNull) return MTFloat.Null;
            return new MTFloat(op1.Value / op2.Value);
        }

        /// <summary>
        /// Determines whether first specified instances of MTFloat are greater than second instance.
        /// </summary>
        /// <param name="op1">A <see cref="MTFloat"/>.</param>
        /// <param name="op2">A <b>MTFloat</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> less than <i>op2</i>; otherwise <b>false</b>.;</returns>
        public static MTBoolean operator >(MTFloat op1, MTFloat op2)
        {
            if (op1.IsNull || op2.IsNull) return MTBoolean.Null;
            return new MTBoolean(op1.Value > op2.Value);
        }

        /// <summary>
        /// Determines whether first specified instances of MTFloat are greater than or equal to second instance.
        /// </summary>
        /// <param name="op1">A <see cref="MTFloat"/>.</param>
        /// <param name="op2">A <b>MTFloat</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> greater than or equal to<i>op2</i>; otherwise <b>false</b>.;</returns>
        public static MTBoolean operator >=(MTFloat op1, MTFloat op2)
        {
            if (op1.IsNull || op2.IsNull) return MTBoolean.Null;
            return new MTBoolean(op1.Value >= op2.Value);
        }

        /// <summary>
        /// Determines whether first specified instances of MTFloat are less than second instance.
        /// </summary>
        /// <param name="op1">A <see cref="MTFloat"/>.</param>
        /// <param name="op2">A <b>MTFloat</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> less than <i>op2</i>; otherwise <b>false</b>.;</returns>
        public static MTBoolean operator <(MTFloat op1, MTFloat op2)
        {
            if (op1.IsNull || op2.IsNull) return MTBoolean.Null;
            return new MTBoolean(op1.Value < op2.Value);
        }

        /// <summary>
        /// Determines whether first specified instances of MTFloat are less than or equal to second instance.
        /// </summary>
        /// <param name="op1">A <see cref="MTFloat"/>.</param>
        /// <param name="op2">A <b>MTFloat</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> less than or equal to<i>op2</i>; otherwise <b>false</b>.;</returns>
        public static MTBoolean operator <=(MTFloat op1, MTFloat op2)
        {
            if (op1.IsNull || op2.IsNull) return MTBoolean.Null;
            return new MTBoolean(op1.Value <= op2.Value);
        }

        /// <summary>
        /// Computes the remainder after dividing first operand by second.
        /// </summary>
        /// <param name="op1">A <see cref="MTSingle"/>.</param>
        /// <param name="op2">A <b>MTFloat</b>.</param>
        /// <returns>A <see cref="MTFloat"/> instance.</returns>
        public static MTFloat operator %(MTFloat op1, MTFloat op2)
        {
            if (op1.IsNull || op2.IsNull) return MTFloat.Null;
            return new MTFloat(op1.Value % op2.Value);
        }

        /// <summary>
        /// Decrements its operand by 1.
        /// </summary>
        /// <param name="op1">A <b>MTFloat</b>.</param>
        /// <returns>A <see cref="MTFloat"/> instance.</returns>
        public static MTFloat operator ++(MTFloat op1)
        {
            if (op1.IsNull) return MTFloat.Null;
            double i = op1.Value;
            i++;
            return new MTFloat(i);
        }

        /// <summary>
        /// Decrements its operand by 1.
        /// </summary>
        /// <param name="op1">A <b>MTFloat</b>.</param>
        /// <returns>A <see cref="MTFloat"/> instance.</returns>
        public static MTFloat operator --(MTFloat op1)
        {
            if (op1.IsNull) return MTFloat.Null;
            double i = op1.Value;
            i--;
            return new MTFloat(i);
        }

        /// <summary>
        /// Implicitly creates a <see cref="MTFloat"/> instance that value represent the value of specified <see cref="double"/>.
        /// </summary>
        /// <param name="op1">A <see cref="double"/>.</param>
        /// <returns>A <see cref="MTFloat"/> instance.</returns>
        public static implicit operator MTFloat(double op1)
        {
            return new MTFloat(op1);
        }

        /// <summary>
        /// Implicitly creates a <see cref="MTFloat"/> instance that value represent the value of specified <see cref="double"/>.
        /// </summary>
        /// <param name="op1">A <see cref="double"/>.</param>
        /// <returns>A <see cref="MTFloat"/> instance.</returns>
        public static implicit operator MTFloat(double? op1)
        {
            return new MTFloat(op1);
        }

        /// <summary>
        /// Implicitly creates a <see cref="double"/> instance that value represent the value of specified <see cref="MTFloat"/>.
        /// </summary>
        /// <param name="op1">A <see cref="MTFloat"/>.</param>
        /// <returns>A <see cref="double"/> instance.</returns>
        public static implicit operator double(MTFloat op1)
        {
            if (op1.IsNull)
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("Operand can not be Null.\n{0}", Environment.StackTrace);
                throw new InvalidCastException("Operand can not be null.");
            }
            return op1.Value;
        }

        /// <summary>
        /// Implicitly creates a <see cref="double"/> instance that value represent the value of specified <see cref="MTFloat"/>.
        /// </summary>
        /// <param name="op1">A <see cref="MTFloat"/>.</param>
        /// <returns>A <see cref="double"/> instance.</returns>
        public static implicit operator double?(MTFloat op1)
        {
            if (op1.IsNull)
                return null;
            return op1.Value;
        }

        /// <summary>
        /// Explicitly creates a <see cref="MTSingle"/> instance that value represent the value of specified <see cref="MTFloat"/>.
        /// </summary>
        /// <param name="op1">A <see cref="MTFloat"/>.</param>
        /// <returns>A <see cref="MTSingle"/> instance.</returns>
        public static explicit operator MTSingle(MTFloat op1)
        {
            if (op1.IsNull) return MTSingle.Null;
            return new MTSingle((float)op1.Value);
        }

        /// <summary>
        /// Explicitly creates a <see cref="string"/> instance that value represent the value of specified <see cref="MTFloat"/>.
        /// </summary>
        /// <param name="op1">A <see cref="MTFloat"/>.</param>
        /// <returns>A <see cref="string"/> instance.</returns>
        public static explicit operator string(MTFloat op1)
        {
            if (op1.IsNull)
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("Operand can not be Null.\n{0}", Environment.StackTrace);
                throw new InvalidCastException("Operand can not be null.");
            }
            return op1.ToString();
        }
        #endregion

        /// <summary>
        /// Returns the <see cref="TypeCode"/> for this instance.
        /// </summary>
        /// <returns>The enumerated constant that is the <see cref="TypeCode"/> of the class or value type that implements this interface.</returns>
        public TypeCode GetTypeCode()
        {
            return ((IConvertible)Value).GetTypeCode();
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToBoolean"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        bool IConvertible.ToBoolean(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToBoolean(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToByte"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        byte IConvertible.ToByte(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToByte(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToChar"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        char IConvertible.ToChar(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToChar(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToDateTime"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        DateTime IConvertible.ToDateTime(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToDateTime(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToDecimal"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        decimal IConvertible.ToDecimal(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToDecimal(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToDouble"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        double IConvertible.ToDouble(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToDouble(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToInt16"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        short IConvertible.ToInt16(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToInt16(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToInt32"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        int IConvertible.ToInt32(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToInt32(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToInt64"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        long IConvertible.ToInt64(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToInt64(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToSByte"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        sbyte IConvertible.ToSByte(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToSByte(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToSingle"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        float IConvertible.ToSingle(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToSingle(provider);
        }

        /// <summary>
        /// Converts the double value of this instance to its equivalent string representation using the specified culture-specific format information. 
        /// </summary>
        /// <param name="provider">An <see cref="IFormatProvider"/> that supplies culture-specific formatting information.</param>
        /// <returns>The string representation of the value of this instance as specified by provider.</returns>
        public string ToString(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToString(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToType"/>. 
        /// </summary>
        /// <param name="conversionType"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        object IConvertible.ToType(Type conversionType, IFormatProvider provider)
        {
            return ((IConvertible)Value).ToType(conversionType, provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToUInt16"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        ushort IConvertible.ToUInt16(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToUInt16(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToUInt32"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        uint IConvertible.ToUInt32(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToUInt32(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToUInt64"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        ulong IConvertible.ToUInt64(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToUInt64(provider);
        }
    }
}