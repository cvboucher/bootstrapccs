//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Common
{
    /// <summary>
    /// All the MTField structures implement the INullable interface.
    /// </summary>
    public interface INullable
    {
        /// <summary>
        /// Indicates whether a value of MTField is null. This property is read-only.
        /// </summary>
        bool IsNull { get;}
    }
}
