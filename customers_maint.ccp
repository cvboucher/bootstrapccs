<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" validateRequest="True" scriptingSupport="Automatic" cachingDuration="1 minutes" masterPage="{CCS_PathToMasterPage}/MasterPage.ccp" useDesign="True" wizardTheme="None" needGeneration="0" wizardThemeVersion="3.0">
	<Components>
		<Panel id="13" visible="True" generateDiv="False" name="Head" contentPlaceholder="Head">
			<Components/>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="14" visible="True" generateDiv="False" name="Content" contentPlaceholder="Content">
			<Components>
				<Record id="18" sourceType="Table" urlType="Relative" secured="False" allowInsert="True" allowUpdate="True" allowDelete="True" validateData="True" preserveParameters="GET" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="customers" connection="IntranetDB" dataSource="customers" errorSummator="Error" allowCancel="True" recordDeleteConfirmation="True" buttonsType="button" wizardRecordKey="customer_id" encryptPasswordField="False" wizardUseInterVariables="False" pkIsAutoincrement="True" wizardCaption="Add/Edit Customers " wizardThemeApplyTo="Page" wizardFormMethod="post" wizardType="Record" changedCaptionRecord="False" recordDirection="Custom" templatePageRecord="D:/Users/CBoucher.CSUS2100/Documents/CodeChargeStudio5/Projects/BootstrapDesign/Templates/Record/BootstrapHorizontalForm.ccp|userTemplate" recordAddTemplatePanel="False" PathID="Contentcustomers" returnPage="customers_list.ccp">
					<Components>
						<Button id="21" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Insert" operation="Insert" wizardCaption="Add" PathID="ContentcustomersButton_Insert">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Button>
						<Button id="22" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Update" operation="Update" wizardCaption="Submit" PathID="ContentcustomersButton_Update">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Button>
						<Button id="23" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Delete" operation="Delete" wizardCaption="Delete" PathID="ContentcustomersButton_Delete">
							<Components/>
							<Events>
								<Event name="OnClick" type="Client">
									<Actions>
										<Action actionName="Confirmation Message" actionCategory="General" id="24" message="Delete record?"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</Button>
						<Button id="25" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Cancel" operation="Cancel" wizardCaption="Cancel" PathID="ContentcustomersButton_Cancel">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Button>
						<TextBox id="26" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="customer_name" fieldSource="customer_name" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Customer Name" caption="Customer Name" required="True" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentcustomerscustomer_name">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="27" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="customer_phone" fieldSource="customer_phone" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Customer Phone" caption="Customer Phone" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentcustomerscustomer_phone">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="28" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="customer_address" fieldSource="customer_address" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Customer Address" caption="Customer Address" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentcustomerscustomer_address">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="29" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="customer_city" fieldSource="customer_city" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Customer City" caption="Customer City" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentcustomerscustomer_city">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="30" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="customer_state" fieldSource="customer_state" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Customer State" caption="Customer State" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentcustomerscustomer_state">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="31" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="customer_zip" fieldSource="customer_zip" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardCaption="Customer Zip" caption="Customer Zip" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="Contentcustomerscustomer_zip">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
					</Components>
					<Events/>
					<TableParameters>
						<TableParameter id="20" conditionType="Parameter" useIsNull="False" field="customer_id" parameterSource="customer_id" dataType="Integer" logicOperator="And" searchConditionType="Equal" parameterType="URL" orderNumber="1"/>
					</TableParameters>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="19" tableName="customers"/>
					</JoinTables>
					<JoinLinks/>
					<Fields/>
					<PKFields/>
					<ISPParameters/>
					<ISQLParameters/>
					<IFormElements/>
					<USPParameters/>
					<USQLParameters/>
					<UConditions/>
					<UFormElements/>
					<DSPParameters/>
					<DSQLParameters/>
					<DConditions/>
					<SecurityGroups/>
					<Attributes/>
					<Features/>
				</Record>
			</Components>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="15" visible="True" generateDiv="False" name="Menu" contentPlaceholder="Menu">
			<Components>
				<IncludePage id="17" name="MenuIncludablePage" PathID="MenuMenuIncludablePage" page="MenuIncludablePage.ccp">
					<Components/>
					<Events/>
					<Features/>
				</IncludePage>
			</Components>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="16" visible="True" generateDiv="False" name="Footer" contentPlaceholder="Footer">
			<Components/>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
	</Components>
	<CodeFiles>
		<CodeFile id="1" language="C#InMotion" name="customers_maint.aspx" forShow="True" url="customers_maint.aspx" comment="&lt;!--" commentEnd="--&gt;" codePage="windows-1252"/>
		<CodeFile id="1.cs" language="C#InMotion" name="customers_maintEvents.aspx.cs" forShow="False" comment="//" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
