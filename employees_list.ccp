<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" validateRequest="True" scriptingSupport="Automatic" cachingDuration="1 minutes" masterPage="{CCS_PathToMasterPage}/MasterPage.ccp" useDesign="True" wizardTheme="None" needGeneration="0" wizardThemeVersion="3.0">
	<Components>
		<Panel id="97" visible="True" generateDiv="False" name="Head" contentPlaceholder="Head">
			<Components/>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="98" visible="True" generateDiv="False" name="Content" contentPlaceholder="Content">
			<Components>
				<Grid id="102" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" name="employees" connection="IntranetDB" dataSource="employees" pageSizeLimit="100" pageSize="False" wizardCaption="List of Employees " wizardThemeApplyTo="Page" wizardGridType="Custom" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="No records" wizardGridPagingType="Centered" wizardUseSearch="True" wizardAddNbsp="False" gridTotalRecords="False" wizardAddPanels="False" wizardType="Grid" wizardUseInterVariables="False" templatePage="D:/Users/CBoucher.CSUS2100/Documents/CodeChargeStudio5/Projects/BootstrapDesign/Templates/Grid/BootstrapTabular.ccp|userTemplate" addTemplatePanel="False" changedCaptionGrid="False" gridExtendedHTML="False" PathID="Contentemployees" composition="2" isParent="True" orderBy="emp_login">
					<Components>
						<Sorter id="117" visible="True" name="Sorter_emp_login" column="emp_login" wizardCaption="Emp Login" wizardSortingType="SimpleDir" wizardControl="emp_login" wizardAddNbsp="False" PathID="ContentemployeesSorter_emp_login">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="118" visible="True" name="Sorter_emp_password" column="emp_password" wizardCaption="Emp Password" wizardSortingType="SimpleDir" wizardControl="emp_password" wizardAddNbsp="False" PathID="ContentemployeesSorter_emp_password">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="119" visible="True" name="Sorter_emp_name" column="emp_name" wizardCaption="Emp Name" wizardSortingType="SimpleDir" wizardControl="emp_name" wizardAddNbsp="False" PathID="ContentemployeesSorter_emp_name">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="120" visible="True" name="Sorter_title" column="title" wizardCaption="Title" wizardSortingType="SimpleDir" wizardControl="title" wizardAddNbsp="False" PathID="ContentemployeesSorter_title">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="121" visible="True" name="Sorter_email" column="email" wizardCaption="Email" wizardSortingType="SimpleDir" wizardControl="email" wizardAddNbsp="False" PathID="ContentemployeesSorter_email">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="122" visible="True" name="Sorter_picture" column="picture" wizardCaption="Picture" wizardSortingType="SimpleDir" wizardControl="picture" wizardAddNbsp="False" PathID="ContentemployeesSorter_picture">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="123" visible="True" name="Sorter_phone_home" column="phone_home" wizardCaption="Phone Home" wizardSortingType="SimpleDir" wizardControl="phone_home" wizardAddNbsp="False" PathID="ContentemployeesSorter_phone_home">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="124" visible="True" name="Sorter_phone_work" column="phone_work" wizardCaption="Phone Work" wizardSortingType="SimpleDir" wizardControl="phone_work" wizardAddNbsp="False" PathID="ContentemployeesSorter_phone_work">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="125" visible="True" name="Sorter_phone_cell" column="phone_cell" wizardCaption="Phone Cell" wizardSortingType="SimpleDir" wizardControl="phone_cell" wizardAddNbsp="False" PathID="ContentemployeesSorter_phone_cell">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="126" visible="True" name="Sorter_fax" column="fax" wizardCaption="Fax" wizardSortingType="SimpleDir" wizardControl="fax" wizardAddNbsp="False" PathID="ContentemployeesSorter_fax">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="127" visible="True" name="Sorter_city" column="city" wizardCaption="City" wizardSortingType="SimpleDir" wizardControl="city" wizardAddNbsp="False" PathID="ContentemployeesSorter_city">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="128" visible="True" name="Sorter_zip" column="zip" wizardCaption="Zip" wizardSortingType="SimpleDir" wizardControl="zip" wizardAddNbsp="False" PathID="ContentemployeesSorter_zip">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="129" visible="True" name="Sorter_address" column="address" wizardCaption="Address" wizardSortingType="SimpleDir" wizardControl="address" wizardAddNbsp="False" PathID="ContentemployeesSorter_address">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Link id="130" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" preserveParameters="GET" name="emp_login" fieldSource="emp_login" wizardCaption="Emp Login" wizardIsPassword="False" wizardUseTemplateBlock="False" hrefSource="employees_maint.ccp" linkProperties="{'textSource':'','textSourceDB':'emp_login','hrefSource':'employees_maint.ccp','hrefSourceDB':'emp_login','title':'','target':'','name':'','linkParameters':{'0':{'sourceType':'DataField','parameterSource':'emp_id','parameterName':'emp_id'},'length':1,'objectType':'linkParameters'}}" wizardAddNbsp="False" PathID="Contentemployeesemp_login" urlType="Relative">
							<Components/>
							<Events/>
							<LinkParameters>
								<LinkParameter id="161" sourceType="DataField" name="emp_id" source="emp_id"/>
							</LinkParameters>
							<Attributes/>
							<Features/>
						</Link>
						<Label id="131" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="emp_password" fieldSource="emp_password" wizardCaption="Emp Password" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentemployeesemp_password">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="132" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="emp_name" fieldSource="emp_name" wizardCaption="Emp Name" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentemployeesemp_name">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="133" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="title" fieldSource="title" wizardCaption="Title" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentemployeestitle">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="134" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="email" fieldSource="email" wizardCaption="Email" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentemployeesemail">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="135" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="picture" fieldSource="picture" wizardCaption="Picture" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentemployeespicture">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="136" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="phone_home" fieldSource="phone_home" wizardCaption="Phone Home" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentemployeesphone_home">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="137" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="phone_work" fieldSource="phone_work" wizardCaption="Phone Work" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentemployeesphone_work">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="138" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="phone_cell" fieldSource="phone_cell" wizardCaption="Phone Cell" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentemployeesphone_cell">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="139" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="fax" fieldSource="fax" wizardCaption="Fax" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentemployeesfax">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="140" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="city" fieldSource="city" wizardCaption="City" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentemployeescity">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="141" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="zip" fieldSource="zip" wizardCaption="Zip" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentemployeeszip">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Label id="142" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="address" fieldSource="address" wizardCaption="Address" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentemployeesaddress">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Navigator id="143" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="of" wizardPageSize="False" wizardImagesScheme="None">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Navigator>
					</Components>
					<Events/>
					<TableParameters>
						<TableParameter id="163" conditionType="Parameter" useIsNull="False" dataType="Text" field="emp_login" logicOperator="And" orderNumber="1" parameterSource="s_emp_login" parameterType="URL" searchConditionType="Contains" searchFormParameter="True"/>
						<TableParameter id="164" conditionType="Parameter" useIsNull="False" dataType="Text" field="emp_password" logicOperator="And" orderNumber="2" parameterSource="s_emp_password" parameterType="URL" searchConditionType="Contains" searchFormParameter="True"/>
						<TableParameter id="165" conditionType="Parameter" useIsNull="False" dataType="Text" field="emp_name" logicOperator="And" orderNumber="3" parameterSource="s_emp_name" parameterType="URL" searchConditionType="Contains" searchFormParameter="True"/>
						<TableParameter id="166" conditionType="Parameter" useIsNull="False" dataType="Text" field="title" logicOperator="And" orderNumber="4" parameterSource="s_title" parameterType="URL" searchConditionType="Contains" searchFormParameter="True"/>
						<TableParameter id="167" conditionType="Parameter" useIsNull="False" dataType="Text" field="email" logicOperator="And" orderNumber="5" parameterSource="s_email" parameterType="URL" searchConditionType="Contains" searchFormParameter="True"/>
						<TableParameter id="168" conditionType="Parameter" useIsNull="False" dataType="Text" field="picture" logicOperator="And" orderNumber="6" parameterSource="s_picture" parameterType="URL" searchConditionType="Contains" searchFormParameter="True"/>
						<TableParameter id="169" conditionType="Parameter" useIsNull="False" dataType="Text" field="phone_home" logicOperator="And" orderNumber="7" parameterSource="s_phone_home" parameterType="URL" searchConditionType="Contains" searchFormParameter="True"/>
						<TableParameter id="170" conditionType="Parameter" useIsNull="False" dataType="Text" field="phone_work" logicOperator="And" orderNumber="8" parameterSource="s_phone_work" parameterType="URL" searchConditionType="Contains" searchFormParameter="True"/>
						<TableParameter id="171" conditionType="Parameter" useIsNull="False" dataType="Text" field="phone_cell" logicOperator="And" orderNumber="9" parameterSource="s_phone_cell" parameterType="URL" searchConditionType="Contains" searchFormParameter="True"/>
						<TableParameter id="172" conditionType="Parameter" useIsNull="False" dataType="Text" field="fax" logicOperator="And" orderNumber="10" parameterSource="s_fax" parameterType="URL" searchConditionType="Contains" searchFormParameter="True"/>
						<TableParameter id="173" conditionType="Parameter" useIsNull="False" dataType="Text" field="city" logicOperator="And" orderNumber="11" parameterSource="s_city" parameterType="URL" searchConditionType="Contains" searchFormParameter="True"/>
						<TableParameter id="174" conditionType="Parameter" useIsNull="False" dataType="Text" field="zip" logicOperator="And" orderNumber="12" parameterSource="s_zip" parameterType="URL" searchConditionType="Contains" searchFormParameter="True"/>
						<TableParameter id="175" conditionType="Parameter" useIsNull="False" dataType="Text" field="address" logicOperator="And" orderNumber="13" parameterSource="s_address" parameterType="URL" searchConditionType="Contains" searchFormParameter="True"/>
					</TableParameters>
					<JoinTables>
						<JoinTable id="162" posHeight="180" posLeft="10" posTop="10" posWidth="151" tableName="employees"/>
					</JoinTables>
					<JoinLinks/>
					<Fields>
						<Field id="176" fieldName="*"/>
					</Fields>
					<PKFields>
						<PKField id="177" dataType="Integer" fieldName="emp_id" tableName="employees"/>
					</PKFields>
					<SPParameters/>
					<SQLParameters/>
					<SecurityGroups/>
					<Attributes/>
					<Features/>
				</Grid>
				<Record id="144" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="employeesSearch" searchIds="144" fictitiousConnection="IntranetDB" wizardCaption="Search  " wizardOrientation="Custom" wizardFormMethod="post" gridSearchClearLink="True" wizardTypeComponent="Search" gridSearchType="And" wizardInteractiveSearch="False" gridSearchRecPerPage="True" wizardTypeButtons="button" wizardDefaultButton="True" gridSearchSortField="False" wizardUseInterVariables="False" templatePageSearch="D:/Users/CBoucher.CSUS2100/Documents/CodeChargeStudio5/Projects/BootstrapDesign/Templates/Search/BootstrapSearch.ccp|userTemplate" wizardThemeApplyTo="Page" addTemplatePanel="False" wizardType="Grid" returnPage="employees_list.ccp" PathID="ContentemployeesSearch" composition="2">
					<Components>
						<Button id="146" urlType="Relative" enableValidation="True" isDefault="True" name="Button_DoSearch" operation="Search" wizardCaption="Search" PathID="ContentemployeesSearchButton_DoSearch">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Button>
						<TextBox id="147" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_emp_login" fieldSource="emp_login" wizardIsPassword="False" wizardCaption="Emp Login" caption="Emp Login" required="False" unique="False" wizardSize="20" wizardMaxLength="20" PathID="ContentemployeesSearchs_emp_login">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="148" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_emp_password" fieldSource="emp_password" wizardIsPassword="False" wizardCaption="Emp Password" caption="Emp Password" required="False" unique="False" wizardSize="20" wizardMaxLength="20" PathID="ContentemployeesSearchs_emp_password">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="149" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_emp_name" fieldSource="emp_name" wizardIsPassword="False" wizardCaption="Emp Name" caption="Emp Name" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentemployeesSearchs_emp_name">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="150" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_title" fieldSource="title" wizardIsPassword="False" wizardCaption="Title" caption="Title" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentemployeesSearchs_title">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="151" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_email" fieldSource="email" wizardIsPassword="False" wizardCaption="Email" caption="Email" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentemployeesSearchs_email">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="152" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_picture" fieldSource="picture" wizardIsPassword="False" wizardCaption="Picture" caption="Picture" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentemployeesSearchs_picture">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="153" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_phone_home" fieldSource="phone_home" wizardIsPassword="False" wizardCaption="Phone Home" caption="Phone Home" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentemployeesSearchs_phone_home">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="154" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_phone_work" fieldSource="phone_work" wizardIsPassword="False" wizardCaption="Phone Work" caption="Phone Work" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentemployeesSearchs_phone_work">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="155" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_phone_cell" fieldSource="phone_cell" wizardIsPassword="False" wizardCaption="Phone Cell" caption="Phone Cell" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentemployeesSearchs_phone_cell">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="156" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_fax" fieldSource="fax" wizardIsPassword="False" wizardCaption="Fax" caption="Fax" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentemployeesSearchs_fax">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="157" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_city" fieldSource="city" wizardIsPassword="False" wizardCaption="City" caption="City" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentemployeesSearchs_city">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="158" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_zip" fieldSource="zip" wizardIsPassword="False" wizardCaption="Zip" caption="Zip" required="False" unique="False" wizardSize="20" wizardMaxLength="20" PathID="ContentemployeesSearchs_zip">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<TextBox id="159" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_address" fieldSource="address" wizardIsPassword="False" wizardCaption="Address" caption="Address" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentemployeesSearchs_address">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<ListBox id="160" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" returnValueType="Number" name="employeesPageSize" dataSource=";Select Value;5;5;10;10;25;25;100;100" wizardCaption="Records per page" wizardNoEmptyValue="True" PathID="ContentemployeesSearchemployeesPageSize">
							<Components/>
							<Events/>
							<TableParameters/>
							<SPParameters/>
							<SQLParameters/>
							<JoinTables/>
							<JoinLinks/>
							<Fields/>
							<PKFields/>
							<Attributes/>
							<Features/>
						</ListBox>
						<Link id="145" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="ClearParameters" hrefSource="employees_list.ccp" removeParameters="s_emp_login;s_emp_password;s_emp_name;s_title;s_email;s_picture;s_phone_home;s_phone_work;s_phone_cell;s_fax;s_city;s_zip;s_address;employeesPageSize" wizardThemeItem="SorterLink" wizardDefaultValue="Clear" PathID="ContentemployeesSearchClearParameters">
							<Components/>
							<Events/>
							<LinkParameters/>
							<Attributes/>
							<Features/>
						</Link>
</Components>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<PKFields/>
					<ISPParameters/>
					<ISQLParameters/>
					<IFormElements/>
					<USPParameters/>
					<USQLParameters/>
					<UConditions/>
					<UFormElements/>
					<DSPParameters/>
					<DSQLParameters/>
					<DConditions/>
					<SecurityGroups/>
					<Attributes/>
					<Features/>
				</Record>
			</Components>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="99" visible="True" generateDiv="False" name="Menu" contentPlaceholder="Menu">
			<Components>
				<IncludePage id="101" name="MenuIncludablePage" PathID="MenuMenuIncludablePage" page="MenuIncludablePage.ccp">
					<Components/>
					<Events/>
					<Features/>
				</IncludePage>
			</Components>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="100" visible="True" generateDiv="False" name="Footer" contentPlaceholder="Footer">
			<Components/>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
	</Components>
	<CodeFiles>
		<CodeFile id="1" language="C#InMotion" name="employees_list.aspx" forShow="True" url="employees_list.aspx" comment="&lt;!--" commentEnd="--&gt;" codePage="windows-1252"/>
		<CodeFile id="1.cs" language="C#InMotion" name="employees_listEvents.aspx.cs" forShow="False" comment="//" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
