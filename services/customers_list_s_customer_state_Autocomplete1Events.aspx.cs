//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-841B08D1
namespace BootstrapDesign.services
{
//End Namespace

//Page class @1-1ADE698A
public partial class customers_list_s_customer_state_Autocomplete1_Page : MTPage
{
//End Page class

//Page customers_list_s_customer_state_Autocomplete1 Event Load @1-D9372652
    protected void Page_Load(object sender, EventArgs e) {
//End Page customers_list_s_customer_state_Autocomplete1 Event Load

//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();
//End DataBind

//Page customers_list_s_customer_state_Autocomplete1 Load event tail @1-FCB6E20C
    }
//End Page customers_list_s_customer_state_Autocomplete1 Load event tail

//Page customers_list_s_customer_state_Autocomplete1 Event On PreInit @1-6204FEF7
    protected void Page_PreInit(object sender, EventArgs e) {
//End Page customers_list_s_customer_state_Autocomplete1 Event On PreInit

//MasterPageInit @1-60BC116D
        this.DesignMasterPagePath = "";
//End MasterPageInit

//ScriptIncludesInit @1-DCC237FD
        this.ScriptIncludes = "|";
//End ScriptIncludesInit

//Page customers_list_s_customer_state_Autocomplete1 On PreInit event tail @1-FCB6E20C
    }
//End Page customers_list_s_customer_state_Autocomplete1 On PreInit event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

