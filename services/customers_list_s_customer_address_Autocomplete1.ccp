<Page id="1" templateExtension="html" relativePath=".." fullRelativePath=".\services" secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="True" cachingEnabled="False" validateRequest="True" scriptingSupport="Automatic" cachingDuration="1 minutes" needGeneration="0">
	<Components>
		<Grid id="2" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" name="customers1" connection="IntranetDB" dataSource="customers" pageSizeLimit="100" wizardCaption="List of Customers1 ">
<Components>
<Label id="89" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="customer_address" fieldSource="customer_address">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Label>
</Components>
<Events/>
<TableParameters>
<TableParameter id="88" conditionType="Parameter" useIsNull="False" field="customer_address" dataType="Text" logicOperator="And" searchConditionType="BeginsWith" parameterType="URL" parameterSource="term"/>
</TableParameters>
<JoinTables/>
<JoinLinks/>
<Fields>
<Field id="87" tableName="customers" fieldName="customer_address"/>
</Fields>
<PKFields/>
<SPParameters/>
<SQLParameters/>
<SecurityGroups/>
<Attributes/>
<Features/>
</Grid>
</Components>
	<CodeFiles>
		<CodeFile id="1" language="C#InMotion" name="customers_list_s_customer_address_Autocomplete1.aspx" forShow="True" url="customers_list_s_customer_address_Autocomplete1.aspx" comment="&lt;!--" commentEnd="--&gt;" codePage="windows-1252"/>
<CodeFile id="1.cs" language="C#InMotion" name="customers_list_s_customer_address_Autocomplete1Events.aspx.cs" forShow="False" comment="//" codePage="windows-1252"/>
</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
