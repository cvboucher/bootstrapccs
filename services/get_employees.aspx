<!--ASPX page header @1-51DE258A-->
<%@ Page language="C#" CodeFile="get_employeesEvents.aspx.cs" Inherits="BootstrapDesign.services.get_employees_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASPX page header-->

<!--ASPX page @1-802112AB-->
<mt:Grid PageSizeLimit="100" RecordsPerPage="10" DataSourceID="employeesDataSource" ID="employees" runat="server">
<HeaderTemplate>[

</HeaderTemplate>
<ItemTemplate>{ "Emp Id" : "<mt:MTLabel Source="emp_id" DataType="Integer" ID="emp_id" runat="server"/>" , "Emp Login" : "<mt:MTLabel Source="emp_login" ID="emp_login" runat="server"/>" , "Emp Password" : "<mt:MTLabel Source="emp_password" ID="emp_password" runat="server"/>" , "Emp Name" : "<mt:MTLabel Source="emp_name" ID="emp_name" runat="server"/>" , "Title" : "<mt:MTLabel Source="title" ID="title" runat="server"/>" , "Group Id" : "<mt:MTLabel Source="group_id" DataType="Integer" ID="group_id" runat="server"/>" , "Department Id" : "<mt:MTLabel Source="department_id" DataType="Integer" ID="department_id" runat="server"/>" , "Email" : "<mt:MTLabel Source="email" ID="email" runat="server"/>" , "Picture" : "<mt:MTLabel Source="picture" ID="picture" runat="server"/>" , "Phone Home" : "<mt:MTLabel Source="phone_home" ID="phone_home" runat="server"/>" , "Phone Work" : "<mt:MTLabel Source="phone_work" ID="phone_work" runat="server"/>" , "Phone Cell" : "<mt:MTLabel Source="phone_cell" ID="phone_cell" runat="server"/>" , "Fax" : "<mt:MTLabel Source="fax" ID="fax" runat="server"/>" , "City" : "<mt:MTLabel Source="city" ID="city" runat="server"/>" , "Zip" : "<mt:MTLabel Source="zip" ID="zip" runat="server"/>" , "Address" : "<mt:MTLabel Source="address" ID="address" runat="server"/>" , "Employee Is Active" : "<mt:MTLabel Source="employee_is_active" DataType="Boolean" ID="employee_is_active" runat="server"/>" } 
</ItemTemplate>
<FooterTemplate>]
</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="employeesDataSource" ConnectionString="InMotion:IntranetDB" SelectCommandType="Table" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM employees {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM employees
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="Urls_emp_login" SourceType="URL" Source="s_emp_login" DataType="Text" Operation="And" Condition="Contains" SourceColumn="emp_login"/>
     <mt:WhereParameter Name="Urls_emp_name" SourceType="URL" Source="s_emp_name" DataType="Text" Operation="And" Condition="Contains" SourceColumn="emp_name"/>
   </SelectParameters>
</mt:MTDataSource>








<!--End ASPX page-->

