<Page id="1" templateExtension="html" relativePath=".." fullRelativePath=".\services" secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="True" cachingEnabled="False" validateRequest="True" scriptingSupport="Automatic" cachingDuration="1 minutes" wizardTheme="None" wizardThemeVersion="3.0" useDesign="False" needGeneration="0">
	<Components>
		<Grid id="2" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" name="employees" connection="IntranetDB" dataSource="employees" pageSizeLimit="100" pageSize="False" wizardCaption="List of Employees " wizardThemeApplyTo="Page" wizardGridType="Custom" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="No records" wizardUseSearch="False" wizardAddNbsp="False" gridTotalRecords="False" wizardAddPanels="False" wizardType="Grid" wizardUseInterVariables="False" templatePage="C:/Program Files (x86)/CodeChargeStudio5/Templates/Grid/jQueryData.ccp|ccsTemplate" addTemplatePanel="False" changedCaptionGrid="False" gridExtendedHTML="False" PathID="employees">
			<Components>
				<Label id="4" fieldSourceType="DBColumn" dataType="Integer" html="False" generateSpan="False" name="emp_id" fieldSource="emp_id" wizardCaption="Emp Id" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeesemp_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="5" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="emp_login" fieldSource="emp_login" wizardCaption="Emp Login" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeesemp_login">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="6" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="emp_password" fieldSource="emp_password" wizardCaption="Emp Password" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeesemp_password">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="7" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="emp_name" fieldSource="emp_name" wizardCaption="Emp Name" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeesemp_name">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="8" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="title" fieldSource="title" wizardCaption="Title" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeestitle">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="9" fieldSourceType="DBColumn" dataType="Integer" html="False" generateSpan="False" name="group_id" fieldSource="group_id" wizardCaption="Group Id" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeesgroup_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="10" fieldSourceType="DBColumn" dataType="Integer" html="False" generateSpan="False" name="department_id" fieldSource="department_id" wizardCaption="Department Id" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeesdepartment_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="11" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="email" fieldSource="email" wizardCaption="Email" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeesemail">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="12" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="picture" fieldSource="picture" wizardCaption="Picture" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeespicture">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="13" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="phone_home" fieldSource="phone_home" wizardCaption="Phone Home" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeesphone_home">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="14" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="phone_work" fieldSource="phone_work" wizardCaption="Phone Work" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeesphone_work">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="15" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="phone_cell" fieldSource="phone_cell" wizardCaption="Phone Cell" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeesphone_cell">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="16" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="fax" fieldSource="fax" wizardCaption="Fax" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeesfax">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="17" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="city" fieldSource="city" wizardCaption="City" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeescity">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="18" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="zip" fieldSource="zip" wizardCaption="Zip" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeeszip">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="19" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="address" fieldSource="address" wizardCaption="Address" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeesaddress">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="20" fieldSourceType="DBColumn" dataType="Boolean" html="False" generateSpan="False" name="employee_is_active" fieldSource="employee_is_active" wizardCaption="Employee Is Active" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="employeesemployee_is_active">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
			</Components>
			<Events/>
			<TableParameters>
<TableParameter id="22" conditionType="Parameter" useIsNull="False" dataType="Text" field="emp_login" logicOperator="And" parameterSource="s_emp_login" parameterType="URL" searchConditionType="Contains"/>
<TableParameter id="23" conditionType="Parameter" useIsNull="False" dataType="Text" field="emp_name" logicOperator="And" parameterSource="s_emp_name" parameterType="URL" searchConditionType="Contains"/>
</TableParameters>
			<JoinTables>
				<JoinTable id="21" posHeight="180" posLeft="10" posTop="10" posWidth="151" tableName="employees"/>
</JoinTables>
			<JoinLinks/>
			<Fields>
<Field id="24" fieldName="*"/>
</Fields>
			<PKFields>
<PKField id="25" dataType="Integer" fieldName="emp_id" tableName="employees"/>
</PKFields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
	</Components>
	<CodeFiles>
		<CodeFile id="1" language="C#InMotion" name="get_employees.aspx" forShow="True" url="get_employees.aspx" comment="&lt;!--" commentEnd="--&gt;" codePage="windows-1252"/>
		<CodeFile id="1.cs" language="C#InMotion" name="get_employeesEvents.aspx.cs" forShow="False" comment="//" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
