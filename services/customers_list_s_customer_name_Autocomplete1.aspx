<!--ASPX page header @1-88816699-->
<%@ Page language="C#" CodeFile="customers_list_s_customer_name_Autocomplete1Events.aspx.cs" Inherits="BootstrapDesign.services.customers_list_s_customer_name_Autocomplete1_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASPX page header-->

<!--ASPX page @1-5D76EFB5-->
<mt:Grid PageSizeLimit="100" RecordsPerPage="10" DataSourceID="customers1DataSource" ID="customers1" runat="server">
<HeaderTemplate>[
</HeaderTemplate>
<ItemTemplate>{ "customer_name" : "<mt:MTLabel Source="customer_name" ID="customer_name" runat="server"/>" }
</ItemTemplate>
<SeparatorTemplate>, 
</SeparatorTemplate>
<FooterTemplate>]
</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="customers1DataSource" ConnectionString="InMotion:IntranetDB" SelectCommandType="Table" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} customer_name 
FROM customers {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM customers
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="Urlterm" SourceType="URL" Source="term" DataType="Text" Operation="And" Condition="BeginsWith" SourceColumn="customer_name"/>
   </SelectParameters>
</mt:MTDataSource>








<!--End ASPX page-->

