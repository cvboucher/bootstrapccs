<!--ASPX page header @1-215BB647-->
<%@ Page language="C#" CodeFile="project_gridEvents.aspx.cs" Inherits="BootstrapDesign.project_grid_Page" MasterPageFile="~/Designs/Bootstrap/MasterPage.master"  %>
<%@ Register TagPrefix="BootstrapDesign" TagName="MenuIncludablePage" Src="MenuIncludablePage.ascx" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASPX page header-->

<!--ASPX page @1-6DDC12AA-->

<asp:Content ContentPlaceHolderID="Head" ID="Head_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Head" runat="server">
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-65835CB2
</script>
<script type="text/javascript" charset="windows-1252" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//Common Script Start @1-8BFA436B
jQuery(function ($) {
    var features = { };
    var actions = { };
    var params = { };
//End Common Script Start

//Plugin Calls @1-E5D0DA8A
    $('*:ccsControl(Content, projects, Cancel)').ccsBind(function() {
        this.bind("click", function(){ $("body").data("disableValidation", true); });
    });
//End Plugin Calls

//Common Script End @1-562554DE
});
//End Common Script End

//End CCS script
</script>


<title>project_grid</title>

<meta content="text/html; charset=windows-1252" http-equiv="Content-Type">
</mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Content" ID="Content_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Content" runat="server">
<mt:Record ID="projects1" runat="server"><ItemTemplate><div data-emulate-form="Contentprojects1" id="Contentprojects1">
<div role="form" class="form-horizontal">
  



    <legend>Search </legend>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <div class="row">
      <div class="col-sm-offset-2 col-sm-10">
        <div role="alert" class="alert alert-danger">
          <mt:MTLabel id="ErrorLabel" runat="server"/> 
        </div>
      </div>
    </div>
    </mt:MTPanel><fieldset>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Project Name</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="project_name" Caption="Project Name" maxlength="50" Columns="50" ID="s_project_name" data-id="Contentprojects1s_project_name" runat="server"/>
      </div>
      <div class="col-sm-2 control-label">
        <label for="<%# projects1.GetControl<InMotion.Web.Controls.MTListBox>("PageSize").ClientID %>">Records per page</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTListBox Rows="1" ID="PageSize" data-id="Contentprojects1PageSize" runat="server">
          <asp:ListItem Value="" Text="Select Value"/>
          <asp:ListItem Value="5" Text="5"/>
          <asp:ListItem Value="10" Text="10"/>
          <asp:ListItem Value="25" Text="25"/>
          <asp:ListItem Value="100" Text="100"/>
        </mt:MTListBox>
 
      </div>
    </div>
    </fieldset> 
    <div class="form-group">
      <div class="col-sm-2 text-right">
<mt:MTLink Text="Clear" ID="ClearParameters" data-id="Contentprojects1ClearParameters" runat="server" HrefSource="~/project_grid.aspx" PreserveParameters="Get" RemoveParameters="s_project_name;OrderOrder;OrderDir;PageSize"/> 
      </div>
      <div class="col-sm-10">
        <mt:MTButton CommandName="Search" Text="Search" CssClass="Button" ID="Button_DoSearch" data-id="Contentprojects1Button_DoSearch" runat="server"/>
      </div>
    </div>
  

</div>

</div></ItemTemplate></mt:Record>
<mt:EditableGrid DeleteControl="CheckBox_Delete" PreserveParameters="Get" PageSizeLimit="100" RecordsPerPage="10" SortExpression="project_name" DataSourceID="projectsDataSource" EmptyRows="3" ID="projects" runat="server">
<HeaderTemplate><div data-emulate-form="Contentprojects" id="Contentprojects">
<div class="panel panel-primary">
  

     
    <div class="panel-heading">
      <strong>Add/Edit Projects </strong>
    </div>
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-hover" cellspacing="0" cellpadding="0">
          <mt:MTPanel id="Error" visible="False" runat="server">
          <tr role="alert" class="alert alert-danger">
            <td colspan="3"><mt:MTLabel id="ErrorLabel" runat="server"/> </td>
          </tr>
          </mt:MTPanel>
          <tr>
            <th scope="col">
            <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="project_name" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Project Name" ID="Sorter_project_name" data-id="ContentprojectsSorter_project_name" runat="server"/></th>
 
            <th scope="col">
            <mt:MTSorter AscOnImage="{CCS_PathToMasterPage}Images/SorterAscActive.png" DescOnImage="{CCS_PathToMasterPage}Images/SorterDescActive.png" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="project_is_active" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Project Is Active" ID="Sorter_project_is_active" data-id="ContentprojectsSorter_project_is_active" runat="server"/></th>
 
            <th scope="col">Delete</th>
          </tr>
 
          
</HeaderTemplate>
<ItemTemplate>
          <mt:MTPanel id="RowError" visible="False" runat="server">
          <tr role="alert" class="alert alert-danger">
            <td colspan="3"><mt:MTLabel id="RowErrorLabel" runat="server"/> </td>
          </tr>
          </mt:MTPanel>
          <tr>
            <td><mt:MTTextBox Source="project_name" Caption="Project Name" maxlength="50" Columns="50" ID="project_name" data-id="Contentprojectsproject_name_{projects:rowNumber}" runat="server"/></td> 
            <td><mt:MTCheckBox Source="project_is_active" DataType="Boolean" OnLoad="projectsproject_is_active_Load" ID="project_is_active" data-id="Contentprojectsproject_is_active_{projects:rowNumber}" runat="server"/></td> 
            <td>
              <mt:MTPanel GenerateDiv="false" ID="CheckBox_Delete_Panel" runat="server">
              <mt:MTCheckBox SourceType="CodeExpression" DataType="Boolean" OnInit="projectsCheckBox_Delete_Init" OnLoad="projectsCheckBox_Delete_Load" ID="CheckBox_Delete" data-id="ContentprojectsCheckBox_Delete_PanelCheckBox_Delete_{projects:rowNumber}" runat="server"/></mt:MTPanel></td>
          </tr>
 
</ItemTemplate>
<FooterTemplate>
          <mt:MTPanel id="NoRecords" visible="False" runat="server">
          <tr class="NoRecords">
            <td colspan="3">No records</td>
          </tr>
          </mt:MTPanel>
          <tr class="Footer">
            <td style="TEXT-ALIGN: right" colspan="3">
              <mt:MTNavigator FirstOnValue="First " FirstOffValue="First " PreviousOnValue="Prev " PreviousOffValue="Prev " NextOnValue="Next " NextOffValue="Next " LastOnValue="Last " LastOffValue="Last " PageOffPreValue="" PageOffPostValue="&amp;nbsp;" DisplayStyle="Links" PageLinksNumber="10" PageNumbersStyle="CurrentPageCentered" TotalPagesText="of" ShowTotalPages="true" ID="Navigator" runat="server"/>
              <mt:MTButton CommandName="Submit" Text="Submit" ID="Button_Submit" data-id="ContentprojectsButton_Submit" runat="server"/>
              <mt:MTButton CommandName="Cancel" EnableValidation="False" Text="Cancel" ID="Cancel" data-id="ContentprojectsCancel" runat="server"/></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</FooterTemplate>
</mt:EditableGrid>
<mt:MTDataSource ID="projectsDataSource" ConnectionString="InMotion:IntranetDB" SelectCommandType="Table" SelectOrderBy="project_name" CountCommandType="Table" InsertCommandType="Table" UpdateCommandType="Table" DeleteCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM projects {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM projects
   </CountCommand>
   <InsertCommand>
INSERT INTO projects(project_name, project_is_active) VALUES ({project_name}, {project_is_active})
  </InsertCommand>
   <UpdateCommand>
UPDATE projects SET project_name={project_name}, project_is_active={project_is_active}
  </UpdateCommand>
   <DeleteCommand>
DELETE FROM projects
  </DeleteCommand>
   <PrimaryKeys>
     <mt:PrimaryKeyInfo FieldName="project_id" Type="Integer" />
   </PrimaryKeys>
   <SelectParameters>
     <mt:WhereParameter Name="Urls_project_name" SourceType="URL" Source="s_project_name" DataType="Text" Operation="And" Condition="Contains" SourceColumn="project_name"/>
   </SelectParameters>
</mt:MTDataSource><br>
</mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Menu" ID="Menu_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Menu" runat="server"><BootstrapDesign:MenuIncludablePage ID="MenuIncludablePage" runat="server"/></mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Footer" ID="Footer_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Footer" runat="server">&nbsp;</mt:MTPanel></asp:Content>
<!--End ASPX page-->

