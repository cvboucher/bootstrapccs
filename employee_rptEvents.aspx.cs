//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-B50C8C34
namespace BootstrapDesign
{
//End Namespace

//Page class @1-25384AC6
public partial class employee_rpt_Page : MTPage
{
//End Page class

//Page employee_rpt Event Load @1-D9372652
    protected void Page_Load(object sender, EventArgs e) {
//End Page employee_rpt Event Load

//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();
//End DataBind

//Page employee_rpt Load event tail @1-FCB6E20C
    }
//End Page employee_rpt Load event tail

//Page employee_rpt Event On PreInit @1-6204FEF7
    protected void Page_PreInit(object sender, EventArgs e) {
//End Page employee_rpt Event On PreInit

//MasterPageInit @1-25245C17
        this.DesignMasterPagePath = "{CCS_PathToMasterPage}/MasterPage.master";
//End MasterPageInit

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page employee_rpt On PreInit event tail @1-FCB6E20C
    }
//End Page employee_rpt On PreInit event tail

//Record employees_departments Event Before Show @44-09175D60
    protected void employees_departments_BeforeShow(object sender, EventArgs e) {
//End Record employees_departments Event Before Show

//Record employees_departments Event Before Show. Action Hide-Show Component @53-BE1CF7FC
        MTText UrlViewMode_53_1 = MTText.Parse((new InMotion.Web.Controls.DataParameter(DataParameterSourceType.Url,"ViewMode")).Evaluate(((IMTControl)sender).OwnerForm as Control));
        MTText ExprParam2_53_2 = MTText.Parse((new InMotion.Web.Controls.DataParameter(DataParameterSourceType.Expression,"Print")).Evaluate(((IMTControl)sender).OwnerForm as Control));
        if (UrlViewMode_53_1 == ExprParam2_53_2) {
            employees_departments.Visible = false;
        }
//End Record employees_departments Event Before Show. Action Hide-Show Component

//Record employees_departments Before Show event tail @44-FCB6E20C
    }
//End Record employees_departments Before Show event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

