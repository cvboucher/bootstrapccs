//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-B50C8C34
namespace BootstrapDesign
{
//End Namespace

//Page class @1-E2810D5D
public partial class project_grid_Page : MTPage
{
//End Page class

//Attributes constants @1-2F9F6C23
    public const string Attribute_rowNumber = "rowNumber";
//End Attributes constants

//Page project_grid Event Load @1-D9372652
    protected void Page_Load(object sender, EventArgs e) {
//End Page project_grid Event Load

//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();
//End DataBind

//Page project_grid Load event tail @1-FCB6E20C
    }
//End Page project_grid Load event tail

//Page project_grid Event On PreInit @1-6204FEF7
    protected void Page_PreInit(object sender, EventArgs e) {
//End Page project_grid Event On PreInit

//MasterPageInit @1-25245C17
        this.DesignMasterPagePath = "{CCS_PathToMasterPage}/MasterPage.master";
//End MasterPageInit

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page project_grid On PreInit event tail @1-FCB6E20C
    }
//End Page project_grid On PreInit event tail

//CheckBox project_is_active Event Load @13-5A9A3311
    protected void projectsproject_is_active_Load(object sender, EventArgs e) {
//End CheckBox project_is_active Event Load

//Set Default Value @13-083A3319
        ((InMotion.Web.Controls.MTCheckBox)sender).DefaultValue = ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue;
//End Set Default Value

//CheckBox project_is_active Load event tail @13-FCB6E20C
    }
//End CheckBox project_is_active Load event tail

//CheckBox CheckBox_Delete Event Init @15-866C7102
    protected void projectsCheckBox_Delete_Init(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Init

//Set Checked Value @15-A3604378
        ((InMotion.Web.Controls.MTCheckBox)sender).CheckedValue = true;
//End Set Checked Value

//Set Unchecked Value @15-4403792B
        ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue = false;
//End Set Unchecked Value

//CheckBox CheckBox_Delete Init event tail @15-FCB6E20C
    }
//End CheckBox CheckBox_Delete Init event tail

//CheckBox CheckBox_Delete Event Load @15-7B5384B3
    protected void projectsCheckBox_Delete_Load(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Load

//Set Default Value @15-083A3319
        ((InMotion.Web.Controls.MTCheckBox)sender).DefaultValue = ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue;
//End Set Default Value

//CheckBox CheckBox_Delete Load event tail @15-FCB6E20C
    }
//End CheckBox CheckBox_Delete Load event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

