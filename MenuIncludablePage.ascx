<!--ASCX page header @1-BA5CBAC9-->
<%@ Control language="C#" CodeFile="MenuIncludablePageEvents.ascx.cs" Inherits="BootstrapDesign.MenuIncludablePage_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-C6286B40-->
<!--
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-5EA5EF98
</script>
<script src="ClientI18N.aspx?file=globalize.js&locale=<%#ResManager.GetString("CCS_LocaleID")%>" type="text/javascript" charset="windows-1252"></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-12EF267B
</script>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>
-->
<mt:MTPanel GenerateDiv="false" OnBeforeShow="Panel1_BeforeShow" ID="Panel1" runat="server">
<nav class="navbar navbar-inverse hidden-print" role="navigation">
<div class="container-fluid">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">





<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> <a href="#" class="navbar-brand">Bootstrap CCS</a> 
  </div>
 
  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      <li><mt:MTLink Text="Customers" ID="Link1" data-id="MenuIncludablePagePanel1Link1" runat="server" HrefSource="~/customers_list.aspx" PreserveParameters="Get"/></li>
 
      <li><mt:MTLink Text="Employees" ID="Link2" data-id="MenuIncludablePagePanel1Link2" runat="server" HrefSource="~/employees_list.aspx" PreserveParameters="Get"/></li>
 
      <li><mt:MTLink Text="Groups" ID="Link3" data-id="MenuIncludablePagePanel1Link3" runat="server" HrefSource="~/groups_list.aspx" PreserveParameters="Get"/></li>
 
      <li><mt:MTLink Text="Projects" ID="Link4" data-id="MenuIncludablePagePanel1Link4" runat="server" HrefSource="~/project_grid.aspx" PreserveParameters="Get"/></li>
 
      <li><mt:MTLink Text="Employee Report" ID="Link5" data-id="MenuIncludablePagePanel1Link5" runat="server" HrefSource="~/employee_rpt.aspx" PreserveParameters="Get"/></li>
 
      <li><a href="BootstrapDesignProject.zip">Download Project</a></li>
 
    </ul>
 
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#">Logout</a></li>
 
    </ul>
 
  </div>
  <!-- /.navbar-collapse -->
</div>
<!-- /.container-fluid -->
</nav>
</mt:MTPanel>



<!--End ASCX page-->

