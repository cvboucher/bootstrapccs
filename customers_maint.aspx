<!--ASPX page header @1-02CCF596-->
<%@ Page language="C#" CodeFile="customers_maintEvents.aspx.cs" Inherits="BootstrapDesign.customers_maint_Page" MasterPageFile="~/Designs/Bootstrap/MasterPage.master"  %>
<%@ Register TagPrefix="BootstrapDesign" TagName="MenuIncludablePage" Src="MenuIncludablePage.ascx" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASPX page header-->

<!--ASPX page @1-03181542-->

<asp:Content ContentPlaceHolderID="Head" ID="Head_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Head" runat="server">
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-65835CB2
</script>
<script type="text/javascript" charset="windows-1252" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//Common Script Start @1-8BFA436B
jQuery(function ($) {
    var features = { };
    var actions = { };
    var params = { };
//End Common Script Start

//ContentcustomersButton_DeleteOnClick Event Start @-538DEE13
    actions["ContentcustomersButton_DeleteOnClick"] = function (eventType, parameters) {
        var result = true;
//End ContentcustomersButton_DeleteOnClick Event Start

//Confirmation Message @24-8243B274
        return confirm('Delete record?');
//End Confirmation Message

//ContentcustomersButton_DeleteOnClick Event End @-A5B9ECB8
        return result;
    };
//End ContentcustomersButton_DeleteOnClick Event End

//Event Binding @1-DB603893
    $('*:ccsControl(Content, customers, Button_Delete)').ccsBind(function() {
        this.bind("click", actions["ContentcustomersButton_DeleteOnClick"]);
    });
//End Event Binding

//Plugin Calls @1-355F6876
    $('*:ccsControl(Content, customers, Button_Delete)').ccsBind(function() {
        this.bind("click", function(){ $("body").data("disableValidation", true); });
    });
    $('*:ccsControl(Content, customers, Button_Cancel)').ccsBind(function() {
        this.bind("click", function(){ $("body").data("disableValidation", true); });
    });
//End Plugin Calls

//Common Script End @1-562554DE
});
//End Common Script End

//End CCS script
</script>


<title>Customers</title>

<meta content="text/html; charset=windows-1252" http-equiv="Content-Type">
</mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Content" ID="Content_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Content" runat="server"><br>
&nbsp; 
<mt:Record PreserveParameters="Get" ReturnPage="~/customers_list.aspx" DataSourceID="customersDataSource" ID="customers" runat="server"><ItemTemplate><div data-emulate-form="Contentcustomers" id="Contentcustomers">
<div class="form-horizontal">
  



    <legend>Add/Edit Customers </legend>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <div class="row">
      <div class="col-sm-offset-2 col-sm-10">
        <div role="alert" class="alert alert-danger">
          <span id="ErrorBlock"><mt:MTLabel id="ErrorLabel" runat="server"/></span> 
        </div>
      </div>
    </div>
    </mt:MTPanel><fieldset>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Customer Name</label> 
      </div>
      <div class="col-sm-10">
        <mt:MTTextBox Source="customer_name" Required="True" Caption="Customer Name" maxlength="50" Columns="50" ID="customer_name" data-id="Contentcustomerscustomer_name" runat="server"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Customer Phone</label> 
      </div>
      <div class="col-sm-10">
        <mt:MTTextBox Source="customer_phone" Caption="Customer Phone" maxlength="50" Columns="50" ID="customer_phone" data-id="Contentcustomerscustomer_phone" runat="server"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Customer Address</label> 
      </div>
      <div class="col-sm-10">
        <mt:MTTextBox Source="customer_address" Caption="Customer Address" maxlength="50" Columns="50" ID="customer_address" data-id="Contentcustomerscustomer_address" runat="server"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Customer City</label> 
      </div>
      <div class="col-sm-10">
        <mt:MTTextBox Source="customer_city" Caption="Customer City" maxlength="50" Columns="50" ID="customer_city" data-id="Contentcustomerscustomer_city" runat="server"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Customer State</label> 
      </div>
      <div class="col-sm-10">
        <mt:MTTextBox Source="customer_state" Caption="Customer State" maxlength="50" Columns="50" ID="customer_state" data-id="Contentcustomerscustomer_state" runat="server"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Customer Zip</label> 
      </div>
      <div class="col-sm-10">
        <mt:MTTextBox Source="customer_zip" Caption="Customer Zip" maxlength="50" Columns="50" ID="customer_zip" data-id="Contentcustomerscustomer_zip" runat="server"/>
      </div>
    </div>
    </fieldset> 
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <mt:MTButton CommandName="Insert" Text="Add" CssClass="Button" ID="Button_Insert" data-id="ContentcustomersButton_Insert" runat="server"/>
        <mt:MTButton CommandName="Update" Text="Submit" CssClass="Button" ID="Button_Update" data-id="ContentcustomersButton_Update" runat="server"/>
        <mt:MTButton CommandName="Delete" EnableValidation="False" Text="Delete" CssClass="Button" ID="Button_Delete" data-id="ContentcustomersButton_Delete" runat="server"/>
        <mt:MTButton CommandName="Cancel" EnableValidation="False" Text="Cancel" CssClass="Button" ID="Button_Cancel" data-id="ContentcustomersButton_Cancel" runat="server"/>
      </div>
    </div>
  

</div>

</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="customersDataSource" ConnectionString="InMotion:IntranetDB" SelectCommandType="Table" InsertCommandType="Table" UpdateCommandType="Table" DeleteCommandType="Table" runat="server">
   <SelectCommand>
SELECT * 
FROM customers {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <InsertCommand>
INSERT INTO customers(customer_name, customer_phone, customer_address, customer_city, customer_state, customer_zip) VALUES ({customer_name}, {customer_phone}, {customer_address}, {customer_city}, {customer_state}, {customer_zip})
  </InsertCommand>
   <UpdateCommand>
UPDATE customers SET customer_name={customer_name}, customer_phone={customer_phone}, customer_address={customer_address}, customer_city={customer_city}, customer_state={customer_state}, customer_zip={customer_zip}
  </UpdateCommand>
   <DeleteCommand>
DELETE FROM customers
  </DeleteCommand>
   <SelectParameters>
     <mt:WhereParameter Name="Urlcustomer_id" SourceType="URL" Source="customer_id" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="customer_id" Required="true"/>
   </SelectParameters>
</mt:MTDataSource><br>
</mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Menu" ID="Menu_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Menu" runat="server"><BootstrapDesign:MenuIncludablePage ID="MenuIncludablePage" runat="server"/></mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Footer" ID="Footer_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Footer" runat="server">&nbsp;</mt:MTPanel></asp:Content>
<!--End ASPX page-->

