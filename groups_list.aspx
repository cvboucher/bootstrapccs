<!--ASPX page header @1-B9EDE975-->
<%@ Page language="C#" CodeFile="groups_listEvents.aspx.cs" Inherits="BootstrapDesign.groups_list_Page" MasterPageFile="~/Designs/Bootstrap/MasterPage.master"  %>
<%@ Register TagPrefix="BootstrapDesign" TagName="MenuIncludablePage" Src="MenuIncludablePage.ascx" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASPX page header-->

<!--ASPX page @1-50A8E5FD-->

<asp:Content ContentPlaceHolderID="Head" ID="Head_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Head" runat="server">
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-65835CB2
</script>
<script type="text/javascript" charset="windows-1252" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>


<title>groups</title>

<meta content="text/html; charset=windows-1252" http-equiv="Content-Type">
</mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Content" ID="Content_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Content" runat="server">
<mt:Record ReturnPage="~/groups_list.aspx" ID="groupsSearch" runat="server"><ItemTemplate><div data-emulate-form="ContentgroupsSearch" id="ContentgroupsSearch">
<div role="form" class="form-horizontal">
  



    <legend>Search</legend>
    
    <mt:MTPanel id="Error" visible="False" runat="server">
    <div class="row">
      <div class="col-sm-offset-2 col-sm-10">
        <div role="alert" class="alert alert-danger">
          <mt:MTLabel id="ErrorLabel" runat="server"/> 
        </div>
      </div>
    </div>
    </mt:MTPanel><fieldset>
    <div class="form-group">
      <div class="col-sm-2 control-label">
        <label>Group Name</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTTextBox Source="group_name" Caption="Group Name" maxlength="50" Columns="50" ID="s_group_name" data-id="ContentgroupsSearchs_group_name" runat="server"/>
      </div>
      <div class="col-sm-2 control-label">
        <label for="<%# groupsSearch.GetControl<InMotion.Web.Controls.MTListBox>("groupsPageSize").ClientID %>">Records per page</label> 
      </div>
      <div class="col-sm-4">
        <mt:MTListBox Rows="1" ID="groupsPageSize" data-id="ContentgroupsSearchgroupsPageSize" runat="server">
          <asp:ListItem Value="" Text="Select Value"/>
          <asp:ListItem Value="5" Text="5"/>
          <asp:ListItem Value="10" Text="10"/>
          <asp:ListItem Value="25" Text="25"/>
          <asp:ListItem Value="100" Text="100"/>
        </mt:MTListBox>
 
      </div>
    </div>
    </fieldset> 
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <mt:MTButton CommandName="Search" Text="Search" CssClass="Button" ID="Button_DoSearch" data-id="ContentgroupsSearchButton_DoSearch" runat="server"/>
        <mt:MTLink Text="Clear" CssClass="btn btn-default" ID="ClearParameters" data-id="ContentgroupsSearchClearParameters" runat="server" HrefSource="~/groups_list.aspx" PreserveParameters="Get" RemoveParameters="s_group_name;groupsPageSize"/>
      </div>
    </div>
  

</div>

</div></ItemTemplate></mt:Record><br>
<mt:Grid PageSizeLimit="100" RecordsPerPage="10" SortExpression="group_name" DataSourceID="groupsDataSource" ID="groups" runat="server">
<HeaderTemplate>
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-primary">
      <div class="panel-heading">
        List of Groups 
      </div>
      <div class="panel-body">
        <div class="row">
          
</HeaderTemplate>
<ItemTemplate>
          <div class="col-md-4 col-sm-6">
            <div class="well">
              Group ID: <mt:MTLink Source="group_id" ID="Link1" data-id="ContentgroupsLink1_{groups:rowNumber}" runat="server" HrefSource="~/groups_maint.aspx" PreserveParameters="Get"><Parameters>
                <mt:UrlParameter Name="group_id" SourceType="DataSourceColumn" Source="group_id" Format="yyyy-MM-dd"/>
              </Parameters></mt:MTLink><br>
              Group Name:&nbsp;<mt:MTLabel Source="group_name" ID="group_name" runat="server"/><br>
            </div>
          </div>
          
</ItemTemplate>
<FooterTemplate>
        </div>
        <mt:MTPanel id="NoRecords" visible="False" runat="server">
        <div class="row">
          <div class="col-sm-12">
            No records 
          </div>
        </div>
        </mt:MTPanel>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <mt:MTNavigator FirstOnValue="First " FirstOffValue="First " PreviousOnValue="Prev " PreviousOffValue="Prev " NextOnValue="Next " NextOffValue="Next " LastOnValue="Last " LastOffValue="Last " PageOffPreValue="" PageOffPostValue="&amp;nbsp;" DisplayStyle="Links" PageLinksNumber="10" PageNumbersStyle="CurrentPageCentered" TotalPagesText="of" ShowTotalPages="true" ID="Navigator" runat="server"/>
  </div>
</div>

</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="groupsDataSource" ConnectionString="InMotion:IntranetDB" SelectCommandType="Table" SelectOrderBy="group_name" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM groups {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM groups
   </CountCommand>
</mt:MTDataSource><br>
</mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Menu" ID="Menu_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Menu" runat="server"><BootstrapDesign:MenuIncludablePage ID="MenuIncludablePage" runat="server"/></mt:MTPanel></asp:Content>
<asp:Content ContentPlaceHolderID="Footer" ID="Footer_Container" runat="server"><mt:MTPanel GenerateDiv="false" ID="Footer" runat="server">&nbsp;</mt:MTPanel></asp:Content>
<!--End ASPX page-->

