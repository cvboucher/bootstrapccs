<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="True" SSLAccess="False" isService="False" cachingEnabled="False" validateRequest="True" scriptingSupport="Automatic" cachingDuration="1 minutes" wizardTheme="None" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<Panel id="7" visible="True" generateDiv="False" name="Panel1" PathID="MenuIncludablePagePanel1">
<Components>
<Link id="9" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link1" PathID="MenuIncludablePagePanel1Link1" hrefSource="customers_list.ccp" wizardUseTemplateBlock="False" linkProperties="{'textSource':'Customers','textSourceDB':'','hrefSource':'customers_list.ccp','hrefSourceDB':'','title':'','target':'','name':'','linkParameters':{'length':0,'objectType':'linkParameters'}}"><Components/>
<Events/>
<LinkParameters/>
<Attributes/>
<Features/>
</Link>
<Link id="10" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link2" PathID="MenuIncludablePagePanel1Link2" hrefSource="employees_list.ccp" wizardUseTemplateBlock="False" linkProperties="{'textSource':'Employees','textSourceDB':'','hrefSource':'employees_list.ccp','hrefSourceDB':'','title':'','target':'','name':'','linkParameters':{'length':0,'objectType':'linkParameters'}}"><Components/>
<Events/>
<LinkParameters/>
<Attributes/>
<Features/>
</Link>
<Link id="11" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link3" PathID="MenuIncludablePagePanel1Link3" hrefSource="groups_list.ccp" wizardUseTemplateBlock="False" linkProperties="{'textSource':'Groups','textSourceDB':'','hrefSource':'groups_list.ccp','hrefSourceDB':'','title':'','target':'','name':'','linkParameters':{'length':0,'objectType':'linkParameters'}}"><Components/>
<Events/>
<LinkParameters/>
<Attributes/>
<Features/>
</Link>
<Link id="12" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link4" PathID="MenuIncludablePagePanel1Link4" hrefSource="project_grid.ccp" wizardUseTemplateBlock="False" linkProperties="{'textSource':'Projects','textSourceDB':'','hrefSource':'project_grid.ccp','hrefSourceDB':'','title':'','target':'','name':'','linkParameters':{'length':0,'objectType':'linkParameters'}}"><Components/>
<Events/>
<LinkParameters/>
<Attributes/>
<Features/>
</Link>
<Link id="13" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link5" PathID="MenuIncludablePagePanel1Link5" hrefSource="employee_rpt.ccp" wizardUseTemplateBlock="False" linkProperties="{'textSource':'Employee Report','textSourceDB':'','hrefSource':'employee_rpt.ccp','hrefSourceDB':'','title':'','target':'','name':'','linkParameters':{'length':0,'objectType':'linkParameters'}}"><Components/>
<Events/>
<LinkParameters/>
<Attributes/>
<Features/>
</Link>
</Components>
<Events>
<Event name="BeforeShow" type="Server">
<Actions>
<Action actionName="Hide-Show Component" actionCategory="General" id="8" action="Hide" conditionType="Parameter" dataType="Text" condition="Equal" name1="ViewMode" sourceType1="URL" name2="&quot;Print&quot;" sourceType2="Expression"/>
</Actions>
</Event>
</Events>
<Attributes/>
<Features/>
</Panel>
</Components>
	<CodeFiles>
		<CodeFile id="1" language="C#InMotion" name="MenuIncludablePage.ascx" forShow="True" url="MenuIncludablePage.ascx" comment="&lt;!--" commentEnd="--&gt;" codePage="windows-1252"/>
		<CodeFile id="1.cs" language="C#InMotion" name="MenuIncludablePageEvents.ascx.cs" forShow="False" comment="//" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
