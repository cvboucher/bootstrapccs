<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" validateRequest="True" scriptingSupport="Automatic" cachingDuration="1 minutes" masterPage="{CCS_PathToMasterPage}/MasterPage.ccp" useDesign="True" wizardTheme="None" needGeneration="0" wizardThemeVersion="3.0">
	<Components>
		<Panel id="20" visible="True" generateDiv="False" name="Head" contentPlaceholder="Head">
			<Components/>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="21" visible="True" generateDiv="False" name="Content" contentPlaceholder="Content">
			<Components>
				<Record id="33" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="groupsSearch" searchIds="33" fictitiousConnection="IntranetDB" wizardCaption="Search  " wizardOrientation="Custom" wizardFormMethod="post" gridSearchClearLink="True" wizardTypeComponent="Search" gridSearchType="And" wizardInteractiveSearch="False" gridSearchRecPerPage="True" wizardTypeButtons="button" wizardDefaultButton="True" gridSearchSortField="False" wizardUseInterVariables="False" templatePageSearch="D:/Users/CBoucher.CSUS2100/Documents/CodeChargeStudio5/Projects/BootstrapDesign/Templates/Search/BootstrapSearch.ccp|userTemplate" wizardThemeApplyTo="Page" addTemplatePanel="False" wizardType="Grid" returnPage="groups_list.ccp" PathID="ContentgroupsSearch" composition="3">
					<Components>
						<Button id="35" urlType="Relative" enableValidation="True" isDefault="True" name="Button_DoSearch" operation="Search" wizardCaption="Search" PathID="ContentgroupsSearchButton_DoSearch">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Button>
						<TextBox id="36" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_group_name" fieldSource="group_name" wizardIsPassword="False" wizardCaption="Group Name" caption="Group Name" required="False" unique="False" wizardSize="50" wizardMaxLength="50" PathID="ContentgroupsSearchs_group_name">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</TextBox>
						<ListBox id="37" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" returnValueType="Number" name="groupsPageSize" dataSource=";Select Value;5;5;10;10;25;25;100;100" wizardCaption="Records per page" wizardNoEmptyValue="True" PathID="ContentgroupsSearchgroupsPageSize">
							<Components/>
							<Events/>
							<TableParameters/>
							<SPParameters/>
							<SQLParameters/>
							<JoinTables/>
							<JoinLinks/>
							<Fields/>
							<PKFields/>
							<Attributes/>
							<Features/>
						</ListBox>
						<Link id="34" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="ClearParameters" hrefSource="groups_list.ccp" removeParameters="s_group_name;groupsPageSize" wizardThemeItem="SorterLink" wizardDefaultValue="Clear" PathID="ContentgroupsSearchClearParameters">
							<Components/>
							<Events/>
							<LinkParameters/>
							<Attributes/>
							<Features/>
						</Link>
</Components>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<PKFields/>
					<ISPParameters/>
					<ISQLParameters/>
					<IFormElements/>
					<USPParameters/>
					<USQLParameters/>
					<UConditions/>
					<UFormElements/>
					<DSPParameters/>
					<DSQLParameters/>
					<DConditions/>
					<SecurityGroups/>
					<Attributes/>
					<Features/>
				</Record>
				<Grid id="38" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" name="groups" connection="IntranetDB" dataSource="groups" orderBy="group_name" pageSizeLimit="100" pageSize="False" wizardCaption="List of Groups " wizardThemeApplyTo="Page" wizardGridType="Custom" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="No records" wizardGridPagingType="Centered" wizardUseSearch="False" wizardAddNbsp="False" gridTotalRecords="False" wizardAddPanels="False" wizardType="Grid" wizardUseInterVariables="False" templatePage="D:/Users/CBoucher.CSUS2100/Documents/CodeChargeStudio5/Projects/BootstrapDesign/Templates/Grid/BootstrapDivGrid.ccp|userTemplate" addTemplatePanel="False" changedCaptionGrid="False" gridExtendedHTML="False" PathID="Contentgroups">
					<Components>
						<Label id="41" fieldSourceType="DBColumn" dataType="Text" html="False" generateSpan="False" name="group_name" fieldSource="group_name" wizardCaption="Group Name" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Contentgroupsgroup_name">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
						<Navigator id="42" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="of" wizardPageSize="False" wizardImagesScheme="None">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Navigator>
						<Link id="43" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link1" PathID="ContentgroupsLink1" hrefSource="groups_maint.ccp" fieldSource="group_id" wizardUseTemplateBlock="False" linkProperties="{'textSource':'','textSourceDB':'group_id','hrefSource':'groups_maint.ccp','hrefSourceDB':'','title':'','target':'','name':'','linkParameters':{'0':{'sourceType':'DataField','parameterSource':'group_id','parameterName':'group_id'},'length':1,'objectType':'linkParameters'}}"><Components/>
							<Events/>
							<LinkParameters>
								<LinkParameter id="44" sourceType="DataField" format="yyyy-mm-dd" name="group_id" source="group_id"/>
							</LinkParameters>
							<Attributes/>
							<Features/>
						</Link>
					</Components>
					<Events/>
					<TableParameters/>
					<JoinTables>
						<JoinTable id="39" tableName="groups"/>
					</JoinTables>
					<JoinLinks/>
					<Fields/>
					<PKFields/>
					<SPParameters/>
					<SQLParameters/>
					<SecurityGroups/>
					<Attributes/>
					<Features/>
				</Grid>
			</Components>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="22" visible="True" generateDiv="False" name="Menu" contentPlaceholder="Menu">
			<Components>
				<IncludePage id="24" name="MenuIncludablePage" PathID="MenuMenuIncludablePage" page="MenuIncludablePage.ccp">
					<Components/>
					<Events/>
					<Features/>
				</IncludePage>
			</Components>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
		<Panel id="23" visible="True" generateDiv="False" name="Footer" contentPlaceholder="Footer">
			<Components/>
			<Events/>
			<Attributes/>
			<Features/>
		</Panel>
	</Components>
	<CodeFiles>
		<CodeFile id="1" language="C#InMotion" name="groups_list.aspx" forShow="True" url="groups_list.aspx" comment="&lt;!--" commentEnd="--&gt;" codePage="windows-1252"/>
		<CodeFile id="1.cs" language="C#InMotion" name="groups_listEvents.aspx.cs" forShow="False" comment="//" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
