function performSubmit(name, value)
{
    var subButton = document.createElement("input");
    subButton.setAttribute("type","hidden");
    subButton.setAttribute("name", name);
    subButton.setAttribute("value", value);

    var form = document.Form1;
    form.appendChild(subButton);
    form.submit();
    return false;
}

function getMTControl(controlId) {
    if (controlId.indexOf("@") != -1) {
        return features.select(controlId);
    }
    var elements = controlId.split(".");
    if(elements.length == 0) return null;

    var result = null;
    if(elements.length == 1)
    {
        result = getControlFromArray('MTControls', controlId);
        if (result == null)
        {
            if (document.getElementById)
                result = document.getElementById(controlId);
            else
                result = document.all[controlId];
        }
    }
    if(elements.length == 2)
    {
        result = getControlFromArray(elements[0], elements[1]);
        if (result == null)
        {
            if (document.getElementById)
                result = document.getElementById(elements[1]);
            else
                result = document.all[elements[1]];
        }
    }
    return result;
}

function getRowMTControl(controlId,rowID)
{
    var elements = controlId.split(".");
    if (elements.length == 2)
    {
        return getControlFromRowArray(elements[0], elements[1], rowID);
    }
}

function getFormRowNumber(formId)
{
    if (typeof(MTForms) != "undefined")
    {
        for (var i = 0; i < MTForms.length; i += 4)
        {
            if (MTForms[i] == formId)
            {
                if (MTForms[i+1] == "Single")
                {
                    return -1;
                }
                else
                {
                    var NumRows = parseInt(MTForms[i + 1]);
                    if (isNaN(NumRows))
                        return -1;
                    else
                        return NumRows;
                }
            }
        }
    }
    return -1;
}

function getControlFromControlArray(arrayName, controlName)
{
    try {
        var controlArray = eval(arrayName);
        if (controlArray && controlArray != null)
        {
            for (var i = 0; i < controlArray.length; i += 2)
            {
                if(controlArray[i] == controlName)
                    return document.getElementById(controlArray[i+1]);
            }
        }
    } catch (e) { }
    return null;
}

function getControlFromArray(arrayName, controlName)
{
    return getControlFromControlArray(arrayName + "Controls", controlName) || getControlFromControlArray(arrayName + "Controls_Footer", controlName);
}

function getControlFromRowArray(arrayName, controlName, rowID)
{
    var i;
    var array_id;
    if (rowID == -2)
        array_id = arrayName+"Controls_Footer";
    if (rowID == -1)
        array_id = arrayName+"Controls_Header";
    if (rowID >= 0)
        array_id = arrayName+"Controls_Row" + rowID;

    return getControlFromControlArray(array_id, controlName);
}

function callOnLoadEvents()
{
    if(typeof(MTForms)!="undefined")
        for(var i = 0; i < MTForms.length; i += 4)
        {
            if (MTForms[i] != "null" && MTForms[i+2] != "")
            {
                try {
                    var str = MTForms[i+2];
                    eval(str + "()");
                }
                catch (e) {}
            }
        }
}

function MT_check_and_bind(element,event,func)
{
    if (element != null)
    {
        try {
        	var funcRef;
        	if (typeof(func) == "string")
        	    funcRef = eval(func);
        	else
        	    funcRef = func;
            if (eval(element).isFeature)
                eval(element).addHandler(event, funcRef);
            else {
            	if (event.toLowerCase() == "onsubmit")
            		eval("document.getElementsByTagName('form')[0].onsubmit = " + func);
                if (event.toLowerCase() == "onload")
                    eval(func + "(" + element + ")");
                else
                    eval(element + "." + event + "=" + func);
            }
        }
        catch(e) { }
    }
}

function SubmitForm(FormName)
{
    flag = true;
    if (typeof(MTForms) != "undefined")
        for (var i = 0; i < MTForms.length; i += 4)
        {
            if (FormName == MTForms[i])
                if (MTForms[i + 3] != "")
                {
                    Form1.onsubmit = MTForms[i + 3];
                    break;
                }
        }
    if (flag)
        Form1.submit();
}

function MTvalidate_form(FormName)
{
    if (disableValidation) return true;

    var NumRows = getFormRowNumber(FormName);
    var result = true;

    if (NumRows == -1)
    {
        try
        {
            var FormCtrls = eval(FormName + "Controls");
            for (var i = 1; i < FormCtrls.length && result; i += 2)
                result = validate_control(document.getElementById(FormCtrls[i]));
        }
        catch(e) {}
    }
    else if (NumRows != Number.NaN)
    {
        var FormCtrls;
        for (var i = -2; i < parseInt(NumRows) && result; i++)
        {
            try
            {
                switch (i)
                {
                    case -2: FormCtrls = eval(FormName + "Controls_Header");
                        break;
                    case -1: FormCtrls = eval(FormName + "Controls_Footer");
                        break;
                    default: FormCtrls = eval(FormName + "Controls_Row" + i);
                        break;
                }
                for (var j = 1; j < FormCtrls.length && result; j += 2)
                    result = validate_control(document.getElementById(FormCtrls[j]));
            }
            catch(e) {}
        }
    }

    return result;
}

function isControlInArray(arrayName, controlID)
{
    return isControlInControlArray(arrayName + "Controls", controlID);
}

function isControlInControlArray(arrayName, controlID)
{
    try
    {
        var controlArray = eval(arrayName);
        if (controlArray && controlArray != null)
        {
            for (var i = 0; i < controlArray.length; i += 2)
            {
                if(controlArray[i + 1] == controlID)
                    return true;
            }
        }
    }
    catch (e) { }
    return false;
}

function isControlInRowArray(arrayName, controlName, rowID)
{
    var array_id;
    if (rowID == -2)
        array_id = arrayName + "Controls_Footer";
    if (rowID == -1)
        array_id = arrayName + "Controls_Header";
    if (rowID >= 0)
        array_id = arrayName + "Controls_Row" + rowID;

    return isControlInControlArray(array_id, controlName);
}

function getSameLevelMTControl(controlName, existingElement)
{
    if (existingElement != null && existingElement["id"] != null)
    {
        var existID = existingElement["id"];
        if (typeof(MTForms) != "undefined")
        {
            for (var i = 0; i < MTForms.length; i += 4)
            {
                if (MTForms[i] == existID)
                {
                    return getMTControl(controlName);
                }
                else if (MTForms[i+1] == "Single")
                {
                    if (isControlInArray(MTForms[i], existID))
                    {
                        var ctrl = getControlFromArray(MTForms[i], controlName);
                        if (ctrl == null) ctrl = getMTControl(controlName);
                        return ctrl;
                    }
                }
                else
                {
                    var NumRows = parseInt(MTForms[i + 1]);
                    if (!isNaN(NumRows))
                    {
                        for (var j = -2; j < NumRows; j++)
                        {
                            if (isControlInRowArray(MTForms[i], existID, j))
                            {
                                var ctrl = getControlFromRowArray(MTForms[i], controlName, j)
                                if (ctrl == null) ctrl = getRowMTControl(controlName, j);
                                if (ctrl == null) ctrl = getMTControl(controlName);
                                return ctrl;
                            }
                        }
                    }
                }
            }
        }
    }
    return getMTControl(controlName);
}

function Feature() {
    this.events = ({});
    this.isFeature = true;
}

Feature.prototype.addHandler = function(event, handler) {
    var evt = this.events[event];
    if (!evt)
        evt = this.events[event] = [];
    for (var i=0; i<evt.length; i++)
        if (evt[i] == handler)
            return;
    evt.push(handler);
}

Feature.prototype.fireEvent = function(eventName, thisObj, args) {
    var evt = this.events[eventName];
    if (!evt)
        return ;
    if (!args)
        args = [];
    for (var i=0; i<evt.length; i++)
        if (typeof(evt[i]) == "function")
            evt[i].apply(thisObj, args);
}

function FeaturesCollection() {
    this.features = ({});
}

FeaturesCollection.prototype.select = function(q) {
    var res = this.features[q];
    if (!res)
        res = this.features[q] = new Feature();
    return res;
}

var features = new FeaturesCollection();

function UpdatePanel_onrefresh_handler(s, e) {
    if (e.get_response().get_responseAvailable()) {
        var panelName;
        var params = e.get_response().get_responseData().split('|');
        for (var i = 0; i < params.length; i++) {
            if (params[i] == "updatePanel") {
                panelName = params[i + 1];
                break; ;
            }
        }
        if (panelName) {
            var path = panelName + "@UpdatePanel";
            features.select(path).fireEvent("onrefresh");
        }
    }
}